﻿CREATE FUNCTION [dbo].[CalcAverageMargins](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT m.Currency, m.Scenario, GPV = GlobalDB.dbo.WtAvg(m.GPV,m.Divisor)
		, RMC = GlobalDB.dbo.WtAvg(m.RMC, m.Divisor)
		, GrossMargin = GlobalDB.dbo.WtAvg(m.GrossMargin, m.Divisor)
		, CashOpex = GlobalDB.dbo.WtAvg(m.CashOpex, m.Divisor)
		, CashMargin = GlobalDB.dbo.WtAvg(m.CashMargin, m.Divisor)
	FROM MarginCalc m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND m.DataType = 'BBL'
	AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
	GROUP BY m.Currency, m.Scenario
	)
