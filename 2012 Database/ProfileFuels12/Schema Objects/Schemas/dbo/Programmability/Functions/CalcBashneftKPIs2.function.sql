﻿CREATE FUNCTION [dbo].[CalcBashneftKPIs2] (@SubmissionList dbo.SubmissionIDList READONLY, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5))
RETURNS @KPIData TABLE (
	ProcessUtilPcnt real NULL, ProcessEDC real NULL, ProcessUEDC real NULL, OffsitesEDC real NULL, Complexity real NULL,
	EII real NULL, EnergyUseDay real NULL, TotStdEnergy real NULL, FuelUseDay real NULL,
	OpAvail real NULL, MechUnavailTA real NULL, NonTAOpUnavail real NULL, TADD real NULL, NTAMDD real NULL, RPDD real NULL, NumDays real NULL,
	MechAvail real NULL, NonTAMechUnavail real NULL,
	MaintEffIndex real NULL, AnnTACost real NULL, RoutCost real NULL, MaintEffDiv real NULL, TAIndex real NULL,
	mPersEffIndex real NULL, MaintWHr real NULL, OCCMaintWHr real NULL, MPSMaintWHr real NULL, mPersEffDiv real NULL, 
	NEOpexEDC real NULL, NEOpex real NULL, EDC real NULL,
	TotCashOpexUEDC real NULL, TAAdj real NULL, EnergyCost real NULL, TotCashOpex real NULL, UEDC real NULL, ExchRate real NULL
)
AS
BEGIN

DECLARE	
	@ProcessUtilPcnt real, @TotProcessEDC real, @TotProcessUEDC real, @OffsitesEDC real, @Complexity real,
	@EII real, @EnergyUseDay real, @FuelUseDay real, @TotStdEnergy real,
	@OpAvail real, @MechAvail real, @MechUnavailTA real, @NonTAMechUnavail real, @NonTAOpUnavail real, @RegUnavail real,
	@TADD real, @NTAMDD real, @RPDD real, @NumDays real,
	@MaintEffIndex real, @TAAdj real, @AnnTACost real, @RoutCost real, @MaintEffDiv real, @EDC real, @TAIndex real, @TAEffIndex real,
	@mPersEffIndex real, @MaintWHr real, @OCCMaintWHr real, @MPSMaintWHr real, @mPersEffDiv real,
	@OpexUEDC real, @EnergyCost real, @TotCashOpex real, @UEDC real, @NEOpexEDC real, @NEOpex real, @ExchRate real

SELECT 	@EII = EII, @EDC = AvgEDC/1000, @UEDC = AvgUEDC/1000, @Complexity = Complexity,
		@ProcessUtilPcnt = ProcessUtilPcnt, @TotProcessEDC = AvgProcessEDC/1000, @TotProcessUEDC = AvgProcessUEDC/1000,
		@EnergyUseDay = EnergyUseDay, @TotStdEnergy = AvgStdEnergy, 
		@mPersEffDiv = MaintPersEffDiv, @MaintEffDiv = MaintEffDiv, @NumDays = NumDays
FROM dbo.SLAverageFactors(@SubmissionList, @FactorSet)

SELECT @FuelUseDay = SUM(PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU)/(SUM(s.NumDays*1.0)/COUNT(DISTINCT s.RefineryID+s.Dataset))
FROM EnergyTot e INNER JOIN Submissions s ON s.SubmissionID = e.SubmissionID
INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID

SELECT @TAEffIndex = m.TAEffIndex, @MaintEffIndex = m.MaintEffIndex, @TAIndex = m.TAIndex
FROM dbo.SLMaintIndex(@SubmissionList, 0, @FactorSet, @Currency) m

SELECT @OpAvail = OpAvail_Ann, @MechUnavailTA = MechUnavailTA_Ann, @NonTAOpUnavail = MechUnavailRout + RegUnavail, @NonTAMechUnavail = MechUnavailRout, @RegUnavail = RegUnavail
FROM dbo.SLAvailability(@SubmissionList, @FactorSet)

IF @NonTAMechUnavail < 0.05
	SELECT @NonTAMechUnavail = 0
IF @NonTAOpUnavail < 0.05
	SELECT @NonTAOpUnavail = 0, @RegUnavail = 0
	
SELECT @OpAvail = 100 - @NonTAOpUnavail - @MechUnavailTA, @MechAvail = 100 - @NonTAMechUnavail - @MechUnavailTA

SELECT @NEOpex = NEOpex*Divisor, @EnergyCost = EnergyCost*Divisor, @TotCashOpex = TotCashOpex*Divisor, @TAAdj = TAAdj*Divisor
FROM dbo.SLAverageOpexByDataType(@SubmissionList, @FactorSet, 'CLIENT', @Currency, 'ADJ')

SELECT @OpexUEDC = TotCashOpex
FROM dbo.SLAverageOpexByDataType(@SubmissionList, @FactorSet, 'CLIENT', @Currency, 'UEDC')

SELECT @NEOpexEDC = NEOpex
FROM dbo.SLAverageOpexByDataType(@SubmissionList, @FactorSet, 'CLIENT', @Currency, 'EDC')

SELECT @RoutCost = CurrRoutCost
FROM dbo.SLSumMaintCost(@SubmissionList, @Currency)

IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @FuelUseDay = @FuelUseDay * 1.055, @TotStdEnergy = @TotStdEnergy * 1.055

SELECT @OffsitesEDC = @EDC - @TotProcessEDC

SELECT @ExchRate = dbo.AvgExchangeRate('RUB', MIN(PeriodStart), MAX(PeriodEnd))
FROM Submissions WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)

SELECT @mPersEffIndex = MaintPEI, @MaintWHr = MaintPEI*@mPersEffDiv/100, @OCCMaintWHr = MaintPEI*@mPersEffDiv/100*(OCCWHrEDC/TotWHrEDC)
	, @MPSMaintWHr = MaintPEI*@mPersEffDiv/100*(MPSWHrEDC/TotWHrEDC)
FROM dbo.SLAveragePersKPIs(@SubmissionList, @FactorSet)

INSERT @KPIData (ProcessUtilPcnt, ProcessEDC, ProcessUEDC, OffsitesEDC, Complexity,
	EII, EnergyUseDay, TotStdEnergy, FuelUseDay, 
	OpAvail, MechUnavailTA, NonTAOpUnavail, TADD, NTAMDD, RPDD, NumDays,
	MechAvail, NonTAMechUnavail,
	MaintEffIndex, AnnTACost, RoutCost, MaintEffDiv, TAIndex,
	mPersEffIndex, MaintWHr, OCCMaintWHr, MPSMaintWHr, mPersEffDiv, 
	NEOpexEDC, NEOpex, EDC,
	TotCashOpexUEDC, TAAdj, EnergyCost, TotCashOpex, UEDC, ExchRate)
SELECT @ProcessUtilPcnt, @TotProcessEDC, @TotProcessUEDC, @OffsitesEDC, @Complexity, 	
	@EII, @EnergyUseDay, @TotStdEnergy, @FuelUseDay,
	@OpAvail, @MechUnavailTA, @NonTAOpUnavail, TADD = @MechUnavailTA/100*@NumDays, NTAMDD = @NonTAMechUnavail/100*@NumDays, RPDD = @RegUnavail/100*@NumDays, @NumDays,
	@MechAvail, @NonTAMechUnavail,
	@MaintEffIndex, @TAAdj/1000, @RoutCost/1000, @MaintEffDiv/1000000, @TAIndex,
	@mPersEffIndex, @MaintWHr/1000, @OCCMaintWHr/1000, @MPSMaintWHr/1000, @mPersEffDiv/1000,
	@NEOpexEDC, @NEOpex/1000, @EDC,
	@OpexUEDC, @TAAdj/1000, @EnergyCost/1000, @TotCashOpex/1000, @UEDC, @ExchRate

IF @Currency = 'RUB'
	UPDATE @KPIData SET TotCashOpexUEDC = TotCashOpexUEDC/100

RETURN

END

