﻿CREATE FUNCTION [dbo].[ExchangeRate](@FromCurrency CurrencyCode, @ToCurrency CurrencyCode, @StartDate smalldatetime)
RETURNS real
AS
BEGIN
	DECLARE @rate real
	IF @FromCurrency = @ToCurrency
        	SELECT @rate = 1
	ELSE
		SELECT @rate = dbo.ClosestCurrencyPerUSD(@ToCurrency, @StartDate)/dbo.ClosestCurrencyPerUSD(@FromCurrency, @StartDate)
	RETURN @rate
END
