﻿
CREATE FUNCTION [dbo].[GetAvailability](@RefineryID varchar(6), @Dataset varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT g.FactorSet, g.ProcessGrouping AS ProcessID, 
	MechAvail_Ann = GlobalDB.dbo.WtAvg(m.MechAvail_Ann,m.PeriodEDC), MechAvailSlow_Ann = GlobalDB.dbo.WtAvg(m.MechAvailSlow_Ann,m.PeriodEDC), MechAvailOSTA = GlobalDB.dbo.WtAvg(m.MechAvailOSTA, m.PeriodEDC*CASE WHEN m.PeriodHrs > 0 THEN m.PeriodHrsOSTA/m.PeriodHrs END),
	OpAvail_Ann = GlobalDB.dbo.WtAvg(m.OpAvail_Ann,m.PeriodEDC), OpAvailSlow_Ann = GlobalDB.dbo.WtAvg(m.OpAvailSlow_Ann,m.PeriodEDC),
	OnStream_Ann = GlobalDB.dbo.WtAvg(m.OnStream_Ann,m.PeriodEDC), OnStreamSlow_Ann = GlobalDB.dbo.WtAvg(m.OnStreamSlow_Ann,m.PeriodEDC), 
	MechAvail_Act = GlobalDB.dbo.WtAvg(m.MechAvail_Act,m.PeriodEDC), MechAvailSlow_Act = GlobalDB.dbo.WtAvg(m.MechAvailSlow_Act,m.PeriodEDC),
	OpAvail_Act = GlobalDB.dbo.WtAvg(m.OpAvail_Act,m.PeriodEDC), OpAvailSlow_Act = GlobalDB.dbo.WtAvg(m.OpAvailSlow_Act,m.PeriodEDC),
	OnStream_Act = GlobalDB.dbo.WtAvg(m.OnStream_Act,m.PeriodEDC), OnStreamSlow_Act = GlobalDB.dbo.WtAvg(m.OnStreamSlow_Act,m.PeriodEDC),
	MechUnavailTA_Ann = GlobalDB.dbo.WtAvg(m.MechUnavailTA_Ann,m.PeriodEDC), MechUnavailTA_Act = GlobalDB.dbo.WtAvg(m.MechUnavailTA_Act,m.PeriodEDC), 
	MechUnavailPlan = GlobalDB.dbo.WtAvg(m.MechUnavailPlan,m.PeriodEDC), MechUnavailUnp = GlobalDB.dbo.WtAvg(m.MechUnavailUnp,m.PeriodEDC), 
	MechUnavail_Ann = GlobalDB.dbo.WtAvg(m.MechUnavail_Ann,m.PeriodEDC), MechUnavail_Act = GlobalDB.dbo.WtAvg(m.MechUnavail_Act,m.PeriodEDC), 
	RegUnavail = GlobalDB.dbo.WtAvg(m.RegUnavail,m.PeriodEDC), RegUnavailPlan = GlobalDB.dbo.WtAvg(m.RegUnavailPlan,m.PeriodEDC), RegUnavailUnp = GlobalDB.dbo.WtAvg(m.RegUnavailUnp,m.PeriodEDC), 
	OpUnavail_Ann = GlobalDB.dbo.WtAvg(m.OpUnavail_Ann,m.PeriodEDC), OpUnavail_Act = GlobalDB.dbo.WtAvg(m.OpUnavail_Act,m.PeriodEDC), 
	OthUnavailEconomic = GlobalDB.dbo.WtAvg(m.OthUnavailEconomic,m.PeriodEDC), OthUnavailExternal = GlobalDB.dbo.WtAvg(m.OthUnavailExternal,m.PeriodEDC), OthUnavailUnitUpsets = GlobalDB.dbo.WtAvg(m.OthUnavailUnitUpsets,m.PeriodEDC), OthUnavailOffsiteUpsets = GlobalDB.dbo.WtAvg(m.OthUnavailOffsiteUpsets,m.PeriodEDC), OthUnavailOther = GlobalDB.dbo.WtAvg(m.OthUnavailOther,m.PeriodEDC), 
	OthUnavail = GlobalDB.dbo.WtAvg(m.OthUnavail,m.PeriodEDC), OthUnavailPlan = GlobalDB.dbo.WtAvg(m.OthUnavailPlan,m.PeriodEDC), OthUnavailUnp = GlobalDB.dbo.WtAvg(m.OthUnavailUnp,m.PeriodEDC), 
	TotUnavail_Ann = GlobalDB.dbo.WtAvg(m.TotUnavail_Ann,m.PeriodEDC), TotUnavail_Act = GlobalDB.dbo.WtAvg(m.TotUnavail_Act,m.PeriodEDC), TotUnavailUnp = GlobalDB.dbo.WtAvg(m.TotUnavailUnp,m.PeriodEDC)
	FROM [dbo].[GetUnitAvailability](@RefineryID, @Dataset, @StartDate, @EndDate) m 
	INNER JOIN (SELECT TOP 1 SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartDate, @EndDate) ORDER BY PeriodStart DESC) s ON 1=1
	/*INNER JOIN Config c ON c.SubmissionID = s.SubmissionID AND c.UnitID = m.UnitID --This filters the results to only those units reported in last month of period*/
	INNER JOIN RefProcessGroupings g ON g.SubmissionID = s.SubmissionID AND g.UnitID = m.UnitID AND g.FactorSet = m.FactorSet
	GROUP BY g.FactorSet, g.ProcessGrouping
)



