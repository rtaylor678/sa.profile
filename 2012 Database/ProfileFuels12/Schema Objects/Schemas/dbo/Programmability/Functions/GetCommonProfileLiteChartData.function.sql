﻿CREATE FUNCTION [dbo].[GetCommonProfileLiteChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@numMonths tinyint = 12)
RETURNS @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	RefUtilPcnt real NULL, 
	ProcessUtilPcnt real NULL,
	UtilOSTA real NULL,
	VEI real NULL, 
	ProcessEffIndex real NULL, 
	MechAvail real NULL,
	OpAvail real NULL, 
	MaintIndex real NULL,
	PersIndex real NULL, 
	NEOpexEDC real NULL,
	OpexUEDC real NULL,
	MaintEffIndex real NULL,
	nmPersEffIndex real NULL,
	mPersEffIndex real NULL,
	RoutIndex real NULL
)
AS
BEGIN
	IF @numMonths = NULL
		SET @numMonths = 12

	DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
	
	SELECT @PeriodEnd = PeriodEnd
	FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet AND UseSubmission = 1
	
	SELECT @PeriodStart12Mo = DATEADD(mm, -@numMonths, @PeriodEnd)

	IF @PeriodEnd IS NULL
		RETURN /* Data has not been submitted yet */
	IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND UseSubmission = 1 AND CalcsNeeded IS NOT NULL)
		RETURN /* Calculations still need to be done */

	--- Everything Already Available in Gensum
	INSERT INTO @data (PeriodStart, PeriodEnd, RefUtilPcnt, ProcessUtilPcnt, UtilOSTA, MechAvail, OpAvail, 
		EII, VEI, ProcessEffIndex, PersIndex, MaintIndex, NEOpexEDC, OpexUEDC, MaintEffIndex, nmPersEffIndex, mPersEffIndex, RoutIndex)
	SELECT	PeriodStart, PeriodEnd, Gensum.UtilPcnt, GenSum.ProcessUtilPcnt, GenSum.UtilOSTA, Gensum.MechAvail, Gensum.OpAvail, 
		Gensum.EII, Gensum.VEI, ProcessEffIndex = f.PEI, PersIndex = TotWHrEDC, MaintIndex = Gensum.RoutIndex + Gensum.TAIndex_AVG, NEOpexEDC, TotCashOpexUEDC,
		MaintEffIndex = mi.RoutEffIndex + mi.TAEffIndex_Avg, nmPersEffIndex = Gensum.NonMaintPEI, mPersEffIndex = Gensum.MaintPEI, Gensum.RoutIndex
	FROM Gensum LEFT JOIN FactorTotCalc f ON f.SubmissionID = GenSum.SubmissionID AND f.FactorSet = GenSum.FactorSet
	 LEFT JOIN MaintIndex mi ON mi.SubmissionID = Gensum.SubmissionID AND mi.Currency = Gensum.Currency AND mi.FactorSet = Gensum.FactorSet
	WHERE Gensum.RefineryID = @RefineryID AND DataSet = @DataSet AND Gensum.FactorSet = @FactorSet AND Gensum.Currency = @Currency AND Gensum.UOM = @UOM AND Gensum.Scenario = @Scenario
	AND Gensum.PeriodStart >= @PeriodStart12Mo AND Gensum.PeriodStart < @PeriodEnd

	IF (SELECT COUNT(*) FROM @data) < @numMonths
	BEGIN
		DECLARE @Period smalldatetime
		SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
		WHILE @Period >= @PeriodStart12Mo
		BEGIN
			IF NOT EXISTS (SELECT * FROM @data WHERE PeriodStart = @Period)
				INSERT @data (PeriodStart) VALUES (@Period)
			SELECT @Period = DATEADD(mm, -1, @Period)
		END
	END

	IF @Currency = 'RUB'
		UPDATE @data SET OpexUEDC = OpexUEDC/100
		
	RETURN
END

