﻿CREATE FUNCTION [dbo].[LastSubmissionForUnit](@RefineryID varchar(6), @UnitID int)
RETURNS int
AS
BEGIN
	DECLARE @SubmissionID int

	SELECT TOP 1 @SubmissionID = c.SubmissionID
	FROM Config c INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID
	WHERE RefineryID = @RefineryID AND UnitID = @UnitID
	ORDER BY s.PeriodStart DESC

	RETURN @SubmissionID
END
