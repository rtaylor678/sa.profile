﻿
CREATE FUNCTION [dbo].[SLAverageGrossMargin](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT m.Currency, m.Scenario, GPV = GlobalDB.dbo.WtAvg(m.GPV,m.Divisor)
		, RMC = GlobalDB.dbo.WtAvg(m.RMC, m.Divisor)
		, GrossMargin = GlobalDB.dbo.WtAvg(m.GrossMargin, m.Divisor)
		, OthRev = GlobalDB.dbo.WtAvg(m.OthRev, m.Divisor)
--		, CashOpex = GlobalDB.dbo.WtAvg(m.CashOpex, m.Divisor)
--		, CashMargin = GlobalDB.dbo.WtAvg(m.CashMargin, m.Divisor)
	FROM MarginCalc m INNER JOIN @SubmissionList s ON s.SubmissionID = m.SubmissionID
	WHERE m.DataType = 'BBL'
	GROUP BY m.Currency, m.Scenario
	)

