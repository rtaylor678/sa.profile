﻿
CREATE FUNCTION [dbo].[SLAveragePersKPIs](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet FactorSet)
RETURNS @KPIs TABLE (FactorSet varchar(8) NOT NULL, TotWHrEDC real NULL, OCCWHrEDC real NULL, MPSWHrEDC real NULL, NonMaintWHrEDC real NULL
		, TotMaintForceWHrEDC real NULL, NonTAMaintWHrEDC real NULL, TAMaintWHrEDC real NULL
		, PEI real NULL
		, NonMaintPEI real NULL
		, MaintPEI real NULL, NonTAMaintPEI real NULL, TAMaintPEI real NULL)
AS
BEGIN

	DECLARE @Tot TABLE (FactorSet varchar(8) NOT NULL, TotWHrEDC real NULL, OCCWHrEDC real NULL, MPSWHrEDC real NULL, PEI real NULL)
	INSERT @Tot(FactorSet, TotWHrEDC, OCCWHrEDC, MPSWHrEDC, PEI)
	SELECT FactorSet, TotWHrEDC = AVG(CASE WHEN SectionID = 'TP' THEN WHrEDC END)
		, OCCWHrEDC = AVG(CASE WHEN SectionID = 'TO' THEN WHrEDC END)
		, MPSWHrEDC = AVG(CASE WHEN SectionID = 'TM' THEN WHrEDC END)
		, PEI = AVG(CASE WHEN SectionID = 'TP' THEN EffIndex END)
	FROM [dbo].[SLAveragePersSTCalc](@SubmissionList, @FactorSet) st 
	WHERE SectionID IN ('TO','TM','TP')
	GROUP BY FactorSet

	DECLARE @NonTA TABLE (FactorSet varchar(8) NOT NULL, NonTAMaintPEI real NULL, NonTAMaintWHrEDC real NULL, NonMaintPEI real NULL, NonMaintWHrEDC real NULL
				, MaintPEIDivisor float NULL, WHrEDCDivisor float NULL, NonMaintPEIDivisor float NULL)
	INSERT @NonTA (FactorSet, NonTAMaintPEI, NonTAMaintWHrEDC, NonMaintPEI, NonMaintWHrEDC, MaintPEIDivisor, WHrEDCDivisor, NonMaintPEIDivisor)
	SELECT ptc.FactorSet
		, NonTAMaintPEI = GlobalDB.dbo.WtAvg(ptc.NonTAMaintPEI, ptc.MaintPEIDivisor)
		, NonTAMaintWHrEDC = GlobalDB.dbo.WtAvg(ptc.NonTAMaintForceWHrEDC, ptc.WHrEDCDivisor)
		, NonMaintPEI = GlobalDB.dbo.WtAvg(ptc.NonMaintPEI, ptc.NonMaintPEIDivisor)
		, NonMaintWHrEDC = GlobalDB.dbo.WtAvg(ptc.NonMaintWHrEDC, ptc.WHrEDCDivisor)
		, MaintPEIDivisor = SUM(ptc.MaintPEIDivisor), WHrEDCDivisor = SUM(ptc.WHrEDCDivisor), NonMaintPEIDivisor = SUM(ptc.NonMaintPEIDivisor)
	FROM Submissions s INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	INNER JOIN PersTotCalc ptc ON ptc.SubmissionID = s.SubmissionID AND ptc.Currency = 'USD'
	WHERE ptc.FactorSet = ISNULL(@FactorSet, ptc.FactorSet)
	GROUP BY ptc.FactorSet

	/* Get Current T/A personnel adjustments */
	DECLARE @TAAdj TABLE (FactorSet varchar(8) NOT NULL
					, OCCTAWHrEffIndex real NULL, OCCTAmPEI real NULL, OCCTAWHrEDC real NULL
					, MPSTAWHrEffIndex real NULL, MPSTAmPEI real NULL, MPSTAWHrEDC real NULL)
	INSERT @TAAdj (FactorSet, OCCTAWHrEffIndex, OCCTAmPEI, OCCTAWHrEDC, MPSTAWHrEffIndex, MPSTAmPEI, MPSTAWHrEDC)
	SELECT FactorSet, OCCTAWHrEffIndex, OCCTAmPEI, OCCTAWHrEDC, MPSTAWHrEffIndex, MPSTAmPEI, MPSTAWHrEDC
	FROM dbo.SLPersTAAdj(@SubmissionList, @FactorSet)

	INSERT @KPIs (FactorSet, TotWHrEDC, OCCWHrEDC, MPSWHrEDC, NonMaintWHrEDC
		, TotMaintForceWHrEDC, NonTAMaintWHrEDC, TAMaintWHrEDC
		, PEI, NonMaintPEI, MaintPEI, NonTAMaintPEI, TAMaintPEI)
	SELECT t.FactorSet, t.TotWHrEDC, t.OCCWHrEDC, t.MPSWHrEDC, nta.NonMaintWHrEDC
		, TotMaintForceWHrEDC = nta.NonTAMaintWHrEDC + ta.OCCTAWHrEDC + ta.MPSTAWHrEDC, nta.NonTAMaintWHrEDC, TAMaintWHrEDC = ta.OCCTAWHrEDC + ta.MPSTAWHrEDC
		, t.PEI, nta.NonMaintPEI
		, MaintPEI = nta.NonTAMaintPEI + ta.OCCTAmPEI + ta.MPSTAmPEI, nta.NonTAMaintPEI, TAMaintPEI = ta.OCCTAmPEI + ta.MPSTAmPEI
	FROM @Tot t INNER JOIN @NonTA nta ON nta.FactorSet = t.FactorSet
	INNER JOIN @TAAdj ta ON ta.FactorSet = t.FactorSet

RETURN

END

