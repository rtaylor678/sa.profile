﻿CREATE FUNCTION [dbo].[SLRoutIndex](@SubmissionList dbo.SubmissionIDList READONLY, @NumMonths tinyint, @FactorSet varchar(8), @Currency char(4))
RETURNS @tblIndexes TABLE (
	FactorSet varchar(8) NOT NULL,
	Currency char(4) NOT NULL,
	RoutIndex real NULL,
	RoutMatlIndex real NULL,
	RoutEffIndex real NULL)
AS
BEGIN
	DECLARE @StartDate smalldatetime, @EndDate smalldatetime
	SELECT @EndDate = MAX(PeriodEnd) FROM Submissions WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)
	SELECT @StartDate = DATEADD(MM, -@NumMonths, @EndDate)
	
	IF @NumMonths > 0
	BEGIN
		DECLARE @Datasets TABLE (
			RefineryID char(6) NOT NULL,
			Dataset varchar(15) NOT NULL)
		INSERT @Datasets SELECT DISTINCT RefineryID, Dataset FROM Submissions WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList sl)
		
		DECLARE @tblRout TABLE (
			RefineryID char(6) NOT NULL,
			Dataset varchar(15) NOT NULL,
			Currency char(4) NOT NULL,
			PeriodStart smalldatetime NOT NULL,
			PeriodEnd smalldatetime NOT NULL,
			RoutCost real NULL,
			RoutMatl real NULL)
		INSERT INTO @tblRout (RefineryID, Dataset, Currency, PeriodStart, PeriodEnd, RoutCost, RoutMatl)
		SELECT mrh.RefineryID, mrh.Dataset, mrh.Currency, mrh.PeriodStart, mrh.PeriodEnd, mrh.RoutCost, mrh.RoutMatl
		FROM MaintRoutHist mrh INNER JOIN @Datasets r 
			ON r.RefineryID = mrh.RefineryID AND r.Dataset = mrh.Dataset
		WHERE mrh.PeriodStart >= @StartDate AND mrh.PeriodEnd <= @EndDate AND Currency = ISNULL(@Currency, Currency)
		
		SELECT @StartDate = MIN(PeriodStart), @EndDate = MAX(PeriodEnd) FROM @tblRout

		DECLARE @tblEDC TABLE (
			RefineryID char(6) NOT NULL,
			Dataset varchar(15) NOT NULL,
			FactorSet char(8) NOT NULL,
			PeriodStart smalldatetime NOT NULL,
			PeriodEnd smalldatetime NOT NULL,
			PlantEDC real NULL,
			PlantMaintEffDiv real NULL)
		INSERT INTO @tblEDC (RefineryID, Dataset, FactorSet, PeriodStart, PeriodEnd, PlantEDC, PlantMaintEffDiv)
		SELECT s.RefineryID, s.Dataset, f.FactorSet, s.PeriodStart, s.PeriodEnd, f.PlantEDC, f.PlantMaintEffDiv
		FROM @Datasets r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartDate, @EndDate) s INNER JOIN FactorTotCalc f ON f.SubmissionID = s.SubmissionID
		WHERE f.FactorSet = ISNULL(@FactorSet, f.FactorSet)
		/*
		DECLARE @MinStart smalldatetime, @MinStartID int
		SELECT TOP 1 @MinStart = PeriodStart, @MinStartID = SubmissionID
		FROM Submissions
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UseSubmission = 1
		AND EXISTS (SELECT * FROM FactorTotCalc f WHERE f.SubmissionID = Submissions.SubmissionID AND PlantEDC > 0)
		ORDER BY PeriodStart ASC
		*/
		IF EXISTS (SELECT * FROM @tblRout r WHERE NOT EXISTS (SELECT * FROM @tblEDC e WHERE e.RefineryID = r.RefineryID AND e.Dataset = r.Dataset AND e.PeriodStart = r.PeriodStart))
			INSERT INTO @tblEDC (RefineryID, Dataset, FactorSet, PeriodStart, PeriodEnd, PlantEDC, PlantMaintEffDiv)
			SELECT r.RefineryID, r.Dataset, e.FactorSet, r.PeriodStart, r.PeriodEnd, e.PlantEDC, e.PlantMaintEffDiv
			FROM @tblEDC e INNER JOIN @tblRout r ON r.RefineryID = e.RefineryID AND r.Dataset = e.Dataset
			INNER JOIN (SELECT RefineryID, Dataset, MIN(PeriodStart) AS MinPeriodStart FROM @tblEDC GROUP BY RefineryID, Dataset) fs 
				ON fs.RefineryID = e.RefineryID AND fs.Dataset = e.Dataset AND fs.MinPeriodStart = e.PeriodStart
			WHERE NOT EXISTS (SELECT * FROM @tblEDC x WHERE x.RefineryID = r.RefineryID AND x.Dataset = r.Dataset AND x.PeriodStart = r.PeriodStart)

		INSERT @tblIndexes(FactorSet, Currency, RoutIndex, RoutMatlIndex, RoutEffIndex)
		SELECT e.FactorSet, r.Currency, 
			RoutIndex = r.RoutCost*1000/e.PlantEDC, 
			RoutMatlIndex = r.RoutMatl*1000/e.PlantEDC, 
			RoutEffIndex = r.RoutCost*100000/e.PlantMaintEffDiv
		FROM (SELECT Currency, RoutCost = SUM(RoutCost), RoutMatl = SUM(RoutMatl) FROM @tblRout GROUP BY Currency) r,
			(SELECT FactorSet, PlantEDC = SUM(PlantEDC*CAST(DATEDIFF(dd, PeriodStart, PeriodEnd) AS real)/365), 
				PlantMaintEffDiv = SUM(PlantMaintEffDiv*CAST(DATEDIFF(dd, PeriodStart, PeriodEnd) AS real)/365)
			 FROM @tblEDC GROUP BY FactorSet) e
	END
	ELSE BEGIN
		INSERT @tblIndexes(FactorSet, Currency, RoutIndex, RoutMatlIndex, RoutEffIndex)
		SELECT mi.FactorSet, mi.Currency
			, RoutIndex = GlobalDB.dbo.WtAvg(mi.RoutIndex, ftc.PlantEDC*s.FractionOfYear)
			, RoutMatlIndex = GlobalDB.dbo.WtAvg(mi.RoutMatlIndex, ftc.PlantEDC*s.FractionOfYear)
			, RoutEffIndex = GlobalDB.dbo.WtAvg(mi.RoutEffIndex, ftc.PlantMaintEffDiv*s.FractionOfYear)
		FROM dbo.MaintIndex mi INNER JOIN dbo.FactorTotCalc ftc ON ftc.SubmissionID = mi.SubmissionID AND ftc.FactorSet = mi.FactorSet
		INNER JOIN @SubmissionList sl ON sl.SubmissionID = mi.SubmissionID
		INNER JOIN Submissions s ON s.SubmissionID = mi.SubmissionID
		WHERE mi.FactorSet = ISNULL(@FactorSet, mi.FactorSet) AND mi.Currency = ISNULL(@Currency, mi.Currency)
		GROUP BY mi.FactorSet, mi.Currency
	END
RETURN

END

		