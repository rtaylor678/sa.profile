﻿CREATE RULE [dbo].[chkMonth]
    AS @Value BETWEEN 1 AND 12;


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkMonth]', @objname = N'[dbo].[CurrencyConv].[Month]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkMonth]', @objname = N'[dbo].[tMonth]';

