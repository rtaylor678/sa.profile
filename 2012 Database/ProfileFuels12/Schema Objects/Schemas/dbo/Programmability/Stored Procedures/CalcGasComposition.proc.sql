﻿CREATE PROC [dbo].[CalcGasComposition](@SubmissionID int)
AS

DECLARE @LbPerKg real, @BTUPerFOEBbl real, @BblPerM3 real, @CFPerM3 real, @BTUPerMJ real, @SCFPerNM3 real, @CFPerLbMole real, @MJPerFOEBbl real
SELECT @LbPerKg = dbo.UnitsConv(1, 'KG', 'LB')
	, @BTUPerFOEBbl = 6050000
	, @BblPerM3 = dbo.UnitsConv(1, 'M3', 'B')
	, @CFPerM3 = dbo.UnitsConv(1, 'M3', 'CF')
	, @BTUPerMJ = dbo.UnitsConv(1, 'MJ', 'BTU')
	, @SCFPerNM3 = dbo.UnitsConv(1, 'NM3', 'SCF')
	, @CFPerLbMole = 359.05*519.6/491.6
SELECT @MJPerFOEBbl = dbo.UnitsConv(@BTUPerFOEBbl, 'BTU', 'MJ')

	DECLARE @M27Bbl float, @HPFMBTU float
	SELECT @M27Bbl = SUM(Bbl) FROM Yield WHERE SubmissionID = @SubmissionID AND Category In ('RMI','OTHRM') AND MaterialID = 'M27'
	IF ISNULL(@M27Bbl, 0) = 0 
		SELECT @HPFMBTU = 1
	ELSE
		SELECT @HPFMBTU = @M27Bbl*(@BTUPerFOEBbl/1000000)

	SELECT c.SubmissionID, c.TransCode, c.TransType, c.EnergyType, mw.Property
		  ,Total = ISNULL(c.Hydrogen, 0) + ISNULL(c.Methane, 0) + ISNULL(c.Ethane, 0) + ISNULL(c.Ethylene, 0) + ISNULL(c.Propane, 0) + ISNULL(c.Propylene, 0) + ISNULL(c.Butane, 0) + ISNULL(c.Isobutane, 0) + ISNULL(c.Butylenes, 0) + ISNULL(c.C5Plus, 0) + ISNULL(c.CO, 0) + ISNULL(c.CO2, 0) + ISNULL(c.N2, 0) + ISNULL(c.Inerts, 0)
		  ,Hydrogen = ISNULL(c.Hydrogen, 0)
		  ,Methane = ISNULL(c.Methane, 0)
		  ,Ethane = ISNULL(c.Ethane, 0)
		  ,Ethylene = ISNULL(c.Ethylene, 0)
		  ,Propane = ISNULL(c.Propane, 0)
		  ,Propylene = ISNULL(c.Propylene, 0)
		  ,Butane = ISNULL(c.Butane, 0)
		  ,Isobutane = ISNULL(c.Isobutane, 0)
		  ,Butylenes = ISNULL(c.Butylenes, 0)
		  ,C5Plus = ISNULL(c.C5Plus, 0)
		  ,CO = ISNULL(c.CO, 0)
		  ,CO2 = ISNULL(c.CO2, 0)
		  ,N2 = ISNULL(c.N2, 0)
		  ,Inerts = ISNULL(c.Inerts, 0)
		  , InertsWt = ISNULL(c.Inerts*mw.OthInerts, 0)/100
		  , HydrogenWt = ISNULL(c.Hydrogen*mw.Hydrogen, 0)/100
		  , MethaneWt = ISNULL(c.Methane*mw.Methane, 0)/100
		  , EthaneWt = ISNULL(c.Ethane*mw.Ethane, 0)/100
		  , EthyleneWt = ISNULL(c.Ethylene*mw.Ethylene, 0)/100
		  , PropaneWt = ISNULL(c.Propane*mw.Propane, 0)/100
		  , PropyleneWt = ISNULL(c.Propylene*mw.Propylene, 0)/100
		  , ButaneWt = ISNULL(c.Butane*mw.Butane, 0)/100
		  , IsobutaneWt = ISNULL(c.Isobutane*mw.Isobutane, 0)/100
		  , ButylenesWt = ISNULL(c.Butylenes*mw.Butylenes, 0)/100
		  , C5PlusWt = ISNULL(c.C5Plus*mw.C5Plus, 0)/100
		  , COWt = ISNULL(c.CO*mw.CO, 0)/100
		  , CO2Wt = ISNULL(c.CO2*mw.CO2, 0)/100
		  , N2Wt = ISNULL(c.N2*mw.N2, 0)/100
		  ,TotalWt = CAST(NULL as real)
		  ,HeatContentVol = (ISNULL(c.Hydrogen*hv.Hydrogen, 0) + ISNULL(c.Methane*hv.Methane, 0) + ISNULL(c.Ethane*hv.Ethane, 0) + ISNULL(c.Ethylene*hv.Ethylene, 0) + ISNULL(c.Propane*hv.Propane, 0) + ISNULL(c.Propylene*hv.Propylene, 0) + ISNULL(c.Butane*hv.Butane, 0) + ISNULL(c.Isobutane*hv.Isobutane, 0) + ISNULL(c.Butylenes*hv.Butylenes, 0) + ISNULL(c.C5Plus*hv.C5Plus, 0) + ISNULL(c.CO*hv.CO, 0) + ISNULL(c.CO2*hv.CO2, 0) + ISNULL(c.N2*hv.N2, 0) + ISNULL(c.Inerts*hv.OthInerts, 0))/100
		  ,BTUPerLb = CAST(NULL as real), FOEBblPerMSCF = CAST(NULL as real), FOEBblPerMT = CAST(NULL as real)
		  ,PsuedoDensity = CAST(NULL as real)
		  ,CarbonDensity = (ISNULL(c.Hydrogen*cd.Hydrogen, 0) + ISNULL(c.Methane*cd.Methane, 0) + ISNULL(c.Ethane*cd.Ethane, 0) + ISNULL(c.Ethylene*cd.Ethylene, 0) + ISNULL(c.Propane*cd.Propane, 0) + ISNULL(c.Propylene*cd.Propylene, 0) + ISNULL(c.Butane*cd.Butane, 0) + ISNULL(c.Isobutane*cd.Isobutane, 0) + ISNULL(c.Butylenes*cd.Butylenes, 0) + ISNULL(c.C5Plus*cd.C5Plus, 0) + ISNULL(c.CO*cd.CO, 0) + ISNULL(c.CO2*CASE WHEN c.EnergyType='PSG' THEN 0 ELSE cd.CO2 END, 0) + ISNULL(c.N2*cd.N2, 0) + ISNULL(c.Inerts*cd.OthInerts, 0))/100
	INTO #Gases
	FROM dbo.Energy c, dbo.GasProperties mw, dbo.GasProperties hv, dbo.GasProperties cd
	WHERE mw.Property = 'MolWt' AND hv.Property = 'BTU/SCF' AND cd.Property = 'gC/SCF' AND c.SubmissionID = @SubmissionID

	UPDATE #Gases
	SET TotalWt = ISNULL(HydrogenWt, 0) + ISNULL(MethaneWt, 0) + ISNULL(EthaneWt, 0) + ISNULL(EthyleneWt, 0) + ISNULL(PropaneWt, 0) + ISNULL(PropyleneWt, 0) + ISNULL(ButaneWt, 0) + ISNULL(IsobutaneWt, 0) + ISNULL(ButylenesWt, 0) + ISNULL(C5PlusWt, 0) + ISNULL(COWt, 0) + ISNULL(CO2Wt, 0) + ISNULL(N2Wt, 0) + ISNULL(InertsWt, 0)
	UPDATE #Gases
	SET BTUPerLb = CASE WHEN TotalWt > 0 THEN HeatContentVol/TotalWt*@CFPerLbMole END
		, FOEBblPerMSCF = HeatContentVol*1000000/@BTUPerFOEBbl
	UPDATE #Gases
	SET FOEBblPerMT = dbo.UnitsConv(BTUPerLb, 'BTU/LB', 'MJ/MT')/@MJPerFOEBbl
	UPDATE #Gases
	SET PsuedoDensity = (1000/(FOEBblPerMT/@BblPerM3))
	WHERE FOEBblPerMT > 0

	UPDATE Energy
	SET HeatContentVol = g.HeatContentVol, HeatContentWt = g.BTUPerLb, MolWt = g.TotalWt, PsuedoDensity = g.PsuedoDensity, FOEBblPerMT = g.FOEBblPerMT
		, HydrogenWt = g.HydrogenWt/g.TotalWt*100
		, MethaneWt = g.MethaneWt/g.TotalWt*100
		, EthaneWt = g.EthaneWt/g.TotalWt*100
		, EthyleneWt = g.EthyleneWt/g.TotalWt*100
		, PropaneWt = g.PropaneWt/g.TotalWt*100
		, PropyleneWt = g.PropyleneWt/g.TotalWt*100
		, ButaneWt = g.ButaneWt/g.TotalWt*100
		, IsobutaneWt = g.IsobutaneWt/g.TotalWt*100
		, ButylenesWt = g.ButylenesWt/g.TotalWt*100
		, C5PlusWt = g.C5PlusWt/g.TotalWt*100
		, COWt = g.COWt/g.TotalWt*100
		, CO2Wt = g.CO2Wt/g.TotalWt*100
		, N2Wt = g.N2Wt/g.TotalWt*100
		, InertsWt = g.InertsWt/g.TotalWt*100
		, Total = g.Total
		, TotalWt = g.TotalWt/g.TotalWt*100
		, TotalInerts = (g.CO2 + g.N2 + g.Inerts)
		, TotalInertsWt = (g.CO2Wt + g.N2Wt + g.InertsWt)/g.TotalWt*100
		, CEF = CASE WHEN g.HeatContentVol > 0 THEN g.CarbonDensity/g.HeatContentVol END
		, MBTU = CASE WHEN ISNULL(SourceMBTU,0) = 0 THEN ISNULL(UsageMBTU,0) ELSE ISNULL(SourceMBTU,0) END
	FROM Energy INNER JOIN #Gases g ON g.SubmissionID = Energy.SubmissionID AND g.TransCode = Energy.TransCode
	WHERE Energy.SubmissionID = @SubmissionID AND g.TotalWt > 0

	DROP TABLE #Gases
/*
	UPDATE Energy
	SET MBTU = @HPFMBTU
	WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType = 'HPF'
*/
	UPDATE Energy
	SET MSCF = MBTU/HeatContentVol, MLb = MBTU/HeatContentWt
	WHERE SubmissionID = @SubmissionID AND HeatContentVol > 0
