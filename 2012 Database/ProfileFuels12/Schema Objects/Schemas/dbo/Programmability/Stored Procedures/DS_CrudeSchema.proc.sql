﻿CREATE PROCEDURE [dbo].[DS_CrudeSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT RTRIM(CNum) as CNum, RTRIM(CrudeName) as CrudeName, Gravity, Sulfur, 0.0 AS  BBL,0.0 AS CostPerBBL FROM CRUDE WHERE SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID=@RefineryID)
END
