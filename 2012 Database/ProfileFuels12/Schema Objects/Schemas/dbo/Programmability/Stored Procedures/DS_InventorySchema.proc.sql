﻿CREATE PROCEDURE [dbo].[DS_InventorySchema]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS
BEGIN
	SELECT RTRIM(TankType) as TankType, NumTank, FuelsStorage, AvgLevel FROM Inventory i WHERE   SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID = @RefineryID and DataSet = @Dataset and UseSubmission=1)
END
