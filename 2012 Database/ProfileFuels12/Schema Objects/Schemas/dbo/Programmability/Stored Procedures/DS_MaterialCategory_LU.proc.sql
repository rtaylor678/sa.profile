﻿CREATE PROC [dbo].[DS_MaterialCategory_LU]
	
AS

SELECT RTRIM(Category) as Category, RTRIM(CategoryName) as CategoryName 
                     FROM MaterialCategory_LU 
                     WHERE Category not in ('RMI','RPF')
                     ORDER BY SortKey
