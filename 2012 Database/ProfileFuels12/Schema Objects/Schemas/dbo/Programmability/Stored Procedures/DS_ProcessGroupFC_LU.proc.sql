﻿CREATE PROC [dbo].[DS_ProcessGroupFC_LU]
	
AS

SELECT RTRIM(ProcessGroup) AS ProcessGroup, RTRIM(Description) AS GroupDesc, 
                        SortKey
                        FROM ProcessGroup_LU
                        ORDER BY SortKey
