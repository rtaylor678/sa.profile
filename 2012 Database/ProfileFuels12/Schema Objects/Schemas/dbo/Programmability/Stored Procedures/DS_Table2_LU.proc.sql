﻿CREATE PROC [dbo].[DS_Table2_LU]
	@CompanyID nvarchar(20)
AS

SELECT RTRIM(ProcessID) AS ProcessID, RTRIM(Property) AS Property,
                   RTRIM(USDescription) AS USDescription, RTRIM(MetDescription) AS MetDescription, 
                   USDecPlaces, MetDecPlaces,SortKey 
                   FROM Table2_LU WHERE 
                   CustomGroup=0 OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CompanyID=@CompanyID AND CustomType='T2')
