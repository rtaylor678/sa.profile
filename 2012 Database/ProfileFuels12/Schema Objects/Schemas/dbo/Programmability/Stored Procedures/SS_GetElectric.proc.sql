﻿CREATE PROC [dbo].[SS_GetElectric]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@Dataset nvarchar(20)='ACTUAL'
	
AS

SELECT RTRIM(e.TransType) as TransType,RTRIM(e.TransferTo) as TransferTo, RTRIM(e.EnergyType) as EnergyType,e.TransCode,e.RptGenEff,e.RptMWH,e.PriceLocal,elu.SortKey 
             FROM dbo.Electric e ,Energy_LU elu WHERE 
            elu.TransType=e.TransType AND elu.TransferTo=e.TransferTo AND elu.EnergyType=e.EnergyType AND elu.SortKey > 100 AND 
             (SubmissionID IN 
            (SELECT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID  and DataSet = @Dataset and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd))))
