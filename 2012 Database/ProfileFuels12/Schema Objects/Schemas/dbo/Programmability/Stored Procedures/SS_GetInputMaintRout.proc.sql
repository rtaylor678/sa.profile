﻿CREATE PROC [dbo].[SS_GetInputMaintRout]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            mr.UnitID,RTRIM(mr.ProcessID) AS ProcessID,mr.RoutCostLocal,
            mr.RegNum,mr.MaintNum,mr.OthNum,mr.OthDownEconomic,mr.OthDownExternal, 
            mr.OthDownUnitUpsets,mr.OthDownOffsiteUpsets,mr.OthDownOther, 
            
            mr.RegDown,mr.MaintDown,mr.OthDown, 
            cfg.SortKey,ISNULL(RTRIM(cfg.UnitName), RTRIM(lu.Description)) AS UnitName 
            FROM  
            dbo.MaintRout mr LEFT JOIN dbo.Config cfg ON mr.SubmissionID = cfg.SubmissionID AND mr.UNITID=cfg.UNITID  
            LEFT JOIN dbo.ProcessID_LU lu ON lu.ProcessID = mr.ProcessID  
            ,dbo.Submissions s  
            WHERE   
            mr.SubmissionID = s.SubmissionID AND 
            mr.SubmissionID IN (SELECT DISTINCT SubmissionID FROM dbo.Submissions WHERE RefineryID = @RefineryID and DataSet = @Dataset and UseSubmission=1 )
