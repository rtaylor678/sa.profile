﻿CREATE PROC [dbo].[SS_GetInputMaintTA_Process]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS

SELECT  
s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            mt.TAID,mt.UnitID,RTRIM(cfg.ProcessID) AS ProcessID,mt.TADate,mt.TAHrsDown, 
            mt.TACostLocal,mt.TAExpLocal, mt.TACptlLocal, mt.TAOvhdLocal, mt.TALaborCostLocal, mt.TAOCCSTH,mt.TAOCCOVT,mt.TAMPSSTH,mt.TAMPSOVTPcnt, 
            cfg.SortKey,mt.TAContOCC,mt.TAContMPS,mt.PrevTADate, 
            mt.TAExceptions,RTRIM(cfg.UnitName) AS UnitName  
            FROM  
            dbo.MaintTA mt, Config cfg  
            ,dbo.Submissions s  
            WHERE   
            cfg.SubmissionID = s.SubmissionID AND 
            mt.RefineryId=@RefineryID
            AND mt.UnitId = cfg.UnitId AND cfg.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1)
