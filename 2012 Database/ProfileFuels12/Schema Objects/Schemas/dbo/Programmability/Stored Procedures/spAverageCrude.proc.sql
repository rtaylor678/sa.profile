﻿
CREATE PROC spAverageCrude(@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
		@Gravity real OUTPUT, @Sulfur real OUTPUT)
AS
SELECT @Gravity = CASE WHEN SUM(TotMT) > 0 THEN dbo.KGM3toAPI(SUM(AvgDensity*TotMT)/SUM(TotMT)) ELSE NULL END,
@Sulfur = CASE WHEN SUM(TotMT) > 0 THEN SUM(AvgSulfur*TotMT)/SUM(TotMT) ELSE NULL END
FROM CrudeTot c INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd

