﻿CREATE      PROC [dbo].[spAverageOpex](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @Currency CurrencyCode, @Scenario Scenario,
	@TotCashOpexUEDC real OUTPUT, @VolOpexUEDC real OUTPUT, @NonVolOpexUEDC real OUTPUT, 
	@NEOpexUEDC real OUTPUT, @NEOpexEDC real OUTPUT, @ROI real OUTPUT, @VAI real OUTPUT, @NEI real OUTPUT, @RV real OUTPUT, @TotCptl real OUTPUT)
AS

SELECT @ROI = CASE WHEN SUM(r.TotCptl*s.FractionOfYear) > 0 THEN SUM(r.ROI*r.TotCptl*s.FractionOfYear)/SUM(r.TotCptl*s.FractionOfYear) ELSE NULL END,
@VAI = CASE WHEN SUM(f.UEDC*s.NumDays) > 0 THEN SUM(r.VAI*f.UEDC*s.NumDays)/SUM(f.UEDC*s.NumDays) ELSE NULL END,
@RV = SUM(r.RV*s.FractionOfYear)/SUM(s.FractionOfYear),
@TotCptl = SUM(r.TotCptl*s.FractionOfYear)/SUM(s.FractionOfYear)
FROM ROICalc r INNER JOIN Submissions s ON s.SubmissionID = r.SubmissionID
INNER JOIN FactorTotCalc f ON f.SubmissionID = r.SubmissionID AND f.FactorSet = r.FactorSet
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND r.Scenario = @Scenario AND r.FactorSet = @FactorSet AND r.Currency = @Currency

SET @Scenario = 'CLIENT' -- Using Client energy prices for all pricing scenarios

SELECT @TotCashOpexUEDC = SUM(TotCashOpex*Divisor)/SUM(Divisor),
@VolOpexUEDC = SUM(STVol*Divisor)/SUM(Divisor),
@NonVolOpexUEDC = SUM(STNonVol*Divisor)/SUM(Divisor),
@NEOpexUEDC = SUM(NEOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND o.DataType = 'UEDC' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

SELECT @NEOpexEDC = SUM(NEOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND o.DataType = 'EDC' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

SELECT @NEI = SUM(NEOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND o.DataType = 'NEI' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency
