﻿CREATE  PROC [dbo].[spDeleteSubmission](@SubmissionID int)
AS
DECLARE @tablename varchar(30), @qry varchar(255)
DECLARE tnames_cursor CURSOR FOR SELECT name FROM sysobjects 
	WHERE type = 'U' AND id IN (SELECT id FROM syscolumns WHERE name = 'SubmissionID')

OPEN tnames_cursor
FETCH NEXT FROM tnames_cursor INTO @tablename
WHILE (@@FETCH_STATUS <> -1)
BEGIN
	IF (@@FETCH_STATUS <> -2)
	BEGIN
		PRINT ' '
		SELECT @qry = 'DELETE FROM ' + @tablename + ' WHERE SubmissionId = ' + CAST(@SubmissionID AS varchar(10)) + ''
		EXEC (@qry)
		PRINT @qry
	END
	FETCH NEXT FROM tnames_cursor INTO @tablename
END
CLOSE tnames_cursor
DEALLOCATE tnames_cursor
