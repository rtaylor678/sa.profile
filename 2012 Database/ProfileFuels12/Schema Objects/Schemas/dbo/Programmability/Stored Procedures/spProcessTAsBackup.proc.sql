﻿CREATE      PROC [dbo].[spProcessTAsBackup] (@SubmissionID int)
AS
DECLARE @RefineryID varchar(6), @DataSet varchar(15), @RptCurrency CurrencyCode
DECLARE @LoadUnitID int, @LoadTAID int, @LoadProcessID ProcessID,
	@LoadSchedInt real, @LoadTADate smalldatetime, @LoadPrevTADate smalldatetime,
	@LoadHrsDown real, @LoadCost real, @LoadMatl real, 
	@LoadOCCSTH real, @LoadOCCOVT real, @LoadMPSSTH real, @LoadMPSOVT real,
	@LoadContOCC real, @LoadContMPS real, @LoadExceptions tinyint, @LoadCurrency CurrencyCode
DECLARE @OldUnitID int, @OldTAID int,  @OldProcessID ProcessID,
	@OldSchedInt real, @OldTADate smalldatetime, @OldPrevTADate smalldatetime,
	@OldHrsDown real, @OldCost real, @OldMatl real, 
	@OldOCCSTH real, @OldOCCOVT real, @OldMPSSTH real, @OldMPSOVT real,
	@OldContOCC real, @OldContMPS real, @OldExceptions tinyint, @OldCurrency CurrencyCode
DECLARE @UnitID int, @TAID int, @UpdateAction char(1), @RecalcFrom smalldatetime, @RecalcTo smalldatetime
DECLARE @ta TABLE (
	UnitID int NOT NULL,
	TAID int NOT NULL,
	UpdateAction char(1) NULL,
	RecalcFrom smalldatetime NULL,
	RecalcTo smalldatetime NULL
)
SELECT @RefineryID = RefineryID, @DataSet = DataSet, @RptCurrency = RptCurrency
FROM Submissions WHERE SubmissionID = @SubmissionID
UPDATE LoadTA
SET TACurrency = @RptCurrency
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND TACurrency IS NULL
INSERT INTO @ta (UnitID, TAID)
SELECT UnitID, TAID
FROM MaintTA
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
UNION
SELECT UnitID, TAID
FROM LoadTA
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
DECLARE cTA CURSOR SCROLL 
FOR SELECT UnitID, TAID FROM @ta
OPEN cTA
FETCH NEXT FROM cTA INTO @UnitID, @TAID
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @UpdateAction = NULL, @RecalcFrom = NULL, @RecalcTo = NULL
	SELECT @LoadUnitID = UnitID, @LoadTAID = TAID, @LoadProcessID = ProcessID,
	@LoadSchedInt = SchedTAInt, @LoadTADate = TADate, @LoadPrevTADate = PrevTADate,
	@LoadHrsDown = TAHrsDown, @LoadCost = TACostLocal, @LoadMatl = TAMatlLocal, 
	@LoadOCCSTH = TAOCCSTH, @LoadOCCOVT = TAOCCOVT, @LoadMPSSTH = TAMPSSTH, @LoadMPSOVT = TAMPSOVTPcnt,
	@LoadContOCC = TAContOCC, @LoadContMPS = TAContMPS, @LoadExceptions = TAExceptions, @LoadCurrency = TACurrency
	FROM LoadTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
	SELECT @OldUnitID = UnitID, @OldTAID = TAID, @OldProcessID = ProcessID,
	@OldSchedInt = SchedTAInt, @OldTADate = TADate, @OldPrevTADate = PrevTADate,
	@OldHrsDown = TAHrsDown, @OldCost = TACostLocal, @OldMatl = TAMatlLocal, 
	@OldOCCSTH = TAOCCSTH, @OldOCCOVT = TAOCCOVT, @OldMPSSTH = TAMPSSTH, @OldMPSOVT = TAMPSOVTPcnt,
	@OldContOCC = TAContOCC, @OldContMPS = TAContMPS, @OldExceptions = TAExceptions, @OldCurrency = TACurrency
	FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
	IF @OldUnitID IS NULL
		SELECT @UpdateAction = 'A', @RecalcFrom = @LoadTADate
	IF @LoadUnitID IS NULL
		SELECT @UpdateAction = 'D', @RecalcFrom = @OldTADate
	IF @UpdateAction IS NULL
	BEGIN
		IF NOT ((@OldSchedInt IS NULL AND @LoadSchedInt IS NULL) OR ABS(@OldSchedInt - @LoadSchedInt)<0.01)
			SELECT @UpdateAction = 'S'
		IF NOT ((@OldProcessID IS NULL AND @LoadProcessID IS NULL) OR (@OldProcessID = @LoadProcessID))
			SELECT @UpdateAction = 'S'
		IF NOT (((@OldTADate IS NULL AND @LoadTADate IS NULL) OR (@OldTADate = @LoadTADate))
			AND ((@OldHrsDown IS NULL AND @LoadHrsDown IS NULL) OR ABS(@OldHrsDown - @LoadHrsDown)<0.01)
			AND ((@OldCost IS NULL AND @LoadCost IS NULL) OR ABS(@OldCost - @LoadCost)<0.01)
			AND ((@OldMatl IS NULL AND @LoadMatl IS NULL) OR ABS(@OldMatl - @LoadMatl)<0.01)
			AND ((@OldOCCSTH IS NULL AND @LoadOCCSTH IS NULL) OR ABS(@OldOCCSTH - @LoadOCCSTH)<0.01)
			AND ((@OldOCCOVT IS NULL AND @LoadOCCOVT IS NULL) OR ABS(@OldOCCOVT - @LoadOCCOVT)<0.01)
			AND ((@OldMPSSTH IS NULL AND @LoadMPSSTH IS NULL) OR ABS(@OldMPSSTH - @LoadMPSSTH)<0.01)
			AND ((@OldMPSOVT IS NULL AND @LoadMPSOVT IS NULL) OR ABS(@OldMPSOVT - @LoadMPSOVT)<0.01)
			AND ((@OldContOCC IS NULL AND @LoadContOCC IS NULL) OR ABS(@OldContOCC - @LoadContOCC)<0.01)
			AND ((@OldContMPS IS NULL AND @LoadContMPS IS NULL) OR ABS(@OldContMPS - @LoadContMPS)<0.01)
			AND ((@OldPrevTADate IS NULL AND @LoadPrevTADate IS NULL) OR (@OldPrevTADate = @LoadPrevTADate))
			AND ((@OldExceptions IS NULL AND @LoadExceptions IS NULL) OR (@OldExceptions = @LoadExceptions))
			AND ((@OldCurrency IS NULL AND @LoadCurrency IS NULL) OR (@OldCurrency = @LoadCurrency)))
			SELECT @UpdateAction = 'U', @RecalcFrom = CASE WHEN @LoadTADate < @OldTADate THEN @LoadTADate ELSE @OldTADate END
	END

	SELECT @RecalcFrom = MIN(s.PeriodStart)
	FROM Submissions s INNER JOIN Config c ON c.SubmissionID = c.SubmissionID
	WHERE c.UnitID = @UnitID AND s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
	AND s.PeriodStart >= @RecalcFrom

	IF @UpdateAction = 'D'
	BEGIN
		IF NOT EXISTS (SELECT * FROM Config WHERE SubmissionID = @SubmissionID AND UnitID = @UnitID)
		AND EXISTS (SELECT * FROM Config WHERE SubmissionID <> @SubmissionID AND UnitID = @UnitID)
			SELECT @UpdateAction = 'X'  -- Deleted whole unit, but T/A should still apply for previous months
	END

	UPDATE @ta
	SET UpdateAction = @UpdateAction, RecalcFrom = @RecalcFrom
	WHERE UnitId = @UnitID AND TAID = @TAID

	IF @UpdateAction IN ('U','D')
	BEGIN
		DELETE FROM MaintTA 
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet
		AND UnitID = @UnitID AND TAID = @TAID
		DELETE FROM MaintTACost
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet
		AND UnitID = @UnitID AND TAID = @TAID
	END
	IF @UpdateAction = 'S'
		UPDATE MaintTA SET SchedTAInt = @LoadSchedInt, ProcessID = @LoadProcessID
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet
		AND UnitID = @UnitID AND TAID = @TAID
	IF @UpdateAction IN ('A','U')
	BEGIN
		INSERT INTO MaintTA (RefineryID, DataSet, UnitID, TAID, 
			SchedTAInt, TADate, PrevTADate, TAHrsDown, TACostLocal, TAMatlLocal,
			TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt,
			TAContOCC, TAContMPS, TAExceptions, TACurrency, ProcessID)
		VALUES (@RefineryID, @DataSet, @LoadUnitID, @LoadTAID, 
			@LoadSchedInt, @LoadTADate, @LoadPrevTADate, @LoadHrsDown, @LoadCost, @LoadMatl, 
			@LoadOCCSTH, @LoadOCCOVT, @LoadMPSSTH, @LoadMPSOVT,
			@LoadContOCC, @LoadContMPS, @LoadExceptions, @LoadCurrency, @LoadProcessID)
		EXEC spTACalcs @RefineryID, @DataSet, @LoadUnitID, @LoadTAID
	END
	FETCH NEXT FROM cTA INTO @UnitID, @TAID
END
CLOSE cTA
DEALLOCATE cTA

UPDATE ta
SET RecalcTo = (SELECT MIN(TADate) FROM MaintTA 
	WHERE MaintTA.RefineryID = @RefineryID AND MaintTA.DataSet = @DataSet 
	AND MaintTA.UnitID = ta.UnitID AND MaintTA.TADate > ta.RecalcFrom)
FROM @ta ta
WHERE UpdateAction IN ('A','D','U')

UPDATE @ta
SET RecalcTo = (SELECT MAX(PeriodEnd) FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet)
WHERE RecalcFrom IS NOT NULL AND RecalcTo IS NULL

SELECT @RecalcFrom = MIN(RecalcFrom), @RecalcTo = MAX(RecalcTo)
FROM @TA

UPDATE Submissions
SET CalcsNeeded = 'T'
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
AND PeriodStart <= @RecalcTo AND PeriodEnd >= @RecalcFrom
AND CalcsNeeded IS NULL

EXEC spUpdateNextTADate @RefineryID, @DataSet
