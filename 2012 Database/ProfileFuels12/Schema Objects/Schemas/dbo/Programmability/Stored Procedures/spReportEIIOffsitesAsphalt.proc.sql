﻿CREATE    PROC [dbo].[spReportEIIOffsitesAsphalt] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
DECLARE @OffsitesEnergyConstant decimal(5,0), @OffsitesEnergyCmplxFactor decimal(5,1), @OffsitesFormula varchar(20), @ASPEIIFormula varchar(255)
SELECT @OffsitesEnergyConstant = OffsitesEnergyConstant, @OffsitesEnergyCmplxFactor = OffsitesEnergyCmplxFactor
FROM FactorSets WHERE FactorSet = @FactorSet AND RefineryType = 'FUELS'
SELECT @OffsitesFormula = CONVERT(varchar(6), @OffsitesEnergyConstant) + '+' + CONVERT(varchar(6), @OffsitesEnergyCmplxFactor) + '*(A)'
SELECT @ASPEIIFormula = EIIFormula FROM Factors WHERE ProcessID = 'ASP' AND FactorSet = @FactorSet

SELECT s.SubmissionID, s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, 
ASPUtilCap, ASPStdEnergy, OffsitesUtilCap, OffsitesStdEnergy, Complexity
INTO #Monthly
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE FactorSet=@FactorSet
AND s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth)

SELECT s1.SubmissionID, 
ASPUtilCap_YTD = SUM(fc.ASPUtilCap*s2.NumDays)/SUM(s2.NumDays), EstASPMBTU_YTD = SUM(fc.AspStdEnergy*s2.NumDays), AspStdEnergy_YTD = SUM(fc.AspStdEnergy*s2.NumDays)/SUM(s2.NumDays),
OffsitesUtilCap_YTD = SUM(fc.OffsitesUtilCap*s2.NumDays)/SUM(s2.NumDays), EstOffsitesMBTU_YTD = SUM(fc.OffsitesStdEnergy*s2.NumDays), OffsitesStdEnergy_YTD = SUM(fc.OffsitesStdEnergy*s2.NumDays)/SUM(s2.NumDays)
INTO #YTD
FROM Submissions s1 INNER JOIN Submissions s2 ON s2.RefineryID = s1.RefineryID AND s2.DataSet = s1.DataSet AND s2.PeriodYear = s1.PeriodYear AND s2.PeriodMonth <= s1.PeriodMonth
INNER JOIN FactorTotCalc fc ON fc.submissionId = s2.submissionid 
WHERE fc.factorset=@FactorSet 
And s1.RefineryID = @RefineryID AND s1.DataSet=@DataSet AND s1.PeriodYear = ISNULL(@PeriodYear, s1.PeriodYear) AND s1.PeriodMonth = ISNULL(@PeriodMonth, s1.PeriodMonth)
GROUP BY s1.SubmissionID

SET NOCOUNT OFF

SELECT m.Location, m.PeriodStart, m.PeriodEnd, m.DaysInPeriod, Currency= @Currency, UOM = @UOM, 
m.ASPUtilCap, m.ASPStdEnergy, ASPEIIFormula = @ASPEIIFormula,
m.OffsitesUtilCap, m.OffsitesStdEnergy, @OffsitesFormula As OffsitesFormula, m.Complexity,
y.ASPUtilCap_YTD, y.EstASPMBTU_YTD, y.AspStdEnergy_YTD, y.OffsitesUtilCap_YTD, y.EstOffsitesMBTU_YTD, y.OffsitesStdEnergy_YTD
FROM #Monthly m INNER JOIN #YTD y ON y.SubmissionID = m.SubmissionID
ORDER BY m.PeriodStart DESC

DROP TABLE #Monthly
DROP TABLE #YTD
