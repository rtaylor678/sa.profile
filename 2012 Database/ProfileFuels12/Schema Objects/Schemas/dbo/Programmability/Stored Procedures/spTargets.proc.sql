﻿CREATE           PROC [dbo].[spTargets](@SubmissionID int)
AS
UPDATE MaintCalc
SET MechAvail_Ann_Target = t.MechAvail,
MechAvail_Act_Target = t.MechAvail,
MechAvailSlow_Ann_Target = t.MechAvail,
MechAvailSlow_Act_Target = t.MechAvail,
OpAvail_Ann_Target = t.OpAvail,
OpAvail_Act_Target = t.OpAvail,
OpAvailSlow_Ann_Target = t.OpAvail,
OpAvailSlow_Act_Target = t.OpAvail,
OnStream_Ann_Target = t.OnStream,
OnStream_Act_Target = t.OnStream,
OnStreamSlow_Ann_Target = t.OnStream,
OnStreamSlow_Act_Target = t.OnStream
FROM MaintCalc INNER JOIN UnitTargets t ON t.SubmissionID = MaintCalc.SubmissionID AND t.UnitID = MaintCalc.UnitID
WHERE MaintCalc.SubmissionID = @SubmissionID
SELECT m.SubmissionID, e.FactorSet, m.ProcessID, MechAvail_Ann_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.MechAvail_Ann_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
MechAvail_Act_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.MechAvail_Act_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
MechAvailSlow_Ann_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.MechAvailSlow_Ann_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
MechAvailSlow_Act_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.MechAvailSlow_Act_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
OpAvail_Ann_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.OpAvail_Ann_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
OpAvail_Act_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.OpAvail_Act_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
OpAvailSlow_Ann_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.OpAvailSlow_Ann_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
OpAvailSlow_Act_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.OpAvailSlow_Act_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
OnStream_Ann_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.OnStream_Ann_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
OnStream_Act_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.OnStream_Act_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
OnStreamSlow_Ann_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.OnStreamSlow_Ann_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END,
OnStreamSlow_Act_Target = CASE WHEN SUM(e.EDCNoMult) > 0 THEN SUM(m.OnStreamSlow_Act_Target * e.EDCNoMult)/SUM(e.EDCNoMult) END
INTO #ProcessTargets
FROM MaintCalc m INNER JOIN FactorCalc e ON e.SubmissionID = m.SubmissionID AND e.UnitID = m.UnitID
WHERE m.SubmissionID = @SubmissionID
GROUP BY m.SubmissionID, e.FactorSet, m.ProcessID
UPDATE MaintProcess
SET MechAvail_Ann_Target = t.MechAvail_Ann_Target, MechAvail_Act_Target = t.MechAvail_Act_Target,
MechAvailSlow_Ann_Target = t.MechAvailSlow_Ann_Target, MechAvailSlow_Act_Target = t.MechAvailSlow_Act_Target,
OpAvail_Ann_Target = t.OpAvail_Ann_Target, OpAvail_Act_Target = t.OpAvail_Act_Target,
OpAvailSlow_Ann_Target = t.OpAvailSlow_Ann_Target, OpAvailSlow_Act_Target = t.OpAvailSlow_Act_Target,
OnStream_Ann_Target = t.OnStream_Ann_Target, OnStream_Act_Target = t.OnStream_Act_Target,
OnStreamSlow_Ann_Target = t.OnStreamSlow_Ann_Target, OnStreamSlow_Act_Target = t.OnStreamSlow_Act_Target
FROM MaintProcess INNER JOIN #ProcessTargets t ON t.SubmissionID = MaintProcess.SubmissionID AND t.FactorSet = MaintProcess.FactorSet AND t.ProcessID = MaintProcess.ProcessID
WHERE MaintProcess.SubmissionID = @SubmissionID
DROP TABLE #ProcessTargets
UPDATE MaintAvailCalc
SET MechAvail_Ann_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'MechAvail_Ann_Target'),
MechAvail_Act_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'MechAvail_Act_Target'),
MechAvailSlow_Ann_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'MechAvailSlow_Ann_Target'),
MechAvailSlow_Act_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'MechAvailSlow_Act_Target'),
OpAvail_Ann_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'OpAvail_Ann_Target'),
OpAvail_Act_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'OpAvail_Act_Target'),
OpAvailSlow_Ann_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'OpAvailSlow_Ann_Target'),
OpAvailSlow_Act_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'OpAvailSlow_Act_Target'),
OnStream_Ann_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'OnStream_Ann_Target'),
OnStream_Act_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'OnStream_Act_Target'),
OnStreamSlow_Ann_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'OnStreamSlow_Ann_Target'),
OnStreamSlow_Act_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = MaintAvailCalc.SubmissionID AND t.Property = 'OnStreamSlow_Act_Target')
WHERE SubmissionID = @SubmissionID
UPDATE Gensum
SET UtilPcnt_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'UtilPcnt_Target'),
UtilOSTA_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'UtilOSTA_Target'),
ProcessUtilPcnt_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'ProcessUtilPcnt_Target'),
MechAvail_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MechAvail_Ann_Target'),
--MechAvailSlow_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MechAvail_Ann_Target'),
OpAvail_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'OpAvail_Ann_Target'),
--OpAvailSlow_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'OpAvail_Ann_Target'),
OnStream_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'OnStream_Ann_Target'),
OnStreamSlow_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'OnStream_Ann_Target'),
NetInputBPD_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'NetInputBPD_Target'),
CrudeAPI_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'CrudeAPI_Target'),
CrudeSulfur_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'CrudeSulfur_Target'),
ROI_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'ROI_Target'),
--VAI_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'VAI_Target'),
GainPcnt_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'GainPcnt_Target'),
VEI_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'VEI_Target'),
EII_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'EII_Target'),
TotWHrEDC_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'TotWHrEDC_Target'),
OCCWHrEDC_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'OCCWHrEDC_Target'),
MPSWHrEDC_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MPSWHrEDC_Target'),
TotEqPEDC_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'TotEqPEDC_Target'),
OCCEqPEDC_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'OCCEqPEDC_Target'),
MPSEqPEDC_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MPSEqPEDC_Target'),
PEI_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'PEI_Target'),
OCCOvtPcnt_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'OCCOvtPcnt_Target'),
MPSOvtPcnt_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MPSOvtPcnt_Target'),
ProcOCCMPSRatio_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'ProcOCCMPSRatio_Target'),
MaintOCCMPSRatio_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MaintOCCMPSRatio_Target'),
OCCAbsPcnt_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'OCCAbsPcnt_Target'),
MPSAbsPcnt_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MPSAbsPcnt_Target'),
TotMaintForceWHrEDC_Target = (SELECT Target FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'TotMaintForceWHrEDC_Target')
WHERE SubmissionID = @SubmissionID
UPDATE Gensum
SET EnergyCost_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart)*CASE Gensum.UOM WHEN s.UOM THEN 1 WHEN 'US' THEN 1.055 WHEN 'MET' THEN (1/1.055) END FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'EnergyCost_Target'),
EnergyCost_Pur_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart)*CASE Gensum.UOM WHEN s.UOM THEN 1 WHEN 'US' THEN 1.055 WHEN 'MET' THEN (1/1.055) END FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'EnergyCost_Pur_Target'),
EnergyCost_Prod_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart)*CASE Gensum.UOM WHEN s.UOM THEN 1 WHEN 'US' THEN 1.055 WHEN 'MET' THEN (1/1.055) END FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'EnergyCost_Prod_Target'),
EnergyConsPerBbl_Target = (SELECT Target*CASE Gensum.UOM WHEN s.UOM THEN 1 WHEN 'MET' THEN 1.055 WHEN 'US' THEN (1/1.055) END FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'EnergyConsPerBbl_Target')
FROM Gensum INNER JOIN Submissions s ON s.SubmissionID = Gensum.SubmissionID
WHERE Gensum.SubmissionID = @SubmissionID
UPDATE Gensum
SET TotCashOpexUEDC_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'TotCashOpexUEDC_Target'),
NonVolOpexUEDC_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'NonVolOpexUEDC_Target'),
VolOpexUEDC_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'VolOpexUEDC_Target'),
NEOpexUEDC_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'NEOpexUEDC_Target'),
NEOpexEDC_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'NEOpexEDC_Target'),
NEI_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'NEI_Target'),
MaintIndex_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MaintIndex_Target'),
RoutIndex_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'RoutIndex_Target'),
TAIndex_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'TAIndex_Target'),
MEI_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MEI_Target'),
MEI_Rout_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MEI_Rout_Target'),
MEI_TA_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'MEI_TA_Target'),
GPV_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'GPV_Target'),
RMC_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'RMC_Target'),
GrossMargin_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'GrossMargin_Target'),
TotCashOpexBbl_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'TotCashOpexBbl_Target'),
CashMargin_Target = (SELECT Target*dbo.ExchangeRate(t.CurrencyCode, Gensum.Currency, Gensum.PeriodStart) FROM RefTargets t WHERE t.SubmissionID = Gensum.SubmissionID AND t.Property = 'CashMargin_Target')
WHERE Gensum.SubmissionID = @SubmissionID
