﻿CREATE TABLE [dbo].[APIScale] (
    [StudyYear] [dbo].[StudyYear] NOT NULL,
    [APIScale]  TINYINT           NOT NULL,
    [APILevel]  TINYINT           NOT NULL,
    [MinAPI]    REAL              NOT NULL,
    [MaxAPI]    REAL              NOT NULL,
    [Adj]       [dbo].[Price]     NOT NULL,
    [SaveDate]  SMALLDATETIME     NOT NULL
);

