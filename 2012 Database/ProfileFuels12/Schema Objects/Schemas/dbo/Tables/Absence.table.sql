﻿CREATE TABLE [dbo].[Absence] (
    [SubmissionID] INT      NOT NULL,
    [SortKey]      SMALLINT NOT NULL,
    [CategoryID]   CHAR (6) NOT NULL,
    [OCCAbs]       REAL     NULL,
    [MPSAbs]       REAL     NULL,
    [TotAbs]       REAL     NULL,
    [OCCPcnt]      REAL     NULL,
    [MPSPcnt]      REAL     NULL,
    [TotPcnt]      REAL     NULL
);

