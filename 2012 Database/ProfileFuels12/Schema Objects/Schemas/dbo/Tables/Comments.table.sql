﻿CREATE TABLE [dbo].[Comments] (
    [SubmissionID] INT            NOT NULL,
    [TableNum]     CHAR (6)       NOT NULL,
    [CommentCode]  DECIMAL (9, 4) NOT NULL,
    [Comment]      VARCHAR (255)  NULL
);

