﻿CREATE TABLE [dbo].[CrudeCalc] (
    [SubmissionID] INT              NOT NULL,
    [Scenario]     [dbo].[Scenario] NOT NULL,
    [CrudeID]      [dbo].[CrudeID]  NOT NULL,
    [CNum]         [dbo].[CrudeNum] NULL,
    [BasePrice]    REAL             NULL,
    [TransCost]    REAL             NULL,
    [GravityAdj]   REAL             NULL,
    [SulfurAdj]    REAL             NULL,
    [PricePerBbl]  REAL             NULL
);

