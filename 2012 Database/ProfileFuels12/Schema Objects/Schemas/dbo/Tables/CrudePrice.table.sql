﻿CREATE TABLE [dbo].[CrudePrice] (
    [StudyYear]      [dbo].[StudyYear] NOT NULL,
    [CNum]           CHAR (5)          NOT NULL,
    [BasePrice]      REAL              NOT NULL,
    [APIScale]       TINYINT           NOT NULL,
    [PostedAPI]      REAL              NOT NULL,
    [SaveDate]       SMALLDATETIME     NOT NULL,
    [CrudeOrigin]    SMALLINT          NULL,
    [PostedSulfur]   REAL              NULL,
    [SulfurAdjSlope] REAL              NULL
);

