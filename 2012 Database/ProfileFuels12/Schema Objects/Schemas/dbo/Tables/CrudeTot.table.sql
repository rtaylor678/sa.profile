﻿CREATE TABLE [dbo].[CrudeTot] (
    [SubmissionID] INT   NOT NULL,
    [TotBBL]       FLOAT NULL,
    [AvgGravity]   REAL  NULL,
    [AvgSulfur]    REAL  NULL,
    [AvgCost]      REAL  NULL,
    [TotMT]        FLOAT NULL,
    [AvgDensity]   REAL  NULL
);

