﻿CREATE TABLE [dbo].[CurrencyConv] (
    [CurrencyCode] CHAR (4)       NOT NULL,
    [Year]         SMALLINT       NOT NULL,
    [Month]        [dbo].[tMonth] NOT NULL,
    [ConvRate]     REAL           NOT NULL,
    [SaveDate]     SMALLDATETIME  NOT NULL
);

