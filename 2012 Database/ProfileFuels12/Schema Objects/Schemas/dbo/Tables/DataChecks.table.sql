﻿CREATE TABLE [dbo].[DataChecks] (
    [SubmissionID] INT           NOT NULL,
    [DataCheckID]  CHAR (10)     NOT NULL,
    [ItemSortKey]  INT           NOT NULL,
    [ItemDesc]     VARCHAR (200) NOT NULL,
    [Value1]       VARCHAR (50)  NULL,
    [Value2]       VARCHAR (50)  NULL,
    [Value3]       VARCHAR (50)  NULL,
    [Value4]       VARCHAR (50)  NULL,
    [Value5]       VARCHAR (50)  NULL,
    [Value6]       VARCHAR (50)  NULL,
    [Value7]       VARCHAR (50)  NULL
);

