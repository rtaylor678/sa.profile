﻿CREATE TABLE [dbo].[Divisors] (
    [SubmissionID] INT               NOT NULL,
    [FactorSet]    [dbo].[FactorSet] NOT NULL,
    [Divisor]      VARCHAR (8)       NOT NULL,
    [DivValue]     REAL              NOT NULL
);

