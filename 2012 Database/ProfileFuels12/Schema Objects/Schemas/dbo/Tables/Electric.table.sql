﻿CREATE TABLE [dbo].[Electric] (
    [SubmissionID] INT                     NOT NULL,
    [TransCode]    SMALLINT                NOT NULL,
    [EnergyType]   [dbo].[EnergyType]      NOT NULL,
    [TransType]    [dbo].[EnergyTransType] NOT NULL,
    [TransferTo]   CHAR (3)                NULL,
    [RptMWH]       REAL                    NOT NULL,
    [PriceLocal]   REAL                    NOT NULL,
    [RptGenEff]    REAL                    NULL,
    [SourceMWH]    FLOAT                   NULL,
    [UsageMWH]     FLOAT                   NULL,
    [PriceUS]      REAL                    NULL,
    [GenEff]       REAL                    NULL
);

