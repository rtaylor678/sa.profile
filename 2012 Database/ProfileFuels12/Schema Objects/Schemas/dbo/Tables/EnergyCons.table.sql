﻿CREATE TABLE [dbo].[EnergyCons] (
    [SubmissionID]   INT                     NOT NULL,
    [TransType]      [dbo].[EnergyTransType] NOT NULL,
    [EnergyType]     [dbo].[EnergyType]      NOT NULL,
    [MBTU]           FLOAT                   NULL,
    [PercentOfTotal] REAL                    NULL,
    [MBTUPerUEDC]    REAL                    NULL,
    [KBTUPerBbl]     REAL                    NULL
);

