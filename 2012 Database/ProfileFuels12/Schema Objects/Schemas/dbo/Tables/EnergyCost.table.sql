﻿CREATE TABLE [dbo].[EnergyCost] (
    [SubmissionID] INT                     NOT NULL,
    [Scenario]     [dbo].[Scenario]        NOT NULL,
    [Currency]     [dbo].[CurrencyCode]    NOT NULL,
    [TransType]    [dbo].[EnergyTransType] NOT NULL,
    [EnergyType]   [dbo].[EnergyType]      NOT NULL,
    [CostMBTU]     REAL                    NULL,
    [ProductRef]   CHAR (15)               NULL,
    [CostBbl]      REAL                    NULL,
    [CostMT]       REAL                    NULL,
    [CostK]        REAL                    NULL
);

