﻿CREATE TABLE [dbo].[EnergyTotCost] (
    [SubmissionID]   INT                  NOT NULL,
    [Currency]       [dbo].[CurrencyCode] NOT NULL,
    [Scenario]       [dbo].[Scenario]     NOT NULL,
    [PurFGCostK]     REAL                 NULL,
    [PurLiquidCostK] REAL                 NULL,
    [PurSolidCostK]  REAL                 NULL,
    [PurSteamCostK]  REAL                 NULL,
    [PurThermCostK]  REAL                 NULL,
    [PurPowerCostK]  REAL                 NULL,
    [PurTotCostK]    REAL                 NULL,
    [ProdFGCostK]    REAL                 NULL,
    [ProdOthCostK]   REAL                 NULL,
    [ProdTotCostK]   REAL                 NULL,
    [TotCostK]       REAL                 NULL
);

