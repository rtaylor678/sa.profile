﻿CREATE TABLE [dbo].[IdleUnits] (
    [RefineryID]  CHAR (6)      NOT NULL,
    [DataSet]     VARCHAR (15)  NOT NULL,
    [UnitID]      INT           NOT NULL,
    [IdledDate]   SMALLDATETIME NOT NULL,
    [RestartDate] SMALLDATETIME NULL
);

