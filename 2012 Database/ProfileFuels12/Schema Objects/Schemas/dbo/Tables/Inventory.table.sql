﻿CREATE TABLE [dbo].[Inventory] (
    [SubmissionID] INT      NOT NULL,
    [TankType]     CHAR (3) NOT NULL,
    [SortKey]      TINYINT  NOT NULL,
    [TotStorage]   FLOAT    NULL,
    [MktgStorage]  FLOAT    NULL,
    [MandStorage]  FLOAT    NULL,
    [FuelsStorage] FLOAT    NULL,
    [NumTank]      SMALLINT NULL,
    [LeasedPcnt]   REAL     NULL,
    [AvgLevel]     REAL     NULL,
    [MandLevel]    REAL     NULL,
    [Inven]        REAL     NULL
);

