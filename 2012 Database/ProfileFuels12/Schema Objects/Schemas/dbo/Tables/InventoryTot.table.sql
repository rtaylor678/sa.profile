﻿CREATE TABLE [dbo].[InventoryTot] (
    [SubmissionID]  INT      NOT NULL,
    [TotStorage]    FLOAT    NULL,
    [MktgStorage]   FLOAT    NULL,
    [MandStorage]   FLOAT    NULL,
    [FuelsStorage]  FLOAT    NULL,
    [NumTank]       SMALLINT NULL,
    [Inven]         REAL     NULL,
    [LeasedPcnt]    REAL     NULL,
    [AvgLevel]      REAL     NULL,
    [AvgVesselSize] REAL     NULL
);

