﻿CREATE TABLE [dbo].[Kerosene] (
    [SubmissionID] INT                NOT NULL,
    [BlendID]      INT                NOT NULL,
    [Grade]        [dbo].[MaterialID] NULL,
    [Type]         CHAR (5)           NULL,
    [Gravity]      REAL               NULL,
    [Density]      REAL               NULL,
    [Sulfur]       REAL               NULL,
    [SulfurSpec]   REAL               NULL,
    [FreezePT]     REAL               NULL,
    [SmokePT]      REAL               NULL,
    [Nap]          REAL               NULL,
    [KBbl]         REAL               NULL,
    [KMT]          REAL               NULL
);

