﻿ALTER TABLE [dbo].[CustomUnitReport]
    ADD CONSTRAINT [PK_CustomUnitReport] PRIMARY KEY CLUSTERED ([CompanyID] ASC, [ProcessID] ASC, [Property] ASC) WITH (FILLFACTOR = 90, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

