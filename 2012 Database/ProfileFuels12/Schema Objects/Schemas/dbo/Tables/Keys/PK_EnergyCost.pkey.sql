﻿ALTER TABLE [dbo].[EnergyCost]
    ADD CONSTRAINT [PK_EnergyCost] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [Currency] ASC, [TransType] ASC, [EnergyType] ASC) WITH (FILLFACTOR = 90, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

