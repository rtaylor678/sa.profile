﻿ALTER TABLE [dbo].[NewFactors]
    ADD CONSTRAINT [PK_NewFactors] PRIMARY KEY CLUSTERED ([FactorSet] ASC, [ProcessID] ASC, [ProcessType] ASC, [CogenES] ASC) WITH (FILLFACTOR = 90, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

