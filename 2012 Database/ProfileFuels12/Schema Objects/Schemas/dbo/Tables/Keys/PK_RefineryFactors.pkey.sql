﻿ALTER TABLE [dbo].[RefineryFactors]
    ADD CONSTRAINT [PK_RefineryFactors] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [EffDate] ASC, [EffUntil] ASC) WITH (FILLFACTOR = 90, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

