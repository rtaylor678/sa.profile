﻿CREATE TABLE [dbo].[LoadRoutHist] (
    [SubmissionID]  INT           NOT NULL,
    [PeriodStart]   SMALLDATETIME NOT NULL,
    [RoutCostLocal] REAL          NULL,
    [RoutMatlLocal] REAL          NULL
);

