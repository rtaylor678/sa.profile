﻿CREATE TABLE [dbo].[MaintCost] (
    [SubmissionID]      INT                  NOT NULL,
    [UnitID]            [dbo].[UnitID]       NOT NULL,
    [Currency]          [dbo].[CurrencyCode] NOT NULL,
    [ProcessID]         [dbo].[ProcessID]    NULL,
    [AnnTACost]         REAL                 NULL,
    [AnnRoutCost]       REAL                 NULL,
    [AnnMaintCost]      REAL                 NULL,
    [AnnTAMatl]         REAL                 NULL,
    [AnnRoutMatl]       REAL                 NULL,
    [AnnMaintMatl]      REAL                 NULL,
    [CurrTACost]        REAL                 NULL,
    [CurrRoutCost]      REAL                 NULL,
    [CurrMaintCost]     REAL                 NULL,
    [CurrTAMatl]        REAL                 NULL,
    [CurrRoutMatl]      REAL                 NULL,
    [CurrMaintMatl]     REAL                 NULL,
    [AllocAnnTACost]    REAL                 NULL,
    [AllocAnnTAMatl]    REAL                 NULL,
    [AllocCurrRoutCost] REAL                 NULL,
    [AllocCurrRoutMatl] REAL                 NULL
);

