﻿CREATE TABLE [dbo].[MaintDivisors] (
    [SubmissionID] INT              NOT NULL,
    [UnitID]       [dbo].[UnitID]   NOT NULL,
    [Scenario]     [dbo].[Scenario] NOT NULL,
    [Divisor]      CHAR (8)         NOT NULL,
    [DivValue]     REAL             NULL
);

