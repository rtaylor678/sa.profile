﻿CREATE TABLE [dbo].[MaintTACost] (
    [RefineryID]     CHAR (6)             NOT NULL,
    [DataSet]        VARCHAR (15)         NOT NULL,
    [UnitID]         [dbo].[UnitID]       NOT NULL,
    [TAID]           INT                  NOT NULL,
    [Currency]       [dbo].[CurrencyCode] NOT NULL,
    [TACost]         REAL                 NULL,
    [TAMatl]         REAL                 NULL,
    [AnnTACost]      REAL                 NULL,
    [AnnTAMatl]      REAL                 NULL,
    [TADate]         SMALLDATETIME        NULL,
    [NextTADate]     SMALLDATETIME        NULL,
    [ProcessID]      [dbo].[ProcessID]    NULL,
    [RestartDate]    SMALLDATETIME        NULL,
    [AnnTACptl]      REAL                 NULL,
    [AnnTAExp]       REAL                 NULL,
    [AnnTALaborCost] REAL                 NULL,
    [AnnTAOvhd]      REAL                 NULL,
    [TACptl]         REAL                 NULL,
    [TAExp]          REAL                 NULL,
    [TALaborCost]    REAL                 NULL,
    [TAOvhd]         REAL                 NULL
);

