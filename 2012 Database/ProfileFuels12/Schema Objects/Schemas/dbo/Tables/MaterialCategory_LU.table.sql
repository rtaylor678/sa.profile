﻿CREATE TABLE [dbo].[MaterialCategory_LU] (
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [CategoryName] VARCHAR (40)          NULL,
    [GrossGroup]   CHAR (2)              NULL,
    [SortKey]      SMALLINT              NULL,
    [NetGroup]     CHAR (2)              NULL
);

