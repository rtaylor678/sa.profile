﻿CREATE TABLE [dbo].[MaterialCostCalc] (
    [SubmissionID] INT                   NOT NULL,
    [Scenario]     [dbo].[Scenario]      NOT NULL,
    [SortKey]      INT                   NOT NULL,
    [MaterialID]   [dbo].[MaterialID]    NULL,
    [Category]     [dbo].[YieldCategory] NULL,
    [Bbl]          FLOAT                 NULL,
    [PriceLocal]   REAL                  NULL,
    [PriceUS]      REAL                  NULL
);

