﻿CREATE TABLE [dbo].[MaterialNetCalc] (
    [SubmissionID] INT                   NOT NULL,
    [Scenario]     [dbo].[Scenario]      NOT NULL,
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [MaterialID]   [dbo].[MaterialID]    NOT NULL,
    [BBL]          FLOAT                 NULL,
    [MT]           FLOAT                 NULL,
    [PricePerBbl]  REAL                  NULL,
    [PricePerMT]   REAL                  NULL
);

