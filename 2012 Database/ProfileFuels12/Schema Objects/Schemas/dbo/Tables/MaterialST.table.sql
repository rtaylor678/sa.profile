﻿CREATE TABLE [dbo].[MaterialST] (
    [SubmissionID] INT                   NOT NULL,
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [GrossBbl]     FLOAT                 NULL,
    [GrossMT]      FLOAT                 NULL,
    [NetBbl]       FLOAT                 NULL,
    [NetMT]        FLOAT                 NULL
);

