﻿CREATE TABLE [dbo].[MaterialSTCalc] (
    [SubmissionID]  INT                   NOT NULL,
    [Scenario]      [dbo].[Scenario]      NOT NULL,
    [Category]      [dbo].[YieldCategory] NOT NULL,
    [GrossBbl]      FLOAT                 NULL,
    [GrossMT]       FLOAT                 NULL,
    [GrossValueMUS] REAL                  NULL,
    [NetBbl]        FLOAT                 NULL,
    [NetMT]         FLOAT                 NULL,
    [NetValueMUS]   REAL                  NULL
);

