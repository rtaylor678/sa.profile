﻿CREATE TABLE [dbo].[MultFactors] (
    [FactorSet]   [dbo].[FactorSet] NOT NULL,
    [MultGroup]   [dbo].[ProcessID] NOT NULL,
    [NumUnits]    SMALLINT          NOT NULL,
    [MaintFactor] REAL              NULL,
    [MaintExp]    REAL              NULL,
    [PersFactor]  REAL              NULL,
    [PersExp]     REAL              NULL
);

