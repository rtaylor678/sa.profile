﻿CREATE TABLE [dbo].[MultLimits] (
    [FactorSet]       [dbo].[FactorSet] NOT NULL,
    [MultGroup]       [dbo].[ProcessID] NOT NULL,
    [MinMultiplicity] NUMERIC (4, 3)    NOT NULL,
    [MaxMultiplicity] NUMERIC (4, 3)    NOT NULL
);

