﻿CREATE TABLE [dbo].[OpexRpt] (
    [OpexID]         INT             IDENTITY (1, 1) NOT NULL,
    [SubmissionID]   INT             NOT NULL,
    [OpexCategory]   VARCHAR (50)    NOT NULL,
    [RptValue]       NUMERIC (18, 4) NULL,
    [OthDescription] NVARCHAR (400)  NULL
);

