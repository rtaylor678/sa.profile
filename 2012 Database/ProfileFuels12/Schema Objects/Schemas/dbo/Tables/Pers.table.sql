﻿CREATE TABLE [dbo].[Pers] (
    [SubmissionID] INT                   NOT NULL,
    [PersID]       [dbo].[PersID]        NOT NULL,
    [SortKey]      SMALLINT              NULL,
    [SectionID]    [dbo].[PersSectionID] NULL,
    [NumPers]      REAL                  NULL,
    [STH]          REAL                  NULL,
    [OVTHours]     REAL                  NULL,
    [OVTPcnt]      REAL                  NULL,
    [Contract]     REAL                  NULL,
    [GA]           REAL                  NULL,
    [AbsHrs]       REAL                  NULL,
    [MaintPcnt]    REAL                  NULL,
    [CompEqP]      REAL                  NULL,
    [ContEqP]      REAL                  NULL,
    [GAEqP]        REAL                  NULL,
    [TotEqP]       REAL                  NULL,
    [CompWHr]      REAL                  NULL,
    [ContWHr]      REAL                  NULL,
    [GAWHr]        REAL                  NULL,
    [TotWHr]       REAL                  NULL
);

