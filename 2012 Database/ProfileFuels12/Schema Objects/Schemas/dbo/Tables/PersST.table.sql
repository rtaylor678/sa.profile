﻿CREATE TABLE [dbo].[PersST] (
    [SubmissionID] INT                   NOT NULL,
    [SectionID]    [dbo].[PersSectionID] NOT NULL,
    [NumPers]      REAL                  NULL,
    [STH]          FLOAT                 NULL,
    [OVTHours]     FLOAT                 NULL,
    [OVTPcnt]      REAL                  NULL,
    [Contract]     FLOAT                 NULL,
    [GA]           REAL                  NULL,
    [AbsHrs]       REAL                  NULL,
    [CompEqP]      REAL                  NULL,
    [ContEqP]      REAL                  NULL,
    [GAEqP]        REAL                  NULL,
    [TotEqP]       REAL                  NULL,
    [CompWHr]      REAL                  NULL,
    [ContWHr]      REAL                  NULL,
    [GAWHr]        REAL                  NULL,
    [TotWHr]       REAL                  NULL
);

