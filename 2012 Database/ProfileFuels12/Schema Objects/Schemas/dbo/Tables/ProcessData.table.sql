﻿CREATE TABLE [dbo].[ProcessData] (
    [SubmissionID] INT            NOT NULL,
    [UnitID]       [dbo].[UnitID] NOT NULL,
    [Property]     VARCHAR (30)   NOT NULL,
    [RptValue]     REAL           NULL,
    [RptUOM]       [dbo].[UOM]    NULL,
    [SAValue]      REAL           NULL
);

