﻿CREATE TABLE [dbo].[Product_LU] (
    [ProductID]      CHAR (5)     NOT NULL,
    [Name]           VARCHAR (50) NULL,
    [TypicalDensity] REAL         NULL
);

