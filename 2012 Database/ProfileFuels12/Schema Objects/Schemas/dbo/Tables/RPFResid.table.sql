﻿CREATE TABLE [dbo].[RPFResid] (
    [SubmissionID] INT                NOT NULL,
    [EnergyType]   [dbo].[EnergyType] NOT NULL,
    [Gravity]      REAL               NULL,
    [Sulfur]       REAL               NULL,
    [ViscCS]       REAL               NULL,
    [ViscTemp]     REAL               NULL,
    [ViscCSAtTemp] REAL               NULL,
    [Density]      REAL               NULL
);

