﻿CREATE TABLE [dbo].[RefProcessGroupings] (
    [SubmissionID]    INT               NOT NULL,
    [FactorSet]       [dbo].[FactorSet] NOT NULL,
    [ProcessGrouping] [dbo].[ProcessID] NOT NULL,
    [UnitID]          [dbo].[UnitID]    NOT NULL
);

