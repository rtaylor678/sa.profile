﻿CREATE TABLE [dbo].[ReportProcs] (
    [CustomGroup]       TINYINT      NOT NULL,
    [ReportCode]        CHAR (10)    NOT NULL,
    [DataTableName]     VARCHAR (50) NOT NULL,
    [ProcName]          [sysname]    NOT NULL,
    [DataDumpTableName] VARCHAR (50) NULL,
    [DataDumpOrder]     TINYINT      NULL
);

