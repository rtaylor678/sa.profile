﻿CREATE TABLE [dbo].[Report_LU] (
    [ReportCode]  CHAR (10)    NOT NULL,
    [ReportName]  VARCHAR (50) NOT NULL,
    [SortKey]     SMALLINT     NOT NULL,
    [CustomGroup] TINYINT      NOT NULL,
    [Template]    VARCHAR (50) NOT NULL
);

