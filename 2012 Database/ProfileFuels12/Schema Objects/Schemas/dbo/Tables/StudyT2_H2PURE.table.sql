﻿CREATE TABLE [dbo].[StudyT2_H2PURE] (
    [RefineryID] CHAR (6)       NOT NULL,
    [EffDate]    SMALLDATETIME  NOT NULL,
    [EffUntil]   SMALLDATETIME  NOT NULL,
    [UnitID]     [dbo].[UnitID] NOT NULL,
    [FeedH2]     REAL           NULL,
    [H2Loss]     REAL           NULL
);

