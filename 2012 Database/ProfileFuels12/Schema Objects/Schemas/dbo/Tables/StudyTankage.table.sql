﻿CREATE TABLE [dbo].[StudyTankage] (
    [RefineryID]   CHAR (6)      NOT NULL,
    [DataSet]      VARCHAR (15)  NOT NULL,
    [EffDate]      SMALLDATETIME NOT NULL,
    [EffUntil]     SMALLDATETIME NOT NULL,
    [TankType]     CHAR (3)      NOT NULL,
    [FuelsStorage] REAL          NULL,
    [NumTank]      SMALLINT      NULL,
    [AvgLevel]     REAL          NULL
);

