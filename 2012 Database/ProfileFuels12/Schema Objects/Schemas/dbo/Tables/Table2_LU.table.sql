﻿CREATE TABLE [dbo].[Table2_LU] (
    [ProcessID]      [dbo].[ProcessID] NOT NULL,
    [Property]       VARCHAR (30)      NOT NULL,
    [USDescription]  VARCHAR (60)      NOT NULL,
    [USDecPlaces]    TINYINT           NOT NULL,
    [MetDescription] VARCHAR (60)      NOT NULL,
    [MetDecPlaces]   TINYINT           NOT NULL,
    [USUOM]          VARCHAR (20)      NULL,
    [MetricUOM]      VARCHAR (20)      NULL,
    [CustomGroup]    TINYINT           NOT NULL,
    [FormulaSymbol]  VARCHAR (1)       NULL,
    [SortKey]        INT               NULL
);

