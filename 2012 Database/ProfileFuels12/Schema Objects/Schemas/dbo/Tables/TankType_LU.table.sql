﻿CREATE TABLE [dbo].[TankType_LU] (
    [TankType]    CHAR (3)  NOT NULL,
    [Description] CHAR (40) NULL,
    [SortKey]     TINYINT   NULL
);

