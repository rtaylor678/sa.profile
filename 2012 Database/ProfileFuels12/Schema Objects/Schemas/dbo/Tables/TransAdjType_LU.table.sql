﻿CREATE TABLE [dbo].[TransAdjType_LU] (
    [AdjType]     CHAR (2)  NOT NULL,
    [Description] CHAR (25) NULL,
    [AdjMethod]   CHAR (1)  NOT NULL
);

