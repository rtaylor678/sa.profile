﻿CREATE TABLE [dbo].[UnitTargets] (
    [SubmissionID] INT                  NOT NULL,
    [UnitID]       [dbo].[UnitID]       NOT NULL,
    [MechAvail]    REAL                 NULL,
    [OpAvail]      REAL                 NULL,
    [OnStream]     REAL                 NULL,
    [UtilPcnt]     REAL                 NULL,
    [RoutCost]     REAL                 NULL,
    [TACost]       REAL                 NULL,
    [CurrencyCode] [dbo].[CurrencyCode] NULL,
    [UnitEII]      REAL                 NULL
);

