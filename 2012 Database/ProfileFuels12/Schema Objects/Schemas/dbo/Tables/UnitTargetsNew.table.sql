﻿CREATE TABLE [dbo].[UnitTargetsNew] (
    [SubmissionID] INT                  NOT NULL,
    [UnitID]       INT                  NOT NULL,
    [Property]     VARCHAR (50)         NOT NULL,
    [Target]       REAL                 NULL,
    [CurrencyCode] [dbo].[CurrencyCode] NULL
);

