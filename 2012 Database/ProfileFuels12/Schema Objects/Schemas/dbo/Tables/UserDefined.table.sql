﻿CREATE TABLE [dbo].[UserDefined] (
    [SubmissionID]    INT           NOT NULL,
    [HeaderText]      VARCHAR (50)  NOT NULL,
    [VariableDesc]    VARCHAR (100) NOT NULL,
    [RptValue]        FLOAT         NULL,
    [DecPlaces]       TINYINT       NULL,
    [RptValue_Target] FLOAT         NULL,
    [RptValue_Avg]    FLOAT         NULL,
    [RptValue_YTD]    FLOAT         NULL
);

