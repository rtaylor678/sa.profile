﻿CREATE TABLE [dbo].[Yield] (
    [SubmissionID] INT                   NOT NULL,
    [SortKey]      INT                   NOT NULL,
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [MaterialID]   CHAR (5)              NOT NULL,
    [MaterialName] NVARCHAR (50)         NOT NULL,
    [BBL]          FLOAT                 NOT NULL,
    [Gravity]      REAL                  NULL,
    [Density]      REAL                  NULL,
    [MT]           FLOAT                 NULL,
    [PriceLocal]   REAL                  NOT NULL,
    [PriceUS]      REAL                  NULL
);

