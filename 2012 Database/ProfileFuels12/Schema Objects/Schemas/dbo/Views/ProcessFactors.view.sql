﻿CREATE VIEW dbo.ProcessFactors
AS
		SELECT fpc.SubmissionID, fpc.FactorSet, EDC, UEDC, UtilPcnt, RV, RVBbl, InservicePcnt, YearsOper, UtilOSTA, UEDCOSTA, NEOpexEffDiv, MaintEffDiv, PersEffDiv, MaintPersEffDiv, NonMaintPersEffDiv
		FROM FactorProcessCalc fpc 
		WHERE fpc.ProcessID = CASE WHEN EXISTS (SELECT * FROM FactorSets WHERE IdleUnitsInProcessResults = 'N' AND FactorSet = fpc.FactorSet) THEN 'OperProc' ELSE 'TotProc' END

