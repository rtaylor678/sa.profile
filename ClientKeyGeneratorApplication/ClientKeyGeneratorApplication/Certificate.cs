﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Threading.Tasks;


namespace ClientKeyGeneratorApplication
{
    [Serializable]
	public static class Certificate
	{
        
        public class Install
        {
            public static bool AuthRoot(string path)
            {
                return Certificate.install(StoreName.AuthRoot, new X509Certificate2(path));
            }

            public static bool My(string path, string password)
            {
                return Certificate.install(StoreName.My, new X509Certificate2(path, password));
            }

            public static bool Trusted(string path)
            {
                return Certificate.install(StoreName.TrustedPeople, new X509Certificate2(path));
            }
        }

        public static class Find
        {
            public static bool My(string name, out X509Certificate2 certificate)
            {
                return Certificate.find(StoreName.My, name, out certificate);
            }

            public static bool Trusted(string name, out X509Certificate2 certificate)
            {
                return Certificate.find(StoreName.TrustedPeople, name, out certificate);
            }
        }


        
        internal static bool install(StoreName storeName, X509Certificate2 certificate)
        {
            try
            {
                    X509Store store = new X509Store(storeName, StoreLocation.LocalMachine);
               
                    store.Open(OpenFlags.ReadWrite);
                    store.Add(certificate);
                    store.Close();
              
                return true;
            }
            catch
            {
                return false;
            }
        }


        internal static bool remove(StoreName storeName, X509Certificate2 certificate)
        {
            try
            {
                X509Store store = new X509Store(storeName, StoreLocation.LocalMachine);
              
                store.Open(OpenFlags.ReadWrite);
                store.Remove(certificate);
                store.Close();
                    
                return true;
            }
            catch
            {
                return false;
            }
        }


        internal static bool find(StoreName storeName, string name, out X509Certificate2 certificate)
        {
            try
            {
                X509Store storeMy = new X509Store(storeName, StoreLocation.LocalMachine);
                
                    storeMy.Open(OpenFlags.ReadOnly);

                    X509Certificate2Collection certColl = storeMy.Certificates.Find(X509FindType.FindBySubjectName, name, true);

                    storeMy.Close();

                    if (certColl.Count == 1)
                    {
                        certificate = certColl[0];
                        certColl.Clear();
                        return true;
                    }
                    else
                    {
                        certificate = new X509Certificate2();
                        certColl.Clear();
                        return false;
                    }
                
            }
            catch
            {
                certificate = new X509Certificate2();
                return false;
            }
        }



	}
}