﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using ClientKeyGeneratorApplication.WsXmlProLite;
using System.IO;



namespace ClientKeyGeneratorApplication
{
	public partial class TestCertificateForm : Form
	{

		private string clientKey = string.Empty;
		private byte[] encodedSignedCms = new byte[0];
        private string keyFile = string.Empty;
        private string name = string.Empty;


		public TestCertificateForm()
		{
			InitializeComponent();
		}

        public TestCertificateForm(string key)
        {
            InitializeComponent();
            txtClientKey.Text = key;
        }

		private void btnCheckService_Click(object sender, EventArgs e)
		{
			using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
			{
				txtCheckService.Text = client.CheckService();
			}
		}


		private void btnGetCertificate_Click(object sender, EventArgs e)
		{

			if (txtNameAndRefNum.Text == string.Empty)
			{
				MessageBox.Show("Please enter text");
				txtNameAndRefNum.Focus();
				return;
			}

			using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
			{
				
                Form s = new ClientGenForm();

                //Set value
				string name = txtNameAndRefNum.Text + " (" + txtClientKey.Text + ")";
				
                clientKey = txtClientKey.Text.Trim();
				X509Certificate2 privateX509Cert = new X509Certificate2();
				bool success = Certificate.Find.My(name, out privateX509Cert);

				if (lblRefNumDisplay.Text.Length > 0)
				{
					if (privateX509Cert != null)
					{

						if (success == true)
						{
							lblCertResponse.Text = "Successful";
							txtNameAndRefNum.Text = name;
						}
						else
						{
							lblCertResponse.Text = "Unsuccessful";
						}
					}
					else
					{
						lblCertResponse.Text = "Unsuccessful";
					}

				}
				else
				{
					MessageBox.Show("RefNum required");
				}

				NonRepudiation nonRep = new NonRepudiation();
				string errorMsg = string.Empty;
				byte[] messageBytes = System.Text.Encoding.ASCII.GetBytes(clientKey);


				nonRep.Sign(ref errorMsg, messageBytes, privateX509Cert, out encodedSignedCms, true);
                
			}

		}

        private byte[] GetEncodedSignedCmsFromNameWithRefnum(string nameWithRefNum)
        {
            using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
            {
              
                //string name = txtGetCertResponse.Text + " (" + txtClientKey.Text + ")";
                clientKey = txtClientKey.Text.Trim();
                X509Certificate2 privateX509Cert = new X509Certificate2();
                bool success = Certificate.Find.Trusted(nameWithRefNum, out privateX509Cert);

                if (lblRefNumDisplay.Text.Length > 0)
                {
                    if (privateX509Cert != null)
                    {

                        if (success == true)
                        {
                            lblCertResponse.Text = "Successful";
                            txtNameAndRefNum.Text = nameWithRefNum;
                        }
                        else
                        {
                            lblCertResponse.Text = "Unsuccessful";
                        }
                    }
                    else
                    {
                        lblCertResponse.Text = "Unsuccessful";
                    }

                }
                else
                {
                    MessageBox.Show("RefNum required");
                }

                NonRepudiation nonRep = new NonRepudiation();
                string errorMsg = string.Empty;
                byte[] messageBytes = System.Text.Encoding.ASCII.GetBytes(clientKey);
                byte[] encodedAndSignedCms = new byte[0];
                nonRep.Sign(ref errorMsg, messageBytes, privateX509Cert, out encodedAndSignedCms, true);
                return encodedSignedCms;
            }
        }




		private void btnVerifyCertificate_Click(object sender, EventArgs e)
		{
			using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
			{
				X509Certificate2 x509 = new X509Certificate2();
				string errorResponse = string.Empty;

                //DWW TODO:
                byte[] encodedSignedCms = GetEncodedSignedCmsFromNameWithRefnum(name);

				bool success = client.AuthenticateByCertificateAndClientId(ref errorResponse, encodedSignedCms, clientKey, true);

				if (success != true)
				{
					txtNameAndRefNum.Text = errorResponse;
				}
				else
				{
                    
                    txtVerify.Clear();
					txtNameAndRefNum.Text = "Verification Successful";
                    string result = System.Text.Encoding.UTF8.GetString(encodedSignedCms);
                    txtVerify.Text = result;

				}
			}

		}


		private WsXmlProLite.ClientKey GetClientKeyInfo(string clientToken)
		{
			try
			{
				using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
				{
					WsXmlProLite.ClientKey clientKey = new ClientKey();

					clientKey = client.GetClientKeyInfoFromToken(clientToken);

					return clientKey;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private void btnGetRefNum_Click(object sender, EventArgs e)
		{

            if (txtClientKey.Text == string.Empty)
            {

                //File.OpenRead(@"C:\ClientKeys\" + 
            }
            else
            {

            }

			WsXmlProLite.ClientKey clientKey = new ClientKey();
			string clientToken = txtClientKey.Text;
			clientKey = GetClientKeyInfo(clientToken);
			lblRefNumDisplay.Text  = clientKey.RefNum;
            txtNameAndRefNum.Text = name;


		}

		private void btnTestNonRepudiationSigning_Click(object sender, EventArgs e)
		{
			const string sndMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

			byte[] sndBytes = System.Text.Encoding.ASCII.GetBytes(sndMessage);

			X509Certificate2 certSndPrivate;
			
			if (Certificate.Find.My("NonRepudiation", out certSndPrivate))
			{
				//	verify encryption was by signer
				NonRepudiation n = new NonRepudiation();
				string s = string.Empty;

				byte[] sndAuthenticated;
				if (n.Sign(ref s, sndBytes, certSndPrivate, out sndAuthenticated, false))
				{
					btnTestNonRepudiationSigning.Text = "Okay";
				}
				else
				{
					btnTestNonRepudiationSigning.Text = "Bad signing";
				}
			}
			else
			{
				btnTestNonRepudiationSigning.Text = "No certificate";
			}
		}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
	}
}