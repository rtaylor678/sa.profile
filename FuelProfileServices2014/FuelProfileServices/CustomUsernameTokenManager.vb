'================================================================================
'  File:      UsernameSigningService.asmx.vb
'
'  Summary:   This is a sample which allows the user to send a message to a Web
'             service signed with a username and password defined in a policy.
'----------------------------------------------------------------------------------
'This file is part of the Web Services Enhancements 2.0 for Microsoft .NET Samples.
'
'Copyright (C) Microsoft Corporation.  All rights reserved.
'
'This source code is intended only as a supplement to Microsoft Development Tools
'and/or on-line documentation.  See these other materials for detailed information
'regarding Microsoft code samples.
'
'This sample is designed to demonstrate WSE features and is not intended 
'for production use. Code and policy for a production application must be 
'developed to meet the specific data and security requirements of the 
'application.
'
'THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
'EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
'OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
'=================================================================================
Imports System
Imports System.IO
Imports System.Xml
Imports System.Security.Permissions
Imports Microsoft.Web.Services2.Security
Imports Microsoft.Web.Services2.Security.Tokens

'Namespace FuelProfileWebServices

' <summary>
' By implementing UsernameTokenManager we can verify the signature
' on messages received.
' </summary>
' <remarks>
' This class includes this demand to ensure that any untrusted
' assemblies cannot invoke this code. This helps mitigate
' brute-force discovery attacks.
' </remarks>
<SecurityPermissionAttribute(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
Public Class CustomUsernameTokenManager
    Inherits UsernameTokenManager

    ' <summary>
    ' Constructs an instance of this security token manager.
    ' </summary>
    Public Sub New()

    End Sub

  

    ' <summary>
    ' Returns the password or password equivalent for the username provided.
    '</summary>
    ' <param name="token">The username token</param>
    '<returns>The password (or password equivalent) for the username</returns>
    Protected Overrides Function AuthenticateToken(ByVal token As UsernameToken) As String
        ' This is a very simple manager.
        ' In most production systems the following code
        ' typically consults an external database of (username,password) pairs where
        ' the password is often not the real password but a password equivalent
        ' (for example, the hash of the password). Provided that both client and
        ' server can generate the same value for a particular username, there is
        ' no requirement that the password be the actual password for the user.
        ' For this sample the password is simply the reverse of the username.

        'Return "password"
        Dim key, company, location As String
        Dim locIndex As Integer
        Try
            company = Decrypt(token.Username).Split("$".ToCharArray)(0)

            locIndex = Decrypt(token.Username).Split("$".ToCharArray).Length - 2
            location = Decrypt(token.Username).Split("$".ToCharArray)(locIndex)

            'Try new way 
            If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                Dim reader As New StreamReader("C:\\ClientKeys\\" + company + "_" + location + ".key")
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return CalcMiniKey(key) 'ceate mini key 
                End If


            ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                Dim reader As New StreamReader("D:\\ClientKeys\\" + company + "_" + location + ".key")
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return CalcMiniKey(key) 'ceate mini key 
                End If

            End If

           
            '-------Try the old way --------
            ' Will be deleted later
            If File.Exists("C:\\ClientKeys\\" + company + ".key") Then
                Dim reader As New StreamReader("C:\\ClientKeys\\" + company + ".key")
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return CalcMiniKey(key) 'ceate mini key 
                End If
            ElseIf File.Exists("D:\\ClientKeys\\" + company + ".key") Then
                Dim reader As New StreamReader("D:\\ClientKeys\\" + company + ".key")
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return CalcMiniKey(key) 'ceate mini key 
                End If
            End If


           
            '----------------------------------------
            Return "BadPassword"
        Catch ex As Exception
            Throw ex
        End Try
    End Function 'AuthenticateToken


    Private Function CalcMiniKey(ByVal key As String) As String
        Dim lastpos As Integer = key.Length - 1
        Dim firstPart As String = key.Substring(0, Math.Round(lastpos * (3 / 8)))
        Dim secondPart As String = key.Substring(Math.Round(lastpos * 0.75), lastpos - (lastpos * 0.75) + 1)

        Return secondPart + firstPart
    End Function
End Class 'CustomUsernameTokenManager

'End Namespace

 