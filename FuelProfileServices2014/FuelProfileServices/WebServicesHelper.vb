Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Security
Imports Microsoft.Web.Services2.Security.Tokens

Module WebServicesHelper
    

    '<summary>
    '   Return refinery id from user token string
    '</summary>
    ' <param name="context">Current context</param>
    '<returns>Refinery id</returns>
    Public Function GetRefineryID(clientKey As String) As String

        If clientKey.Length > 0 Then
            Dim tokens() As String = Decrypt(clientKey).Split("$".ToCharArray)
            Return tokens(tokens.Length - 1)
        End If

        Throw New Exception("Key is not found. If you have registered with Solomon Associates, contact Solomon Associates and someone will assit you.")
    End Function


    '<summary>
    '   Return refinery id from user token string
    '</summary>
    ' <param name="context">Current context</param>
    '<returns>Refinery id</returns>
    Public Function GetCompanyID(clientKey As String) As String

        If clientKey.Length > 0 Then
            Dim tokens() As String = Decrypt(clientKey).Split("$".ToCharArray)
            Return tokens(0)
        End If

        Throw New Exception("Key is not found. If you have registered with Solomon Associates, contact Solomon Associates and someone will assit you.")
    End Function
End Module
