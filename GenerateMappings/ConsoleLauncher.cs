﻿using System;

namespace Sa
{
	public class ConsoleLauncher
	{
		static void Main()
		{
			string d = "IoFiles\\";

			string map = d + "Mappings.xml";
			string xls = d + "Profile Upload Template.xlsx";
			string o = d + "EtlMappings2.xml";

			Sa.Extensible.CreateMappings(map, xls, o);

			//Console.WriteLine("Press any key to continue...");
			//Console.ReadKey();
		}
	}
}
