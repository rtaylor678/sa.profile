﻿using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Extensible
	{
		internal partial class Load
		{
			internal static void ProcessFacilities(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  Process Facilities:   {0}", DateTime.Now.ToString());

				Excel.Worksheet wks = wkb.Worksheets["Process Facilities"];
				Excel.Range rng = null;

				int UnitId;
				string UnitName;
				string ProcessId;
				string PropertyId;
				string CellAddress;

				int rowUnitId = 9;
				int rowUnitName = 10;
				int rowProcessId = 11;

				int colProcessId = 1;
				int colPropertyID = 2;

				int rowRegNum = 138;
				int rowRegDown = 139;
				int rowMaintNum = 140;
				int rowMaintDown = 141;
				int rowOthNum = 142;
				int rowOthDown = 143;

				int rowOthDownEconomic = 149;
				int rowOthDownExternal = 150;
				int rowOthDownUnitUpsets = 151;
				int rowOthDownOffsiteUpsets = 152;
				int rowOthDownOther = 153;

				map.ProcessData.BeginLoadData();
				map.MaintRout.BeginLoadData();

				for (int c = 4; c <= 50; c++)
				{
					rng = wks.Cells[rowUnitId, c];

					if (Common.RangeHasValue(rng))
					{
						UnitId = Common.ReturnInt32(rng);

						rng = wks.Cells[rowUnitName, c];
						UnitName = Common.ReturnString(rng);

						rng = wks.Cells[rowProcessId, c];
						ProcessId = Common.ReturnString(rng);

						for (int r = 25; r <= 135; r++)
						{
							rng = wks.Cells[r, colProcessId];
							if (ProcessId == Common.ReturnString(rng))
							{
								rng = wks.Cells[r, colPropertyID];
								PropertyId = Common.ReturnString(rng);

								rng = wks.Cells[r, c];
								CellAddress = Common.ReturnAddress(rng);

								Xsd.MappingsSchema.ProcessDataRow dr = map.ProcessData.FindByUnitIDProperty(UnitId, PropertyId);

								if (dr != null)
								{
									dr.RptValue = CellAddress;
								}
								else
								{
									Xsd.MappingsSchema.ProcessDataRow nr = map.ProcessData.NewProcessDataRow();

									nr.UnitID = UnitId;
									nr.UnitName = UnitName;
									nr.ProcessID = ProcessId;
									nr.Property = PropertyId;
									nr.RptValue = CellAddress;

									map.ProcessData.AddProcessDataRow(dr);

									//  <SortKey></SortKey>
									//  <USDescription></USDescription>
									//  <MetDescription></MetDescription>
								}
							}
						}

						Xsd.MappingsSchema.MaintRoutRow mr = map.MaintRout.FindByUnitID(UnitId);

						if (mr != null)
						{
							rng = wks.Cells[rowRegNum, c];
							mr.RegNum = Common.ReturnAddress(rng);

							rng = wks.Cells[rowRegDown, c];
							mr.RegDown = Common.ReturnAddress(rng);

							rng = wks.Cells[rowMaintNum, c];
							mr.MaintNum = Common.ReturnAddress(rng);

							rng = wks.Cells[rowMaintDown, c];
							mr.MaintDown = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthNum, c];
							mr.OthNum = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDown, c];
							mr.OthDown = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownEconomic, c];
							mr.OthDownEconomic = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownExternal, c];
							mr.OthDownExternal = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownUnitUpsets, c];
							mr.OthDownUnitUpsets = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownOffsiteUpsets, c];
							mr.OthDownOffsiteUpsets = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownOther, c];
							mr.OthDownOther = Common.ReturnAddress(rng);
						}
						else
						{
							Xsd.MappingsSchema.MaintRoutRow nr = map.MaintRout.NewMaintRoutRow();

							nr.UnitID = UnitId;
							nr.ProcessID = ProcessId;
							nr.UnitName = UnitName;

							rng = wks.Cells[rowRegNum, c];
							nr.RegNum = Common.ReturnAddress(rng);

							rng = wks.Cells[rowRegDown, c];
							nr.RegDown = Common.ReturnAddress(rng);

							rng = wks.Cells[rowMaintNum, c];
							nr.MaintNum = Common.ReturnAddress(rng);

							rng = wks.Cells[rowMaintDown, c];
							nr.MaintDown = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthNum, c];
							nr.OthNum = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDown, c];
							nr.OthDown = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownEconomic, c];
							nr.OthDownEconomic = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownExternal, c];
							nr.OthDownExternal = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownUnitUpsets, c];
							nr.OthDownUnitUpsets = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownOffsiteUpsets, c];
							nr.OthDownOffsiteUpsets = Common.ReturnAddress(rng);

							rng = wks.Cells[rowOthDownOther, c];
							nr.OthDownOther = Common.ReturnAddress(rng);

							map.MaintRout.AddMaintRoutRow(nr);

							//<xs:element name="MaintRout">
							//  <xs:complexType>
							//    <xs:sequence>
							//
							//      <xs:element name="RoutCostLocal" type="xs:string" minOccurs="0" />
							//      <xs:element name="RoutMatlLocal" type="xs:string" minOccurs="0" />
							//      <xs:element name="UnpRegNum" type="xs:string" minOccurs="0" />
							//      <xs:element name="UnpRegDown" type="xs:string" minOccurs="0" />
							//      <xs:element name="UnpMaintNum" type="xs:string" minOccurs="0" />
							//      <xs:element name="UnpMaintDown" type="xs:string" minOccurs="0" />
							//      <xs:element name="UnpOthNum" type="xs:string" minOccurs="0" />
							//      <xs:element name="UnpOthDown" type="xs:string" minOccurs="0" />
							//      <xs:element name="RegSlow" type="xs:string" minOccurs="0" />
							//      <xs:element name="MaintSlow" type="xs:string" minOccurs="0" />
							//      <xs:element name="OthSlow" type="xs:string" minOccurs="0" />
							//
							//      <xs:element name="SortKey" type="xs:int" minOccurs="0" />
							//
							//    </xs:sequence>
							//  </xs:complexType>
							//</xs:element>
						}
					}
				}
				map.ProcessData.AcceptChanges();
				map.ProcessData.EndLoadData();

				map.MaintRout.AcceptChanges();
				map.MaintRout.EndLoadData();
			}
		}
	}
}