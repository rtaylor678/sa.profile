Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports C1.Web.C1WebChart
Imports C1.Web.C1WebChartBase
Imports C1.Win.C1Chart
Imports System.Drawing
Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports System.Drawing.Drawing2D

Public Class PlotData
    
    ' copy data from a data source to the chart
    ' c1c          chart
    ' series       index of the series to bind (0-based, will add if necessary)
    ' datasource   datasource object (cannot be DataTable, DataView is OK)
    ' field        name of the field that contains the y values
    ' labels       name of the field that contains the x labels
    Private Overloads Sub BindSeries(ByVal c1c As C1WebChart, ByVal groups As Integer, ByVal series As Integer, ByVal dataSource As Object, ByVal field As String, ByVal labels As String)
        ' check data source object
        Dim il As ITypedList = CType(dataSource, ITypedList)
        Dim list As IList = CType(dataSource, IList)
        Dim BarColors() As String = {"Blue", "Yellow", "Green", "Orange", "Lavender"}
     

        If list Is Nothing OrElse il Is Nothing Then
            Throw New ApplicationException("Invalid DataSource object.")
        End If
       
        ' add series if necessary
        Dim coll As ChartDataSeriesCollection = c1c.ChartGroups(groups).ChartData.SeriesList
        Dim c1 As Single
        While series >= coll.Count
            coll.AddNewSeries()
        End While

        'Set Bar colors
        If c1c.ChartGroups(groups).ChartType = Chart2DTypeEnum.Bar Then
            c1c.ChartGroups(groups).ChartData.SeriesList(series).LineStyle.Color = Color.FromName(BarColors(series))
        End If

        ' copy series data
        If list.Count = 0 Then
            Return
        End If
        Dim data As PointF() = CType(Array.CreateInstance(GetType(PointF), list.Count), PointF())
        Dim pdc As PropertyDescriptorCollection = il.GetItemProperties(Nothing)
        Dim pd As PropertyDescriptor = pdc(field)
        If pd Is Nothing Then
            Throw New ApplicationException(String.Format("Invalid field name used for Y values ({0}).", field))
        End If
        Dim i As Integer
        For i = 0 To list.Count - 1
            data(i).X = i

            Try

                data(i).Y = Single.Parse(pd.GetValue(list(i)).ToString())
            Catch
                data(i).Y = Single.NaN

            End Try
            coll(series).PointData.CopyDataIn(data)
            coll(series).Label = field
        Next i

        ' copy series labels
        If Not (labels Is Nothing) AndAlso labels.Length > 0 Then
            pd = pdc(labels)
            If pd Is Nothing Then
                Throw New ApplicationException(String.Format("Invalid field name used for X values ({0}).", labels))
            End If
            Dim ax As Axis = c1c.ChartArea.AxisX
            ax.ValueLabels.Clear()
            For i = 0 To list.Count - 1
                Dim label As String = pd.GetValue(list(i)).ToString()
                ax.ValueLabels.Add(i, label)
            Next i

            'Added to config X-Axis
            ax.AnnoMethod = AnnotationMethodEnum.ValueLabels
        End If
    End Sub 'BindSeries

    Private Overloads Sub BindSeries(ByVal c1c As C1WebChart, ByVal group As Integer, ByVal series As Integer, ByVal dataSource As Object, ByVal field As String)
        BindSeries(c1c, group, series, dataSource, field, Nothing)
    End Sub 'BindSeries

    Public Function Plot(ByVal inData As DataSet, ByVal chart As C1WebChart, ByVal chartOptions As NameValueCollection) As DataSet
        Return Plot(inData, chart, 0, chartOptions("UOM"), chartOptions("currency"), _
                    chartOptions("TwelveMonthAvg"), chartOptions("target"), chartOptions("YTD"), _
                    chartOptions("field1"), chartOptions("field2"), chartOptions("field3"), _
                    chartOptions("field4"), chartOptions("field5"))
    End Function


    Public Function Plot(ByVal inData As DataSet, ByVal chart As C1WebChart, ByVal timespan As Integer, ByVal UOM As String, ByVal currency As String, _
                            ByVal twelveMonthAvg As String, ByVal target As String, ByVal YTD As String, ByVal field1 As String, _
                            ByVal field2 As String, ByVal field3 As String, ByVal field4 As String, ByVal field5 As String) As DataSet
        Dim dsSet As New DataSet
        dsSet = inData

        'Format X Y Axis
        Dim ay As Axis = chart.ChartArea.AxisY
        Dim ay2 As Axis = chart.ChartArea.AxisY2
        Dim ax As Axis = chart.ChartArea.AxisX
        Dim fieldFormat As String = dsSet.Tables(0).Rows(0)("DecFormat")
        fieldFormat = fieldFormat.Substring(3, fieldFormat.Length - 4)

        'Select field format
        'Select Case Integer.Parse(dsSet.Tables(0).Rows(0)("DecPlaces"))
        '    Case 0
        '        fieldFormat = "#,###"
        '    Case 1
        '        fieldFormat = "#,###.0"
        '    Case 2
        '        fieldFormat = "#,###.#0"
        'End Select

        ax.Max = dsSet.Tables(0).Rows.Count
        ax.TickMinor = TickMarksEnum.None
        ax.UnitMajor = 10

        ay.AnnoFormat = FormatEnum.NumericManual
        ay.AnnoFormatString = fieldFormat

        ay2.AnnoFormat = FormatEnum.NumericManual
        ay2.AnnoFormatString = fieldFormat
        ay2.Visible = False

        If Not IsNothing(UOM) Then
            If UOM.StartsWith("US") Then

                ay.Text = dsSet.Tables(0).Rows(0)("AxisLabelUS").ToString()
            Else
                ay.Text = dsSet.Tables(0).Rows(0)("AxisLabelMetric").ToString()
            End If
            ay.Text = ay.Text.Replace("CurrencyCode", currency)
        End If

        'Plot default line group
        Dim d As Integer
        Dim valueFieldsIndex, group As Single
        Dim specCases As String

        If Not IsNothing(twelveMonthAvg) Then
            If twelveMonthAvg.Length > 0 Then
                specCases = twelveMonthAvg
                'If specCases.ToUpper.EndsWith("_AVG") Then
                BindSeries(chart, group, d, dsSet.Tables(0).DefaultView, "Rolling Average", "PeriodStartMod")

                chart.ChartGroups(group).ChartData(d).LineStyle.Color = Color.Black
                chart.ChartGroups(group).ChartData(d).LineStyle.Pattern = LinePatternEnum.None
                chart.ChartGroups(group).ChartData(d).SymbolStyle.Shape = SymbolShapeEnum.Dot
                chart.ChartGroups(group).ChartData(d).SymbolStyle.Color = Color.Black
                d += 1
                'End If
            End If
        End If

        If Not IsNothing(YTD) Then
            If YTD.Length > 0 Then
                specCases = YTD
                'If specCases.ToUpper.EndsWith("_YTD") Then
                BindSeries(chart, group, d, dsSet.Tables(0).DefaultView, "Year-To-Date", "PeriodStartMod")
                chart.ChartGroups(group).ChartData(d).LineStyle.Color = Color.Black
                chart.ChartGroups(group).ChartData(d).LineStyle.Pattern = LinePatternEnum.Solid
                chart.ChartGroups(group).ChartData(d).SymbolStyle.Shape = SymbolShapeEnum.InvertedTri
                chart.ChartGroups(group).ChartData(d).SymbolStyle.Color = Color.Black
                d += 1
                'End If
            End If
        End If

        If Not IsNothing(target) Then
            If target.Length > 0 Then
                specCases = target
                'If specCases.ToUpper.EndsWith("_TARGET") Then
                BindSeries(chart, group, d, dsSet.Tables(0).DefaultView, "Target", "PeriodStartMod")
                chart.ChartGroups(group).ChartData(d).LineStyle.Color = Color.Red
                chart.ChartGroups(group).ChartData(d).LineStyle.Pattern = LinePatternEnum.Solid
                chart.ChartGroups(group).ChartData(d).SymbolStyle.Shape = SymbolShapeEnum.Box
                chart.ChartGroups(group).ChartData(d).SymbolStyle.Color = Color.Red
                d += 1
                'End If
            End If
        End If


        'Increment group count if there items in the first series
        If chart.ChartGroups(group).ChartData.SeriesList.Count > 0 Then
            If Double.IsNaN(chart.ChartGroups.Group0.ChartData.MaxY) Or (chart.ChartGroups.Group0.ChartData.MaxY = 0) Then
                chart.ChartGroups(group).ChartData.SeriesList.RemoveAll()
            Else
                group += 1
            End If
        End If

        'If there is nothing in the first series change type to a stacked-bar
        If group = 0 Then
            chart.ChartGroups(group).ChartType = Chart2DTypeEnum.Bar
            chart.ChartGroups(group).Stacked = True
        End If

        'Plot defaut bar group
        Dim columnNames() As String = {field1, field2, field3, field4, field5}
        For valueFieldsIndex = 1 To 5
            If Not IsNothing(columnNames(valueFieldsIndex - 1)) Then
                If columnNames(valueFieldsIndex - 1).Trim.Length > 0 Then

                    Dim m As Match
                    'get the column's alias
                    m = Regex.Match(columnNames(valueFieldsIndex - 1), "'[^'\r\n]*'")

                    If m.Success Then
                        'name the series the column's alias ,eg. 'select <column> AS <alias> from <table>
                        BindSeries(chart, group, valueFieldsIndex - 1, dsSet.Tables(0).DefaultView, HttpUtility.UrlDecode(m.Value).Substring(1, m.Value.Length - 2), "PeriodStartMod")
                    Else
                        'name the series the column name 
                        BindSeries(chart, group, valueFieldsIndex - 1, dsSet.Tables(0).DefaultView, HttpUtility.UrlDecode(columnNames(valueFieldsIndex - 1)), "PeriodStartMod")
                    End If

                End If
            End If
        Next

        Dim isSingleBar As Boolean = chart.ChartGroups.Group1.ChartData.SeriesList.Count = 1
        'Configure chart to use the first Y-Axis only 
        'if there are data for group0 and group1
        If group = 1 Then
            ay2.Visible = True
            ay2.AutoMax = False
            ay.AutoMax = False
            ay.AutoMin = False
            ay2.AutoMin = False
            ay2.AutoOrigin = False

            If chart.ChartGroups.Group1.ChartData.MaxY >= chart.ChartGroups.Group0.ChartData.MaxY Then
                ay.Max = chart.ChartGroups.Group1.ChartData.MaxY + ay.UnitMajor

            Else
                ay2.Max = chart.ChartGroups.Group0.ChartData.MaxY + ay2.UnitMajor
                ay.Max = ay2.Max
            End If

            If isSingleBar Then
                If chart.ChartGroups.Group1.ChartData.MinY <= chart.ChartGroups.Group0.ChartData.MinY Then

                    ay.Min = chart.ChartGroups.Group1.ChartData.MinY - (ay.UnitMajor * 2)
                    If ay.Min < 0 Then
                        ay.Min = 0
                    End If
                Else
                    If chart.ChartGroups.Group0.ChartData.MinY <> 0 Then
                        ay2.Min = chart.ChartGroups.Group0.ChartData.MinY - (ay2.UnitMajor * 2)
                    Else
                        ay2.Min = chart.ChartGroups.Group1.ChartData.MinY - (ay.UnitMajor * 2)
                    End If

                    If ay2.Min < 0 Then
                        ay2.Min = 0
                    End If

                    ay.Min = ay2.Min
                    End If
            Else
                    ay.Min = 0
            End If

            ay2.Min = ay.Min
            ay2.Max = ay.Max

            ay2.Origin = ay.Origin
            ay2.Visible = False
        Else
            'Configure first Y-Axis , if there are only one group
            isSingleBar = chart.ChartGroups.Group0.ChartData.SeriesList.Count = 1

            If Not isSingleBar Then
                ay.AutoMin = False
                ay.Min = 0
            Else
                If (chart.ChartGroups.Group0.ChartData.MinY - (ay.UnitMajor * 2)) > 0 Then
                    ay.Min = chart.ChartGroups.Group0.ChartData.MinY - (ay.UnitMajor * 2)
                End If
                If chart.ChartGroups.Group0.ChartData.MinY < 0 Then
                    ay.AutoOrigin = False
                    ay.Origin = 0
                End If
            End If
        End If

        Return dsSet.Copy
    End Function



End Class
