﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ProfileBusinessManager.Classes
{
    [Serializable]
    public class ReferenceTable
    {
        public ReferenceTable(DataSet dataSet)
        {
        }

        private List<ReferenceTable> tableList = new List<ReferenceTable>();


        public enum SortDirection{
            Ascending = 1,
            Descending = 2,
            None = 3
        }

        public string TableName { get; set; }

        public int Sort { get; set; }

        public List<ReferenceTable> TableList
        { 
            get { return tableList; } 
            set { tableList = value; } 
        }

        public DataSet GetDataTables(DataSet dataSet, List<ReferenceTable> tableList)
        {
            DataSet retDataSet = new DataSet();




            return retDataSet;
        }


       
    }
}
