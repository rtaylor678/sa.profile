﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using WSX509 = Microsoft.Web.Services2.Security.X509;
using System.Security.Cryptography;


namespace ProfileBusinessManager
{
    public class DataHandler
    {

        public DataSet GetAllData()
        {
            DataSet lookups = new DataSet();
            Classes.AppCertificate appCertificate = new Classes.AppCertificate();

            using (LookupTablesQueries service = new LookupTablesQueries())
            {

                if (appCertificate.IsValidCertificateInstalled() == true)
                {
                    Microsoft.Web.Services2.Security.X509.X509Certificate certificate = (Microsoft.Web.Services2.Security.X509.X509Certificate)appCertificate.ValidCertificate();
                    service.ClientCertificates.Add(certificate);

                    try
                    {
                        lookups = service.GetLookups();
                    }
                    catch (Exception ex)
                    {
                        if (ex is WebException)
                        {
                            WebException we = ex as WebException;
                            WebResponse webResponse = we.Response;
                            throw new Exception("Exception calling method. " + ex.Message);
                        }
                    }
                }
                else
                {
                    throw new Exception("Valid certificate not installed");
                }
            }

            return lookups;
        }
    }
}
