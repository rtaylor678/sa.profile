<%@ Register TagPrefix="c1webchart" Namespace="C1.Web.C1WebChart" Assembly="C1.Web.C1WebChart, Version=1.0.20044.14252, Culture=neutral, PublicKeyToken=360971499c5cdc04" %>
<%@ Control Language="vb" ClassName="ChartControl" CodeBehind="ChartControl.ascx.vb" AutoEventWireup="false" Inherits="ProfileII_Corporate.ChartControl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<style type="text/css">#nav { Z-INDEX: 2 }
	#chart { }
	</style>
		<style type="text/css">.noData { FONT-WEIGHT: bold; FONT-SIZE: 18px; COLOR: #646567; FONT-FAMILY: Tahoma }
	.unitsheader { FONT-SIZE: 10pt; FONT-FAMILY: Tahoma }
	.showPrint { DISPLAY: none }
	.style4 { FONT-WEIGHT: bold; FONT-SIZE: 12pt; FONT-FAMILY: Tahoma }
	.style5 { FONT-SIZE: 10pt; FONT-FAMILY: Tahoma }
	</style>
		<LINK media="print" href="styles/print.css" type="text/css" rel="StyleSheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" color="#FFFFFF">
		<table border="0" height="100%" width="650">
			<tr>
				<td height="20%">
					<table style="Z-INDEX: 120" height="85" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td width="314" background="images\ReportBGLeft.gif" bgColor="dimgray">
								<DIV id="LabelName" align="center"><strong> <font color="white">
											<span style="DISPLAY:block;MARGIN-LEFT:5px;WIDTH:68%">
												<%= ChartName %>
											</span>
										</font></strong>
								</DIV>
								<br>
								<DIV id="Methodology" align="center"><font size="2" color="white">
										<%=  StudyYear.ToString + " Methodology" %>
									</font>
								</DIV>
							</td>
							<td align="right" width="32%" colspan="5"><!--div align="right"--><IMG height="50" alt="" src="https://webservices.solomononline.com/ProfileII/images/SA logo RECT.jpg"
									width="300"><br>
								<!--/div-->
								<!--DIV id="LabelMonth" align="right"--><font size="2">
									<%=  Date.Today.ToLongDateString()%>
								</font>
								<!--/DIV--></td>
						</tr>
					</table>
					<asp:panel id="PanelUnits" style="Z-INDEX: 116" Width="368px" CssClass="noshow" Visible="False"
						runat="server">
						<DIV class="unitsheader"><BR>
							Select
							<asp:DropDownList id="ListUnits" runat="server" Width="155px" AutoPostBack="true" Height="338px" font="Tahoma"
								ForeColor="DarkBlue" Font-Bold="True" Font-Size="7pt" Font-Names="Tahoma"></asp:DropDownList></DIV>
					</asp:panel>
					<hr width="100%" color="#000000" SIZE="1">
					<h5>
						<center><%= iif (Request.QueryString("rptOptions").ToString() <> "Current" ,Request.QueryString("rptOptions").ToString(),String.Empty) %></center>
					</h5>
				</td>
			</tr>
			<tr>
				<td align="center" height="352">
					<c1webchart:c1webchart id="C1WebChart1" Width="594px" runat="server" Height="352px" ImageTransferMethod="Cache"
						ImageAlign="Middle" SlidingExpiration="00:10:00" ImageFormat="Png">
						<Serializer Value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot;&gt;
      &lt;StyleData&gt;GradientStyle=None;Border=None,ControlText,1;BackColor2=White;BackColor=White;Opaque=True;HatchStyle=None;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot;&gt;
      &lt;StyleData&gt;Font=Tahoma, 6.75pt;AlignVert=Top;GradientStyle=None;AlignHorz=General;ForeColor=ControlText;Border=None,Black,1;HatchStyle=None;Opaque=True;BackColor2=;BackColor=Transparent;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot;&gt;
      &lt;StyleData&gt;Font=Microsoft Sans Serif, 8.25pt;GradientStyle=None;ForeColor=ControlText;Border=None,Black,1;HatchStyle=None;Opaque=True;BackColor2=;BackColor=White;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot;&gt;
      &lt;StyleData&gt;Font=Tahoma, 9pt;GradientStyle=None;ForeColor=ControlText;Border=None,DarkCyan,2;HatchStyle=None;Opaque=True;BackColor2=White;BackColor=White;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot;&gt;
      &lt;StyleData&gt;Font=Tahoma, 8.25pt;GradientStyle=None;ForeColor=ControlText;Border=None,DarkCyan,2;HatchStyle=None;Opaque=True;BackColor2=;BackColor=White;Rounding=2 2 2 2;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot;&gt;
      &lt;StyleData&gt;Font=Tahoma, 9.75pt;AlignVert=Bottom;Rotation=Rotate0;ForeColor=ControlText;Border=None,Transparent,1;AlignHorz=Center;Opaque=False;BackColor=Transparent;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot;&gt;
      &lt;StyleData&gt;Font=Tahoma, 9.75pt;AlignVert=Center;Rotation=Rotate270;ForeColor=ControlText;Border=None,Transparent,1;AlignHorz=Near;Opaque=False;BackColor=Transparent;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot;&gt;
      &lt;StyleData /&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot;&gt;
      &lt;StyleData&gt;Border=None,Black,1;Wrap=False;AlignVert=Top;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot;&gt;
      &lt;StyleData&gt;Border=None,Black,1;BackColor=Transparent;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot;&gt;
      &lt;StyleData&gt;Font=Tahoma, 14.25pt, style=Bold;GradientStyle=None;ForeColor=ControlText;Border=None,Black,1;HatchStyle=None;Opaque=True;BackColor2=;BackColor=White;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot;&gt;
      &lt;StyleData&gt;ForeColor=ControlText;Border=None,Black,1;BackColor=Control;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot;&gt;
      &lt;StyleData&gt;Font=Tahoma, 9.75pt;AlignVert=Center;Rotation=Rotate90;ForeColor=ControlText;Border=None,Transparent,1;AlignHorz=Near;Opaque=False;BackColor=Transparent;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot;&gt;
      &lt;StyleData&gt;Border=None,Black,1;AlignVert=Top;&lt;/StyleData&gt;
    &lt;/NamedStyle&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup&gt;
      &lt;DataSerializer Hole=&quot;3.4028234663852886E+38&quot; /&gt;
      &lt;Name&gt;Group1&lt;/Name&gt;
      &lt;Stacked&gt;False&lt;/Stacked&gt;
      &lt;ChartType&gt;XYPlot&lt;/ChartType&gt;
      &lt;Pie&gt;OtherOffset=0,Start=0&lt;/Pie&gt;
      &lt;Bar&gt;ClusterOverlap=0,ClusterWidth=50&lt;/Bar&gt;
      &lt;HiLoData&gt;FillFalling=True,FillTransparent=True,FullWidth=False,ShowClose=True,ShowOpen=True&lt;/HiLoData&gt;
      &lt;Bubble&gt;EncodingMethod=Diameter,MaximumSize=20,MinimumSize=5&lt;/Bubble&gt;
      &lt;Polar&gt;Degrees=True,PiRatioAnnotations=True,Start=0&lt;/Polar&gt;
      &lt;Radar&gt;Degrees=True,Filled=False,Start=0&lt;/Radar&gt;
      &lt;Use3D&gt;False&lt;/Use3D&gt;
      &lt;Visible&gt;True&lt;/Visible&gt;
      &lt;ShowOutline&gt;True&lt;/ShowOutline&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup&gt;
      &lt;DataSerializer Hole=&quot;3.4028234663852886E+38&quot; DefaultSet=&quot;True&quot; /&gt;
      &lt;Name&gt;Group2&lt;/Name&gt;
      &lt;Stacked&gt;True&lt;/Stacked&gt;
      &lt;ChartType&gt;Bar&lt;/ChartType&gt;
      &lt;Pie&gt;OtherOffset=0,Start=0&lt;/Pie&gt;
      &lt;Bar&gt;ClusterOverlap=0,ClusterWidth=50&lt;/Bar&gt;
      &lt;HiLoData&gt;FillFalling=True,FillTransparent=True,FullWidth=False,ShowClose=True,ShowOpen=True&lt;/HiLoData&gt;
      &lt;Bubble&gt;EncodingMethod=Diameter,MaximumSize=20,MinimumSize=5&lt;/Bubble&gt;
      &lt;Polar&gt;Degrees=True,PiRatioAnnotations=True,Start=0&lt;/Polar&gt;
      &lt;Radar&gt;Degrees=True,Filled=False,Start=0&lt;/Radar&gt;
      &lt;Use3D&gt;False&lt;/Use3D&gt;
      &lt;Visible&gt;True&lt;/Visible&gt;
      &lt;ShowOutline&gt;True&lt;/ShowOutline&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot;&gt;
    &lt;Text /&gt;
  &lt;/Header&gt;
  &lt;Footer Compass=&quot;South&quot;&gt;
    &lt;Text /&gt;
  &lt;/Footer&gt;
  &lt;Legend Compass=&quot;South&quot; Visible=&quot;True&quot; Orientation=&quot;Horizontal&quot;&gt;
    &lt;Text /&gt;
  &lt;/Legend&gt;
  &lt;ChartArea /&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;1&quot; Min=&quot;0&quot; AnnotationRotation=&quot;-90&quot; AnnoFormat=&quot;DateShort&quot; UnitMajor=&quot;20&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;False&quot; AutoMinor=&quot;False&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; AnnoMethod=&quot;ValueLabels&quot; _onTop=&quot;-1&quot; Compass=&quot;South&quot;&gt;
      &lt;Text /&gt;
      &lt;GridMajor Spacing=&quot;20&quot; AutoSpace=&quot;True&quot; Color=&quot;LightGray&quot; Pattern=&quot;Dash&quot; /&gt;
      &lt;GridMinor AutoSpace=&quot;True&quot; Color=&quot;LightGray&quot; Pattern=&quot;Dash&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;1&quot; Min=&quot;0&quot; UnitMajor=&quot;0.2&quot; UnitMinor=&quot;0.1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; _onTop=&quot;-1&quot; Compass=&quot;West&quot;&gt;
      &lt;Text /&gt;
      &lt;GridMajor Visible=&quot;True&quot; AutoSpace=&quot;True&quot; Color=&quot;LightGray&quot; Pattern=&quot;Dash&quot; /&gt;
      &lt;GridMinor AutoSpace=&quot;True&quot; Color=&quot;LightGray&quot; Pattern=&quot;Dash&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;21&quot; Min=&quot;19&quot; UnitMajor=&quot;0.5&quot; UnitMinor=&quot;0.25&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; _onTop=&quot;-1&quot; Compass=&quot;West&quot; Visible=&quot;False&quot;&gt;
      &lt;Text /&gt;
      &lt;GridMajor AutoSpace=&quot;True&quot; Color=&quot;LightGray&quot; Pattern=&quot;Dash&quot; /&gt;
      &lt;GridMinor AutoSpace=&quot;True&quot; Color=&quot;LightGray&quot; Pattern=&quot;Dash&quot; /&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
&lt;/Chart2DPropBag&gt;"></Serializer>
					</c1webchart:c1webchart></td>
			</tr>
			<tr>
				<td align="center" height="20%">
					<%=GetChartData()%>
				</td>
			</tr>
		</table>
	</body>
</HTML>
