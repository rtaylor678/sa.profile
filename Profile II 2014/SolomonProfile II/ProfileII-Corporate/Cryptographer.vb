Imports Microsoft.DSG.Security.CryptoServices

Module Cryptographer
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Private Const PartialKey As String = "SOALCDMSCCHOOL1996DUSTYRHODES"
    Private Const EncryptionIV As String = "SACTW0S1G2L3C4D5B6R7M8F9"

    Public Function Encrypt(ByVal aString As String, ByVal salt As String) As String
        ' Note that the key and IV must be the same for the encrypt and decrypt calls.
        Dim results As String
        Dim tdesEngine = ConfigTDES(salt)
        results = tdesEngine.Encrypt(aString)
        Return (results)
    End Function

    Public Function Decrypt(ByVal aString As String, ByVal salt As String) As String
        ' Note that the key and IV must be the same for the encrypt and decript calls.
        Dim results As String
        Dim tdesEngine = ConfigTDES(salt)
        results = tdesEngine.Decrypt(aString)
        Return (results)
    End Function

    Private Function ConfigTDES(ByVal salt As String) As TDES
        Dim key As String
        Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
        Try

            key = salt + PartialKey
            tdesEngine.StringKey = key.Substring(0, 24)
            tdesEngine.StringIV = EncryptionIV
        Catch anError As Exception
            Throw anError
        End Try
        Return tdesEngine
    End Function

End Module
