Option Strict Off
Option Explicit On 

Imports Microsoft.VisualBasic.Compatibility

<System.Runtime.InteropServices.ProgId("HTMLTORTFOCX_NET.HTMLTORTFOCX")> Public Class HTMLTORTFOCX
    Inherits System.Windows.Forms.UserControl
#Region "Windows Form Designer generated code "
    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(HTMLTORTFOCX))
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
        Me.ToolTip1.Active = True
        Me.ClientSize = New System.Drawing.Size(31, 30)
        MyBase.Location = New System.Drawing.Point(0, 0)
        MyBase.BackgroundImage = CType(resources.GetObject("HTMLTORTFOCX.BackgroundImage"), System.Drawing.Image)
        MyBase.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MyBase.Name = "HTMLTORTFOCX"
    End Sub
#End Region
    '************************************************************************
    'Author            :   Vijay Phulwadhawa     Date    : 9/8/2001 9:38:51 AM
    'Project Name      :   HTMLRTF
    'Form/Class Name   :   HTMLTORTFOCX (Code)
    'Version           :   6.00
    'Description       :   <Purpose>
    'Links             :   <Links With Any Other Form Modules>
    'Change History    :
    'Date      Author      Description Of Changes          Reason Of Change
    '************************************************************************



    Private m_HTML As String
    Private m_HTMLFile As String
    Private m_RTF As String

    Public Function HTML2RTF() As String
        If m_HTML <> "" Then
            m_RTF = HTMLtoRTF(m_HTML)
        Else
            m_RTF = ""
        End If
        HTML2RTF = m_RTF
    End Function

    Private Sub HTMLTORTFOCX_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        MyBase.Width = VB6.TwipsToPixelsX(500)
        MyBase.Height = VB6.TwipsToPixelsY(500)
    End Sub


    Public Property HTML() As String
        Get
            HTML = m_HTML
        End Get
        Set(ByVal Value As String)
            m_HTML = Value
        End Set
    End Property

    Public ReadOnly Property RTF() As String
        Get
            RTF = m_RTF
        End Get
    End Property


    Public Property HTMLFileName() As String
        Get
            HTMLFileName = m_HTMLFile
        End Get
        Set(ByVal Value As String)
            m_HTMLFile = Value
            'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
            If Dir(m_HTMLFile) <> "" Then
                m_HTML = GetFileContents(m_HTMLFile)
            End If
        End Set
    End Property
End Class