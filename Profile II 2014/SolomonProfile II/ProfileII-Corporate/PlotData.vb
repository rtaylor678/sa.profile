Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports C1.Web.C1WebChart
Imports C1.Web.C1WebChartBase
Imports C1.Win.C1Chart
Imports System.Drawing
Imports System.Text.RegularExpressions
Imports System.ComponentModel

Public Class PlotData
    ' copy data from a data source to the chart
    ' c1c          chart
    ' series       index of the series to bind (0-based, will add if necessary)
    ' datasource   datasource object (cannot be DataTable, DataView is OK)
    ' field        name of the field that contains the y values
    ' labels       name of the field that contains the x labels
    Private Overloads Sub BindSeries(ByVal c1c As C1WebChart, ByVal groups As Integer, ByVal series As Integer, ByVal dataSource As Object, ByVal field As String, ByVal labels As String, ByVal seriesName As String)
        ' check data source object
        Dim il As ITypedList = CType(dataSource, ITypedList)
        Dim list As IList = CType(dataSource, IList)
        Dim coll As ChartDataSeriesCollection = c1c.ChartGroups(groups).ChartData.SeriesList
        Dim c1 As Single
        Dim BarColors() As String = {"Blue", "Green", "Orange", "Red", "Purple", "Gray", "Brown", "Black"}

        If list Is Nothing OrElse il Is Nothing Then
            Throw New ApplicationException("Invalid DataSource object.")
        End If

        ' add series if necessary
        While series >= coll.Count
            coll.AddNewSeries()
        End While

        'Set colors
        '
        If series < BarColors.Length Then
            c1c.ChartGroups(groups).ChartData.SeriesList(series).LineStyle.Color = Color.FromName(BarColors(series))
            c1c.ChartGroups(groups).ChartData.SeriesList(series).SymbolStyle.Color = Color.FromName(BarColors(series))
        Else
            c1c.ChartGroups(groups).ChartData.SeriesList(series).SymbolStyle.Color = c1c.ChartGroups(groups).ChartData.SeriesList(series).LineStyle.Color
        End If

        'Set Series Name
        '
        If Not IsNothing(series) Then
            c1c.ChartGroups(groups).ChartData.SeriesList(series).Label = seriesName
            c1c.ChartGroups(groups).ChartData.SeriesList(series).LineStyle.Thickness = 2
        End If

        ' copy series data
        If list.Count = 0 Then
            Return
        End If
        Dim data As PointF() = CType(Array.CreateInstance(GetType(PointF), list.Count), PointF())
        Dim pdc As PropertyDescriptorCollection = il.GetItemProperties(Nothing)
        Dim pd As PropertyDescriptor = pdc(field)
        If pd Is Nothing Then
            Throw New ApplicationException(String.Format("Invalid field name used for Y values ({0}).", field))
        End If
        Dim i As Integer
        For i = 0 To list.Count - 1
            data(i).X = i
            Try
                data(i).Y = Single.Parse(pd.GetValue(list(i)).ToString())
            Catch
                data(i).Y = Single.NaN
            End Try

            coll(series).PointData.CopyDataIn(data)

            If Not IsNothing(seriesName) Then
                field = seriesName
            End If

            coll(series).Label = field
        Next i

        ' copy series labels
        If Not (labels Is Nothing) AndAlso labels.Length > 0 Then
            pd = pdc(labels)
            If pd Is Nothing Then
                Throw New ApplicationException(String.Format("Invalid field name used for X values ({0}).", labels))
            End If

            Dim ax As Axis = c1c.ChartArea.AxisX
            ax.ValueLabels.Clear()
            For i = 0 To list.Count - 1
                Dim label As String = pd.GetValue(list(i)).ToString()
                ax.ValueLabels.Add(i, label)
            Next i

            'Added to config X-Axis
            ax.AnnoMethod = AnnotationMethodEnum.ValueLabels
        End If
    End Sub 'BindSeries

    Private Overloads Sub BindSeries(ByVal c1c As C1WebChart, ByVal group As Integer, ByVal series As Integer, ByVal dataSource As Object, ByVal field As String)
        BindSeries(c1c, group, series, dataSource, field, Nothing, Nothing)
    End Sub 'BindSeries
   

    'Public Function Plot(ByVal inData As DataSet, ByVal chart As C1WebChart, ByVal chartOptions As NameValueCollection) As DataSet
    '    ' Return Plot(inData, chart, chartOptions("span"), chartOptions("UOM"), chartOptions("currency"), chartOptions("field1"))
    'End Function

    Public Function Plot(ByVal inData As DataSet, ByVal chart As C1WebChart, ByVal timespan As Integer, ByVal UOM As String, ByVal currency As String, ByVal columnName As String, ByVal YTD As String, ByVal average As String) As DataSet
        Dim dsSet As New DataSet
        dsSet = inData

        'Format X Y Axis
        Dim ay As Axis = chart.ChartArea.AxisY
        'Dim ay2 As Axis = chart.ChartArea.AxisY2
        Dim ax As Axis = chart.ChartArea.AxisX
        Dim fieldFormat As String = dsSet.Tables(0).Rows(0)("DecFormat").ToString
        fieldFormat = fieldFormat.Substring(3, fieldFormat.Length - 4)

        ax.Max = timespan 'Set date range
        ax.TickMinor = TickMarksEnum.None
        ax.UnitMajor = 10

        ay.AnnoFormat = FormatEnum.NumericManual
        ay.AnnoFormatString = fieldFormat

        'ay2.AnnoFormat = FormatEnum.NumericManual
        'ay2.AnnoFormatString = fieldFormat
        'ay2.Visible = False

        If Not IsNothing(UOM) Then
            If UOM.StartsWith("US") Then
                ay.Text = dsSet.Tables(0).Rows(0)("AxisLabelUS")
            Else
                ay.Text = dsSet.Tables(0).Rows(0)("AxisLabelMetric")
            End If
            ay.Text = ay.Text.Replace("CurrencyCode", currency)
        End If

        'Plot default line group
        Dim d As Integer
        Dim valueFieldsIndex, group As Single
        Dim specCases As String

        'Increment group count if there items in the first series
        If chart.ChartGroups(group).ChartData.SeriesList.Count > 0 Then
            If (Double.IsNaN(chart.ChartGroups.Group0.ChartData.MaxY) Or _
              (chart.ChartGroups.Group0.ChartData.MaxY = 0)) Then
                chart.ChartGroups(group).ChartData.SeriesList.RemoveAll()
            Else
                group += 1
            End If
        End If

        chart.ChartGroups(group).ChartType = Chart2DTypeEnum.XYPlot
        Dim dtLocations As DataTable = SelectDistinct(dsSet.Tables(0), "Location")

        'Plot series  

        For d = 0 To dtLocations.Rows.Count - 1
            Dim dv As New DataView
            With dv
                .Table = dsSet.Tables(0)
                .RowFilter = "Location='" + dtLocations.Rows(d)("Location") + "'"
                .Sort = "PeriodStart"
            End With

            Dim location As String = dtLocations.Rows(d)("Location")

            If Not IsNothing(average) AndAlso average.Length > 0 Then
                specCases = average
                If specCases.ToUpper.EndsWith("_AVG") Then
                    BindSeries(chart, group, d, dv, "Rolling Average", "PeriodStartMod", location)
                End If
            End If


            If Not IsNothing(YTD) AndAlso YTD.Length > 0 Then
                specCases = YTD
                If specCases.ToUpper.EndsWith("_YTD") Then
                    BindSeries(chart, group, d, dv, "Year-To-Date", "PeriodStartMod", location)
                End If
            End If


            If Not IsNothing(columnName) AndAlso columnName.Trim.Length > 0 Then
                Dim m As Match
                'get the column's alias
                m = Regex.Match(columnName, "'[^'\r\n]*'")
                If m.Success Then
                    BindSeries(chart, group, d, _
                               dv, _
                                HttpUtility.UrlDecode(m.Value).Substring(1, m.Value.Length - 2), _
                                "PeriodStartMod", _
                                location)
                Else
                    BindSeries(chart, group, d, _
                               dv, _
                               columnName, _
                               "PeriodStartMod", _
                               location)
                End If
            End If

        Next
       

        Return dsSet.Copy
    End Function


End Class
