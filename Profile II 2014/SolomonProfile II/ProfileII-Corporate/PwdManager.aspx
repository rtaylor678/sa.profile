<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PwdManager.aspx.vb" Inherits="ProfileII_Corporate.PwdManager"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Encryptor</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm" runat="server">
			<asp:Panel id="Panel1" style="Z-INDEX: 103; LEFT: 16px; POSITION: absolute; TOP: 16px" runat="server"
				Width="320px" Height="180px">
				<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="300" border="1">
					<TR>
						<TD style="WIDTH: 134px">
							<asp:label id="Label3" runat="server">Company</asp:label></TD>
						<TD>
							<asp:DropDownList id=ddlCompany runat="server" DataSource="<%# DsCompanyLU1 %>" DataTextField="CompanyName" DataValueField="CompanyName" DataMember="Company_LU">
							</asp:DropDownList></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 134px">
							<asp:label id="Label1" runat="server" Width="64px" Height="16px">Password</asp:label></TD>
						<TD>
							<asp:textbox id="txtPassword" runat="server" Width="152px"></asp:textbox></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 134px"></TD>
						<TD>
							<asp:button id="btnAddPassword" runat="server" Text="Add Password"></asp:button></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 134px"></TD>
						<TD>
							<P>
								<asp:label id="Label2" runat="server" Width="184px" Height="16px" Font-Size="8pt" Font-Names="Tahoma">Copy Text and Paste into database</asp:label></P>
						</TD>
						<TD></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 134px"></TD>
						<TD></TD>
						<TD></TD>
					</TR>
				</TABLE>
				<P>&nbsp;</P>
				<P>
					<asp:Literal id="ltStatus" runat="server"></asp:Literal></P>
			</asp:Panel>
		</form>
	</body>
</HTML>
