Imports System.IO

Class RTFElements

    Private MyFile As StreamWriter

    Public Sub New(ByVal path As String)
        MyFile = New StreamWriter(path)
        MyFile.WriteLine("{\rtf1")

        'Write the font table
        'sRTF = "{\fonttbl {\f0\froman\fcharset0 Times New Roman;}" & _
        '"{\f1\froman\fcharset0 Times New Roman;}" & _
        '"{\f2\fmodern\fcharset0 Courier New;}}"
        'MyFile.WriteLine sRTF

        'Write the title and author for the document properties
        MyFile.WriteLine("{\info{\title Results}" & _
        "{\author Solomon Associates}}")

    End Sub
    Public Sub RTF_b(ByVal Text)
        MyFile.WriteLine("\b")
        MyFile.WriteLine(Text)
        MyFile.WriteLine("\b0")
    End Sub

    'declare a line break	
    Public Sub RTF_br()
        MyFile.WriteLine("\par")
    End Sub

    'italic block
    Public Sub RTF_i(ByVal Text)
        MyFile.WriteLine("\i")
        MyFile.WriteLine(Text)
        MyFile.WriteLine("\i0")
    End Sub

    'center a paragraph
    Public Sub RTF_Center(ByVal Text)
        MyFile.WriteLine("\qc")
        MyFile.WriteLine(Text)
    End Sub

    'justify a paragraph
    Public Sub RTF_Justify(ByVal Text)
        MyFile.WriteLine("\ql")
        MyFile.WriteLine(Text)
    End Sub

    'insert image
    Public Sub RTF_Image(ByVal image)
        MyFile.WriteLine(RTFImage.GetRtfImage(image))
    End Sub


    'Public Sub RTF_BuildTable()
    '    ' populate header row
    '    MyFile.WriteLine("\trowd\trautofit1\intbl")

    '    Dim j As Integer = 1
    '    For i = 1 To NumCells
    '        MyFile.WriteLine("\cellx" & j)
    '        j = j + 1
    '    Next

    '    MyFile.WriteLine("{")

    '    For Each column In myRS.Fields
    '        MyFile.WriteLine(column.name & "\cell ")
    '        NumCells = NumCells + 1
    '    Next

    '    MyFile.WriteLine("}")

    '    MyFile.WriteLine("{")
    '    MyFile.WriteLine("\trowd\trautofit1\intbl")
    '    j = 1
    '    For i = 1 To NumCells
    '        MyFile.WriteLine("\cellx" & j)
    '        j = j + 1
    '    Next
    '    MyFile.WriteLine("\row }")

    '    ' write table contents
    '    For k = 1 To NumRows
    '        MyFile.WriteLine("\trowd\trautofit1\intbl")
    '        j = 1
    '        For i = 1 To NumCells
    '            MyFile.WriteLine("\cellx" & j)
    '            j = j + 1
    '        Next
    '        MyFile.WriteLine("{")

    '        For Each column In myRS.Fields
    '            MyFile.WriteLine(column & "\cell ")
    '        Next

    '        MyFile.WriteLine("}")

    '        MyFile.WriteLine("{")
    '        MyFile.WriteLine("\trowd\trautofit1\intbl")
    '        j = 1
    '        For i = 1 To NumCells
    '            MyFile.WriteLine("\cellx" & j)
    '            j = j + 1
    '        Next
    '        MyFile.WriteLine("\row }")
    '        myRS.MoveNext()
    '    Next
    'End Sub

    Public Sub Close()
        'close the RTF string and file
        MyFile.WriteLine("}")
        MyFile.Close()
    End Sub
End Class