﻿Imports System.IO
Imports System.Xml
Imports System.Security.Principal
Imports System.Security.Cryptography
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.Data

Public Class frmUserAdmin

    Friend users As New DataSet
    Friend usersToGroups As New DataSet
    Friend groups As New DataSet
    Friend pathAdmin As String

    Friend Function GetPath() As String
        ' Declare a variable named theFolderBrowser of type FolderBrowserDialog.
        Dim theFolderBrowser As New FolderBrowserDialog

        ' Set theFolderBrowser object's Description property to
        '   give the user instructions.
        theFolderBrowser.Description = "Please browse to your Admin directory."

        ' Set theFolderBrowser object's ShowNewFolder property to false when
        '   the a FolderBrowserDialog is to be used only for selecting an existing folder.
        theFolderBrowser.ShowNewFolderButton = False

        ' Optionally set the RootFolder and SelectedPath properties to
        '   control which folder will be selected when browsing begings
        '   and to make it the selected folder.
        ' For this example start browsing in the Desktop folder.
        theFolderBrowser.RootFolder = System.Environment.SpecialFolder.Desktop
        ' Default theFolderBrowserDialog object's SelectedPath property to the path to the Desktop folder.
        theFolderBrowser.SelectedPath = My.Computer.FileSystem.SpecialDirectories.Desktop

        ' If the user clicks theFolderBrowser's OK button..
        If theFolderBrowser.ShowDialog = Windows.Forms.DialogResult.OK Then
            ' Set the FolderChoiceTextBox's Text to theFolderBrowserDialog's
            '    SelectedPath property.
            Return theFolderBrowser.SelectedPath

        End If
    End Function
    Friend Sub LoadAdminCtl()

        
        Dim path As String

        path = GetPath()

        pathAdmin = path
        groups.Clear()

        If File.Exists(pathAdmin & "\groupsXP.xml") Then
            ReadEncrpytedXML(pathAdmin & "\groupsXP.xml", groups)
        Else
            MessageBox.Show(pathAdmin & "\GroupsXP.xml not found")
            Exit Sub
        End If
        cboRole.Enabled = True
        For Each g In groups.Tables(0).Rows
            cboRole.Items.Add(g(1))
        Next
        cboRole.SelectedIndex = 0
    End Sub

    Private Sub frmUserAdmin_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub

    Sub ReadEncrpytedXML(ByVal file As String, ByVal ds As DataSet)
        'Encrypt the data set and write it to file. 
        Dim aFileStream As FileStream = Nothing

        While (True)
            Try
                ' 20081001 RRH Path - aFileStream = New FileStream(frmMain.AppPath & "\_ADMIN\" & file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                aFileStream = New FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Exit While
            Catch ex As Exception
                'keep looping until free
                If ex.GetType() Is Type.GetType("IOException") Then
                    Throw ex
                End If
            End Try
        End While

        'Dim aStreamReader As New StreamReader(aFileStream)
        Dim aUE As New UnicodeEncoding
        Dim key() As Byte = aUE.GetBytes("password")
        Dim RMCrypto As RijndaelManaged = New RijndaelManaged
        Dim aCryptoStream As New CryptoStream(aFileStream, _
        RMCrypto.CreateDecryptor(key, key), CryptoStreamMode.Read)

        ds.ReadXml(aCryptoStream, XmlReadMode.ReadSchema)

        aCryptoStream.Close()
        aFileStream.Close()
        'aStreamReader.Close()

    End Sub

    Sub WriteEncrpytedXML(ByVal file As String, ByVal ds As DataSet)
        'Encrypt the data set and write it to file. 
        Dim aXmlTextWriter As System.Xml.XmlTextWriter
        ' 20081001 RRH Path - aXmlTextWriter = New XmlTextWriter(frmMain.AppPath & "\_ADMIN\" & file, Encoding.UTF8)
        aXmlTextWriter = New XmlTextWriter(file, Encoding.UTF8)

        Dim aUE As New UnicodeEncoding
        Dim key() As Byte = aUE.GetBytes("password")
        Dim RMCrypto As RijndaelManaged = New RijndaelManaged
        Dim aCryptoStream As New CryptoStream(aXmlTextWriter.BaseStream, _
        RMCrypto.CreateEncryptor(key, key), CryptoStreamMode.Write)

        ds.WriteXml(aCryptoStream, XmlWriteMode.WriteSchema)

        aCryptoStream.Close()

    End Sub
    

    Private Sub dgUser_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs)

        ' 20081001 RRH UI - Change password field to show "•" rather than the text.

        Dim t As TextBox = DirectCast(e.Control, System.Windows.Forms.TextBox)

        If ((t.Text <> "") And (CType(sender, DataGridView).CurrentCell.ColumnIndex = 3)) Then
            t.UseSystemPasswordChar = True
        End If


    End Sub
    
   

    
    Friend Function EncryptText(ByVal vstrTextToBeEncrypted As String, _
                                         ByVal vstrEncryptionKey As String) As String

        Dim bytValue() As Byte
        Dim bytKey() As Byte
        Dim bytEncoded() As Byte = Nothing
        Dim bytIV() As Byte = {121, 241, 10, 1, 132, 74, 11, 39, 255, 91, 45, 78, 14, 211, 22, 62}
        Dim intLength As Integer
        Dim intRemaining As Integer
        Dim objMemoryStream As New MemoryStream
        Dim objCryptoStream As CryptoStream
        Dim objRijndaelManaged As RijndaelManaged


        '   **********************************************************************
        '   ******  Strip any null character from string to be encrypted    ******
        '   **********************************************************************

        vstrTextToBeEncrypted = StripNullCharacters(vstrTextToBeEncrypted)

        '   **********************************************************************
        '   ******  Value must be within ASCII range (i.e., no DBCS chars)  ******
        '   **********************************************************************

        bytValue = Encoding.ASCII.GetBytes(vstrTextToBeEncrypted.ToCharArray)

        intLength = Len(vstrEncryptionKey)

        '   ********************************************************************
        '   ******   Encryption Key must be 256 bits long (32 bytes)      ******
        '   ******   If it is longer than 32 bytes it will be truncated.  ******
        '   ******   If it is shorter than 32 bytes it will be padded     ******
        '   ******   with upper-case Xs.                                  ****** 
        '   ********************************************************************

        If intLength >= 32 Then
            vstrEncryptionKey = Strings.Left(vstrEncryptionKey, 32)
        Else
            intLength = Len(vstrEncryptionKey)
            intRemaining = 32 - intLength
            vstrEncryptionKey = vstrEncryptionKey & Strings.StrDup(intRemaining, "X")
        End If

        bytKey = Encoding.ASCII.GetBytes(vstrEncryptionKey.ToCharArray)

        objRijndaelManaged = New RijndaelManaged

        '   ***********************************************************************
        '   ******  Create the encryptor and write value to it after it is   ******
        '   ******  converted into a byte array                              ******
        '   ***********************************************************************

        Try

            objCryptoStream = New CryptoStream(objMemoryStream, _
              objRijndaelManaged.CreateEncryptor(bytKey, bytIV), _
              CryptoStreamMode.Write)
            objCryptoStream.Write(bytValue, 0, bytValue.Length)

            objCryptoStream.FlushFinalBlock()

            bytEncoded = objMemoryStream.ToArray
            objMemoryStream.Close()
            objCryptoStream.Close()
        Catch



        End Try

        '   ***********************************************************************
        '   ******   Return encryptes value (converted from  byte Array to   ******
        '   ******   a base64 string).  Base64 is MIME encoding)             ******
        '   ***********************************************************************

        Return Convert.ToBase64String(bytEncoded)

    End Function

    Friend Function DecryptText(ByVal vstrStringToBeDecrypted As String, _
                                        ByVal vstrDecryptionKey As String) As String

        Dim bytDataToBeDecrypted() As Byte
        Dim bytTemp() As Byte
        Dim bytIV() As Byte = {121, 241, 10, 1, 132, 74, 11, 39, 255, 91, 45, 78, 14, 211, 22, 62}
        Dim objRijndaelManaged As New RijndaelManaged
        Dim objMemoryStream As MemoryStream
        Dim objCryptoStream As CryptoStream
        Dim bytDecryptionKey() As Byte

        Dim intLength As Integer
        Dim intRemaining As Integer
        Dim strReturnString As String = String.Empty

        '   *****************************************************************
        '   ******   Convert base64 encrypted value to byte array      ******
        '   *****************************************************************

        bytDataToBeDecrypted = Convert.FromBase64String(vstrStringToBeDecrypted)

        '   ********************************************************************
        '   ******   Encryption Key must be 256 bits long (32 bytes)      ******
        '   ******   If it is longer than 32 bytes it will be truncated.  ******
        '   ******   If it is shorter than 32 bytes it will be padded     ******
        '   ******   with upper-case Xs.                                  ****** 
        '   ********************************************************************

        intLength = Len(vstrDecryptionKey)

        If intLength >= 32 Then
            vstrDecryptionKey = Strings.Left(vstrDecryptionKey, 32)
        Else
            intLength = Len(vstrDecryptionKey)
            intRemaining = 32 - intLength
            vstrDecryptionKey = vstrDecryptionKey & Strings.StrDup(intRemaining, "X")
        End If

        bytDecryptionKey = Encoding.ASCII.GetBytes(vstrDecryptionKey.ToCharArray)

        ReDim bytTemp(bytDataToBeDecrypted.Length)

        objMemoryStream = New MemoryStream(bytDataToBeDecrypted)

        '   ***********************************************************************
        '   ******  Create the decryptor and write value to it after it is   ******
        '   ******  converted into a byte array                              ******
        '   ***********************************************************************

        Try

            objCryptoStream = New CryptoStream(objMemoryStream, _
               objRijndaelManaged.CreateDecryptor(bytDecryptionKey, bytIV), _
               CryptoStreamMode.Read)

            objCryptoStream.Read(bytTemp, 0, bytTemp.Length)

            objCryptoStream.FlushFinalBlock()
            objMemoryStream.Close()
            objCryptoStream.Close()

        Catch

        End Try

        '   *****************************************
        '   ******   Return decypted value     ******
        '   *****************************************

        Return StripNullCharacters(Encoding.ASCII.GetString(bytTemp))

    End Function

    Friend Function StripNullCharacters(ByVal s As String) As String

        If s Is Nothing Then
            Return False
        End If
        s = s.Trim(New String(vbNullChar, 1))
        Return String.IsNullOrWhiteSpace(s)

    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        End
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        'USERS
        If users.HasChanges Then
            WriteEncrpytedXML("usersXP.xml", users)
        End If
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        LoadAdminCtl()
    End Sub

    Private Sub cboRole_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRole.SelectedIndexChanged
        Dim GroupID As String
        Dim AdminUser(20) As String
        Dim AdminPassword(20) As String

        ListView1.Items.Clear()
        usersToGroups.Clear()
        users.Clear()

        If File.Exists(pathAdmin & "\usersToGroupsXP.xml") Then
            ReadEncrpytedXML(pathAdmin & "\usersToGroupsXP.xml", usersToGroups)
        Else
            MessageBox.Show(pathAdmin & "\usersToGroupsXP.xml not found")
            Exit Sub
        End If

        If File.Exists(pathAdmin & "\usersXP.xml") Then
            ReadEncrpytedXML(pathAdmin & "\usersXP.xml", users)
        Else
            MessageBox.Show(pathAdmin & "\usersXP.xml not found")
            Exit Sub
        End If

        For Each r As DataRow In groups.Tables(0).Rows
            If r("groupName").ToString.ToUpper = cboRole.Text.ToUpper Then
                GroupID = r("GroupId").ToString
            End If

        Next
        Dim count As Integer = 0
        For Each r As DataRow In usersToGroups.Tables(0).Rows

            If r("groupid").ToString.ToUpper = GroupID Then
                AdminUser(count) = r("LoginId").ToString
                count = count + 1
            End If
        Next

        For j = 0 To count - 1
            For Each r As DataRow In users.Tables(0).Rows

                If r("Loginid").ToString.ToUpper = AdminUser(j).ToString.ToUpper Then
                    AdminPassword(j) = r("password").ToString

                End If
            Next
        Next
        ListView1.View = View.Details
        ListView1.Columns.Add("Login", 200)
        ListView1.Columns.Add("Password", 200)

        Dim d(1) As String
        Dim tn As ListViewItem
        For i = 0 To count - 1
            d(0) = AdminUser(i)
            d(1) = AdminPassword(i)
            tn = New ListViewItem(d)
            ListView1.Items.Add(tn)
        Next i

    End Sub
End Class
