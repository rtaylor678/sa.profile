Option Explicit On

Imports System.IO

Friend Class Key

    Friend Shared Function GetRefineryID() As String

        ' 20081001 RRH Path - If Not File.Exists(frmMain.AppPath & "\_CERT\policy.ct") Then
        If Not File.Exists(pathCert & Policy.GetFileName()) Then
            Throw New Exception("Key was not found. If you have a valid key, restart your application. Otherwise, please register your application " + _
                                "before you submit or download data.")
        End If

        ' 20081001 RRH Path - Dim reader As New StreamReader(frmMain.AppPath & "\_CERT\policy.ct")
        Dim reader As New StreamReader(pathCert & Policy.GetFileName())
        Dim id As String
        Dim delim As String = "$"
        Dim key As String
        If reader.Peek <> -1 Then
            key = Decrypt(reader.ReadLine)
            Dim tokens() As String = key.Split(delim.ToCharArray)
            id = tokens(tokens.Length - 1)
            Return id
        Else
            Throw New Exception("A key was not found")
        End If

    End Function


    Friend Shared Function GetCompanyID() As String

        Dim reader As New StreamReader(pathCert & Policy.GetFileName())
        Dim id As String
        Dim delim As String = "$"
        Dim key As String
        If reader.Peek <> -1 Then
            key = Decrypt(reader.ReadLine)
            Dim tokens() As String = key.Split(delim.ToCharArray)
            Return tokens(0)
        End If

        Throw New Exception("Key is not found. If you have registered with Solomon Associates, contact Solomon Associates and someone will assit you.")
    End Function
End Class
