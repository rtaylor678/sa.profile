Option Compare Binary
Option Explicit On
Option Strict On

Friend Module modUploadNotify

    Friend Function uploadNotify(ByVal lbl0 As Label, _
                                 ByVal lbl1 As Label, _
                                 ByVal upload As Boolean, _
                                 ByVal ds As Data.DataSet, _
                                 ByVal strPath As String) As Boolean

        ds.Tables(0).Rows(0)!Uploaded = upload
        ds.AcceptChanges()

        ds.WriteXml(strPath & "Settings.xml", XmlWriteMode.WriteSchema)

        If Not upload Then
            lbl0.ForeColor = Color.Red
            lbl0.Text = "Local data has changed." & Microsoft.VisualBasic.ControlChars.CrLf & "Upload revised data for consistent results."
            lbl0.Height = 45
            lbl1.ForeColor = Color.Red
            lbl1.Text = "Local data has changed." & Microsoft.VisualBasic.ControlChars.CrLf & "Upload revised data for consistent results."
            lbl1.Height = 45
        Else
            lbl0.Text = ""
            lbl0.Height = 10
            lbl1.Text = ""
            lbl1.Height = 10
        End If

    End Function

    Friend Function filesNotModified(ByVal strPath As String) As Boolean

        filesNotModified = True

        Dim dt As Date = IO.File.GetLastWriteTime(strPath & "Settings.xls")

        For Each f As String In IO.Directory.GetFiles(strPath, "*.xls")
            If IO.File.GetCreationTime(f) = IO.File.GetLastWriteTime(f) AndAlso IO.File.GetCreationTime(f) = dt Then
                dt = IO.File.GetLastWriteTime(f)
            Else
                filesNotModified = False
                Exit For
            End If

        Next f

    End Function

End Module



