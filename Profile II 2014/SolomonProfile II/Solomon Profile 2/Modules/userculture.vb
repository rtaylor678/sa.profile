﻿Imports System.Data
Imports ProfileEnvironment


Public Class UserCulture

    Public Function GetCurrentUserCulture(ByVal userId As String) As String

        Dim dsUsersCulture As New DataSet
        Dim retVal As String = String.Empty

        Dim xmlFile As String = (Application.StartupPath + ("\_ENVIRONMENT\EnvironmentCulture.xml"))
        dsUsersCulture.ReadXml(xmlFile)
        Dim row As DataRow

        For Each row In dsUsersCulture.Tables(0).Rows
            Dim user As String = row("UserId").ToString

            If user = userId Then
                retVal = row("CultureName").ToString
                Exit For
            End If
        Next


        Return retVal

    End Function

    'Private Function CreateUserCultureTable() As DataTable()

    '    Dim dataTableFiles As New DataTable()

    '    Dim dcDirFile As DataColumn = New DataColumn("DirFile")
    '    dataTableFiles.Columns.Add(dcDirFile)
    '    Dim dcType As DataColumn = New DataColumn("DirFileType")
    '    dataTableFiles.Columns.Add(dcType)
    '    Dim dcDirFileExclude As DataColumn = New DataColumn("DirFileExclude")
    '    dataTableFiles.Columns.Add(dcDirFileExclude)

    '    Return dataTableFiles

    'End Function
End Class
