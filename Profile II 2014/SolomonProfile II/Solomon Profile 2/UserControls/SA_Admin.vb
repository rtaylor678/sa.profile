Option Explicit On

Imports System.IO
Imports System.Collections.Specialized
Imports System.Xml

'Imports System                         ' 20081013 RRH - Commented un-necessary Imports
'Imports System.Windows.Forms           ' 20081013 RRH - Commented un-necessary Imports
'Imports System.Drawing                 ' 20081013 RRH - Commented un-necessary Imports
'Imports System.Xml                     ' 20081013 RRH - Commented un-necessary Imports
'Imports System.Data                    ' 20081013 RRH - Commented un-necessary Imports
'Imports System.Data.OleDb              ' 20081013 RRH - Commented un-necessary Imports
'Imports System.Data.Odbc               ' 20081013 RRH - Commented un-necessary Imports
'Imports System.Security.Principal      ' 20081013 RRH - Commented un-necessary Imports

'Imports System.Text                    
'Imports Microsoft.ApplicationBlocks.ExceptionManagement
'Imports SetupSettings.Settings

Friend Class SA_Admin

    Inherits System.Windows.Forms.UserControl
    Dim GroupDict As New HybridDictionary
    Dim UserDict As New HybridDictionary
    Dim LoginDict As New HybridDictionary
    Dim AdminNotSaved As Boolean
    Dim myparent As Main
    ' 20081013 RRH - Commented to remove - Public AppName As String = "PROFILE_II"

    ' 20081013 RRH - Commented to remove - Public DsGroups2 As DataSet
    ' 20081013 RRH - Commented to remove - Public DsUsers1 As DataSet
    ' 20081013 RRH - Commented to remove - Dim connect As String = "DataEntry"
    ' 20081013 RRH - Commented to remove - Dim cmGroups As CurrencyManager

    ' 20081013 RRH - Commented to remove - Dim role As String = "SAI DEL"

    Dim groups As dsGroups = New dsGroups
    Dim tablePerms As DsTablePerm = New DsTablePerm
    Dim users As DsUsers = New DsUsers
    Dim usersToGroups As DsUserToGroup = New DsUserToGroup
    Dim wasMe As Boolean = False
    Dim entered As Boolean = False
    Dim looped As Integer = 0

    Friend WithEvents dgUser As System.Windows.Forms.DataGridView
    Friend WithEvents dgGroup As System.Windows.Forms.DataGridView
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents tcAdmin As System.Windows.Forms.TabControl
    Friend WithEvents pageUsers As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pageGroups As System.Windows.Forms.TabPage
    Private WithEvents pagePrivileges As System.Windows.Forms.TabPage
    Friend WithEvents pageAssign As System.Windows.Forms.TabPage
    Friend WithEvents pageInstruct As System.Windows.Forms.TabPage
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel17 As System.Windows.Forms.Panel
    Friend WithEvents Panel18 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Panel19 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents chkAssign As System.Windows.Forms.CheckBox
    Friend WithEvents chkPrivileges As System.Windows.Forms.CheckBox
    Friend WithEvents groupsGroupID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents groupsGroupName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents userLoginID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents userLast_Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents userFirst_Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents userPassword As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents userEmail_Address As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents userPhone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents userLocationDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents pageLogs As System.Windows.Forms.TabPage
    Friend WithEvents dgvLogEvents As System.Windows.Forms.DataGridView
    Friend WithEvents EventDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EventDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EventUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnPurge As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents PurgeDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents groupsYearLimit As System.Windows.Forms.DataGridViewTextBoxColumn


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

#Region "The following procedure is required by the Windows Form Designer"

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lbGroups As System.Windows.Forms.ListBox
    Friend WithEvents clbUsers As System.Windows.Forms.CheckedListBox
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents clbTables As System.Windows.Forms.CheckedListBox
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents FileSystemWatcher1 As System.IO.FileSystemWatcher
    'Friend WithEvents C1DockingTab1 As C1.Win.C1Command.C1DockingTab
    'Friend WithEvents tpUsers As C1.Win.C1Command.C1DockingTabPage
    'Friend WithEvents tpGroups As C1.Win.C1Command.C1DockingTabPage
    'Friend WithEvents tpPrivToGrp As C1.Win.C1Command.C1DockingTabPage
    'Friend WithEvents tpUserToGrp As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents lbGroups2 As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_Admin))
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.lbGroups = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.clbTables = New System.Windows.Forms.CheckedListBox()
        Me.clbUsers = New System.Windows.Forms.CheckedListBox()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.lbGroups2 = New System.Windows.Forms.ListBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.FileSystemWatcher1 = New System.IO.FileSystemWatcher()
        Me.dgUser = New System.Windows.Forms.DataGridView()
        Me.userLoginID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.userLast_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.userFirst_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.userPassword = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.userEmail_Address = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.userPhone = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.userLocationDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgGroup = New System.Windows.Forms.DataGridView()
        Me.groupsGroupID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.groupsGroupName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.groupsYearLimit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlTools = New System.Windows.Forms.Panel()
        Me.tcAdmin = New System.Windows.Forms.TabControl()
        Me.pageInstruct = New System.Windows.Forms.TabPage()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.pageUsers = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pageGroups = New System.Windows.Forms.TabPage()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.pagePrivileges = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkPrivileges = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.pageAssign = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.chkAssign = New System.Windows.Forms.CheckBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.pageLogs = New System.Windows.Forms.TabPage()
        Me.btnPurge = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.PurgeDate = New System.Windows.Forms.DateTimePicker()
        Me.dgvLogEvents = New System.Windows.Forms.DataGridView()
        Me.EventDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EventDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EventUser = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.Panel9.SuspendLayout()
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcAdmin.SuspendLayout()
        Me.pageInstruct.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel18.SuspendLayout()
        Me.pageUsers.SuspendLayout()
        Me.pageGroups.SuspendLayout()
        Me.pagePrivileges.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pageAssign.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pageLogs.SuspendLayout()
        CType(Me.dgvLogEvents, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.SystemColors.Window
        Me.Panel9.Controls.Add(Me.lbGroups)
        Me.Panel9.Controls.Add(Me.Label1)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel9.Location = New System.Drawing.Point(6, 36)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Padding = New System.Windows.Forms.Padding(12, 0, 12, 12)
        Me.Panel9.Size = New System.Drawing.Size(160, 482)
        Me.Panel9.TabIndex = 8
        '
        'lbGroups
        '
        Me.lbGroups.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbGroups.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbGroups.ForeColor = System.Drawing.Color.MediumBlue
        Me.HelpProvider1.SetHelpKeyword(Me.lbGroups, "groups")
        Me.HelpProvider1.SetHelpNavigator(Me.lbGroups, System.Windows.Forms.HelpNavigator.KeywordIndex)
        Me.lbGroups.Location = New System.Drawing.Point(12, 24)
        Me.lbGroups.Name = "lbGroups"
        Me.HelpProvider1.SetShowHelp(Me.lbGroups, True)
        Me.lbGroups.Size = New System.Drawing.Size(136, 446)
        Me.lbGroups.Sorted = True
        Me.lbGroups.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(12, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 24)
        Me.Label1.TabIndex = 968
        Me.Label1.Text = "Groups"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'clbTables
        '
        Me.clbTables.CheckOnClick = True
        Me.clbTables.Dock = System.Windows.Forms.DockStyle.Top
        Me.clbTables.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clbTables.ForeColor = System.Drawing.Color.MediumBlue
        Me.HelpProvider1.SetHelpKeyword(Me.clbTables, "units")
        Me.HelpProvider1.SetHelpNavigator(Me.clbTables, System.Windows.Forms.HelpNavigator.KeywordIndex)
        Me.clbTables.Items.AddRange(New Object() {strRightsAdmin, strRightsRefinery, strRightsConfiguration, strRightsProcess, strRightsInventory, strRightsOpEx, strRightsPersonnel, strRightsCrude, strRightsMaterial, strRightsEnergy, strRightsUser, strRightsView})
        Me.clbTables.Location = New System.Drawing.Point(12, 24)
        Me.clbTables.Name = "clbTables"
        Me.HelpProvider1.SetShowHelp(Me.clbTables, True)
        Me.clbTables.Size = New System.Drawing.Size(538, 436)
        Me.clbTables.TabIndex = 2
        '
        'clbUsers
        '
        Me.clbUsers.CheckOnClick = True
        Me.clbUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clbUsers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clbUsers.ForeColor = System.Drawing.Color.MediumBlue
        Me.HelpProvider1.SetHelpKeyword(Me.clbUsers, "users")
        Me.HelpProvider1.SetHelpNavigator(Me.clbUsers, System.Windows.Forms.HelpNavigator.KeywordIndex)
        Me.clbUsers.Location = New System.Drawing.Point(12, 24)
        Me.clbUsers.Name = "clbUsers"
        Me.HelpProvider1.SetShowHelp(Me.clbUsers, True)
        Me.clbUsers.Size = New System.Drawing.Size(538, 446)
        Me.clbUsers.Sorted = True
        Me.clbUsers.TabIndex = 9
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "F:\GADSNG Data Entry\AdminConsole\AdminConsole.chm"
        '
        'lbGroups2
        '
        Me.lbGroups2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbGroups2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbGroups2.ForeColor = System.Drawing.Color.MediumBlue
        Me.HelpProvider1.SetHelpKeyword(Me.lbGroups2, "groups")
        Me.HelpProvider1.SetHelpNavigator(Me.lbGroups2, System.Windows.Forms.HelpNavigator.KeywordIndex)
        Me.lbGroups2.Location = New System.Drawing.Point(12, 24)
        Me.lbGroups2.Name = "lbGroups2"
        Me.HelpProvider1.SetShowHelp(Me.lbGroups2, True)
        Me.lbGroups2.Size = New System.Drawing.Size(136, 446)
        Me.lbGroups2.Sorted = True
        Me.lbGroups2.TabIndex = 6
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(580, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'FileSystemWatcher1
        '
        Me.FileSystemWatcher1.EnableRaisingEvents = True
        Me.FileSystemWatcher1.Filter = "*.xml*"
        Me.FileSystemWatcher1.NotifyFilter = System.IO.NotifyFilters.LastWrite
        Me.FileSystemWatcher1.SynchronizingObject = Me
        '
        'dgUser
        '
        DataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control
        Me.dgUser.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle29
        Me.dgUser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgUser.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle30
        Me.dgUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgUser.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.userLoginID, Me.userLast_Name, Me.userFirst_Name, Me.userPassword, Me.userEmail_Address, Me.userPhone, Me.userLocationDescription})
        Me.dgUser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgUser.Location = New System.Drawing.Point(6, 36)
        Me.dgUser.MultiSelect = False
        Me.dgUser.Name = "dgUser"
        Me.dgUser.RowHeadersWidth = 25
        Me.dgUser.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.Color.MediumBlue
        Me.dgUser.RowsDefaultCellStyle = DataGridViewCellStyle32
        Me.dgUser.Size = New System.Drawing.Size(722, 482)
        Me.dgUser.TabIndex = 948
        '
        'userLoginID
        '
        Me.userLoginID.DataPropertyName = "LoginID"
        Me.userLoginID.FillWeight = 75.0!
        Me.userLoginID.Frozen = True
        Me.userLoginID.HeaderText = "Username"
        Me.userLoginID.Name = "userLoginID"
        Me.userLoginID.Width = 75
        '
        'userLast_Name
        '
        Me.userLast_Name.DataPropertyName = "Last_Name"
        Me.userLast_Name.HeaderText = "Last Name"
        Me.userLast_Name.Name = "userLast_Name"
        '
        'userFirst_Name
        '
        Me.userFirst_Name.DataPropertyName = "First_Name"
        Me.userFirst_Name.HeaderText = "First Name"
        Me.userFirst_Name.Name = "userFirst_Name"
        '
        'userPassword
        '
        Me.userPassword.DataPropertyName = "Password"
        DataGridViewCellStyle31.Format = "*"
        Me.userPassword.DefaultCellStyle = DataGridViewCellStyle31
        Me.userPassword.HeaderText = "Password"
        Me.userPassword.Name = "userPassword"
        '
        'userEmail_Address
        '
        Me.userEmail_Address.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.userEmail_Address.DataPropertyName = "Email_Address"
        Me.userEmail_Address.FillWeight = 150.0!
        Me.userEmail_Address.HeaderText = "Email Address"
        Me.userEmail_Address.Name = "userEmail_Address"
        '
        'userPhone
        '
        Me.userPhone.DataPropertyName = "Phone"
        Me.userPhone.FillWeight = 150.0!
        Me.userPhone.HeaderText = "Phone Number"
        Me.userPhone.Name = "userPhone"
        Me.userPhone.Visible = False
        Me.userPhone.Width = 150
        '
        'userLocationDescription
        '
        Me.userLocationDescription.DataPropertyName = "LocationDescription"
        Me.userLocationDescription.HeaderText = "userLocationDescription"
        Me.userLocationDescription.Name = "userLocationDescription"
        Me.userLocationDescription.ReadOnly = True
        Me.userLocationDescription.Visible = False
        '
        'dgGroup
        '
        DataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control
        Me.dgGroup.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle33
        Me.dgGroup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle34.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgGroup.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle34
        Me.dgGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgGroup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.groupsGroupID, Me.groupsGroupName, Me.groupsYearLimit})
        Me.dgGroup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgGroup.Location = New System.Drawing.Point(6, 36)
        Me.dgGroup.MultiSelect = False
        Me.dgGroup.Name = "dgGroup"
        Me.dgGroup.RowHeadersWidth = 25
        Me.dgGroup.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle36.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle36.ForeColor = System.Drawing.Color.MediumBlue
        Me.dgGroup.RowsDefaultCellStyle = DataGridViewCellStyle36
        Me.dgGroup.Size = New System.Drawing.Size(722, 482)
        Me.dgGroup.TabIndex = 949
        '
        'groupsGroupID
        '
        Me.groupsGroupID.DataPropertyName = "GroupID"
        Me.groupsGroupID.FillWeight = 75.0!
        Me.groupsGroupID.HeaderText = "Group ID"
        Me.groupsGroupID.Name = "groupsGroupID"
        Me.groupsGroupID.ReadOnly = True
        Me.groupsGroupID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.groupsGroupID.Visible = False
        Me.groupsGroupID.Width = 75
        '
        'groupsGroupName
        '
        Me.groupsGroupName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.groupsGroupName.DataPropertyName = "GroupName"
        Me.groupsGroupName.FillWeight = 150.0!
        Me.groupsGroupName.HeaderText = "Group Name"
        Me.groupsGroupName.Name = "groupsGroupName"
        Me.groupsGroupName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'groupsYearLimit
        '
        Me.groupsYearLimit.DataPropertyName = "YearLimit"
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.groupsYearLimit.DefaultCellStyle = DataGridViewCellStyle35
        Me.groupsYearLimit.FillWeight = 75.0!
        Me.groupsYearLimit.HeaderText = "Year Limit"
        Me.groupsYearLimit.Name = "groupsYearLimit"
        Me.groupsYearLimit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.groupsYearLimit.ToolTipText = "Data prior to this year will be permanently closed for group"
        Me.groupsYearLimit.Width = 75
        '
        'pnlTools
        '
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 6)
        Me.pnlTools.TabIndex = 3
        '
        'tcAdmin
        '
        Me.tcAdmin.Controls.Add(Me.pageInstruct)
        Me.tcAdmin.Controls.Add(Me.pageUsers)
        Me.tcAdmin.Controls.Add(Me.pageGroups)
        Me.tcAdmin.Controls.Add(Me.pagePrivileges)
        Me.tcAdmin.Controls.Add(Me.pageAssign)
        Me.tcAdmin.Controls.Add(Me.pageLogs)
        Me.tcAdmin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcAdmin.ItemSize = New System.Drawing.Size(125, 18)
        Me.tcAdmin.Location = New System.Drawing.Point(4, 46)
        Me.tcAdmin.Name = "tcAdmin"
        Me.tcAdmin.SelectedIndex = 0
        Me.tcAdmin.Size = New System.Drawing.Size(742, 550)
        Me.tcAdmin.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcAdmin.TabIndex = 4
        '
        'pageInstruct
        '
        Me.pageInstruct.Controls.Add(Me.Panel17)
        Me.pageInstruct.Controls.Add(Me.Label4)
        Me.pageInstruct.Location = New System.Drawing.Point(4, 22)
        Me.pageInstruct.Name = "pageInstruct"
        Me.pageInstruct.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageInstruct.Size = New System.Drawing.Size(734, 524)
        Me.pageInstruct.TabIndex = 4
        Me.pageInstruct.Text = "Instructions"
        Me.pageInstruct.UseVisualStyleBackColor = True
        '
        'Panel17
        '
        Me.Panel17.Controls.Add(Me.Panel19)
        Me.Panel17.Controls.Add(Me.Label5)
        Me.Panel17.Controls.Add(Me.Panel18)
        Me.Panel17.Controls.Add(Me.Label9)
        Me.Panel17.Controls.Add(Me.Label59)
        Me.Panel17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel17.Location = New System.Drawing.Point(6, 36)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Padding = New System.Windows.Forms.Padding(12, 0, 0, 0)
        Me.Panel17.Size = New System.Drawing.Size(722, 482)
        Me.Panel17.TabIndex = 967
        '
        'Panel19
        '
        Me.Panel19.Controls.Add(Me.Label6)
        Me.Panel19.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel19.Location = New System.Drawing.Point(12, 262)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Padding = New System.Windows.Forms.Padding(12, 0, 0, 0)
        Me.Panel19.Size = New System.Drawing.Size(710, 46)
        Me.Panel19.TabIndex = 970
        '
        'Label6
        '
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(12, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(698, 58)
        Me.Label6.TabIndex = 958
        Me.Label6.Text = "- Select a USER or GROUP in the left panel, check the applicable associations in " & _
    "the right column" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "- Use the CHECK/UNCHECK ALL check box when needed"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(12, 226)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(710, 36)
        Me.Label5.TabIndex = 969
        Me.Label5.Text = "Additional PRIVILEGES and GROUP ASSIGNMENTS Table Commands"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel18
        '
        Me.Panel18.Controls.Add(Me.Label13)
        Me.Panel18.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel18.Location = New System.Drawing.Point(12, 180)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Padding = New System.Windows.Forms.Padding(12, 0, 0, 0)
        Me.Panel18.Size = New System.Drawing.Size(710, 46)
        Me.Panel18.TabIndex = 968
        '
        'Label13
        '
        Me.Label13.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(12, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(698, 58)
        Me.Label13.TabIndex = 958
        Me.Label13.Text = resources.GetString("Label13.Text")
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(12, 144)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(710, 36)
        Me.Label9.TabIndex = 967
        Me.Label9.Text = "Additional USERS and GROUPS Table Commands"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label59
        '
        Me.Label59.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label59.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label59.Location = New System.Drawing.Point(12, 0)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(710, 144)
        Me.Label59.TabIndex = 957
        Me.Label59.Text = resources.GetString("Label59.Text")
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(6, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(722, 36)
        Me.Label4.TabIndex = 966
        Me.Label4.Text = "Please follow these exact steps on first-time entry"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pageUsers
        '
        Me.pageUsers.Controls.Add(Me.dgUser)
        Me.pageUsers.Controls.Add(Me.Label3)
        Me.pageUsers.Location = New System.Drawing.Point(4, 22)
        Me.pageUsers.Name = "pageUsers"
        Me.pageUsers.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageUsers.Size = New System.Drawing.Size(734, 524)
        Me.pageUsers.TabIndex = 0
        Me.pageUsers.Text = "Users"
        Me.pageUsers.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(722, 36)
        Me.Label3.TabIndex = 965
        Me.Label3.Text = "Create and Manage Users"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pageGroups
        '
        Me.pageGroups.Controls.Add(Me.dgGroup)
        Me.pageGroups.Controls.Add(Me.Label7)
        Me.pageGroups.Location = New System.Drawing.Point(4, 22)
        Me.pageGroups.Name = "pageGroups"
        Me.pageGroups.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageGroups.Size = New System.Drawing.Size(734, 524)
        Me.pageGroups.TabIndex = 1
        Me.pageGroups.Text = "Groups"
        Me.pageGroups.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(6, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(722, 36)
        Me.Label7.TabIndex = 966
        Me.Label7.Text = "Create and Manage Groups"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pagePrivileges
        '
        Me.pagePrivileges.Controls.Add(Me.Panel2)
        Me.pagePrivileges.Controls.Add(Me.Panel9)
        Me.pagePrivileges.Controls.Add(Me.chkPrivileges)
        Me.pagePrivileges.Controls.Add(Me.Label8)
        Me.pagePrivileges.Location = New System.Drawing.Point(4, 22)
        Me.pagePrivileges.Name = "pagePrivileges"
        Me.pagePrivileges.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pagePrivileges.Size = New System.Drawing.Size(734, 524)
        Me.pagePrivileges.TabIndex = 2
        Me.pagePrivileges.Text = "Privileges"
        Me.pagePrivileges.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.Window
        Me.Panel2.Controls.Add(Me.clbTables)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(166, 36)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(12, 0, 12, 12)
        Me.Panel2.Size = New System.Drawing.Size(562, 482)
        Me.Panel2.TabIndex = 969
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(12, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(538, 24)
        Me.Label2.TabIndex = 968
        Me.Label2.Text = "Privileges"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkPrivileges
        '
        Me.chkPrivileges.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkPrivileges.Location = New System.Drawing.Point(605, 10)
        Me.chkPrivileges.Name = "chkPrivileges"
        Me.chkPrivileges.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkPrivileges.Size = New System.Drawing.Size(123, 18)
        Me.chkPrivileges.TabIndex = 968
        Me.chkPrivileges.Text = "CHECK ALL"
        Me.chkPrivileges.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(6, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(722, 36)
        Me.Label8.TabIndex = 967
        Me.Label8.Text = "Assign Privileges to Groups"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pageAssign
        '
        Me.pageAssign.Controls.Add(Me.Panel3)
        Me.pageAssign.Controls.Add(Me.Panel1)
        Me.pageAssign.Controls.Add(Me.chkAssign)
        Me.pageAssign.Controls.Add(Me.Label15)
        Me.pageAssign.Location = New System.Drawing.Point(4, 22)
        Me.pageAssign.Name = "pageAssign"
        Me.pageAssign.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageAssign.Size = New System.Drawing.Size(734, 524)
        Me.pageAssign.TabIndex = 3
        Me.pageAssign.Text = "Group Assignments"
        Me.pageAssign.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.Window
        Me.Panel3.Controls.Add(Me.clbUsers)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(166, 36)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Padding = New System.Windows.Forms.Padding(12, 0, 12, 12)
        Me.Panel3.Size = New System.Drawing.Size(562, 482)
        Me.Panel3.TabIndex = 970
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(12, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(538, 24)
        Me.Label11.TabIndex = 968
        Me.Label11.Text = "Users"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Window
        Me.Panel1.Controls.Add(Me.lbGroups2)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(6, 36)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(12, 0, 12, 12)
        Me.Panel1.Size = New System.Drawing.Size(160, 482)
        Me.Panel1.TabIndex = 969
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(12, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(136, 24)
        Me.Label10.TabIndex = 968
        Me.Label10.Text = "Groups"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkAssign
        '
        Me.chkAssign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkAssign.Location = New System.Drawing.Point(605, 10)
        Me.chkAssign.Name = "chkAssign"
        Me.chkAssign.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkAssign.Size = New System.Drawing.Size(123, 18)
        Me.chkAssign.TabIndex = 954
        Me.chkAssign.Text = "CHECK ALL"
        Me.chkAssign.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(6, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(722, 36)
        Me.Label15.TabIndex = 968
        Me.Label15.Text = "Assign Users to Groups"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pageLogs
        '
        Me.pageLogs.Controls.Add(Me.btnPurge)
        Me.pageLogs.Controls.Add(Me.Label12)
        Me.pageLogs.Controls.Add(Me.PurgeDate)
        Me.pageLogs.Controls.Add(Me.dgvLogEvents)
        Me.pageLogs.Location = New System.Drawing.Point(4, 22)
        Me.pageLogs.Name = "pageLogs"
        Me.pageLogs.Padding = New System.Windows.Forms.Padding(3)
        Me.pageLogs.Size = New System.Drawing.Size(734, 524)
        Me.pageLogs.TabIndex = 5
        Me.pageLogs.Text = "Logs"
        Me.pageLogs.UseVisualStyleBackColor = True
        '
        'btnPurge
        '
        Me.btnPurge.Location = New System.Drawing.Point(32, 387)
        Me.btnPurge.Name = "btnPurge"
        Me.btnPurge.Size = New System.Drawing.Size(75, 23)
        Me.btnPurge.TabIndex = 952
        Me.btnPurge.Text = "Purge"
        Me.btnPurge.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(29, 334)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(119, 13)
        Me.Label12.TabIndex = 951
        Me.Label12.Text = "Purge Records Prior to:"
        '
        'PurgeDate
        '
        Me.PurgeDate.Location = New System.Drawing.Point(32, 350)
        Me.PurgeDate.Name = "PurgeDate"
        Me.PurgeDate.Size = New System.Drawing.Size(200, 21)
        Me.PurgeDate.TabIndex = 950
        '
        'dgvLogEvents
        '
        Me.dgvLogEvents.AllowUserToAddRows = False
        Me.dgvLogEvents.AllowUserToDeleteRows = False
        Me.dgvLogEvents.AllowUserToResizeRows = False
        DataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle37.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle37.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvLogEvents.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle37
        Me.dgvLogEvents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvLogEvents.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvLogEvents.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle38.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle38.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLogEvents.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle38
        Me.dgvLogEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLogEvents.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EventDate, Me.EventDescription, Me.EventUser})
        DataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle42.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle42.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLogEvents.DefaultCellStyle = DataGridViewCellStyle42
        Me.dgvLogEvents.Location = New System.Drawing.Point(32, 47)
        Me.dgvLogEvents.MultiSelect = False
        Me.dgvLogEvents.Name = "dgvLogEvents"
        Me.dgvLogEvents.ReadOnly = True
        Me.dgvLogEvents.RowHeadersVisible = False
        Me.dgvLogEvents.RowHeadersWidth = 25
        Me.dgvLogEvents.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLogEvents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLogEvents.Size = New System.Drawing.Size(650, 265)
        Me.dgvLogEvents.TabIndex = 949
        '
        'EventDate
        '
        Me.EventDate.DataPropertyName = "EventDate"
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle39.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle39.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle39.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle39.Format = "d"
        DataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.EventDate.DefaultCellStyle = DataGridViewCellStyle39
        Me.EventDate.FillWeight = 75.0!
        Me.EventDate.Frozen = True
        Me.EventDate.HeaderText = "Date"
        Me.EventDate.Name = "EventDate"
        Me.EventDate.ReadOnly = True
        Me.EventDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.EventDate.Width = 36
        '
        'EventDescription
        '
        Me.EventDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.EventDescription.DataPropertyName = "EventDescription"
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle40.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle40.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle40.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.EventDescription.DefaultCellStyle = DataGridViewCellStyle40
        Me.EventDescription.FillWeight = 150.0!
        Me.EventDescription.HeaderText = "Description of Activity"
        Me.EventDescription.Name = "EventDescription"
        Me.EventDescription.ReadOnly = True
        Me.EventDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'EventUser
        '
        Me.EventUser.DataPropertyName = "EventUser"
        DataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle41.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle41.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle41.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle41.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.EventUser.DefaultCellStyle = DataGridViewCellStyle41
        Me.EventUser.FillWeight = 60.0!
        Me.EventUser.HeaderText = "User"
        Me.EventUser.Name = "EventUser"
        Me.EventUser.ReadOnly = True
        Me.EventUser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.EventUser.Width = 35
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1035
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(660, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Administrative Console"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_Admin
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.tcAdmin)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "SA_Admin"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.Panel9.ResumeLayout(False)
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcAdmin.ResumeLayout(False)
        Me.pageInstruct.ResumeLayout(False)
        Me.Panel17.ResumeLayout(False)
        Me.Panel19.ResumeLayout(False)
        Me.Panel18.ResumeLayout(False)
        Me.pageUsers.ResumeLayout(False)
        Me.pageGroups.ResumeLayout(False)
        Me.pagePrivileges.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.pageAssign.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.pageLogs.ResumeLayout(False)
        Me.pageLogs.PerformLayout()
        CType(Me.dgvLogEvents, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
#End Region

    Private Sub lbGroups_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbGroups.SelectedIndexChanged

        If pagePrivileges.ContainsFocus Then

            If lbGroups.SelectedIndex <> -1 Then

                If GroupDict.Contains(lbGroups.SelectedItem) Then

                    Dim tabledict As New ArrayList

                    'Clear old selection
                    For i As Integer = 0 To Me.clbTables.CheckedIndices.Count - 1
                        Me.clbTables.SetItemChecked(Me.clbTables.CheckedIndices(0), False)
                    Next i

                    tablePerms.TablePerm.DefaultView.RowFilter = "GroupID ='" + GroupDict.Item(lbGroups.SelectedItem).ToString + "'"

                    ' Always call Read before accessing data.

                    For i As Integer = 0 To tablePerms.TablePerm.DefaultView.Count - 1
                        Try
                            Me.clbTables.SetItemChecked(Me.clbTables.FindString(CStr(tablePerms.TablePerm.DefaultView(i)("Tablename"))), True)
                        Catch ex1 As Exception
                        End Try
                    Next

                    clbTables.Refresh()
                    clbTables.ResumeLayout()

                End If

            End If

        End If

    End Sub

    Private Sub ChangeMade()
        Dim page As TabPage
        For Each page In tcAdmin.TabPages
            If tcAdmin.SelectedTab.Name <> page.Name And page.Name <> pageInstruct.Name Then
                page.Enabled = False
            End If
        Next
        btnSave.BackColor = Color.Red
        btnSave.ForeColor = Color.White
        lbGroups.Enabled = False
        lbGroups2.Enabled = False
        AdminNotSaved = True
    End Sub

    Private Sub UnchangeMade()
        btnSave.BackColor = SystemColors.Highlight
        btnSave.ForeColor = Color.White

        Dim page As TabPage
        For Each page In tcAdmin.TabPages
            page.Enabled = True
        Next

        lbGroups.Enabled = True
        lbGroups2.Enabled = True

        AdminNotSaved = False
    End Sub

    Protected Sub Grid_CurCellChange(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs)
        ChangeMade()
    End Sub

    Private Sub clbTables_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clbTables.SelectedIndexChanged
        ChangeMade()
    End Sub

    Private Sub lbGroups2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbGroups2.SelectedIndexChanged

        If pageAssign.ContainsFocus Then
            If lbGroups2.SelectedIndex <> -1 Then
                If GroupDict.Contains(lbGroups2.SelectedItem) Then
                    Try
                        Me.UserDict.Clear()
                        Me.LoginDict.Clear()
                        Me.clbUsers.Items.Clear()
                        Dim t As Integer
                        For t = 0 To users.Users.Count - 1
                            Me.UserDict.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", users.Users(t)("LoginID"))
                            Me.LoginDict.Add(users.Users(t)("LoginID"), CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")")

                            usersToGroups.UserToGroup.DefaultView.RowFilter = "GroupID='" & CStr(GroupDict.Item(lbGroups2.SelectedItem)) & "' AND LoginID='" & CStr(users.Users(t)("LoginID")) & "'"
                            If usersToGroups.UserToGroup.DefaultView.Count > 0 Then
                                Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", True)
                            Else
                                Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", False)
                            End If
                            usersToGroups.UserToGroup.DefaultView.RowFilter = ""
                        Next
                    Catch ex As Exception
                        ProfileMsgBox("CRIT", "", ex.ToString + vbCrLf + vbCrLf + "See Event Viewer under Administrative Tools")
                    End Try
                    clbUsers.Refresh()
                    clbUsers.ResumeLayout()

                End If

            End If

        End If

    End Sub

    Private Sub clbUsers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clbUsers.SelectedIndexChanged
        ChangeMade()
    End Sub

    Friend Sub LoadAdminCtl()

        ' 20081013 RRH - Commented to remove - Dim myParent As Main
        ' 20081013 RRH - Commented to remove - myParent = DirectCast(Me.ParentForm, Solomon_Profile.Main)
        myparent = CType(Me.ParentForm, Main)

        FileSystemWatcher1.EnableRaisingEvents = False

        Try
            AddHandler dgGroup.CellBeginEdit, AddressOf Grid_CurCellChange
            AddHandler dgUser.CellBeginEdit, AddressOf Grid_CurCellChange

            users.Clear()
            groups.Clear()
            tablePerms.Clear()
            usersToGroups.Clear()

            If Directory.Exists(pathAdmin) = False Then
                Directory.CreateDirectory(pathAdmin)
            End If

            If File.Exists(pathAdmin & "usersXP.xml") Then
                ReadEncrpytedXML("usersXP.xml", users)
            End If

            If File.Exists(pathAdmin & "groupsXP.xml") Then
                ReadEncrpytedXML("groupsXP.xml", groups)
            End If

            If File.Exists(pathAdmin & "usersToGroupsXP.xml") Then
                ReadEncrpytedXML("usersToGroupsXP.xml", usersToGroups)
            End If

            If File.Exists(pathAdmin & "tablePermsXP.xml") Then
                ReadEncrpytedXML("tablePermsXP.xml", tablePerms)
            End If

            Me.FileSystemWatcher1.Filter = "*.xml"
            ' 20081001 RRH Path - Me.FileSystemWatcher1.Path = myparent.MyParent.AppPath & "/_ADMIN"
            Me.FileSystemWatcher1.Path = pathAdmin

            groups.Tables(0).Columns("ARPermissions").ColumnMapping = MappingType.Hidden
            groups.Tables(0).Columns("RefID").ColumnMapping = MappingType.Hidden
            Me.dgGroup.DataSource = groups.Tables(0)


            ' GroupName
            Dim col1 As DataGridTextBoxColumn = New DataGridTextBoxColumn
            col1.MappingName = "GroupName"
            col1.HeaderText = "Group Name"
            col1.Alignment = HorizontalAlignment.Center
            col1.Width = 130
            'XXX Me.DataGridTableStyle1.GridColumnStyles.Add(col1)

            ' GroupID
            Dim col2 As DataGridTextBoxColumn = New DataGridTextBoxColumn
            col2.MappingName = "GroupID"
            col2.HeaderText = "ID No."
            col2.Alignment = HorizontalAlignment.Center
            col2.Width = 40
            'XXX Me.DataGridTableStyle1.GridColumnStyles.Add(col2)

            ' YearLimit
            Dim col3 As DataGridTextBoxColumn = New DataGridTextBoxColumn
            col3.MappingName = "YearLimit"
            col3.HeaderText = "Edit Year"
            col3.Alignment = HorizontalAlignment.Center
            col3.Width = 100
            'XXX Me.DataGridTableStyle1.GridColumnStyles.Add(col3)

            ' RefID
            Dim col4 As DataGridTextBoxColumn = New DataGridTextBoxColumn
            col4.MappingName = "RefID"
            col4.HeaderText = "Refinery ID"
            col4.Alignment = HorizontalAlignment.Center
            col4.Width = 140
            'XXX Me.DataGridTableStyle1.GridColumnStyles.Add(col4)

            'Dim o As Integer
            GroupDict.Clear()
            lbGroups.Items.Clear()
            lbGroups2.Items.Clear()
            For o As Integer = 0 To groups.Groups.Count - 1
                GroupDict.Add(groups.Groups(o)("GroupName"), groups.Groups(o)("GroupID"))
                lbGroups.Items.Add(groups.Groups(o)("GroupName"))
                lbGroups2.Items.Add(groups.Groups(o)("GroupName"))
            Next

            If lbGroups.Items.Count > 0 Then
                lbGroups.SelectedIndex() = 0
                lbGroups2.SelectedIndex() = 0
            End If

            ' **********
            ' Getting the units associated with each group
            ' **********

            ' ===========================================
            Dim tabledict As New ArrayList

            If lbGroups.SelectedIndices.Count > 0 Then
                tablePerms.TablePerm.DefaultView.RowFilter = "GroupID=" & CStr(GroupDict.Item(lbGroups.SelectedItem))

                tabledict.Clear()
                For t As Integer = 0 To tablePerms.TablePerm.DefaultView.Count - 1
                    'Dim tmpstr As String = tablePerms.TablePerm.DefaultView(t)("TableName")
                    Dim tmpstr As String = tablePerms.TablePerm.DefaultView(t)("TableName").ToString
                    Select Case tmpstr
                        'Case "Administrator" : tabledict.Add("Administrator - Full Access")
                        ' 20090108 RRH 
                        Case "Administrator" : tabledict.Add(strRightsAdmin)
                        Case "Refinery Configuration" : tabledict.Add(strRightsConfiguration)
                        Case "Performance Targets" : tabledict.Add(strRightsRefinery)
                        Case "Process Facilities" : tabledict.Add(strRightsProcess)
                        Case "Inventory - Storage and Transport" : tabledict.Add(strRightsInventory)
                        Case "Operating Expenses" : tabledict.Add(strRightsOpEx)
                        Case "Personnel" : tabledict.Add(strRightsPersonnel)
                        Case "Crude Charge Detail" : tabledict.Add(strRightsCrude)
                        Case "Material Balance" : tabledict.Add(strRightsMaterial)
                        Case "Energy" : tabledict.Add(strRightsEnergy)
                        Case "View Reports" : tabledict.Add(strRightsView)
                    End Select
                Next

                For t As Integer = 0 To tabledict.Count - 1
                    Me.clbTables.SetItemChecked(Me.clbTables.Items.IndexOf(tabledict(t)), True)
                Next
            End If

            ' **********
            ' Get the users associated with each Group
            ' **********
            Dim userAList As New ArrayList
            UserDict.Clear()
            LoginDict.Clear()
            clbUsers.Items.Clear()

            For t As Integer = 0 To users.Users.Count - 1
                Me.UserDict.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", users.Users(t)("LoginID"))
                Me.LoginDict.Add(users.Users(t)("LoginID"), CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")")

                usersToGroups.UserToGroup.DefaultView.RowFilter = "GroupID='" & CStr(GroupDict.Item(lbGroups.SelectedItem)) & "' AND LoginID='" & CStr(users.Users(t)("LoginID")) & "'"
                If usersToGroups.UserToGroup.DefaultView.Count > 0 Then
                    Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", True)
                Else
                    Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", False)
                End If
            Next

            For t As Integer = 0 To userAList.Count - 1
                Me.clbUsers.SetItemChecked(Me.clbUsers.Items.IndexOf(userAList(t)), True)
            Next

            ' **********
            ' Refresh all of the list boxes that have the groups displayed
            ' **********
            Me.lbGroups.Refresh()
            Me.lbGroups2.Refresh()

            Me.lbGroups.SelectedIndex = -1
            Me.lbGroups2.SelectedIndex = -1

            ' **********
            ' Fill the dsUsers1 dataset
            ' **********

            Me.dgUser.DataSource = users.Tables(0)

            FileSystemWatcher1.EnableRaisingEvents = True ' Enable FileWatcher
        Catch ex As System.IO.FileNotFoundException
            ProfileMsgBox("CRIT", "", ex.ToString + vbCrLf + vbCrLf + "See Event Viewer under Administrative Tools")

        Catch ex As Exception    ' Catch the error.
            'ExceptionManager.Publish(ex)
            ProfileMsgBox("CRIT", "", ex.ToString + vbCrLf + vbCrLf + "See Event Viewer under Administrative Tools")
        End Try
        FileSystemWatcher1.EnableRaisingEvents = True
    End Sub

    Private Sub FileSystemWatcher1_Changed(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcher1.Changed

        If Not wasMe And Not entered Then

            entered = True
            Select Case e.Name

                Case "groupsxp.xml"
                    groups.Clear()
                    ReadEncrpytedXML(e.Name, groups)

                    GroupDict.Clear()
                    lbGroups.Items.Clear()
                    lbGroups2.Items.Clear()

                    For o As Integer = 0 To groups.Groups.Count - 1

                        GroupDict.Add(groups.Groups(o)("GroupName"), groups.Groups(o)("GroupID"))
                        lbGroups.Items.Add(groups.Groups(o)("GroupName"))
                        lbGroups2.Items.Add(groups.Groups(o)("GroupName"))
                    Next

                    If lbGroups.Items.Count > 0 Then
                        lbGroups.SelectedIndex() = 0
                        lbGroups2.SelectedIndex() = 0
                    End If


                    ' **********
                    ' Getting the units associated with each group
                    ' **********
                    Dim tablename As String

                    If lbGroups.SelectedIndex <> -1 Then
                        tablePerms.TablePerm.DefaultView.RowFilter = "GroupID=" & CStr(GroupDict.Item(lbGroups.SelectedItem))
                        For t As Integer = 0 To Me.clbTables.Items.Count - 1
                            Me.clbTables.SetItemCheckState(t, CheckState.Unchecked)
                        Next

                        For t As Integer = 0 To tablePerms.TablePerm.DefaultView.Count - 1
                            tablename = CStr(tablePerms.TablePerm.DefaultView(t)("TableName"))
                            Me.clbTables.SetItemChecked(Me.clbTables.Items.IndexOf(tablename), True)
                        Next
                    End If

                    ' **********
                    ' Get the users associated with each Group
                    ' **********
                    Dim userdict As New ArrayList
                    Dim userAList As New ArrayList

                    Me.UserDict.Clear()
                    Me.LoginDict.Clear()
                    Me.clbUsers.Items.Clear()

                    For t As Integer = 0 To users.Users.Count - 1
                        Me.UserDict.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", users.Users(t)("LoginID"))
                        Me.LoginDict.Add(users.Users(t)("LoginID"), CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")")

                        usersToGroups.UserToGroup.DefaultView.RowFilter = "GroupID='" & CStr(GroupDict.Item(lbGroups.SelectedItem)) & "' AND LoginID='" & CStr(users.Users(t)("LoginID")) & "'"
                        If usersToGroups.UserToGroup.DefaultView.Count > 0 Then
                            Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", True)
                        Else
                            Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", False)
                        End If
                    Next

                    dgGroup.Refresh()
                Case "usersxp.xml"
                    users.Clear()
                    ReadEncrpytedXML(e.Name, users)
                    dgUser.Refresh()

                    Me.UserDict.Clear()
                    Me.LoginDict.Clear()
                    Me.clbUsers.Items.Clear()
                    For t As Integer = 0 To users.Users.Count - 1
                        Me.UserDict.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", users.Users(t)("LoginID"))
                        Me.LoginDict.Add(users.Users(t)("LoginID"), CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")")

                        usersToGroups.UserToGroup.DefaultView.RowFilter = "GroupID='" & CStr(GroupDict.Item(lbGroups2.SelectedItem)) + "' and LoginID='" + CStr(users.Users(t)("LoginID")) + "'"
                        If usersToGroups.UserToGroup.DefaultView.Count > 0 Then
                            Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", True)
                        Else
                            Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", False)
                        End If
                    Next

                Case "userstogroupsxp.xml"
                    usersToGroups.Clear()
                    ReadEncrpytedXML(e.Name, usersToGroups)
                    Me.UserDict.Clear()
                    Me.LoginDict.Clear()
                    Me.clbUsers.Items.Clear()
                    For t As Integer = 0 To users.Users.Count - 1
                        Me.UserDict.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", users.Users(t)("LoginID"))
                        Me.LoginDict.Add(users.Users(t)("LoginID"), CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")")

                        usersToGroups.UserToGroup.DefaultView.RowFilter = "GroupID='" & CStr(GroupDict.Item(lbGroups2.SelectedItem)) + "' and LoginID='" + CStr(users.Users(t)("LoginID")) + "'"
                        If usersToGroups.UserToGroup.DefaultView.Count > 0 Then
                            Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", True)
                        Else
                            Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", False)
                        End If
                    Next

                Case "tablepermsxp.xml"
                    tablePerms.Clear()
                    ReadEncrpytedXML(e.Name, tablePerms)
                    ' **********
                    ' Getting the units associated with each group
                    ' **********
                    Dim tablename As String

                    If lbGroups.SelectedIndex <> -1 Then
                        tablePerms.TablePerm.DefaultView.RowFilter = "GroupID=" & CStr(GroupDict.Item(lbGroups.SelectedItem))
                        For t As Integer = 0 To Me.clbTables.Items.Count - 1
                            Me.clbTables.SetItemCheckState(t, CheckState.Unchecked)
                        Next

                        For t As Integer = 0 To tablePerms.TablePerm.DefaultView.Count - 1
                            tablename = CStr(tablePerms.TablePerm.DefaultView(t)("TableName"))
                            Me.clbTables.SetItemChecked(Me.clbTables.Items.IndexOf(tablename), True)
                        Next
                    End If

            End Select
            entered = False
            wasMe = False
        End If

        looped += 1
        'Reset on third 
        If looped = 2 Then
            If wasMe Then
                wasMe = False
            End If
            If entered Then
                entered = False
            End If
            looped = 0
        End If


    End Sub
    Private Sub SetupLogPage()
        Dim ds As New DataSet
        Dim dtEvents As New DataTable

        ' 20081001 RRH Path - ds.ReadXml(myparent.MyParent.AppPath & "/_ADMIN/Activity.xml")
        ds.ReadXml(pathAdmin & "Activity.xml")

        Dim dv As New DataView(ds.Tables("Events"))
        dv.Sort = "EventDate DESC"
        dgvLogEvents.DataSource = dv
    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

#Region " Use password character in P/W column "

    Private Sub dgUser_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgUser.CellBeginEdit
        ChangeMade()
    End Sub

    Private Sub dgUser_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgUser.EditingControlShowing

        ' 20081001 RRH UI - Change password field to show "�" rather than the text.

        Dim t As TextBox = DirectCast(e.Control, System.Windows.Forms.TextBox)

        If ((t.Text <> "") And (CType(sender, DataGridView).CurrentCell.ColumnIndex = 3)) Then
            t.UseSystemPasswordChar = True
        End If


    End Sub

    Private Sub dgUser_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgUser.CellFormatting

        ' 20081001 RRH UI - Change password field to show "�" rather than the text.

        If ((e.ColumnIndex <> -1) AndAlso (dgUser.Columns(e.ColumnIndex).Name = "userPassword")) Then
            If (Not (e.Value Is Nothing)) Then
                e.Value = New String(Convert.ToChar("�"), e.Value.ToString().Length)
            End If
        End If

    End Sub

#End Region ' Use password character in P/W column


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        FileSystemWatcher1.EnableRaisingEvents = False
        Select Case tcAdmin.SelectedTab.Name
            'USERS
            Case pageUsers.Name
                Dim changes As New DataSet
                Dim userAList As New ArrayList
                Dim userdict As New ArrayList

                If users.HasChanges() Then
                    Try
                        Me.UserDict.Clear()
                        Me.LoginDict.Clear()
                        Me.clbUsers.Items.Clear()
                        Dim t As Integer
                        For t = 0 To users.Users.Count - 1
                            Me.UserDict.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", users.Users(t)("LoginID"))
                            Me.LoginDict.Add(users.Users(t)("LoginID"), CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")")
                            If lbGroups2.SelectedIndex <> -1 Then
                                usersToGroups.UserToGroup.DefaultView.RowFilter = "GroupID='" & CStr(GroupDict.Item(lbGroups2.SelectedItem)) + "' and LoginID='" + CStr(users.Users(t)("LoginID")) + "'"
                                If usersToGroups.UserToGroup.DefaultView.Count > 0 Then
                                    Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", True)
                                Else
                                    Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", False)
                                End If
                            End If
                        Next
                        wasMe = True
                        entered = False
                        WriteEncrpytedXML("usersXP.xml", users)
                    Catch ex As Exception
                        ProfileMsgBox("CRIT", "", ex.ToString + vbCrLf + vbCrLf + "See Event Viewer under Administrative Tools")
                    End Try
                End If
            Case pageGroups.Name
                Try
                    GroupDict.Clear()
                    lbGroups.Items.Clear()
                    lbGroups2.Items.Clear()

                    ' 20081013 RRH - Chaned o to i and combined dim/for statement
                    For i As Integer = 0 To groups.Groups.Count - 1

                        GroupDict.Add(groups.Groups(i)("GroupName"), groups.Groups(i)("GroupID"))
                        lbGroups.Items.Add(groups.Groups(i)("GroupName"))
                        lbGroups2.Items.Add(groups.Groups(i)("GroupName"))
                    Next


                    If lbGroups.Items.Count > 0 Then
                        lbGroups.SelectedIndex() = 0
                        lbGroups2.SelectedIndex() = 0
                    End If

                    ' Getting the units associated with each group
                    Dim tablename As String

                    If lbGroups.SelectedIndex <> -1 Then
                        tablePerms.TablePerm.DefaultView.RowFilter = "GroupID=" & CStr(GroupDict.Item(lbGroups.SelectedItem))
                        For t As Integer = 0 To Me.clbTables.Items.Count - 1
                            Me.clbTables.SetItemCheckState(t, CheckState.Unchecked)
                        Next

                        For t As Integer = 0 To tablePerms.TablePerm.DefaultView.Count - 1
                            tablename = CStr(tablePerms.TablePerm.DefaultView(t)("TableName"))
                            Me.clbTables.SetItemChecked(Me.clbTables.Items.IndexOf(tablename), True)
                        Next
                    End If

                    ' Get the users associated with each Group
                    Dim userdict As New ArrayList
                    Dim userAList As New ArrayList

                    Me.UserDict.Clear()
                    Me.LoginDict.Clear()
                    Me.clbUsers.Items.Clear()

                    For t As Integer = 0 To users.Users.Count - 1
                        Me.UserDict.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", users.Users(t)("LoginID"))
                        Me.LoginDict.Add(users.Users(t)("LoginID"), CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")")

                        usersToGroups.UserToGroup.DefaultView.RowFilter = "GroupID='" & CStr(GroupDict.Item(lbGroups.SelectedItem)) & "' AND LoginID='" & CStr(users.Users(t)("LoginID")) & "'"
                        If usersToGroups.UserToGroup.DefaultView.Count > 0 Then
                            Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", True)
                        Else
                            Me.clbUsers.Items.Add(CStr(users.Users(t)("Last_Name")) + ", " + CStr(users.Users(t)("First_Name")) + " (" + CStr(users.Users(t)("LoginID")) + ")", False)
                        End If
                    Next
                    wasMe = True
                    entered = False
                    WriteEncrpytedXML("groupsXP.xml", groups)
                Catch ex As Exception
                    ProfileMsgBox("CRIT", "", ex.ToString + vbCrLf + vbCrLf + "See Event Viewer under Administrative Tools")
                Finally
                    Me.lbGroups.Refresh()
                    Me.lbGroups2.Refresh()
                End Try

            Case pagePrivileges.Name
                Dim rows() As DataRow = tablePerms.TablePerm.Select("GroupID =" + GroupDict.Item(lbGroups.SelectedItem).ToString)
                For x As Integer = 0 To rows.Length - 1
                    rows(x).Delete()
                Next

                'Dim mySelectQuery As String = "DELETE FROM TablePerm WHERE GroupID = " + GroupDict.Item(lbGroups.SelectedItem).ToString
                Try
                    ' Delete the old list so that I can create a new list
                    Dim myItem As Object
                    Dim myTest As String

                    For Each myItem In clbTables.CheckedItems
                        ' myTest contains the name of the selected unit such as "Big Brown Unit 1" or whatever is in the listbox
                        myTest = myItem.ToString
                        tablePerms.TablePerm.AddTablePermRow(CInt(GroupDict.Item(lbGroups.SelectedItem)), myTest)
                    Next myItem

                    wasMe = True
                    entered = False
                    WriteEncrpytedXML("tablePermsXP.xml", tablePerms)

                Catch ex As Exception
                    ProfileMsgBox("CRIT", "", ex.ToString + vbCrLf + vbCrLf + "See Event Viewer under Administrative Tools")
                End Try
            Case pageAssign.Name
                Try
                    ' Delete the old list so that I can create a new list
                    Dim myItem As Object
                    Dim myTest As String
                    Dim myTemp As String

                    If lbGroups2.SelectedIndex <> -1 Then
                        Dim rows() As DataRow = usersToGroups.UserToGroup.Select("GroupID= " + GroupDict.Item(lbGroups2.SelectedItem).ToString)
                        For x As Integer = 0 To rows.Length - 1
                            rows(x).Delete()
                        Next
                    End If

                    For Each myItem In clbUsers.CheckedItems
                        myTest = myItem.ToString
                        myTemp = UserDict.Item(myTest).ToString
                        If lbGroups2.SelectedIndex <> -1 Then
                            usersToGroups.UserToGroup.AddUserToGroupRow(myTemp, CInt(GroupDict.Item(lbGroups2.SelectedItem)))
                        End If
                    Next

                    wasMe = True
                    entered = False
                    WriteEncrpytedXML("usersToGroupsXP.xml", usersToGroups)

                Catch ex As Exception
                    ProfileMsgBox("CRIT", "", ex.ToString + vbCrLf + vbCrLf + "See Event Viewer under Administrative Tools")
                End Try
        End Select

        UnchangeMade()

        FileSystemWatcher1.EnableRaisingEvents = True
    End Sub

    Private Sub chkPrivileges_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPrivileges.CheckedChanged
        If chkPrivileges.Checked Then
            For x As Integer = 0 To clbTables.Items.Count - 1
                clbTables.SetItemCheckState(x, CheckState.Checked)
            Next x
        Else
            If clbTables.CheckedItems.Count <> 0 Then
                For x As Integer = 0 To clbTables.Items.Count - 1
                    clbTables.SetItemCheckState(x, CheckState.Unchecked)
                Next x
            End If
        End If

        clbTables.Refresh()
        ChangeMade()
    End Sub

    Private Sub chkAssign_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAssign.CheckedChanged
        If chkAssign.Checked Then
            For x As Integer = 0 To clbUsers.Items.Count - 1
                clbUsers.SetItemCheckState(x, CheckState.Checked)
            Next x
        Else
            If clbUsers.CheckedItems.Count <> 0 Then
                For x As Integer = 0 To clbUsers.Items.Count - 1
                    clbUsers.SetItemCheckState(x, CheckState.Unchecked)
                Next x
            End If
        End If
        clbUsers.Refresh()
        ChangeMade()
    End Sub

    Private Sub dgGroup_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgGroup.CellBeginEdit
        ChangeMade()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If AdminNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "the Administrative Console")
            Select Case result
                Case DialogResult.Yes : myparent.SaveCrude()
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        myparent.HideTopLayer(Me)
        myparent.tcControlPanel.Enabled = True
        myparent.MyParent.MenuFile.Enabled = True
        myparent.MyParent.MenuEdit.Enabled = True
        myparent.MyParent.MenuView.Enabled = True
        myparent.MyParent.MenuTools.Enabled = True
        myparent.MyParent.MenuWindow.Enabled = True
        myparent.MyParent.MenuHelp.Enabled = True
        myparent.btnWorkspace.Enabled = True
        myparent.btnCP.Enabled = True

        Me.LoadAdminCtl()
        UnchangeMade()
    End Sub

    Private Sub dgUser_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgUser.UserDeletingRow
        Dim result As DialogResult = ProfileMsgBox("YN", "DELETE", "user entry")
        If result = DialogResult.Yes Then
            ChangeMade()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgGroup_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgGroup.UserDeletingRow
        Dim result As DialogResult = ProfileMsgBox("YN", "DELETE", "group entry")
        If result = DialogResult.Yes Then
            ChangeMade()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgGroup_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgGroup.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub tcAdmin_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles tcAdmin.SelectedIndexChanged
        If tcAdmin.SelectedIndex = 5 Then
            SetupLogPage()
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnPurge.Click

        Dim ds As DataSet = New DataSet()
        Dim dt As DataTable = New DataTable()
        Dim counter As Integer = 0

        Try
            ds.ReadXml(pathAdmin & "Activity.xml")
            dt = ds.Tables(0).Select("EventDate < " & CDate(PurgeDate.Text)).CopyToDataTable
            ds.Tables(0).Clear()
            ds.Tables(0).Merge(dt)
            dt.Dispose()
            ds.WriteXml(pathAdmin & "Activity.xml")
            MessageBox.Show("Purged " & dt.Rows.Count & " records from Activity Log")
        Catch ex As Exception
            MessageBox.Show("There are no records to purge.")
        End Try



    End Sub
End Class