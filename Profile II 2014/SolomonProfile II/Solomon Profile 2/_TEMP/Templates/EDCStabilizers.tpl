<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small border=0 width=100%>
	<tr>
		<td width=10></td>
		<td width=10></td>
		<td width=180></td>
		<td width=100></td>
	</tr>
	<tr>
		<td colspan=4 align=left><strong>Annual Figures</strong></td>
	</tr>
Section(EDCStabilizers,,EffDate DESC)
 BEGIN
	<tr>
		<td colspan=4>&nbsp</td>
	</tr>
	<tr>
		<td></td>
		<td colspan=2 align=left>Starting Month of Period</td>
		<td align=right>Format(EffDate,'y')</td>
	</tr>
	<tr>
		<td></td>
		<td colspan=2 align=left>Net Input Barrels</td>
		<td align=right>Format(AnnInputBbl,'#,##0')</td>
	</tr>
	<tr>
		<td></td>
		<td colspan=2 align=left>Coke Sales, bbl</td>
		<td align=right>Format(AnnCokeBbl,'#,##0')</td>
	</tr>
	<tr>
		<td></td>
		<td colspan=2 align=left>Electricity Consumption, MWh</td>
		<td align=right>Format(AnnElecConsMWH,'#,##0')</td>
	</tr>
	<tr>
		<td></td>
		<td colspan=3 align=left>Raw Material Receipts, bbl</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Railcar</td>
		<td align=right>FORMAT(AnnRSCRUDE_RAIL,'#,##0')</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Tank Truck</td>
		<td align=right>FORMAT(AnnRSCRUDE_TT,'#,##0')</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Tanker Berth</td>
		<td align=right>Format(AnnRSCRUDE_TB,'#,##0')</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Offshore Buoy</td>
		<td align=right>Format(AnnRSCRUDE_OMB,'#,##0')</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Barge Berth</td>
		<td align=right>Format(AnnRSCRUDE_BB,'#,##0')</td>
	</tr>
	<tr>
		<td></td>
		<td colspan=3 align=left>Product Shipments, bbl</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Railcar</td>
		<td align=right>Format(AnnRSPROD_RAIL,'#,##0')</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Tank Truck</td>
		<td align=right>Format(AnnRSPROD_TT,'#,##0')</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Tanker Berth</td>
		<td align=right>Format(AnnRSPROD_TB,'#,##0')</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Offshore Buoy</td>
		<td align=right>Format(AnnRSPROD_OMB,'#,##0')</td>
	</tr>
	<tr>
		<td colspan=2></td>
		<td align=left>Barge Berth</td>
		<td align=right>Format(AnnRSPROD_BB,'#,##0')</td>
	</tr>
END
</table>
<!-- template-end -->