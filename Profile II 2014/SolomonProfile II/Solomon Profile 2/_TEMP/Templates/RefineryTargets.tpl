<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class='small' width="100%" border="0">
  <tr>
    <td colspan=5></td>
  </tr>
  <tr>
    <td width=2%></td>
    <td width=78%></td>
    <td colspan=2 align=center><strong>Refinery<br/>Target</strong></td>
    <td align=center><strong>Currency</strong></td>
  </tr>
  SECTION(RefTargets,,SortKey ASC)
  	RELATE(Chart_LU,RefTargets,SortKey,SortKey)
  BEGIN
  HEADER('
  <tr>
    <td colspan=5 height=30 valign=bottom><strong>@SectionHeader</strong></td>
  </tr>
  ')
  <tr>
    <td></td>
    <td>@ChartTitle USEUNIT(AxisLabelUS,AxisLabelMetric)</td>
    <td width=15% align=right>NoShowZero(Format(@Target,@DecPlaces))</td>
    <td width=5%></td>
    <td width=15% align=right>NoShowZero(@CurrencyCode)</td>
  </tr>
  END
</table>
<!-- template-end -->
