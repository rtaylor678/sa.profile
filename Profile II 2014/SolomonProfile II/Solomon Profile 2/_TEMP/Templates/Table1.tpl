<!-- config-start -->
FILE=_CONFIG/Process.xml;
FILE=_REF/Process_LU.xml;
<!-- config-end -->
<!-- template-start -->
<table class=small width=100% border=0>
  <tr>
    <td colspan=2></td>
    <td valign=bottom align=center><strong>Process</br>Type</td>
    <td colspan=2 valign=bottom align=center><strong>Percent</br>of Month</br>In Service</td>
    <td colspan=2 valign=bottom align=center><strong>Capacity</strong></td>
    <td colspan=2 valign=bottom align=center><strong>Capacity</br>Utilization</br>Percent</td>
    <td colspan=2 valign=bottom align=center><strong>Energy</br>Consumption</br>Percent</td>
    <td colspan=4></td>
  </tr>

  SECTION(Config,ProcessID <>'FTCOGEN',)
  	RELATE(ProcessID_LU,Config,SortKey,SortKey)
  BEGIN
  HEADER('
  <tr>
    <td colspan=15 height=30 valign=bottom><strong>@ProcessDesc</strong></td>
  </tr> ')
  <tr>
    <td width=5></td>
    <td width=180>@UnitName</td>
    <td width=50 align=center>@ProcessType</td>
    <td width=50 align=right>Format(InServicePcnt,'#,##0.0')</td>
    <td width=5></td>
    <td width=80 align=right>Format(RptCap,'#,##0') USEUNIT(DisplayTextUS,DisplayTextMet)</td>
    <td width=5></td>
    <td width=60 align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td width=5></td>
    <td width=60 align=right>Format(EnergyPcnt,'#,##0.0')</td>
    <td colspan=5></td>
  </tr>
  END

  <tr>
    <td colspan=2></td>
    <td height=75 valign=bottom align=center><strong>Process</br>Type</td>
    <td height=75 colspan=2 valign=bottom align=center><strong>Percent</br>of Month</br>In Service</td>
    <td height=75 colspan=2 valign=bottom align=center><strong>Electrical</br>Capacity</strong></td>
    <td height=75 colspan=2 valign=bottom align=center><strong>Electrical</br>Utilization</br>Percent</td>
    <td height=75 colspan=2 valign=bottom align=center><strong>Steam</br>Capacity</strong></td>
    <td height=75 colspan=2 valign=bottom align=center><strong>Steam</br>Utilization</br>Percent</td>
    <td height=75 colspan=2 valign=bottom align=center><strong>Energy</br>Consumption</br>Percent</td>
  </tr>

  SECTION(Config,ProcessID ='FTCOGEN',)
  	RELATE(ProcessID_LU,Config,SortKey,SortKey)
  BEGIN

  HEADER('
  <tr>
    <td colspan=13 height=30 valign=bottom><strong>Fired Turbine Cogeneration</strong></td>
  </tr> ')
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td align=center>@ProcessType</td>
    <td align=right>Format(InServicePcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(RptCap,'#,##0')USEUNIT(DisplayTextUS,DisplayTextMet)</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td width=5></td>
    <td width=70 align=right>Format(RptStmCap,'#,##0')USEUNIT("klb/h","MT/h")</td>
    <td width=5></td>
    <td width=50 align=right>Format(StmUtilPcnt,'#,##0.0')</td>
    <td width=5></td>
    <td width=50 align=right>Format(EnergyPcnt,'#,##0.0')</td>
    <td width=5></td>
  </tr>
  END
</table>
<p>
  <!-- template-end -->
</p>
