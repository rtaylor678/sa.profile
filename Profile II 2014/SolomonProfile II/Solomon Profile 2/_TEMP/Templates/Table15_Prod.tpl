<!-- config-start -->
FILE=_CONFIG/Yield.xml;

<!-- config-end -->
<!-- template-start -->
<table class='small' width=100% border=0>
  <tr>
    <td width=5></td>
    <td width=20></td>
    <td width=5></td>
    <td width=330></td>
    <td width=75></td>
    <td width=10></td>
    <td width=75></td>
    <td width=20></td>
  </tr>
  <tr>
    <td colspan=2 align=center valign=bottom><strong>Solomon<br/>Material<br/>Number</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Material Name</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Total<br/>Barrels<br/>Produced</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Refinery Gate<br/>Product Prices<br/>(CurrencyCode/bbl)</strong></td>
  </tr>
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Product Yields</strong></td>
  </tr>
 SECTION(YIELD_PROD,Category='PROD',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName</td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Asphalt</strong></td>
  </tr>
 SECTION(YIELD_PROD,Category='ASP',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Feedstocks to Chemical Plant</strong></td>
  </tr>
 SECTION(YIELD_PROD,Category='FCHEM',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Specialty Solvents</strong></td>
  </tr>
 SECTION(YIELD_PROD,Category='SOLV',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Feedstocks to Paraffinic Lube Refinery</strong></td>
  </tr>
 SECTION(YIELD_PROD,Category='FLUBE',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Saleable Petroleum Coke</strong></td>
  </tr>
 SECTION(YIELD_PROD,Category='COKE',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
  <tr>
    <td height=30 valign=bottom colspan=8 align=left><strong>Miscellaneous Products</strong></td>
  </tr>
 SECTION(YIELD_PROD,Category='MPROD',)
 BEGIN
  <tr>
    <td></td>
    <td align=left>@MaterialID </td>
    <td></td>
    <td align=left>@MaterialName </td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(PriceLocal,'N')</td>
    <td></td>
  </tr>
 END
</table>
<!-- template-end -->
