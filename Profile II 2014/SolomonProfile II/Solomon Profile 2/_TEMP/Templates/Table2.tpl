<!-- config-start -->
FILE=_CONFIG/Process.xml;
FILE=_REF/Chart_LU.xml;
<!-- config-end -->
<!-- template-start -->
<table class=small border=0 width=100%>
   <tr>
      <td width=2%></td>
      <td width=20%></td>
      <td width=48%></td>
      <td width=20%></td>
      <td width=10%></td>
   </tr>
   <tr>
      <td colspan=2 align=left><strong>Process ID: Unit Name</strong></td>
      <td align=center><strong>Process Unit Property</strong></td>
      <td colspan=2 align=center><strong>Reported Value</strong></td>
   </tr>

SECTION(ProcessData,,UnitID ASC,SortKey ASC)
 RELATE(Table2_LU,ProcessData,ProcessID,ProcessID)
 RELATE(Table2_LU,ProcessData,Property,Property)
 BEGIN
   Header('<tr>
      <td></td>
      <td colspan=4 align=left height=30 valign=bottom>@ProcessID: @UnitName</td>
   </tr>')
   <tr>
      <td colspan=2></td>
      <td align=left valign=bottom> USEUNIT(USDescription,MetDescription) </td>
      <td align=right valign=bottom>NoShowZero(Format(@RptValue,USEUNIT(USDecPlaces,MetDecPlaces)))</td>
      <td></td>
   </tr>
END
</table>
<!-- template-end -->  