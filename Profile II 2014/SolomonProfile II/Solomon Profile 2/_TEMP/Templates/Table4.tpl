<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<style>
.Indent0{width:0px;}
.Indent1{width:20px;}
.Indent2{width:40px;}
.Indent3{width:60px;}
.Indent4{width:80px;}
</style>
<table  class='small' width=100% border=0>
   <tr>
      <td width=75%></td>
      <td width=20%></td>
      <td width=5%></td>
   </tr>
   <tr>
      <td align=left><strong></strong></td>
      <td colspan=2 align=center><strong>Refining Costs</strong></td>
   </tr>
   <tr>
      <td align=left><strong></strong></td>
      <td colspan=2 align=center><strong>Excluding</strong></td>
   </tr>
   <tr>
      <td align=left><strong></strong></td>
      <td colspan=2 align=center><strong>Turnarounds</strong></td>
   </tr>
   <tr>
      <td align=left><strong></strong></td>
      <td colspan=2 align=center><strong>and Energy</strong></td>
   </tr>
   <tr>
      <td align=left><strong></strong></td>
      <td colspan=2 align=center><strong>(CurrencyCode/1000)</strong></td>
   </tr>

SECTION(tblFriendOpEx,,Sortkey ASC)
BEGIN
	<tr>
          <td><span class="Indent@Indent">&nbsp;&nbsp;</span>@Description</td>
          <td align=right>NoShowZero(Format(@CostsRefinery,1))</td>
          <td></td>
	</tr>
 END
</TABLE>
<!-- template-end -->