<!-- config-start -->
FILE=_CONFIG/UserDefined.xml;
<!-- config-end -->
<!-- template-start -->
<table class='small' border=0  width=100% cellspacing=0 cellpadding=0>
   <tr>
      <td width=2%></td>
      <td width=38%></td>
      <td width=13%></td>
      <td width=3%></td>
      <td width=13%></td>
      <td width=2%></td>
      <td width=13%></td>
      <td width=2%></td>
      <td width=13%></td>
      <td width=2%></td>
   </tr>
   <tr>
      <td colspan=2 align=center><strong>Variable Description</strong></td>
      <td colspan=2 align=center><strong>Current Month</strong></td>
      <td colspan=2 align=center><strong>Target Value</strong></td>
      <td colspan=2 align=center><strong>Rolling Average</strong></td>
      <td colspan=2 align=center><strong>Year-to-date</strong></td>
   </tr>
SECTION(UserDefined,,HeaderText ASC)
 BEGIN
   Header('<tr>
      <td colspan=10 align=left height=30 valign=bottom><strong>@HeaderText</strong></td>
   </tr>')
   <tr>
      <td align=left></td>
      <td align=left>@VariableDesc</td>
      <td align=right>Format(@RptValue,@DecPlaces)</td>
      <td align=left></td>
      <td align=right>Format(@RptValue_Target,@DecPlaces)</td>
      <td align=left></td>
      <td align=right>Format(@RptValue_AVG,@DecPlaces)</td>
      <td align=left></td>
      <td align=right>Format(@RptValue_YTD,@DecPlaces)</td>
      <td align=left></td>
   </tr>
END
</table>
<!-- template-end -->  