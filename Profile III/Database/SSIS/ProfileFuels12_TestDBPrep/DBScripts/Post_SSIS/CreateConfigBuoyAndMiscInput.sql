/****** Object:  Table [dbo].[ConfigBuoy]    Script Date: 10/07/2014 14:29:12 ******/
use ProfileFuels12
go

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ConfigBuoy](
	[Refnum] [dbo].[Refnum] NOT NULL,
	[UnitID] [dbo].[UnitID] NOT NULL,
	[UnitName] [nvarchar](50) NULL,
	[ProcessID] [dbo].[ProcessID] NOT NULL,
	[ShipCap] [real] NULL,
	[LineSize] [real] NULL,
	[PcntOwnership] [real] NULL,
 CONSTRAINT [PK_ConfigBuoy] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC,
	[UnitID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ConfigBuoy] ADD  CONSTRAINT [DF__ConfigBuo__Proce__00FF1D08]  DEFAULT ('BUOY') FOR [ProcessID]
GO


CREATE TABLE [dbo].[MiscInput](
	[Refnum] [dbo].[Refnum] NOT NULL,
	[UnitsExcl] [dbo].[YorN] NULL,
	[UtlyAlloc] [dbo].[YorN] NULL,
	[OffsitesAlloc] [dbo].[YorN] NULL,
	[Napthenic] [dbo].[YorN] NULL,
	[DiagramIncluded] [dbo].[YorN] NULL,
	[SpecFracIncluded] [dbo].[YorN] NULL,
	[CDUChargeBbl] [float] NULL,
	[CDUChargeMT] [float] NULL,
	[RPFRXVSulf] [real] NULL,
	[RPFRXVCS] [real] NULL,
	[RPFRXVTemp] [real] NULL,
	[NapCurrRoutCostLocal] [real] NULL,
	[NapCurrRoutCostUS] [real] NULL,
	[NapCurrRoutMatlLocal] [real] NULL,
	[NapCurrRoutMatlUS] [real] NULL,
	[NapPrevRoutCostLocal] [real] NULL,
	[NapPrevRoutCostUS] [real] NULL,
	[NapPrevRoutMatlLocal] [real] NULL,
	[NapPrevRoutMatlUS] [real] NULL,
	[VACOCCOperHrs] [real] NULL,
	[VACOCCMaintHrs] [real] NULL,
	[VACOCCTechHrs] [real] NULL,
	[VACOCCAdminHrs] [real] NULL,
	[VACMPSOperHrs] [real] NULL,
	[VACMPSMaintHrs] [real] NULL,
	[VACMPSTechHrs] [real] NULL,
	[VACMPSAdminHrs] [real] NULL,
	[VACThermEnergyMBTU] [float] NULL,
	[VACElecMWH] [float] NULL,
	[VACProdMBTU] [float] NULL,
	[VACTankEnergyMBTU] [float] NULL,
	[CRMRefMarine] [real] NULL,
	[CRMOthMarine] [real] NULL,
	[CRMLOOP] [real] NULL,
	[CRMBuoy] [real] NULL,
	[CRMPipeline] [real] NULL,
	[CRMTruck] [real] NULL,
	[PrcOpTimeOff] [char](1) NULL,
	[SiteAcres] [real] NULL,
	[SiteUnitsPcnt] [real] NULL,
	[SiteNonRefPcnt] [real] NULL,
	[SiteOffsitesPcnt] [real] NULL,
	[SiteUnusedPcnt] [real] NULL,
	[SiteTotPcnt] [real] NULL,
	[DCRedFlags] [int] NULL,
	[DCBlueFlags] [int] NULL,
	[OffsiteEnergyPcnt] [real] NULL,
	[EnergyMethod] [tinyint] NULL,
	[MiscEnergyPcnt] [real] NULL,
	[MaintSoftwareVendor] [varchar](20) NULL,
	[MaintSoftwareVendorOth] [nvarchar](50) NULL,
	[LimitingUnit] [nvarchar](50) NULL,
	[AirTemp] [real] NULL,
 CONSTRAINT [PK___6__15] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO