/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
use ProfileFuels12
go

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ConfigBuoy ADD
	SubmissionID int NULL
GO
ALTER TABLE dbo.ConfigBuoy SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MaintRout ADD
	UtilPcnt real NULL,
	InservicePcnt real NULL
GO
ALTER TABLE dbo.MaintRout SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ProcessData ADD
	RptDValue smalldatetime NULL,
	RptTValue varchar(50) NULL
GO
ALTER TABLE dbo.ProcessData SET (LOCK_ESCALATION = TABLE)
GO
COMMIT




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ConfigBuoy
	DROP CONSTRAINT DF__ConfigBuo__Proce__00FF1D08
GO
CREATE TABLE dbo.Tmp_ConfigBuoy
	(
	Refnum dbo.Refnum NOT NULL,
	UnitID dbo.UnitID NOT NULL,
	UnitName nvarchar(50) NULL,
	ProcessID dbo.ProcessID NOT NULL,
	ShipCap real NULL,
	LineSize real NULL,
	PcntOwnership real NULL,
	SubmissionID int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ConfigBuoy SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_ConfigBuoy ADD CONSTRAINT
	DF__ConfigBuo__Proce__00FF1D08 DEFAULT ('BUOY') FOR ProcessID
GO
IF EXISTS(SELECT * FROM dbo.ConfigBuoy)
	 EXEC('INSERT INTO dbo.Tmp_ConfigBuoy (Refnum, UnitID, UnitName, ProcessID, ShipCap, LineSize, PcntOwnership, SubmissionID)
		SELECT Refnum, UnitID, UnitName, ProcessID, ShipCap, LineSize, PcntOwnership, SubmissionID FROM dbo.ConfigBuoy WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.ConfigBuoy
GO
EXECUTE sp_rename N'dbo.Tmp_ConfigBuoy', N'ConfigBuoy', 'OBJECT' 
GO
ALTER TABLE dbo.ConfigBuoy ADD CONSTRAINT
	PK_ConfigBuoy PRIMARY KEY CLUSTERED 
	(
	SubmissionID,
	UnitID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 70, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ConfigBuoy
	DROP COLUMN Refnum
GO
ALTER TABLE dbo.ConfigBuoy SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MiscInput ADD
	SubmissionID int NULL
GO
ALTER TABLE dbo.MiscInput SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_MiscInput
	(
	UnitsExcl dbo.YorN NULL,
	UtlyAlloc dbo.YorN NULL,
	OffsitesAlloc dbo.YorN NULL,
	Napthenic dbo.YorN NULL,
	DiagramIncluded dbo.YorN NULL,
	SpecFracIncluded dbo.YorN NULL,
	CDUChargeBbl float(53) NULL,
	CDUChargeMT float(53) NULL,
	RPFRXVSulf real NULL,
	RPFRXVCS real NULL,
	RPFRXVTemp real NULL,
	NapCurrRoutCostLocal real NULL,
	NapCurrRoutCostUS real NULL,
	NapCurrRoutMatlLocal real NULL,
	NapCurrRoutMatlUS real NULL,
	NapPrevRoutCostLocal real NULL,
	NapPrevRoutCostUS real NULL,
	NapPrevRoutMatlLocal real NULL,
	NapPrevRoutMatlUS real NULL,
	VACOCCOperHrs real NULL,
	VACOCCMaintHrs real NULL,
	VACOCCTechHrs real NULL,
	VACOCCAdminHrs real NULL,
	VACMPSOperHrs real NULL,
	VACMPSMaintHrs real NULL,
	VACMPSTechHrs real NULL,
	VACMPSAdminHrs real NULL,
	VACThermEnergyMBTU float(53) NULL,
	VACElecMWH float(53) NULL,
	VACProdMBTU float(53) NULL,
	VACTankEnergyMBTU float(53) NULL,
	CRMRefMarine real NULL,
	CRMOthMarine real NULL,
	CRMLOOP real NULL,
	CRMBuoy real NULL,
	CRMPipeline real NULL,
	CRMTruck real NULL,
	PrcOpTimeOff char(1) NULL,
	SiteAcres real NULL,
	SiteUnitsPcnt real NULL,
	SiteNonRefPcnt real NULL,
	SiteOffsitesPcnt real NULL,
	SiteUnusedPcnt real NULL,
	SiteTotPcnt real NULL,
	DCRedFlags int NULL,
	DCBlueFlags int NULL,
	OffsiteEnergyPcnt real NULL,
	EnergyMethod tinyint NULL,
	MiscEnergyPcnt real NULL,
	MaintSoftwareVendor varchar(20) NULL,
	MaintSoftwareVendorOth nvarchar(50) NULL,
	LimitingUnit nvarchar(50) NULL,
	AirTemp real NULL,
	SubmissionID int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_MiscInput SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.MiscInput)
	 EXEC('INSERT INTO dbo.Tmp_MiscInput (UnitsExcl, UtlyAlloc, OffsitesAlloc, Napthenic, DiagramIncluded, SpecFracIncluded, CDUChargeBbl, CDUChargeMT, RPFRXVSulf, RPFRXVCS, RPFRXVTemp, NapCurrRoutCostLocal, NapCurrRoutCostUS, NapCurrRoutMatlLocal, NapCurrRoutMatlUS, NapPrevRoutCostLocal, NapPrevRoutCostUS, NapPrevRoutMatlLocal, NapPrevRoutMatlUS, VACOCCOperHrs, VACOCCMaintHrs, VACOCCTechHrs, VACOCCAdminHrs, VACMPSOperHrs, VACMPSMaintHrs, VACMPSTechHrs, VACMPSAdminHrs, VACThermEnergyMBTU, VACElecMWH, VACProdMBTU, VACTankEnergyMBTU, CRMRefMarine, CRMOthMarine, CRMLOOP, CRMBuoy, CRMPipeline, CRMTruck, PrcOpTimeOff, SiteAcres, SiteUnitsPcnt, SiteNonRefPcnt, SiteOffsitesPcnt, SiteUnusedPcnt, SiteTotPcnt, DCRedFlags, DCBlueFlags, OffsiteEnergyPcnt, EnergyMethod, MiscEnergyPcnt, MaintSoftwareVendor, MaintSoftwareVendorOth, LimitingUnit, AirTemp, SubmissionID)
		SELECT UnitsExcl, UtlyAlloc, OffsitesAlloc, Napthenic, DiagramIncluded, SpecFracIncluded, CDUChargeBbl, CDUChargeMT, RPFRXVSulf, RPFRXVCS, RPFRXVTemp, NapCurrRoutCostLocal, NapCurrRoutCostUS, NapCurrRoutMatlLocal, NapCurrRoutMatlUS, NapPrevRoutCostLocal, NapPrevRoutCostUS, NapPrevRoutMatlLocal, NapPrevRoutMatlUS, VACOCCOperHrs, VACOCCMaintHrs, VACOCCTechHrs, VACOCCAdminHrs, VACMPSOperHrs, VACMPSMaintHrs, VACMPSTechHrs, VACMPSAdminHrs, VACThermEnergyMBTU, VACElecMWH, VACProdMBTU, VACTankEnergyMBTU, CRMRefMarine, CRMOthMarine, CRMLOOP, CRMBuoy, CRMPipeline, CRMTruck, PrcOpTimeOff, SiteAcres, SiteUnitsPcnt, SiteNonRefPcnt, SiteOffsitesPcnt, SiteUnusedPcnt, SiteTotPcnt, DCRedFlags, DCBlueFlags, OffsiteEnergyPcnt, EnergyMethod, MiscEnergyPcnt, MaintSoftwareVendor, MaintSoftwareVendorOth, LimitingUnit, AirTemp, SubmissionID FROM dbo.MiscInput WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.MiscInput
GO
EXECUTE sp_rename N'dbo.Tmp_MiscInput', N'MiscInput', 'OBJECT' 
GO
ALTER TABLE dbo.MiscInput ADD CONSTRAINT
	PK___6__15 PRIMARY KEY CLUSTERED 
	(
	SubmissionID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 70, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Crude ADD
	Period varchar(4) NULL
GO
ALTER TABLE dbo.Crude SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Yield ADD
	Period varchar(4) NULL
GO
ALTER TABLE dbo.Yield SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Energy ADD
	OverrideCalcs char(1) NULL,
	MBTUOut float(53) NULL
GO
ALTER TABLE dbo.Energy SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CalcsForKPI](@SubmissionID int, @Stage varchar(20) OUTPUT)
as
DELETE FROM Energy WHERE SubmissionID = @SubmissionID and EnergyType = 'PT';

set @Stage = 'ready';

declare @refID char(6);
set @refID = (select RefineryID from Submissions where SubmissionID=@SubmissionID);
--set @refID = null;
if(@refID is null)
begin
	set @Stage = 'RefIDNotFound'
	return;
end

--step 1
declare @calcsNeeded char(1)
set @calcsNeeded = (select CalcsNeeded from Submissions where SubmissionID = @SubmissionID);
--set @calcsNeeded = 's';
if(@calcsNeeded is not null)
begin
	set @Stage = 'CalcsNeededNotNull'
	return;
end
update Submissions set PeriodYear = 2014, PeriodMonth = 1, UseSubmission = 0 where SubmissionID=@SubmissionID;

--step 2, usually done when ref setup (check if exists)
declare @hasVal int
set @hasVal = (select count(RefineryID) from ReadyForCalcs where RefineryID=@refID and DataSet='Actual');
if(@hasVal = 0)
begin
	insert ReadyForCalcs values (@refID,'Actual',0,0,null);
end
set @Stage = 'ReadyForCalcsDone';

set @hasVal = (select count(RefineryID) from TSort where RefineryID=@refID);
--return;

--step 3,, usually done when ref setup (if exists)
if(@hasVal = 0)
begin
	insert TSort (RefineryID, Company, Location, CoLoc, Study, RVLocFactor, InflFactor, HydroInvInt, FuelsLubesCombo)
	VALUES (@refID,'Test Co', 'Test Loc', 'Test Co - TestLoc', 'EUR', 1, 2.1, 2.3, 0)
end
set @Stage = 'TSortDone';
--return;
set @hasVal = (select count(RefineryID) from RefineryFactors
			where RefineryID=@refID and EffDate = '1/1/2000' and EffUntil = '1/1/2050');

--set 4, usually done when ref setup (if exists)
if(@hasVal = 0)
begin
	insert RefineryFactors (RefineryID, EffDate, EffUntil, RVLocFactor, InflFactor, AvgInputBPD, CrudeDest)
	VALUES ('162EUR', '1/1/2000', '1/1/2050', 1, 2.1, 250000, 1)
end
set @Stage = 'RefineryFactorsDone';
--return;
set @hasVal = (select count(RefineryID) from CurrenciesToCalc
				where RefineryID=@refID and Currency = 'USD');
			
--step 5, usually done when ref setup
if(@hasVal = 0)
begin
	insert CurrenciesToCalc (RefineryID, Currency) --if exists
	VALUES (@refID, 'USD')
end
set @Stage = 'CurrenciesToCalcDone';
--return;
--set 6 do calcs
EXEC SetUseSubmission @SubmissionID
set @Stage = 'SetUseSubmissionDone'
--return;
EXEC spFullCalcs @SubmissionID
set @Stage = 'spFullCalcsDone'


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[GetKPIs]
	@SubmissionID int
AS
IF OBJECT_ID('tempdb..#Submissions') IS NOT NULL DROP TABLE #Submissions;

CREATE TABLE #Submissions
(
	[SubmissionID]		INT				NOT	NULL,
	PRIMARY KEY CLUSTERED([SubmissionID]ASC)
);

IF OBJECT_ID('tempdb..#EtlTable') IS NOT NULL DROP TABLE #EtlTable;

CREATE TABLE #EtlTable
(
	[DataKey]			VARCHAR(60)		NOT	NULL	CHECK([DataKey] <> ''),
	[RptDatabase]		VARCHAR(16)		NOT	NULL	CHECK([RptDatabase] <> '')		DEFAULT('ProfileFuels12'),
	[RptSchema]			VARCHAR(8)		NOT	NULL	CHECK([RptSchema] <> '')		DEFAULT('dbo'),
	[RptTable]			VARCHAR(40)		NOT	NULL	CHECK([RptTable] <> ''),
	[RptColumn]			VARCHAR(30)		NOT	NULL	CHECK([RptColumn] <> ''),
	[WhereClause]		VARCHAR(98)			NULL	CHECK([WhereClause] <> ''),

	PRIMARY KEY CLUSTERED ([DataKey] ASC)
);

IF OBJECT_ID('tempdb..#CalcTime') IS NOT NULL DROP TABLE #CalcTime;

CREATE TABLE #CalcTime
(
	[SubmissionID]		INT				NOT	NULL,
	[CalcTime]			DATETIME		NOT	NULL,

	PRIMARY KEY CLUSTERED ([SubmissionID] ASC)
);

IF OBJECT_ID('tempdb..#DataTable') IS NOT NULL DROP TABLE #DataTable;
CREATE TABLE #DataTable
(
	[SubmissionID]		INT				NOT	NULL,
	[DataKey]			VARCHAR(60)		NOT	NULL	CHECK([DataKey] <> ''),
	[Methodology]		SMALLINT		NOT	NULL	CHECK([Methodology] > 0),
	[NumberValue]		FLOAT				NULL,
	[TextValue]			VARCHAR(256)		NULL,
	[CalcTime]			DATETIME		NOT	NULL,

	PRIMARY KEY CLUSTERED ([DataKey] ASC, [SubmissionID] ASC, [Methodology] DESC)
);


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

DECLARE @Methodology	SMALLINT		= 2012;

INSERT INTO #Submissions([SubmissionID])
SELECT DISTINCT t.[SubmissionID]
FROM (VALUES
	(@SubmissionID)
	) t([SubmissionID])
ORDER BY t.[SubmissionID] ASC;

INSERT INTO #CalcTime([SubmissionID], [CalcTime])
SELECT [ml].[SubmissionID], [MessageTime] = MAX([ml].[MessageTime])
FROM [dbo].[MessageLog] [ml]
INNER JOIN #Submissions [gk]
ON [gk].[SubmissionID] = [ml].[SubmissionID]
WHERE [ml].[Source] = 'StartCalcs' AND [ml].[MessageText] = 'Completed Processing'
GROUP BY [ml].[SubmissionID];

INSERT INTO #EtlTable([DataKey], [RptTable], [RptColumn], [WhereClause])
SELECT t.[DataKey], t.[RptTable], t.[RptColumn], t.[WhereClause]
FROM ( VALUES
('Absence_ONJOB', 'Absence', 'TotPcnt', 'CategoryID = ''ONJOB'''),
('Absence_SICK', 'Absence', 'TotPcnt', 'CategoryID = ''Sick'''),
('Absence_VAC', 'Absence', 'TotPcnt', 'CategoryID = ''VAC'''),
('Absence_HOL', 'Absence', 'TotPcnt', 'CategoryID = ''HOL'''),
('Absence_OTH', 'Absence', 'TotPcnt', 'CategoryID = ''OTH''')
) t([DataKey], [RptTable], [RptColumn], [WhereClause])
ORDER BY t.[DataKey] ASC;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

DECLARE SqlCursor CURSOR FAST_FORWARD
FOR
SELECT CONVERT(NVARCHAR(MAX),
'SELECT [gk].[SubmissionID], [DataKey] = ''' + [et].[DataKey] + ''', [Methodology] = ' + CONVERT(CHAR(4), @Methodology) + ', 
[NumValue] = CASE WHEN (ISNUMERIC([t].[' + [et].[RptColumn] + ']) = 1) THEN [t].[' + [et].[RptColumn] + '] ELSE NULL END,
[TxtValue] = CASE WHEN (ISNUMERIC([t].[' + [et].[RptColumn] + ']) = 0) THEN [t].[' + [et].[RptColumn] + '] ELSE NULL END,
COALESCE([ct].[CalcTime], SYSDATETIME())
FROM #Submissions [gk]
INNER JOIN [' + [et].[RptDatabase] + '].[' + [et].[RptSchema] + '].[' + [et].[RptTable] + '] [t] ON [t].[SubmissionID] = [gk].[SubmissionID]
LEFT OUTER JOIN #CalcTime [ct] ON [ct].[SubmissionID] = [gk].[SubmissionID]'
+ COALESCE('WHERE ' + [et].[WhereClause], '') + ' ORDER BY [gk].[SubmissionID] ASC;')
FROM #EtlTable [et]
ORDER BY [et].[DataKey] ASC;

OPEN SqlCursor;

DECLARE @SqlCommand		NVARCHAR(MAX);

FETCH NEXT FROM SqlCursor INTO @SqlCommand;

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @SqlCommand = N'INSERT INTO #DataTable([SubmissionID], [DataKey], [Methodology], [NumberValue], [TextValue], [CalcTime]) ' + @SqlCommand;
	EXECUTE sp_executesql @SqlCommand;

	FETCH NEXT FROM SqlCursor INTO @SqlCommand;

END;

CLOSE SqlCursor;
DEALLOCATE SqlCursor;



--IF OBJECT_ID('tempdb..#Submissions')	IS NOT NULL DROP TABLE #Submissions;
--IF OBJECT_ID('tempdb..#DataTable')	IS NOT NULL DROP TABLE #DataTable;
--IF OBJECT_ID('tempdb..#EtlTable')		IS NOT NULL DROP TABLE #EtlTable;
--IF OBJECT_ID('tempdb..#CalcTime')		IS NOT NULL DROP TABLE #CalcTime;

 
 select SubmissionID,DataKey,Methodology,NumberValue,TextValue,CalcTime	 FROM #dataTable;