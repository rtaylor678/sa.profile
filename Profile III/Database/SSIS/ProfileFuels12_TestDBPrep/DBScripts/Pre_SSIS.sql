/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
use ProfileFuels12
go
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Currency_LU
	(
	CurrencyCode char(4) NOT NULL,
	Country varchar(30) NOT NULL,
	Currency varchar(30) NOT NULL,
	KCurrency varchar(32) NOT NULL,
	CurrencyID int NOT NULL,
	LastYear smallint NULL,
	NewCurrency char(4) NULL,
	CentsName varchar(30) NULL,
	CentsMultiplier real NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Currency_LU SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Currency_LU)
	 EXEC('INSERT INTO dbo.Tmp_Currency_LU (CurrencyCode, Country, Currency, KCurrency, CurrencyID, LastYear, NewCurrency, CentsName, CentsMultiplier)
		SELECT CurrencyCode, Country, Currency, KCurrency, CurrencyID, LastYear, NewCurrency, CentsName, CentsMultiplier FROM dbo.Currency_LU WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Currency_LU
GO
EXECUTE sp_rename N'dbo.Tmp_Currency_LU', N'Currency_LU', 'OBJECT' 
GO
ALTER TABLE dbo.Currency_LU ADD CONSTRAINT
	PK_Currency_LU PRIMARY KEY CLUSTERED 
	(
	CurrencyCode
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
