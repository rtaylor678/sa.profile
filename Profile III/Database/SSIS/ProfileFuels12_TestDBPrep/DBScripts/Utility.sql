--note depends
--Material_LU->MaterialSumCat_LU 
--ProcessID_LU->DisplayUnits_LU
---------------------------------------------------------TRUNCATE
use ProfileFuels12;

--truncate table [ProfileFuels12].[dbo].[Absence_LU]
--truncate table [ProfileFuels12].[dbo].[APIScale]
--delete [ProfileFuels12].[dbo].[Chart_LU]
--truncate table [ProfileFuels12].[dbo].[CountryFactors]
--truncate table [ProfileFuels12].[dbo].[Crude_LU]
--truncate table [ProfileFuels12].[dbo].[CrudePrice]
--truncate table [ProfileFuels12].[dbo].[Currency_LU]

--truncate table [ProfileFuels12].[dbo].[CurrencyConv]
--delete [ProfileFuels12].[dbo].[CustomTypes]
--truncate table [ProfileFuels12].[dbo].[CustomUnitReport]
--truncate table [ProfileFuels12].[dbo].[Datacheck_LU]
--truncate table [ProfileFuels12].[dbo].[DBInfo]

--truncate table [ProfileFuels12].[dbo].[Energy_LU]

--truncate table [ProfileFuels12].[dbo].[EnergyTransType_LU]
--truncate table [ProfileFuels12].[dbo].[EnergyType_Lu]
--truncate table [ProfileFuels12].[dbo].[Factors]  
 
--truncate table [ProfileFuels12].[dbo].[GasProperties]
--truncate table [ProfileFuels12].[dbo].[Material_LU] 
--truncate table [ProfileFuels12].[dbo].[MaterialCategory_LU] 
--delete [ProfileFuels12].[dbo].[MaterialSumCat_LU]



--truncate table [ProfileFuels12].[dbo].[MultLimits]
--truncate table [ProfileFuels12].[dbo].[Opex_LU]
--truncate table [ProfileFuels12].[dbo].[Pers_LU]
--truncate table [ProfileFuels12].[dbo].[PersSectionID_LU]
--truncate table [ProfileFuels12].[dbo].[ProcessGroup_LU]
--truncate table [ProfileFuels12].[dbo].[ProcessGrouping_LU]
--delete [ProfileFuels12].[dbo].[ProcessID_LU] 
--delete [ProfileFuels12].[dbo].[DisplayUnits_LU]


--truncate table [ProfileFuels12].[dbo].[ProcessType_LU]
--truncate table [ProfileFuels12].[dbo].[Product_LU]
--truncate table [ProfileFuels12].[dbo].[Report_LU]
--truncate table [ProfileFuels12].[dbo].[ReportProcs]
--truncate table [ProfileFuels12].[dbo].[SensHeatCriteria]
--truncate table [ProfileFuels12].[dbo].[MultFactors]
--truncate table [ProfileFuels12].[dbo].[Table2_LU]



--truncate table [ProfileFuels12].[dbo].[TankType_LU]
--truncate table [ProfileFuels12].[dbo].[TransAdjType_LU]
--truncate table [ProfileFuels12].[dbo].[Transportation]
--truncate table [ProfileFuels12].[dbo].[UnitTargets_LU]
--truncate table [ProfileFuels12].[dbo].[UOM]
--truncate table [ProfileFuels12].[dbo].[UOMGroups]
--truncate table [ProfileFuels12].[dbo].[FactorSets]


-----------------------------------------  COUNT

select count(*) as '[FactorSets]' from [ProfileFuels12].[dbo].[Absence_LU] 
select count(*) as '[APIScale]' from [ProfileFuels12].[dbo].[APIScale]
select count(*) as '[Chart_LU]' from [ProfileFuels12].[dbo].[Chart_LU]
select count(*) as '[CountryFactors]' from [ProfileFuels12].[dbo].[CountryFactors]
select count(*) as '[Crude_LU]' from [ProfileFuels12].[dbo].[Crude_LU]
select count(*) as '[CrudePrice]' from [ProfileFuels12].[dbo].[CrudePrice]
select count(*) as '[Currency_LU]' from [ProfileFuels12].[dbo].[Currency_LU]

select count(*) as '[CurrencyConv]' from [ProfileFuels12].[dbo].[CurrencyConv]
select count(*) as '[CustomTypes]' from [ProfileFuels12].[dbo].[CustomTypes]
select count(*) as '[CustomUnitReport]' from [ProfileFuels12].[dbo].[CustomUnitReport]
select count(*) as '[Datacheck_LU]' from [ProfileFuels12].[dbo].[Datacheck_LU]

select count(*) as '[DisplayUnits_LU]' from [ProfileFuels12].[dbo].[DisplayUnits_LU]
select count(*) as '[Energy_LU]' from [ProfileFuels12].[dbo].[Energy_LU]

select count(*) as '[EnergyTransType_LU]' from [ProfileFuels12].[dbo].[EnergyTransType_LU]
select count(*) as '[EnergyType_Lu]' from [ProfileFuels12].[dbo].[EnergyType_Lu]
select count(*) as '[Factors]' from [ProfileFuels12].[dbo].[Factors]  
 
select count(*) as '[GasProperties]' from [ProfileFuels12].[dbo].[GasProperties]
select count(*) as '[Material_LU]' from [ProfileFuels12].[dbo].[Material_LU] --error
select count(*) as '[MaterialCategory_LU]' from [ProfileFuels12].[dbo].[MaterialCategory_LU] 
select count(*) as '[MaterialSumCat_LU]' from [ProfileFuels12].[dbo].[MaterialSumCat_LU]

select count(*) as '[MultLimits]' from [ProfileFuels12].[dbo].[MultLimits]
select count(*) as '[Opex_LU]' from [ProfileFuels12].[dbo].[Opex_LU]
select count(*) as '[Pers_LU]' from [ProfileFuels12].[dbo].[Pers_LU]
select count(*) as '[PersSectionID_LU]' from [ProfileFuels12].[dbo].[PersSectionID_LU]
select count(*) as '[ProcessGroup_LU]' from [ProfileFuels12].[dbo].[ProcessGroup_LU]
select count(*) as '[ProcessGrouping_LU]' from [ProfileFuels12].[dbo].[ProcessGrouping_LU]
select count(*) as '[ProcessID_LU]' from [ProfileFuels12].[dbo].[ProcessID_LU] --error

select count(*) as '[ProcessType_LU]' from [ProfileFuels12].[dbo].[ProcessType_LU]
select count(*) as '[Product_LU]' from [ProfileFuels12].[dbo].[Product_LU]
select count(*) as '[Report_LU]' from [ProfileFuels12].[dbo].[Report_LU]
select count(*) as '[ReportProcs]' from [ProfileFuels12].[dbo].[ReportProcs]
select count(*) as '[SensHeatCriteria]' from [ProfileFuels12].[dbo].[SensHeatCriteria]
select count(*) as '[MultFactors]' from [ProfileFuels12].[dbo].[MultFactors]
select count(*) as '[Table2_LU]' from [ProfileFuels12].[dbo].[Table2_LU]

select count(*) as '[TankType_LU]' from [ProfileFuels12].[dbo].[TankType_LU]
select count(*) as '[TransAdjType_LU]' from [ProfileFuels12].[dbo].[TransAdjType_LU]
select count(*) as '[Transportation]' from [ProfileFuels12].[dbo].[Transportation]
select count(*) as '[UnitTargets_LU]' from [ProfileFuels12].[dbo].[UnitTargets_LU]
select count(*) as '[UOM]' from [ProfileFuels12].[dbo].[UOM]
select count(*) as '[UOMGroups]' from [ProfileFuels12].[dbo].[UOMGroups]
select count(*) as '[FactorSets]' from [ProfileFuels12].[dbo].[FactorSets]

----------------------------------------------------NULL and EMPTY string checks

select 'Absence_LU'
select * from [ProfileFuels12].[dbo].[Absence_LU] 
select '[APIScale]'
select *  from [ProfileFuels12].[dbo].[APIScale]
select '[Chart_LU]'
select *  from [ProfileFuels12].[dbo].[Chart_LU]
select '[CountryFactors]'
select *  from [ProfileFuels12].[dbo].[CountryFactors]
select '[Crude_LU]'
select *  from [ProfileFuels12].[dbo].[Crude_LU]
select '[CrudePrice]'
select *  from [ProfileFuels12].[dbo].[CrudePrice]
select '[Currency_LU]'
select *  from [ProfileFuels12].[dbo].[Currency_LU]
select '[CurrencyConv]'
select *  from [ProfileFuels12].[dbo].[CurrencyConv]
select '[CustomTypes]'
select *  from [ProfileFuels12].[dbo].[CustomTypes]

select '[CustomUnitReport]'
select *  from [ProfileFuels12].[dbo].[CustomUnitReport]
select '[Datacheck_LU]'
select *  from [ProfileFuels12].[dbo].[Datacheck_LU]

select '[DisplayUnits_LU]'
select *  from [ProfileFuels12].[dbo].[DisplayUnits_LU]
select '[Energy_LU]'
select *  from [ProfileFuels12].[dbo].[Energy_LU]
select '[EnergyTransType_LU]'
select *  from [ProfileFuels12].[dbo].[EnergyTransType_LU]
select '[EnergyType_Lu]'
select *  from [ProfileFuels12].[dbo].[EnergyType_Lu]
select '[Factors]'
select * from [ProfileFuels12].[dbo].[Factors]  

select '[GasProperties]'
select * from [ProfileFuels12].[dbo].[GasProperties]
select '[Material_LU]'
select * from [ProfileFuels12].[dbo].[Material_LU] --error
select '[MaterialCategory_LU]'
select * from [ProfileFuels12].[dbo].[MaterialCategory_LU] 
select '[MaterialSumCat_LU]'
select * from [ProfileFuels12].[dbo].[MaterialSumCat_LU]
select '[MultLimits]'
select * from [ProfileFuels12].[dbo].[MultLimits]
select '[Opex_LU]'
select * from [ProfileFuels12].[dbo].[Opex_LU]
select '[Pers_LU]'
select * from [ProfileFuels12].[dbo].[Pers_LU]
select '[PersSectionID_LU]'
select * from [ProfileFuels12].[dbo].[PersSectionID_LU]
select '[ProcessGroup_LU]'
select * from [ProfileFuels12].[dbo].[ProcessGroup_LU]
select '[ProcessGrouping_LU]'
select * from [ProfileFuels12].[dbo].[ProcessGrouping_LU]
select '[ProcessID_LU]'
select * from [ProfileFuels12].[dbo].[ProcessID_LU] --error
select '[ProcessType_LU]'
select * from [ProfileFuels12].[dbo].[ProcessType_LU]
select '[Product_LU]'
select * from [ProfileFuels12].[dbo].[Product_LU]
select '[Report_LU]'
select * from [ProfileFuels12].[dbo].[Report_LU]
select '[ReportProcs]'
select * from [ProfileFuels12].[dbo].[ReportProcs]
select '[SensHeatCriteria]'
select * from [ProfileFuels12].[dbo].[SensHeatCriteria]
select '[MultFactors]'
select * from [ProfileFuels12].[dbo].[MultFactors]
select '[Table2_LU]'
select * from [ProfileFuels12].[dbo].[Table2_LU]
select '[TankType_LU]'
select * from [ProfileFuels12].[dbo].[TankType_LU]
select '[TransAdjType_LU]'
select * from [ProfileFuels12].[dbo].[TransAdjType_LU]
 select '[Transportation]'
select * from [ProfileFuels12].[dbo].[Transportation]
select '[UnitTargets_LU]' 
select * from [ProfileFuels12].[dbo].[UnitTargets_LU]
 select '[UOM]'
select * from [ProfileFuels12].[dbo].[UOM]
 select '[UOMGroups]'
select * from [ProfileFuels12].[dbo].[UOMGroups]
select '[FactorSets]' 
select * from [ProfileFuels12].[dbo].[FactorSets]









