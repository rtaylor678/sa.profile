CategoryID|Description|SortKey|IncludeForInput|ParentID|DetailStudy|DetailProfile|Indent
DISC      |Discipline|2504|N|OTH   |O    ||1
EXC       |Excused|2506|N|OTH   |O    ||1
HOL       |Public or National Holidays|2400|Y||M    |M    |1
JURY      |Jury Duty|2502|N|OTH   |O    ||1
MIL       |Military, Reserve, or National Service|2501|N|OTH   |O    ||1
ONJOB     |On-the-Job Injuries|2100|Y||M    |M    |1
OTH       |Subtotal Other|2599|Y||C    |M    |1
SICK      |Sickness and Off-the-Job Injuries|2200|Y||M    |M    |1
UNEXC     |Unexcused|2507|N|OTH   |O    ||1
UNION     |Union Representation|2503|N|OTH   |O    ||1
VAC       |Personal Vacation|2300|Y||M    |M    |1
