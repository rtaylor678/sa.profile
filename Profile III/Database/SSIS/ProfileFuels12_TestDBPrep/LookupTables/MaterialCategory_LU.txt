Category|CategoryName|GrossGroup|SortKey|NetGroup
RMI  |Primary Raw Materials|I |1|I 
RMB  |Other Raw Materials Blended|I |2|I 
OTHRM|Other Raw Material Inputs|I |3|I 
RLUBE|Returns From Lube Refining|I |4|
RCHEM|Returns From Chemical Plant|I |5|
PROD |Primary Product Yields|Y |6|Y 
ASP  |Asphalt|Y |7|Y 
FCHEM|Refinery Feedstocks To Chemical Plant|Y |8|Y 
SOLV |Specialty Solvents|Y |9|Y 
FLUBE|Refinery Feedstocks To Lube Refining|Y |10|Y 
COKE |Saleable Petroleum Coke|Y |11|Y 
MPROD|Miscellaneous Products|Y |12|Y 
RPF  |Refinery-Produced Fuels|Y |13|Y 
