SumCat|Category
C4  |Butanes
C5C6|C5/C6
CCFd|Cracker Feedstocks
Chem|Petrochemicals
Dist|Distillates
GasB|Gasoline Blendstocks
Gaso|Gasoline
N/A |Not Applicable
NGL |NGL
Oth |All Other
RCrd|Reduced Crude
ReFd|Reformer Feedstocks
VRes|Vacuum Resid
