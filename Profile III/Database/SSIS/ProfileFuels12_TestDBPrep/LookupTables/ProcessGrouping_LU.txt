ProcessGrouping|ProcessGroup|ConvGroup|IsProcessUnit|Operated|InProcessUtil|AncillaryUnit|Comment
TotRef|||||||Total Refinery except for various adders
TotProc|||True||||Total of all process units, whether operated or not
OperProc|||True|True|||Total of all process units operated
IdleProc|||True|False|||Total of all process units not operated
MajConv||M|True|True|||Total of Major conversion units operated
OthProc|O||True||||Total of other process units
SpecFrac|F||True||||Total of special fractionation units
AncUnits||||||True|Total of ancillary units (units supporting production but not directly involved in production so they are excluded from process edc)
TotUtly|U||||||Utilities and offsites
TotRS|R||||||Receipts and Shipments
|||||||Calculate each individual Process ID
