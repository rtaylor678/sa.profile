TankType|Description|SortKey
CRD|Crude Oil                               |1
DST|Finished Products - Distillate          |5
FIN|Finished Products                       |3
GAS|Finished Products - Gasoline            |4
INT|Intermediate Stocks                     |2
NHC|Non-Hydrocarbon Tankage                 |10
OFP|Finished Products - All Other           |6
