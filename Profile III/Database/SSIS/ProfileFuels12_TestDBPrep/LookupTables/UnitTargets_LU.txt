ProcessID|Property|SortKey|USDescription|USDecPlaces|MetDescription|MetDecPlaces|USUOM|MetricUOM|CustomGroup
ALL|MechAvail_Target|85|Mechanical Availability, %|1|Mechanical Availability, %|1|%|%|0
ALL|OnStream_Target|110|On-Stream Factor, %|1|On-Stream Factor, %|1|%|%|0
ALL|OpAvail_Target|100|Operational Availability, %|1|Operational Availability, %|1|%|%|0
ALL|RoutCost_Target|35|Non-Turnaround Costs, CurrencyCode/bbl|2|Non-Turnaround Costs, CurrencyCode/bbl|2|||0
ALL|TACost_Target|25|Turnaround Cost, CurrencyCode/bbl|2|Turnaround Cost, CurrencyCode/bbl|2|||0
ALL|UnitEII_Target|66|Unit EII|1|Unit EII|1|||0
ALL|UtilPcnt_Target|60|Utilization, %|1|Utilization, %|1|%|%|0
CDU|AnnTADownDays_Target|55|Annualized Turnaround Days Down|1|Annualized Turnaround Days Down|1|||1
CDU|BtmTBP_Target|210|TBP Cutpoint for Bottoms, �F|0|TBP Cutpoint for Bottoms, �C|1|TF|TC|1
CDU|EII_Target|10|Energy Intensity Index|0|Energy Intensity Index|0|%|%|1
CDU|FurnInTemp_Target|215|Furnace Inlet Temperature, �F|0|Furnace Inlet Temperature, �C|1|TF|TC|1
CDU|FurnOutTemp_Target|220|Furnace Outlet Temperature, �F|0|Furnace Outlet Temperature, �C|1|TF|TC|1
CDU|FZPress_Target|225|Flash Zone Pressure, mm Hg abs|1|Flash Zone Pressure, mm Hg abs|1|||1
CDU|MaintCostCap_Target|20|Maintenance Cost, CurrencyCode/bbl|2|Maintenance Index, CurrencyCode/bbl|2|||1
CDU|MechAvailPlan_Target|75|Mechanical Availability (Non-Turnaround Planned), %|1|Mechanical Availability (Non-Turnaround Planned), %|1|||1
CDU|MechAvailUnp_Target|80|Mechanical Availability (Non-Turnaround Unplanned), %|1|Mechanical Availability (Non-Turnaround Unplanned), %|1|||1
CDU|OpAvailPlan_Target|90|Operational Planned Availability, %|1|Operational Planned Availability, %|1|||1
CDU|OpAvailUnp_Target|95|Operational Unplanned Availability, %|1|Operational Unplanned Availability, %|1|||1
CDU|Opex_Target|5|Operating Expenses, CurrencyCode/bbl|2|Operating Expenses, CurrencyCode/bbl|2|||1
CDU|PersIndex_Target|15|Personnel Index, work hours/100 EDC|1|Personnel Index, work hours/100 EDC|1|||1
CDU|PostPerShift_Target|70|Posts per Shift|1|Posts per Shift|1|||1
CDU|PTNArea_Target|230|Network Surface Area, ft�/bbl|1|Network Surface Area, m�/bbl|1|SQFT|M2|1
CDU|RegUnavail_Target|105|Mechanical - Operational Availability, %|1|Mechanical - Operational Availability, %|1|||1
CDU|RoutMatlPcnt_Target|40|Non-Turnaround Material, %|1|Non-Turnaround Material, %|1|%|%|1
CDU|StackOxy_Target|205|Stack Gas Oxygen Content, vol %|1|Stack Gas Oxygen Content, vol %|1|||1
CDU|StackTemp_Target|200|Stack Gas Exit Temperature, �F|0|Stack Gas Exit Temperature, �C|1|TF|TC|1
CDU|TADaysDown_Target|50|Turnaround Days Down|1|Turnaround Days Down|1|||1
CDU|TAIntDays_Target|45|Turnaround Interval, days|0|Turnaround Interval, days|0|||1
CDU|TAMatlPcnt_Target|30|Turnaround Material, %|1|Turnaround Material, %|1|%|%|1
CDU|UtilOSTA_Target|65|Utilization, % Outside of Turnaround|1|Utilization, % Outside of Turnaround|1|%|%|1
FCC|AnnTADownDays_Target|55|Annualized Turnaround Days Down|1|Annualized Turnaround Days Down|1|||1
FCC|EII_Target|10|Energy Intensity Index|0|Energy Intensity Index|0|%|%|1
FCC|FeedConCarbon_Target|300|Feed ConCarbon, wt %|2|Feed ConCarbon, wt %|2|||1
FCC|GM_Target|6|Gross Margin, CurrencyCode/bbl|2|Gross Margin, CurrencyCode/bbl|2|||1
FCC|MaintCostCap_Target|20|Maintenance Index, CurrencyCode/bbl|2|Maintenance Index, CurrencyCode/bbl|2|||1
FCC|MechAvailPlan_Target|75|Mechanical Availability (Non-Turnaround Planned), %|1|Mechanical Availability (Non-Turnaround Planned), %|1|||1
FCC|MechAvailUnp_Target|80|Mechanical Availability (Non-Turnaround Unplanned), %|1|Mechanical Availability (Non-Turnaround Unplanned), %|1|||1
FCC|NCM_Target|7|Net Cash Margin, CurrencyCode/bbl|2|Net Cash Margin, CurrencyCode/bbl|2|||1
FCC|OpAvailPlan_Target|90|Operational Planned Availability, %|1|Operational Planned Availability, %|1|||1
FCC|OpAvailUnp_Target|95|Operational Unplanned Availability, %|1|Operational Unplanned Availability, %|1|||1
FCC|Opex_Target|5|Operating Expenses, CurrencyCode/bbl|2|Operating Expenses, CurrencyCode/bbl|2|||1
FCC|PersIndex_Target|15|Personnel Index, work hours/100 EDC|1|Personnel Index, work hours/100 EDC|1|||1
FCC|PostPerShift_Target|70|Posts per Shift|1|Posts per Shift|1|||1
FCC|ProdC3_Target|310|C3, vol % of fresh feed|1|C3, vol % of fresh feed|1|||1
FCC|ProdDSO_Target|330|Decant/Slurry Oil, vol % of fresh feed|1|Decant/Slurry Oil, vol % of fresh feed|1|||1
FCC|ProdLtGasNap_Target|320|Light Gasoline/Naphtha, vol % of fresh feed|1|Light Gasoline/Naphtha, vol % of fresh feed|1|||1
FCC|ProdTotVol_Target|325|Total Yield, vol %|1|Total Yield, vol %|1|||1
FCC|RegUnavail_Target|105|Mechanical - Operational Availability, %|1|Mechanical - Operational Availability, %|1|||1
FCC|RoutMatlPcnt_Target|40|Non-Turnaround Material, %|1|Non-Turnaround Material, %|1|%|%|1
FCC|StackTemp_Target|200|Stack Gas Exit Temperature, �F|0|Stack Gas Exit Temperature, �C|1|TF|TC|1
FCC|TADaysDown_Target|50|Turnaround Days Down|1|Turnaround Days Down|1|||1
FCC|TAIntDays_Target|45|Turnaround Interval, days|0|Turnaround Interval, days|0|||1
FCC|TAMatlPcnt_Target|30|Turnaround Material, %|1|Turnaround Material, %|1|%|%|1
FCC|UtilOSTA_Target|65|Utilization, % Outside of Turnaround|1|Utilization, % Outside of Turnaround|1|%|%|1
HYC|AnnTADownDays_Target|55|Annualized Turnaround Days Down|1|Annualized Turnaround Days Down|1|||1
HYC|EII_Target|10|Energy Intensity Index|0|Energy Intensity Index|0|%|%|1
HYC|GM_Target|6|Gross Margin, CurrencyCode/bbl|2|Gross Margin, CurrencyCode/bbl|2|||1
HYC|MaintCostCap_Target|20|Maintenance Index, CurrencyCode/bbl|2|Maintenance Index, CurrencyCode/bbl|2|||1
HYC|MechAvailPlan_Target|75|Mechanical Availability (Non-Turnaround Planned), %|1|Mechanical Availability (Non-Turnaround Planned), %|1|||1
HYC|MechAvailUnp_Target|80|Mechanical Availability (Non-Turnaround Unplanned), %|1|Mechanical Availability (Non-Turnaround Unplanned), %|1|||1
HYC|NCM_Target|7|Net Cash Margin, CurrencyCode/bbl|2|Net Cash Margin, CurrencyCode/bbl|2|||1
HYC|OpAvailPlan_Target|90|Operational Planned Availability, %|1|Operational Planned Availability, %|1|||1
HYC|OpAvailUnp_Target|95|Operational Unplanned Availability, %|1|Operational Unplanned Availability, %|1|||1
HYC|Opex_Target|5|Operating Expenses, CurrencyCode/bbl|2|Operating Expenses, CurrencyCode/bbl|2|||1
HYC|PersIndex_Target|15|Personnel Index, work hours/100 EDC|1|Personnel Index, work hours/100 EDC|1|||1
HYC|PostPerShift_Target|70|Posts per Shift|1|Posts per Shift|1|||1
HYC|ProdBal_Target|320|Balance of Products, vol %|1|Balance of Products, vol %|1|||1
HYC|ProdC3_Target|300|LPG Yield, vol %|1|LPG Yield, vol %|1|||1
HYC|ProdDiesel_Target|315|Diesel Yield, vol %|1|Diesel Yield, vol %|1|||1
HYC|ProdKJet_Target|310|Kerosene Yield, vol %|1|Kerosene Yield, vol %|1|||1
HYC|ProdNap_Target|305|Naphtha Yield, vol %|1|Naphtha Yield, vol%|1|||1
HYC|ProdTotVol_Target|325|Total Yield, vol %|1|Total Yield, vol %|1|||1
HYC|RegUnavail_Target|105|Mechanical - Operational Availability, %|1|Mechanical - Operational Availability, %|1|||1
HYC|RoutMatlPcnt_Target|40|Non-Turnaround Material, %|1|Non-Turnaround Material, %|1|%|%|1
HYC|StackTemp_Target|200|Stack Gas Exit Temperature, �F|0|Stack Gas Exit Temperature, �C|1|TF|TC|1
HYC|TADaysDown_Target|50|Turnaround Days Down|1|Turnaround Days Down|1|||1
HYC|TAIntDays_Target|45|Turnaround Interval, days|0|Turnaround Interval, days|0|||1
HYC|TAMatlPcnt_Target|30|Turnaround Material, %|1|Turnaround Material, %|1|%|%|1
HYC|UtilOSTA_Target|65|Utilization, % Outside of Turnaround|1|Utilization, % Outside of Turnaround|1|%|%|1
PXYL|AnnTADownDays_Target|55|Annualized Turnaround Days Down|1|Annualized Turnaround Days Down|1|||1
PXYL|EII_Target|10|Energy Intensity Index|0|Energy Intensity Index|0|%|%|1
PXYL|GM_Target|6|Gross Margin, CurrencyCode/bbl|2|Gross Margin, CurrencyCode/bbl|2|||1
PXYL|MaintCostCap_Target|20|Maintenance Index, CurrencyCode/bbl|2|Maintenance Index, CurrencyCode/bbl|2|||1
PXYL|MechAvailPlan_Target|75|Mechanical Availability (Non-Turnaround Planned), %|1|Mechanical Availability (Non-Turnaround Planned), %|1|||1
PXYL|MechAvailUnp_Target|80|Mechanical Availability (Non-Turnaround Unplanned), %|1|Mechanical Availability (Non-Turnaround Unplanned), %|1|||1
PXYL|NCM_Target|7|Net Cash Margin, CurrencyCode/bbl|2|Net Cash Margin, CurrencyCode/bbl|2|||1
PXYL|OpAvailPlan_Target|90|Operational Planned Availability, %|1|Operational Planned Availability, %|1|||1
PXYL|OpAvailUnp_Target|95|Operational Unplanned Availability, %|1|Operational Unplanned Availability, %|1|||1
PXYL|Opex_Target|5|Operating Expenses, CurrencyCode/bbl|2|Operating Expenses, CurrencyCode/bbl|2|||1
PXYL|PersIndex_Target|15|Personnel Index, work hours/100 EDC|1|Personnel Index, work hours/100 EDC|1|||1
PXYL|PostPerShift_Target|70|Posts per Shift|1|Posts per Shift|1|||1
PXYL|RegUnavail_Target|105|Mechanical - Operational Availability, %|1|Mechanical - Operational Availability, %|1|||1
PXYL|RoutMatlPcnt_Target|40|Non-Turnaround Material, %|1|Non-Turnaround Material, %|1|%|%|1
PXYL|TADaysDown_Target|50|Turnaround Days Down|1|Turnaround Days Down|1|||1
PXYL|TAIntDays_Target|45|Turnaround Interval, days|0|Turnaround Interval, days|0|||1
PXYL|TAMatlPcnt_Target|30|Turnaround Material, %|1|Turnaround Material, %|1|%|%|1
PXYL|UtilOSTA_Target|65|Utilization, % Outside of Turnaround|1|Utilization, % Outside of Turnaround|1|%|%|1
REF|AnnTADownDays_Target|55|Annualized Turnaround Days Down|1|Annualized Turnaround Days Down|1|||1
REF|EII_Target|10|Energy Intensity Index|0|Energy Intensity Index|0|%|%|1
REF|EnergyConsPerWt_Target|325|Energy Consumption, btu/lb|1|Energy Consumption, kcal/kg|1|BTU/LB|KCAL/KG|1
REF|FeedGravity_Target|315|Feed Gravity, �API|1|Feed Density, kg/m�|0|API|KGM3|1
REF|GM_Target|6|Gross Margin, CurrencyCode/bbl|2|Gross Margin, CurrencyCode/bbl|2|||1
REF|HHCRatio_Target|345|Hydrogen/Hydrocarbon Ratio|1|Hydrogen/Hydrocarbon Ratio|1|||1
REF|MaintCostCap_Target|20|Maintenance Index, CurrencyCode/bbl|2|Maintenance Index, CurrencyCode/bbl|2|||1
REF|MechAvailPlan_Target|75|Mechanical Availability (Non-Turnaround Planned), %|1|Mechanical Availability (Non-Turnaround Planned), %|1|||1
REF|MechAvailUnp_Target|80|Mechanical Availability (Non-Turnaround Unplanned), %|1|Mechanical Availability (Non-Turnaround Unplanned), %|1|||1
REF|MGas_Target|350|Hydrogen Production Rate, SCF/bbl|1|Hydrogen Production Rate, nm�/bbl|1|SCF|NM3|1
REF|NCM_Target|7|Net Cash Margin, CurrencyCode/bbl|2|Net Cash Margin, CurrencyCode/bbl|2|||1
REF|OpAvailPlan_Target|90|Operational Planned Availability, %|1|Operational Planned Availability, %|1|||1
REF|OpAvailUnp_Target|95|Operational Unplanned Availability, %|1|Operational Unplanned Availability, %|1|||1
REF|Opex_Target|5|Operating Expenses, CurrencyCode/bbl|2|Operating Expenses, CurrencyCode/bbl|2|||1
REF|PersIndex_Target|15|Personnel Index, work hours/100 EDC|1|Personnel Index, work hours/100 EDC|1|||1
REF|PostPerShift_Target|70|Posts per Shift|1|Posts per Shift|1|||1
REF|ProdArom_Target|305|Total Product Aromatics, vol%|1|Total Product Aromatics, vol%|1|||1
REF|ProdBenzene_Target|310|Benzene, vol %|1|Benzene, vol %|1|||1
REF|ProdC5_Target|300|C5 + Reformate, vol % of fresh feed|1|C5 + Reformate, vol % of fresh feed|1|||1
REF|ProdN2A_Target|320|N + 2A, vol %|1|N + 2A, vol %|1|||1
REF|ProdRONC_Target|335|RONC|1|RONC|1|||1
REF|RegUnavail_Target|105|Mechanical - Operational Availability, %|1|Mechanical - Operational Availability, %|1|||1
REF|RoutMatlPcnt_Target|40|Non-Turnaround Material, %|1|Non-Turnaround Material, %|1|%|%|1
REF|SepPress_Target|340|Separator Drum Pressure, psig|1|Separator Drum Pressure, barg|1|PSIG|BARG|1
REF|StackOxy_Target|205|Stack Gas Oxygen Content, vol %|1|Stack Gas Oxygen Content, vol %|1|||1
REF|StackTemp_Target|200|Stack Gas Exit Temperature, �F|0|Stack Gas Exit Temperature, �C|1|TF|TC|1
REF|SumTempDiff_Target|330|Sum of the Temperature Differentials, �F|0|Sum of the Temperature Differentials, �C|1|||1
REF|TADaysDown_Target|50|Turnaround Days Down|1|Turnaround Days Down|1|||1
REF|TAIntDays_Target|45|Turnaround Interval, days|0|Turnaround Interval, days|0|||1
REF|TAMatlPcnt_Target|30|Turnaround Material, %|1|Turnaround Material, %|1|%|%|1
REF|UtilOSTA_Target|65|Utilization, % Outside of Turnaround|1|Utilization, % Outside of Turnaround|1|%|%|1
