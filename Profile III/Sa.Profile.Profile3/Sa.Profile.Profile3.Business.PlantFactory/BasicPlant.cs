
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sa.Profile.Profile3.Business.PlantUnits;

namespace Sa.Profile.Profile3.Business.PlantFactory
{
    
    public abstract class BasicPlant : SABusinessObject
    {
        private string _name;
        
        ~BasicPlant()
        {
        }
        public virtual int? ClientSubmissionID { get; set; }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }
    }
}
