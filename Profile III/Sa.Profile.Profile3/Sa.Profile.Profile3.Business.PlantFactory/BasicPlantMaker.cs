using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Sa.Profile.Profile3.Business.PlantFactory
{
    
    public class BasicPlantMaker : PlantMaker
    {
        public override BasicPlant CreatePlant(string type)
        {
            BasicPlant plant = null;
            string str = type;
            if ((str != null) && (str == "fuels"))
            {
                plant = new FuelsRefineryCompletePlant(new FuelsRefineryPlantUnitFactory(PlantLevel.COMPLETE));
            }
            return plant;
        }
        
        ~BasicPlantMaker()
        {
        }
    }
}
