using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Sa.Profile.Profile3.Business.PlantFactory
{
    
    public class BeerBreweryPlantUnitFactory : PlantUnitFactory
    {
        private int _barrelCapacity;
        private int _beerBottleThroughput;
        private int _maltLevel;
        private int _spiritLevel;
        private int _yeastLevel;
        
        ~BeerBreweryPlantUnitFactory()
        {
        }
        
        public int BarrelCapacity
        {
            get
            {
                return this._barrelCapacity;
            }
            set
            {
                this._barrelCapacity = value;
            }
        }
        
        public int BeerBottleThroughput
        {
            get
            {
                return this._beerBottleThroughput;
            }
            set
            {
                this._beerBottleThroughput = value;
            }
        }
        
        public int MaltLevel
        {
            get
            {
                return this._maltLevel;
            }
            set
            {
                this._maltLevel = value;
            }
        }
        
        public int SpiritLevel
        {
            get
            {
                return this._spiritLevel;
            }
            set
            {
                this._spiritLevel = value;
            }
        }
        
        public int YeastLevel
        {
            get
            {
                return this._yeastLevel;
            }
            set
            {
                this._yeastLevel = value;
            }
        }
    }
}
