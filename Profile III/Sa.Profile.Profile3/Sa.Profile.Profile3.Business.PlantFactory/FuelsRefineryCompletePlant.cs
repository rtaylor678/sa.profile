using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sa.Profile.Profile3.Business.PlantUnits;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace Sa.Profile.Profile3.Business.PlantFactory
{


    [Serializable, XmlInclude(typeof(FuelsRefineryLiquifiedPetroleumGasComplete)), XmlInclude(typeof(FuelsRefineryFiredHeatersComplete)), XmlInclude(typeof(FuelsRefineryGeneralMiscellaneousComplete)), XmlInclude(typeof(FuelsRefineryInventoryComplete)), XmlInclude(typeof(Fuels)), XmlInclude(typeof(FuelsRefineryMaintenanceExpenseAndCapitalComplete)), XmlInclude(typeof(MaterialBalanceBasic)), XmlInclude(typeof(FuelsRefineryMiscellaneousInputComplete)), XmlInclude(typeof(OperatingExpenseBasic)), XmlInclude(typeof(PersonnelAbsenceHours)), XmlInclude(typeof(PersonnelHoursBasic)), XmlInclude(typeof(RefineryProducedFuelResidual)), XmlInclude(typeof(RoutineMaintenanceBasic)), XmlInclude(typeof(FuelsRefinerySteamSystemComplete)), XmlInclude(typeof(TurnaroundMaintenanceBasic)), XmlInclude(typeof(FuelsRefineryLiquifiedPetroleumGasComplete)), XmlInclude(typeof(FuelsRefineryGasolineComplete)), XmlInclude(typeof(FuelsRefineryLiquifiedPetroleumGasComplete)), XmlInclude(typeof(FuelsRefineryConfigurationComplete)), XmlInclude(typeof(ConfigurationReceiptAndShipmentBasic)), XmlInclude(typeof(FuelsRefineryConfigurationBuoyComplete)), XmlInclude(typeof(CrudeBasic)), XmlInclude(typeof(Fuels)), XmlInclude(typeof(FuelsRefineryEmissionComplete)), XmlInclude(typeof(EnergyBasic))]
    public class FuelsRefineryCompletePlant : BasicPlant
    {
     //   private PlantUnitFactory _plantUnitFactory;
        private List<string> _brokenRules = null;
        //note, no need to pass parms to rule funcs because we can use vars in this class via "this."
        //hence simple bool returns on rules ok, however, if needed later can add parmed rules
        private List<string> _brokenRuleWarnings = null;


        public FuelsRefineryCompletePlant()
        {
        }

        public FuelsRefineryCompletePlant(PlantUnitFactory plantUnitFactory)
        {
            this.ConfigurationCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).ConfigurationList.ConvertAll<FuelsRefineryConfigurationComplete>(i => i as FuelsRefineryConfigurationComplete);
            this.GasolineCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).GasolineList.ConvertAll<FuelsRefineryGasolineComplete>(j => j as FuelsRefineryGasolineComplete);
            this.LiquifiedPetroleumGasCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).LiquifiedPetroleumGasList.Cast<FuelsRefineryLiquifiedPetroleumGasComplete>().ToList<FuelsRefineryLiquifiedPetroleumGasComplete>();
            this.ConfigurationBuoyCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).ConfigurationBuoyList.ConvertAll<FuelsRefineryConfigurationBuoyComplete>(k => k as FuelsRefineryConfigurationBuoyComplete);
            this.ConfigurationReceiptAndShipmentCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).ConfigurationReceiptAndShipmentList.ConvertAll<FuelsRefineryConfigurationReceiptAndShipmentComplete>(k => k as FuelsRefineryConfigurationReceiptAndShipmentComplete);
            //this.ConfigurationBuoyCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).ConfigurationBuoyList.ConvertAll<FuelsRefineryConfigurationBuoyComplete>(k => k as FuelsRefineryConfigurationBuoyComplete);
            this.CrudeCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).CrudeList.Cast<FuelsRefineryCrudeComplete>().ToList<FuelsRefineryCrudeComplete>();
            this.DieselCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).DieselList.ConvertAll<FuelsRefineryDieselComplete>(k => k as FuelsRefineryDieselComplete);
            this.EmissionCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).EmmissionList.ConvertAll<FuelsRefineryEmissionComplete>(k => k as FuelsRefineryEmissionComplete);
            this.EnergyCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).EnergyList.ConvertAll<FuelsRefineryEnergyComplete>(k => k as FuelsRefineryEnergyComplete);
            this.ElectricBasicList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).ElectricList.ConvertAll<FuelsRefineryEnergyElectricBasic>(k => k as FuelsRefineryEnergyElectricBasic);
            this.FiredHeatersCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).FiredHeatersList.ConvertAll<FuelsRefineryFiredHeatersComplete>(k => k as FuelsRefineryFiredHeatersComplete);
            this.GeneralMiscellaneousCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).GeneralMiscellaneousList.ConvertAll<FuelsRefineryGeneralMiscellaneousComplete>(k => k as FuelsRefineryGeneralMiscellaneousComplete);
            this.InventoryCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).InventoryList.ConvertAll<FuelsRefineryInventoryComplete>(k => k as FuelsRefineryInventoryComplete);
            this.KeroseneCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).KeroseneList.ConvertAll<FuelsRefineryKeroseneComplete>(k => k as FuelsRefineryKeroseneComplete);
            this.LiquifiedPetroleumGasCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).LiquifiedPetroleumGasList.ConvertAll<FuelsRefineryLiquifiedPetroleumGasComplete>(k => k as FuelsRefineryLiquifiedPetroleumGasComplete);
            this.MaintenanceExpenseAndCapitalCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).MaintenanceExpenseAndCapitalList.ConvertAll<FuelsRefineryMaintenanceExpenseAndCapitalComplete>(k => k as FuelsRefineryMaintenanceExpenseAndCapitalComplete);
            this.MaterialBalanceCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).MaterialBalanceList.ConvertAll<FuelsRefineryMaterialBalanceComplete>(k => k as FuelsRefineryMaterialBalanceComplete);
            this.MarineBunkersCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).MarineBunkerList.ConvertAll<FuelsRefineryMarineBunkersComplete>(k => k as FuelsRefineryMarineBunkersComplete);
            this.MiscellaneousInputCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).MiscellaneousInputList.ConvertAll<FuelsRefineryMiscellaneousInputComplete>(k => k as FuelsRefineryMiscellaneousInputComplete);
            this.OperatingExpenseStandardList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).OperatingExpenseList.ConvertAll<OperatingExpenseStandard>(k => k as OperatingExpenseStandard);
            this.PersonnelAbsenceHoursCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).PersonnelAbsenceHoursList.ConvertAll<FuelsRefineryPersonnelAbsenceHoursComplete>(k => k as FuelsRefineryPersonnelAbsenceHoursComplete);
            this.PersonnelHoursCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).PersonnelHoursList.ConvertAll<FuelsRefineryPersonnelHoursComplete>(k => k as FuelsRefineryPersonnelHoursComplete);
            this.RefineryProducedFuelResidualCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).RefineryProducedFuelResidualList.ConvertAll<FuelsRefineryProducedFuelResidualComplete>(k => k as FuelsRefineryProducedFuelResidualComplete);
            this.FinishedResidualFuelCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).FinishedResidualFuelList.ConvertAll<FuelsRefineryFinishedResidualFuelComplete>(k => k as FuelsRefineryFinishedResidualFuelComplete);
            this.RoutineMaintenanceCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).RoutineMaintenanceList.ConvertAll<FuelsRefineryRoutineMaintenanceComplete>(k => k as FuelsRefineryRoutineMaintenanceComplete);
            this.SteamSystemCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).SteamSystemList.ConvertAll<FuelsRefinerySteamSystemComplete>(k => k as FuelsRefinerySteamSystemComplete);
            this.TurnaroundMaintenanceCompleteList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).TurnaroundMaintenanceList.ConvertAll<FuelsRefineryTurnaroundMaintenanceComplete>(k => k as FuelsRefineryTurnaroundMaintenanceComplete);
            this.PlantOperatingConditionList = ((FuelsRefineryPlantUnitFactory)plantUnitFactory).PlantOperatingConditionList.ConvertAll<PlantOperatingConditionBasic>(k => k as PlantOperatingConditionBasic);
        }

        public void CompletePlant(PlantUnitFactory plantUnitFactory)
        {
            CreateRules();
        }

        ~FuelsRefineryCompletePlant()
        {
        }
        public int? SubmissionIDOverride { get; set; }

        public List<FuelsRefineryConfigurationBuoyComplete> ConfigurationBuoyCompleteList { get; set; }

        public List<FuelsRefineryConfigurationComplete> ConfigurationCompleteList { get; set; }

        public List<FuelsRefineryConfigurationReceiptAndShipmentComplete> ConfigurationReceiptAndShipmentCompleteList { get; set; }

        public List<FuelsRefineryCrudeComplete> CrudeCompleteList { get; set; }

        public List<FuelsRefineryDieselComplete> DieselCompleteList { get; set; }

        public List<FuelsRefineryEmissionComplete> EmissionCompleteList { get; set; }

        public List<FuelsRefineryEnergyComplete> EnergyCompleteList { get; set; }

        public List<FuelsRefineryEnergyElectricBasic> ElectricBasicList { get; set; }

        public List<FuelsRefineryFinishedResidualFuelComplete> FinishedResidualFuelCompleteList { get; set; }

        public List<FuelsRefineryFiredHeatersComplete> FiredHeatersCompleteList { get; set; }

        public List<FuelsRefineryGasolineComplete> GasolineCompleteList { get; set; }

        public List<FuelsRefineryGeneralMiscellaneousComplete> GeneralMiscellaneousCompleteList { get; set; }

        public List<FuelsRefineryInventoryComplete> InventoryCompleteList { get; set; }

        public List<FuelsRefineryKeroseneComplete> KeroseneCompleteList { get; set; }

        public List<FuelsRefineryLiquifiedPetroleumGasComplete> LiquifiedPetroleumGasCompleteList { get; set; }

        public List<FuelsRefineryMaintenanceExpenseAndCapitalComplete> MaintenanceExpenseAndCapitalCompleteList { get; set; }

        public List<FuelsRefineryMarineBunkersComplete> MarineBunkersCompleteList { get; set; }

        public List<FuelsRefineryMaterialBalanceComplete> MaterialBalanceCompleteList { get; set; }

        public List<FuelsRefineryMiscellaneousInputComplete> MiscellaneousInputCompleteList { get; set; }

        public List<OperatingExpenseStandard> OperatingExpenseStandardList { get; set; }

        public List<FuelsRefineryPersonnelAbsenceHoursComplete> PersonnelAbsenceHoursCompleteList { get; set; }

        public List<FuelsRefineryPersonnelHoursComplete> PersonnelHoursCompleteList { get; set; }

        public List<PlantOperatingConditionBasic> PlantOperatingConditionList { get; set; }

        public SubmissionBasic PlantSubmission { get; set; }

        public List<FuelsRefineryProducedFuelResidualComplete> RefineryProducedFuelResidualCompleteList { get; set; }

        public List<FuelsRefineryRoutineMaintenanceComplete> RoutineMaintenanceCompleteList { get; set; }

        public List<FuelsRefinerySteamSystemComplete> SteamSystemCompleteList { get; set; }

        public List<FuelsRefineryTurnaroundMaintenanceComplete> TurnaroundMaintenanceCompleteList { get; set; }

        public bool ValidatePlant(out List<string> brokenRules, out List<string> brokenRuleWarnings)
        {
            _brokenRules = new List<string>();
            _brokenRuleWarnings = new List<string>();         
            ReadOnlyCollection<SARule> broken = GetBrokenRules();
               foreach (SARule r in broken)
            {
                if (r.Relaxed)
                {
                    _brokenRuleWarnings.Add("WARNING: " + r.PropertyName + ":" + r.Description);
                }
                else
                {
                    _brokenRules.Add(r.PropertyName + ":" + r.Description);
                }
            }
            brokenRules = _brokenRules;
            brokenRuleWarnings = _brokenRuleWarnings;
            return (brokenRules.Count == 0);
        }

        protected override List<SARule> CreateRules()
        {
            List<SARule> rules = base.CreateRules();
            rules.Add(new SABasicRule("SubmissionBasic", "Errors detected.", ValidateSubmission, 1, false));
            rules.Add(new SABasicRule("PersonnelAbsenceHoursCompleteList", "FuelsRefineryPersonnelAbsenceHoursComplete Duplicate key CategoryID, SubmissionID detected.", ValidateAbsenceHoursList, 2, false));
            rules.Add(new SABasicRule("ConfigurationBuoyCompleteList", "FuelsRefineryConfigurationBuoyComplete Duplicate key SubmissionID, UnitID detected.", ValidateConfigurationBuoyList, 3, false));
            rules.Add(new SABasicRule("ConfigurationCompleteList", "FuelsRefineryConfigurationComplete Duplicate key SubmissionID, UnitID detected.", ValidateConfigurationList, 4, false));
            rules.Add(new SABasicRule("ConfigurationReceiptAndShipmentCompleteList", "FuelsRefineryConfigurationReceiptAndShipmentComplete Duplicate key SubmissionID, UnitID detected.", ValidateConfigurationReceiptAndShipmentsList, 5, false));
            rules.Add(new SABasicRule("CrudeCompleteList", "FuelsRefineryCrudeComplete Duplicate key SubmissionID, CrudeID detected.", ValidateCrudeList, 6, false));
            rules.Add(new SABasicRule("DieselCompleteList", "FuelsRefineryDieselComplete Duplicate key SubmissionID, BlendID detected.", ValidateDieselList, 7, false));
            rules.Add(new SABasicRule("EmissionCompleteList", "FuelsRefineryEmissionComplete Duplicate key SubmissionID, EmissionType detected.", ValidateEmissionList, 8, false));
            rules.Add(new SABasicRule("EnergyCompleteList", "FuelsRefineryEnergyComplete Duplicate key SubmissionID, TransactionCode detected.", ValidateEnergyList, 9, false));
            rules.Add(new SABasicRule("ElectricBasicList", "FuelsRefineryEnergyElectricBasic Duplicate key SubmissionID, TransactionCode detected.", ValidateEnergyElectricList, 10, false));
            rules.Add(new SABasicRule("FinishedResidualFuelCompleteList", "FuelsRefineryFinishedResidualFuelComplete Duplicate key SubmissionID, BlendID detected.", ValidateFinishedResidualFuelList, 11, false));
            rules.Add(new SABasicRule("FiredHeatersCompleteList", "FuelsRefineryFiredHeatersComplete Duplicate key SubmissionID, HeaterNumber detected.", ValidateFiredHeatersList, 12, false));
            rules.Add(new SABasicRule("GasolineCompleteList", "FuelsRefineryFiredHeatersComplete Duplicate key SubmissionID, BlendID detected.", ValidateGasolineList, 13, false));
            rules.Add(new SABasicRule("GeneralMiscellaneousCompleteList", "FuelsRefineryGeneralMiscellaneousComplete Duplicate key SubmissionID detected.", ValidateGeneralMiscellaneousList, 14, false));

            rules.Add(new SABasicRule("InventoryCompleteList", "FuelsRefineryInventoryComplete Duplicate key SubmissionID, TankType detected.", ValidateInventoryList, 15, false));
            rules.Add(new SABasicRule("KeroseneCompleteList", "FuelsRefineryKeroseneComplete Duplicate key SubmissionID, BlendID detected.", ValidateKeroseneList, 16, false));
            rules.Add(new SABasicRule("MaintenanceExpenseAndCapitalCompleteList", "FuelsRefineryMaintenanceExpenseAndCapitalComplete Duplicate key SubmissionID detected.", ValidateMaintenanceExpenseAndCapitalList, 17, false));
            rules.Add(new SABasicRule("MarineBunkersCompleteList", "FuelsRefineryMarineBunkersComplete Duplicate key SubmissionID, BlendID detected.", ValidateMarineBunkersList, 18, false));
            rules.Add(new SABasicRule("MaterialBalanceCompleteList", "FuelsRefineryMaterialBalanceComplete Duplicate key SubmissionID, Category, MaterialID, MaterialName detected.", ValidateMaterialBalanceList, 19, false));
            rules.Add(new SABasicRule("MiscellaneousInputCompleteList", "FuelsRefineryMiscellaneousInputComplete Duplicate key SubmissionID detected.", ValidateMiscellaneousInputList, 20, false));

            rules.Add(new SABasicRule("PersonnelHoursCompleteList", "FuelsRefineryPersonnelHoursComplete Duplicate key SubmissionID, PersonnelHoursID detected.", ValidatePersonnelHoursList, 21, false));
            rules.Add(new SABasicRule("RefineryProducedFuelResidualCompleteList", "FuelsRefineryProducedFuelResidualComplete Duplicate key SubmissionID, EnergyType detected.", ValidateRefineryProducedFuelResidualList, 22, false));
            rules.Add(new SABasicRule("RoutineMaintenanceCompleteList", "FuelsRefineryRoutineMaintenanceComplete Duplicate key SubmissionID, UnitID detected.", ValidateRoutineMaintenanceList, 23, false));
            rules.Add(new SABasicRule("SteamSystemCompleteList", "FuelsRefinerySteamSystemComplete Duplicate key SubmissionID, PressureRange detected.", ValidateSteamSystemList, 24, false));
            rules.Add(new SABasicRule("TurnaroundMaintenanceCompleteList", "FuelsRefineryTurnaroundMaintenanceComplete Duplicate key SubmissionID, UnitID detected.", ValidateTurnaroundMaintenanceList, 25, false));
            rules.Add(new SABasicRule("OperatingExpenseStandardList", "OperatingExpenseStandard Duplicate key SubmissionID, Property detected.", ValidateOperatingExpenseList, 26, true));

            rules.Add(new SABasicRule("PlantOperatingConditionList", "PlantOperatingConditionBasic Duplicate key SubmissionID, UnitID, Property detected.", ValidatePlantOperatingConditionList, 27, false));
            rules.Add(new SABasicRule("LiquifiedPetroleumGasCompleteList", "FuelsRefineryLiquifiedPetroleumGasComplete Duplicate key SubmissionID, BlendID, Property detected.", ValidateLiquifiedPetroleumGasList, 28, false));                                     
            return rules;
        }

        private bool LoadBusinessRules(List<SABusinessObject> business, int distinctCount)
        {
            bool noBrokenRules = true;
            if (business.Count == 0)
            {
                return true;
            }
            if (business.Where(x => x.SubmissionID != this.PlantSubmission.SubmissionID).Count() > 0)
            {
                _brokenRules.Add("Invalid SubmissionID detected for " + business[0].GetType().Name + "! (must be " + this.PlantSubmission.SubmissionID.ToString() + ")");
                return false;
            }
            //process all child rules, note we still have to do same for parent rule in its GetBrokenRules
            foreach (SABusinessObject h in business)
            {
                ReadOnlyCollection<SARule> broken = h.GetBrokenRules();             
                if (broken.Count > 0)
                {
                    noBrokenRules = false;
                    foreach (SARule r in broken)
                    {
                        if (r.Relaxed)
                        {
                            _brokenRuleWarnings.Add("WARNING: " + r.PropertyName + ":" + r.Description);
                     
                        }
                        else
                        {
                            _brokenRules.Add(r.PropertyName + ":" + r.Description);
                        }
                    }
                }
            }
            //only process distinct if all other mand rules are met
            if (noBrokenRules)
            {
                if (distinctCount != business.Count())
                {
                    noBrokenRules = false;
                }
            }
            return noBrokenRules;      
        }

        private bool ValidateAbsenceHoursList()
        {
            var distinctCount = this.PersonnelAbsenceHoursCompleteList.GroupBy(x => new { x.CategoryID, x.SubmissionID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.PersonnelAbsenceHoursCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);            
        }

        private bool ValidateConfigurationBuoyList()
        {
            var distinctCount = this.ConfigurationBuoyCompleteList.GroupBy(x => new { x.SubmissionID, x.UnitID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.ConfigurationBuoyCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateConfigurationList()
        {
            var distinctCount = this.ConfigurationCompleteList.GroupBy(x => new { x.SubmissionID, x.UnitID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.ConfigurationCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateConfigurationReceiptAndShipmentsList()
        {
            var distinctCount = this.ConfigurationReceiptAndShipmentCompleteList.GroupBy(x => new { x.SubmissionID, x.UnitID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.ConfigurationReceiptAndShipmentCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
                
        }

        private bool ValidateCrudeList()
        {
            var distinctCount = this.CrudeCompleteList.GroupBy(x => new { x.SubmissionID, x.CrudeID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.CrudeCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);

        }

        private bool ValidateDieselList()
        {
            var distinctCount = this.DieselCompleteList.GroupBy(x => new { x.SubmissionID, x.BlendID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.DieselCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);

        }

        private bool ValidateEmissionList()
        {
            var distinctCount = this.EmissionCompleteList.GroupBy(x => new { x.SubmissionID, x.EmissionType }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.EmissionCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateEnergyList()
        {
            var distinctCount = this.EnergyCompleteList.GroupBy(x => new { x.SubmissionID, x.TransactionCode }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.EnergyCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateEnergyElectricList()
        {
            var distinctCount = this.ElectricBasicList.GroupBy(x => new { x.SubmissionID, x.TransactionCode }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.ElectricBasicList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateFinishedResidualFuelList()
        {
            var distinctCount = this.FinishedResidualFuelCompleteList.GroupBy(x => new { x.SubmissionID, x.BlendID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.FinishedResidualFuelCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateFiredHeatersList()
        {
            var distinctCount = this.FiredHeatersCompleteList.GroupBy(x => new { x.SubmissionID, x.HeaterNumber }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.FiredHeatersCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateGasolineList()
        {
            var distinctCount = this.GasolineCompleteList.GroupBy(x => new { x.SubmissionID, x.BlendID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.GasolineCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateGeneralMiscellaneousList()
        {
            var distinctCount = this.GeneralMiscellaneousCompleteList.GroupBy(x => new { x.SubmissionID}).Select(x => x.Key).Count();
            return LoadBusinessRules(this.GeneralMiscellaneousCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateInventoryList()
        {
            var distinctCount = this.InventoryCompleteList.GroupBy(x => new { x.SubmissionID, x.TankType }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.InventoryCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateKeroseneList()
        {
            var distinctCount = this.KeroseneCompleteList.GroupBy(x => new { x.SubmissionID, x.BlendID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.KeroseneCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateMaintenanceExpenseAndCapitalList()
        {
            var distinctCount = this.MaintenanceExpenseAndCapitalCompleteList.GroupBy(x => new { x.SubmissionID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.MaintenanceExpenseAndCapitalCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateMarineBunkersList()
        {
            var distinctCount = this.MarineBunkersCompleteList.GroupBy(x => new { x.SubmissionID, x.BlendID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.MarineBunkersCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateMaterialBalanceList()
        {
            var distinctCount = this.MaterialBalanceCompleteList.GroupBy(x => new { x.SubmissionID, x.Category, x.MaterialID, x.MaterialName }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.MaterialBalanceCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateMiscellaneousInputList()
        {
            var distinctCount = this.MiscellaneousInputCompleteList.GroupBy(x => new { x.SubmissionID}).Select(x => x.Key).Count();
            return LoadBusinessRules(this.MiscellaneousInputCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidatePersonnelHoursList()
        {
            var distinctCount = this.PersonnelHoursCompleteList.GroupBy(x => new { x.SubmissionID, x.PersonnelHoursID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.PersonnelHoursCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateRefineryProducedFuelResidualList()
        {
            var distinctCount = this.RefineryProducedFuelResidualCompleteList.GroupBy(x => new { x.SubmissionID, x.EnergyType }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.RefineryProducedFuelResidualCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateRoutineMaintenanceList()
        {
            var distinctCount = this.RoutineMaintenanceCompleteList.GroupBy(x => new { x.SubmissionID, x.UnitID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.RoutineMaintenanceCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateSteamSystemList()
        {
            var distinctCount = this.SteamSystemCompleteList.GroupBy(x => new { x.SubmissionID, x.PressureRange }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.SteamSystemCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateTurnaroundMaintenanceList()
        {
            var distinctCount = this.TurnaroundMaintenanceCompleteList.GroupBy(x => new { x.SubmissionID, x.UnitID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.TurnaroundMaintenanceCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateOperatingExpenseList()
        {
            var distinctCount = this.OperatingExpenseStandardList.GroupBy(x => new { x.SubmissionID, x.Property }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.OperatingExpenseStandardList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        //process data
        private bool ValidatePlantOperatingConditionList()
        {
            var distinctCount = this.PlantOperatingConditionList.GroupBy(x => new { x.SubmissionID, x.UnitID, x.Property }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.PlantOperatingConditionList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateLiquifiedPetroleumGasList()
        {
            var distinctCount = this.LiquifiedPetroleumGasCompleteList.GroupBy(x => new { x.SubmissionID, x.BlendID }).Select(x => x.Key).Count();
            return LoadBusinessRules(this.LiquifiedPetroleumGasCompleteList.ConvertAll<SABusinessObject>(k => k as SABusinessObject),
                distinctCount);
        }

        private bool ValidateSubmission()
        {
            bool noBrokenRules = true;
            ReadOnlyCollection<SARule> broken = this.PlantSubmission.GetBrokenRules();
            if (broken.Count > 0)
            {
                noBrokenRules = false;
                foreach (SARule r in broken)
                {
                    _brokenRules.Add(r.PropertyName + ":" + r.Description);
                }
            }
            return noBrokenRules;
        }






    }
}
