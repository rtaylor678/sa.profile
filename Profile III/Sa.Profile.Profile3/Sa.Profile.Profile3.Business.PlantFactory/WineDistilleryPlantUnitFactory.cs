using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Sa.Profile.Profile3.Business.PlantFactory
{
   
    public class WineDistilleryPlantUnitFactory : PlantUnitFactory
    {
        private int _age;
        private List<string> _fruitExracts;
        private int _grapeConentrate;
        private int _redCapacity;
        private int _spiritLevel;
        private int _whiteCapacity;
        
        ~WineDistilleryPlantUnitFactory()
        {
        }
        
        public int Age
        {
            get
            {
                return this._age;
            }
            set
            {
                this._age = value;
            }
        }
        
        public List<string> FruitExracts
        {
            get
            {
                return this._fruitExracts;
            }
            set
            {
                this._fruitExracts = value;
            }
        }
        
        public int GrapeConentrate
        {
            get
            {
                return this._grapeConentrate;
            }
            set
            {
                this._grapeConentrate = value;
            }
        }
        
        public int RedCapacity
        {
            get
            {
                return this._redCapacity;
            }
            set
            {
                this._redCapacity = value;
            }
        }
        
        public int SpiritLevel
        {
            get
            {
                return this._spiritLevel;
            }
            set
            {
                this._spiritLevel = value;
            }
        }
        
        public int WhiteCapacity
        {
            get
            {
                return this._whiteCapacity;
            }
            set
            {
                this._whiteCapacity = value;
            }
        }
    }
}
