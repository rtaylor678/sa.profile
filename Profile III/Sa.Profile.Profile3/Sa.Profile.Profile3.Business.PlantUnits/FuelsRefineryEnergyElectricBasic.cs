///////////////////////////////////////////////////////////
//  FuelsRefineryEnergyElectricBasic.cs
//  Implementation of the Class FuelsRefineryEnergyElectricBasic
//  Generated by Enterprise Architect
//  Created on:      03-Nov-2014 10:06:42 AM
//  Original author: HJOHNSON
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;



using Sa.Profile.Profile3.Business.PlantUnits;
namespace Sa.Profile.Profile3.Business.PlantUnits {
	public class FuelsRefineryEnergyElectricBasic : EnergyBasic {

		public FuelsRefineryEnergyElectricBasic(){

		}

		~FuelsRefineryEnergyElectricBasic(){

		}

        protected override List<SARule> CreateRules()
        {
            List<SARule> rules = base.CreateRules();
            rules.Add(new SABasicRule("FuelsRefineryEnergyElectricBasic", "Missing required field: TransCode or SubmissionID.", MandatoryFieldCheck, 0, false));
            return rules;
        }

        private bool MandatoryFieldCheck()
        {
            return (this.TransactionCode != null
                //  && this.TransactionType != null
                && this.SubmissionID != null);
             //   && this.EnergyType!= null);
        }

		public decimal? GenerationEfficiency{
			get;
			set;
		}

	}//end FuelsRefineryEnergyElectricBasic

}//end namespace Sa.Profile.Profile3.Business.PlantUnits