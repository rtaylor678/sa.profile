///////////////////////////////////////////////////////////
//  FuelsRefineryInventoryComplete.cs
//  Implementation of the Class FuelsRefineryInventoryComplete
//  Generated by Enterprise Architect
//  Created on:      03-Nov-2014 10:06:43 AM
//  Original author: HJOHNSON
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;



using Sa.Profile.Profile3.Business.PlantUnits;
namespace Sa.Profile.Profile3.Business.PlantUnits {
	public class FuelsRefineryInventoryComplete : SABusinessObject {

		public FuelsRefineryInventoryComplete(){

		}

		~FuelsRefineryInventoryComplete(){

		}

		public decimal? AverageLevel{
			get;
			set;
		}

		public decimal? LeasedPercent{
			get;
			set;
		}

		public decimal? ManditoryStorageLevel{
			get;
			set;
		}

		public decimal? MarketingStorage{
			get;
			set;
		}

		public decimal? RefineryStorage{
			get;
			set;
		}

		public int? TankNumber{
			get;
			set;
		}

		public string TankType{
			get;
			set;
		}

		public decimal? TotalStorage{
			get;
			set;
		}

        protected override List<SARule> CreateRules()
        {
            List<SARule> rules = base.CreateRules();
            rules.Add(new SABasicRule("FuelsRefineryInventoryComplete", "Missing required field: TankType or SubmissionID.", MandatoryFieldCheck, 0, false));
            return rules;
        }

        private bool MandatoryFieldCheck()
        {
            return (this.TankType != null
                && this.SubmissionID != null);
        }

	}//end FuelsRefineryInventoryComplete

}//end namespace Sa.Profile.Profile3.Business.PlantUnits