///////////////////////////////////////////////////////////
//  FuelsRefineryMaintenanceExpenseAndCapitalComplete.cs
//  Implementation of the Class FuelsRefineryMaintenanceExpenseAndCapitalComplete
//  Generated by Enterprise Architect
//  Created on:      03-Nov-2014 10:06:43 AM
//  Original author: HJOHNSON
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;



using Sa.Profile.Profile3.Business.PlantUnits;
namespace Sa.Profile.Profile3.Business.PlantUnits {
	public class FuelsRefineryMaintenanceExpenseAndCapitalComplete : SABusinessObject {

		public FuelsRefineryMaintenanceExpenseAndCapitalComplete(){

		}

		~FuelsRefineryMaintenanceExpenseAndCapitalComplete(){

		}

		public decimal? RegulatoryExpense{
			get;
			set;
		}



		public decimal? Energy{
			get;
			set;
		}

		public decimal? MaintenanceExpenseRoutine{
			get;
			set;
		}

		public decimal? MaintenanceExpenseTurnaround{
			get;
			set;
		}

		public decimal? MaintenanceOverheadRoutine{
			get;
			set;
		}

		public decimal? MaintenanceOverheadTurnaround{
			get;
			set;
		}

		public decimal? NonMaintenenceInvestmentExpense{
			get;
			set;
		}

		public decimal? NonRefineryExclusions{
			get;
			set;
		}

		public decimal? NonRegulatoryNewProcessUnitConstruction{
			get;
			set;
		}

		public decimal? ConstraintRemoval{
			get;
			set;
		}

		public decimal? RegulatoryDiesel{
			get;
			set;
		}

		public decimal? TurnaroundMaintenanceCapital{
			get;
			set;
		}

		public decimal? RegulatoryGasoline{
			get;
			set;
		}

		public decimal? RegulatoryOther{
			get;
			set;
		}

		public decimal? RoutineMaintenanceCapital{
			get;
			set;
		}

		public decimal? SafetyOther{
			get;
			set;
		}

		public decimal? TotalComplexCapital{
			get;
			set;
		}

		public decimal? TotalMaintenance{
			get;
			set;
		}

		public decimal? TotalMaintenanceExpense{
			get;
			set;
		}

		public decimal? TotalMaintenanceOverhead{
			get;
			set;
		}

		public decimal? OtherCapitalInvestment{
			get;
			set;
		}
        protected override List<SARule> CreateRules()
        {
            List<SARule> rules = base.CreateRules();
            rules.Add(new SABasicRule("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "Missing required field: SubmissionID.", MandatoryFieldCheck, 0, false));
            return rules;
        }

        private bool MandatoryFieldCheck()
        {
            return (this.SubmissionID != null);
        }
	}//end FuelsRefineryMaintenanceExpenseAndCapitalComplete

}//end namespace Sa.Profile.Profile3.Business.PlantUnits