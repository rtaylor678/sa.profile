///////////////////////////////////////////////////////////
//  RoutineMaintenanceBasic.cs
//  Implementation of the Class RoutineMaintenanceBasic
//  Generated by Enterprise Architect
//  Created on:      03-Nov-2014 10:06:44 AM
//  Original author: HJOHNSON
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;



using Sa.Profile.Profile3.Business.PlantUnits;
namespace Sa.Profile.Profile3.Business.PlantUnits {
	public class RoutineMaintenanceBasic : SABusinessObject {

		public RoutineMaintenanceBasic(){

		}

		~RoutineMaintenanceBasic(){

		}

		public decimal? MaintenanceHoursDown{
			get;
			set;
		}

	}//end RoutineMaintenanceBasic

}//end namespace Sa.Profile.Profile3.Business.PlantUnits