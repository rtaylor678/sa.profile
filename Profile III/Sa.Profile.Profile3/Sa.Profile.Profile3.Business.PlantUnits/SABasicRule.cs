﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.Profile.Profile3.Business.PlantUnits
{
    public delegate bool BasicRuleDelegate();

    public class SABasicRule : SARule
    {
        private BasicRuleDelegate _ruleDelegate;

        public SABasicRule(string propertyName, string brokenDescription, BasicRuleDelegate ruleDelegate, int order, bool relaxed) :
            base(propertyName, brokenDescription, order, relaxed)
        {
            this._ruleDelegate = ruleDelegate;
        }

        public virtual BasicRuleDelegate RuleDelegate
        {
            get { return _ruleDelegate; }
            set { _ruleDelegate = value; }
        }

        public override bool ValidationRule(SABusinessObject saObj)
        {
            return RuleDelegate();
        }
    }
}
