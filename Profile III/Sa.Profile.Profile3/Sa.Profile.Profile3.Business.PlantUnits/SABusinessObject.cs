using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Sa.Profile.Profile3.Business.PlantUnits
{
    [Serializable]
	public abstract class SABusinessObject
    {
        private List<SARule> _rules;

        public SABusinessObject() { }

        public virtual bool IsValid
        {
            get { return this.Error == null; }
        }
        public virtual int? SubmissionID { get; set; }
       
        public virtual string Error 
        {
            get 
            {
                string result = this[string.Empty];
                if(result != null && result.Trim().Length ==0)
                {
                    result = null;
                }
                return result;
            }
        }

        public virtual string this[string propertyName]
        {
            get{
                string result = string.Empty;
                propertyName = CheckString(propertyName);
                foreach(SARule r in GetBrokenRules(propertyName))
                {
                    if(propertyName == string.Empty || r.PropertyName == propertyName)
                    {
                        result += r.Description;
                        result += Environment.NewLine;
                    }
                }
                result = result.Trim();
                if(result.Length == 0)
                {
                    result = null;
                }
                return result;
            }
        }

        public virtual ReadOnlyCollection<SARule> GetBrokenRules()
        {
            return GetBrokenRules(string.Empty);
        }

        public virtual ReadOnlyCollection<SARule> GetBrokenRules(string propertyIn)
        {
            string property = CheckString(propertyIn);
            if (_rules == null)
            {
                _rules = new List<SARule>();
                _rules.AddRange(this.CreateRules());

            }
            List<SARule> broken = new List<SARule>();

            foreach (SARule r in this._rules)
            {
                if (r.PropertyName == property || property == string.Empty)
                {
                    bool isRuleBroken = !r.ValidationRule(this);
                    System.Diagnostics.Debug.WriteLine(DateTime.Now.ToLongTimeString() + ": checking rule: '" + r.ToString() + " on SA '" + this.ToString() + "' Outcome= " + ((isRuleBroken == false) ? "Valid" : "Broken"));
                    if (isRuleBroken)
                    {
                        broken.Add(r);
                    }
                }
                
            }
            return broken.AsReadOnly();
        }

        protected virtual List<SARule> CreateRules()
        {
            return new List<SARule>();
        }

        protected String CheckString(String set)
        {
            return (set ?? string.Empty).Trim();
        }

        //note, can also add NotifyChanged() to raise prop changed events
    }
}
