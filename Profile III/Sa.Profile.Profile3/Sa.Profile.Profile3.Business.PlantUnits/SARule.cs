﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.Profile.Profile3.Business.PlantUnits
{
    public abstract class SARule
    {
        public virtual string Description { get; set; }
        public virtual string PropertyName { get; set; }
        public virtual int Order { get; set; }
        public virtual bool Relaxed { get; set; }

        public SARule(string propertyName, string brokenDescription, int order, bool relaxed)
        {
            this.Description = brokenDescription;
            this.PropertyName = propertyName;
            this.Order = order;
            this.Relaxed = relaxed;
        }

        public abstract bool ValidationRule(SABusinessObject saObj);

        public override string ToString()
        {
            return this.Description;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}
