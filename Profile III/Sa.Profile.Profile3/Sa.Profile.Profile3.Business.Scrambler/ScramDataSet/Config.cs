using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Config
    {
        public decimal? AllocPcntOfCap { get; set; }
        
        public string BlockOp { get; set; }
        
        public float? Cap { get; set; }
        
        public float? EnergyPcnt { get; set; }
        
        public decimal? InServicePcnt { get; set; }
        
        public float? MHPerWeek { get; set; }
        
        public decimal? PostPerShift { get; set; }
        
        public string ProcessID { get; set; }
        
        public string ProcessType { get; set; }
        
        public float? StmCap { get; set; }
        
        public decimal? StmUtilPcnt { get; set; }
        
        public int SubmissionID { get; set; }
        
        public int UnitID { get; set; }
        
        public string UnitName { get; set; }
        
        public decimal? UtilPcnt { get; set; }
        
        public decimal? YearsOper { get; set; }
    }
}
