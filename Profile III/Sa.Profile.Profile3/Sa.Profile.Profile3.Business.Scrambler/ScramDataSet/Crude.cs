using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Crude
    {
        public double BBL { get; set; }
        
        public string CNum { get; set; }
        
        public short CrudeID { get; set; }
        
        public string CrudeName { get; set; }
        
        public float? Gravity { get; set; }
        
        public string Period { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? Sulfur { get; set; }
    }
}
