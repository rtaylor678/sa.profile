using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Diesel
    {
        public float? ASTM90 { get; set; }
        
        public float? BiodieselPcnt { get; set; }
        
        public int BlendID { get; set; }
        
        public float? Cetane { get; set; }
        
        public float? CloudPt { get; set; }
        
        public float? Density { get; set; }
        
        public float? E350 { get; set; }
        
        public string Grade { get; set; }
        
        public float? KMT { get; set; }
        
        public string Market { get; set; }
        
        public float? PourPt { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? Sulfur { get; set; }
        
        public string Type { get; set; }
    }
}
