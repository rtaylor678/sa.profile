using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class FiredHeater
    {
        public float? AbsorbedDuty { get; set; }
        
        public float? CombAirTemp { get; set; }
        
        public float? FiredDuty { get; set; }
        
        public string FuelType { get; set; }
        
        public float? FurnInTemp { get; set; }
        
        public float? FurnOutTemp { get; set; }
        
        public string HeaterName { get; set; }
        
        public int HeaterNo { get; set; }
        
        public float? HeatLossPcnt { get; set; }
        
        public float? OthCombDuty { get; set; }
        
        public float? OtherDuty { get; set; }
        
        public float? ProcessDuty { get; set; }
        
        public string ProcessFluid { get; set; }
        
        public string ProcessID { get; set; }
        
        public string Service { get; set; }
        
        public float? ShaftDuty { get; set; }
        
        public float? StackO2 { get; set; }
        
        public float? StackTemp { get; set; }
        
        public float? SteamDuty { get; set; }
        
        public char? SteamSuperHeated { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? ThroughputRpt { get; set; }
        
        public string ThroughputUOM { get; set; }
        
        public int UnitID { get; set; }
    }
}
