using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MExp
    {
        public float? ComplexCptl { get; set; }
        
        public float? ConstraintRemoval { get; set; }
        
        public float? Energy { get; set; }
        
        public float? MaintExp { get; set; }
        
        public float? MaintExpRout { get; set; }
        
        public float? MaintExpTA { get; set; }
        
        public float? MaintOvhd { get; set; }
        
        public float? MaintOvhdRout { get; set; }
        
        public float? MaintOvhdTA { get; set; }
        
        public float? NonMaintInvestExp { get; set; }
        
        public float? NonRefExcl { get; set; }
        
        public float? NonRegUnit { get; set; }
        
        public float? OthInvest { get; set; }
        
        public float? RegDiesel { get; set; }
        
        public float? RegExp { get; set; }
        
        public float? RegGaso { get; set; }
        
        public float? RegOth { get; set; }
        
        public float? RoutMaintCptl { get; set; }
        
        public float? SafetyOth { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? TAMaintCptl { get; set; }
        
        public float? TotMaint { get; set; }
    }
}
