using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MaintRout
    {
        public float? InServicePcnt { get; set; }
        
        public float? MaintDown { get; set; }
        
        public short? MaintNum { get; set; }
        
        public float? OthDown { get; set; }
        
        public float? OthDownEconomic { get; set; }
        
        public float? OthDownExternal { get; set; }
        
        public float? OthDownOffsiteUpsets { get; set; }
        
        public float? OthDownOther { get; set; }
        
        public float? OthDownUnitUpsets { get; set; }
        
        public short? OthNum { get; set; }
        
        public float? OthSlow { get; set; }
        
        public string ProcessID { get; set; }
        
        public float? RegDown { get; set; }
        
        public short? RegNum { get; set; }
        
        public float? RoutCostLocal { get; set; }
        
        public float? RoutCptlLocal { get; set; }
        
        public float? RoutExpLocal { get; set; }
        
        public float? RoutOvhdLocal { get; set; }
        
        public int SubmissionID { get; set; }
        
        public int UnitID { get; set; }
        
        public float? UtilPcnt { get; set; }
    }
}
