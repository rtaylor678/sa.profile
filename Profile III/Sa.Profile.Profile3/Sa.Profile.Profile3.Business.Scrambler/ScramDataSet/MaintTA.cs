using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MaintTA
    {
        public DateTime? PrevTADate { get; set; }
        
        public string ProcessID { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? TAContMPS { get; set; }
        
        public float? TAContOCC { get; set; }
        
        public float? TACostLocal { get; set; }
        
        public float? TACptlLocal { get; set; }
        
        public DateTime? TADate { get; set; }
        
        public int? TAExceptions { get; set; }
        
        public float? TAExpLocal { get; set; }
        
        public float? TAHrsDown { get; set; }
        
        public float? TALaborCostLocal { get; set; }
        
        public float? TAMPSOVTPcnt { get; set; }
        
        public float? TAMPSSTH { get; set; }
        
        public float? TAOCCOVT { get; set; }
        
        public float? TAOCCSTH { get; set; }
        
        public float? TAOvhdLocal { get; set; }
        
        public int UnitID { get; set; }
    }
}
