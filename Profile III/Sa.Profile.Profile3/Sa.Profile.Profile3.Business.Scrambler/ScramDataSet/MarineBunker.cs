using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MarineBunker
    {
        public int BlendID { get; set; }
        
        public float? CrackedStock { get; set; }
        
        public float? Density { get; set; }
        
        public string Grade { get; set; }
        
        public float? KMT { get; set; }
        
        public float? PourPt { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? Sulfur { get; set; }
        
        public float? ViscCSAtTemp { get; set; }
        
        public float? ViscTemp { get; set; }
    }
}
