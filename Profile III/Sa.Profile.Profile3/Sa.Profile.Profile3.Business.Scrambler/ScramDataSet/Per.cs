using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    public class Per
    {
        public float? AbsHrs { get; set; }
        
        public float? Contract { get; set; }
        
        public float? GA { get; set; }
        
        public float? NumPers { get; set; }
        
        public float? OVTHours { get; set; }
        
        public float? OVTPcnt { get; set; }
        
        public string PersID { get; set; }
        
        public float? STH { get; set; }
        
        public int SubmissionID { get; set; }
    }
}
