﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Sa.Profile.Profile3.Common
{
    [Serializable]
    public class StudyException : Exception
    {
        public StudyException() { }

        public StudyException(string message) : base(message) { }

        public StudyException(string message, Exception inner) : base(message, inner) { }

        public StudyException(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }
}
