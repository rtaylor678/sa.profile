﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.Profile.Profile3.DataAccess.EF
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Data.Entity.Core.Objects;

    public partial class ProfileFuels12Entities : DbContext
    {
        public ProfileFuels12Entities()
            : base("name=ProfileFuels12Entities")
        {
        }
    
        public ProfileFuels12Entities(string connectionString)
            : base(connectionString)
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Absence> Absences { get; set; }
        public DbSet<Config> Configs { get; set; }
        public DbSet<ConfigR> ConfigRS { get; set; }
        public DbSet<Diesel> Diesels { get; set; }
        public DbSet<Emission> Emissions { get; set; }
        public DbSet<FiredHeater> FiredHeaters { get; set; }
        public DbSet<Gasoline> Gasolines { get; set; }
        public DbSet<GeneralMisc> GeneralMiscs { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<Kerosene> Kerosenes { get; set; }
        public DbSet<LPG> LPGs { get; set; }
        public DbSet<MaintTA> MaintTAs { get; set; }
        public DbSet<MarineBunker> MarineBunkers { get; set; }
        public DbSet<MExp> MExps { get; set; }
        public DbSet<OpEx> OpExes { get; set; }
        public DbSet<Per> Pers { get; set; }
        public DbSet<Resid> Resids { get; set; }
        public DbSet<RPFResid> RPFResids { get; set; }
        public DbSet<SteamSystem> SteamSystems { get; set; }
        public DbSet<ConfigBuoy> ConfigBuoys { get; set; }
        public DbSet<MiscInput> MiscInputs { get; set; }
        public DbSet<ProcessData> ProcessDatas { get; set; }
        public DbSet<Crude> Crudes { get; set; }
        public DbSet<MaintRout> MaintRouts { get; set; }
        public DbSet<Yield> Yields { get; set; }
        public DbSet<Electric> Electrics { get; set; }
        public DbSet<Energy> Energies { get; set; }
        public DbSet<Submission> Submissions { get; set; }
        public DbSet<OpExAll> OpExAlls { get; set; }
        public DbSet<PlantStudyLog> PlantStudyLogs { get; set; }
    
        public virtual ObjectResult<GetKPIResults_Result> GetKPIResults(Nullable<int> submissionID)
        {
            var submissionIDParameter = submissionID.HasValue ?
                new ObjectParameter("SubmissionID", submissionID) :
                new ObjectParameter("SubmissionID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetKPIResults_Result>("GetKPIResults", submissionIDParameter);
        }
    
        public virtual int CalcsForKPI(Nullable<int> submissionID, ObjectParameter stage)
        {
            var submissionIDParameter = submissionID.HasValue ?
                new ObjectParameter("SubmissionID", submissionID) :
                new ObjectParameter("SubmissionID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("CalcsForKPI", submissionIDParameter, stage);
        }
    }
}
