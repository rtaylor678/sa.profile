//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.Profile.Profile3.DataAccess.MapCheck
{
    using System;
    using System.Collections.Generic;
    
    public partial class TableAttributes_
    {
        public string TABLE { get; set; }
        public string MappedBusClassModel { get; set; }
        public string MappedBusClass { get; set; }
        public string MappedWCFClass { get; set; }
        public string FIELD { get; set; }
        public string MappedBusField { get; set; }
        public string MappedWCFField { get; set; }
        public string MappedType { get; set; }
        public string DataType { get; set; }
        public string KeyField { get; set; }
        public string DOMAIN_NAME { get; set; }
        public Nullable<double> ColPos { get; set; }
        public string Required { get; set; }
        public string Nullable { get; set; }
        public Nullable<double> MaxLength { get; set; }
        public Nullable<double> NumPrecision { get; set; }
        public string Reference_Table { get; set; }
        public string SAF_Tab { get; set; }
        public string MET_Units { get; set; }
        public string US_Units { get; set; }
        public string PL_Set { get; set; }
        public string Before { get; set; }
        public string After { get; set; }
        public string TermDefined { get; set; }
        public int id { get; set; }
    }
}
