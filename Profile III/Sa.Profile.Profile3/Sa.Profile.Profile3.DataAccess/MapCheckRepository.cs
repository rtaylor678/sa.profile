﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Sa.Profile.Profile3.DataAccess.MapCheck;

using Sa.Profile.Profile3.Business.Scrambler;
using Sa.Profile.Profile3.Business.PlantUnits;

namespace Sa.Profile.Profile3.DataAccess
{
    public enum FieldIncremMode
    {
        SMALL,
        MEDIUM,
        LARGE
    }

    public static class FieldIncrem
    {
        static int _intIncrem;
        static double _doubleIncrem;
        static int _currentStringSingleInt;
        static int _currentStringInt;
        static int _currentInt;
        static double _currentDouble;
        static DateTime _currentDateTime;

        private static void SetIncremMode(FieldIncremMode mode)
        {
            switch (mode)
            {
                case FieldIncremMode.SMALL:
                    _intIncrem = -100;
                    _doubleIncrem = .00000000000001;
                    break;
                case FieldIncremMode.MEDIUM:
                    _intIncrem = 10;
                    _doubleIncrem = 10000;
                    break;
                case FieldIncremMode.LARGE:
                    _intIncrem = 1000;
                    _doubleIncrem = 100000000;
                    break;
            }
        }

        public static void InitFieldIncrem(FieldIncremMode mode)
        {
            SetIncremMode(mode);
            _currentStringSingleInt = 1;
            _currentStringInt = 1;
            _currentInt =  _intIncrem;
            _currentDouble = _doubleIncrem;
            DateTime fix = new DateTime(2014, 7, 7);

            _currentDateTime = fix;
        } 

        static FieldIncrem()
        {
            _currentStringSingleInt = 1;
            _currentStringInt = 1;
            _currentInt = 1;
            _currentDouble = 1.0;
            DateTime fix = new DateTime(2014, 7, 7);
            _currentDateTime = DateTime.Now;          
        }

        public static int GetNextInt()
        {
            _currentInt = _currentInt + _intIncrem;
            return _currentInt;
        }

        public static float GetNextFloat()
        {
            _currentDouble = _currentDouble + _doubleIncrem;
            return (float)_currentDouble;
        }

        public static double GetNextDouble()
        {
            _currentDouble = _currentDouble + _doubleIncrem;
            return _currentDouble;
        }

        public static int GetNextSingleInt()
        {
            _currentStringSingleInt++;
            if (_currentStringSingleInt == 10)
            {
                _currentStringSingleInt = 1;
            }
            return _currentStringSingleInt;
        }

        public static string GetNextSingleString()
        {                      
            return GetNextSingleInt().ToString();
        }

        public static string GetNextString()
        {
            _currentStringInt++;
            return "STR" + _currentStringInt.ToString();
        }

        public static DateTime GetNextDate()
        {
            _currentDateTime = _currentDateTime.AddDays((double)1.0);
            return _currentDateTime;
        }
    }

    public class MapCheckRepository
    {
        public void PrimeDataRow(ref DataTable dt, ref DataRow dr)
        {        
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                switch (dt.Columns[i].DataType.Name)
                {
                    case "String":
                        if (dt.Columns[i].MaxLength <= 6)
                        {
                            dr[i] = FieldIncrem.GetNextSingleString();
                        }
                        else
                        {
                            dr[i] = FieldIncrem.GetNextString();
                        }
                        break;
                    case "Int16":
                        dr[i] = FieldIncrem.GetNextSingleInt();
                        break;                
                    case "Int32":
                        dr[i] = FieldIncrem.GetNextInt();
                        break;
                    case "Single":
                        dr[i] = FieldIncrem.GetNextFloat();
                        break;
                    case "Double":
                        dr[i] = FieldIncrem.GetNextDouble();
                        break;
                    case "DateTime":
                        dr[i] = FieldIncrem.GetNextDate();
                        break;
                    default:                    
                        throw new Exception("Type not found: " + dt.Columns[i].DataType.Name);
                }      
            }   
            dt.Rows.Add(dr);
        }

        //for testing
        public Sa.Profile.Profile3.Business.Scrambler.NewDataSet PrimeClientDataSetWithIncremData(out string errorMsg)
        {
            NewDataSet retVal = new Sa.Profile.Profile3.Business.Scrambler.NewDataSet();
            errorMsg = "";            
            try
            {
                DataTable dt = null;
                DataRow dr = null;             
   
                NewDataSet.AbsenceRow ar = retVal.Absence.NewAbsenceRow();
                dt = (DataTable)retVal.Absence;
                dr = (DataRow)ar;                
                PrimeDataRow(ref dt, ref dr);

                NewDataSet.ConfigRow cr = retVal.Config.NewConfigRow();
                dt = (DataTable)retVal.Config;
                dr = (DataRow)cr;                
                PrimeDataRow(ref dt, ref dr);

                NewDataSet.ConfigBuoyRow cb = retVal.ConfigBuoy.NewConfigBuoyRow();
                dt = (DataTable)retVal.ConfigBuoy;
                dr = (DataRow)cb;
                PrimeDataRow(ref dt, ref dr);

                NewDataSet.ConfigRSRow rs = retVal.ConfigRS.NewConfigRSRow();
                dt = (DataTable)retVal.ConfigRS;
                dr = (DataRow)rs;
                PrimeDataRow(ref dt, ref dr);

                //Crude
                NewDataSet.CrudeRow crr = retVal.Crude.NewCrudeRow();
                dt = (DataTable)retVal.Crude;
                dr = (DataRow)crr;
                PrimeDataRow(ref dt, ref dr);
                //Diesel
                NewDataSet.DieselRow drr = retVal.Diesel.NewDieselRow();
                dt = (DataTable)retVal.Diesel;
                dr = (DataRow)drr;
                PrimeDataRow(ref dt, ref dr);
                //Emissions
                NewDataSet.EmissionsRow em = retVal.Emissions.NewEmissionsRow();
                dt = (DataTable)retVal.Emissions;
                dr = (DataRow)em;
                PrimeDataRow(ref dt, ref dr);
                //Energy
                NewDataSet.EnergyRow en = retVal.Energy.NewEnergyRow();
                dt = (DataTable)retVal.Energy;
                dr = (DataRow)en;
                PrimeDataRow(ref dt, ref dr);
                //Electric, for now Electric is in Energy
                NewDataSet.EnergyRow el = retVal.Energy.NewEnergyRow();
                dt = (DataTable)retVal.Energy;
                dr = (DataRow)el;                
                PrimeDataRow(ref dt, ref dr);
                //save the electric vars generated so we can use them in Energy
                string electEnergyTypeField = dr["EnergyType"].ToString();
                string electGenEffField = dr["GenEff"].ToString();
                string electPriceLocalField = dr["PriceLocal"].ToString();

                dt.Rows[1]["EnergyType"] = "ELE";
                //we need to go back and edit the Energy record for the EnergyType, PriceLocal and GenEff
                //to make them the same as that of Elect since that is were these fields
                //are going, note, number is preserved for EnergyType so that we don't
                //see ELE in the Energy section as this is the flag used to break out Electric Data
                retVal.Energy[0]["EnergyType"] = electEnergyTypeField;
                retVal.Energy[0]["GenEff"] = electGenEffField;
                retVal.Energy[0]["PriceLocal"] = electPriceLocalField;

            //FiredHeaters
                NewDataSet.FiredHeatersRow fh = retVal.FiredHeaters.NewFiredHeatersRow();
                dt = (DataTable)retVal.FiredHeaters;
                dr = (DataRow)fh;
                PrimeDataRow(ref dt, ref dr);
            //Gasoline
                NewDataSet.GasolineRow gs = retVal.Gasoline.NewGasolineRow();
                dt = (DataTable)retVal.Gasoline;
                dr = (DataRow)gs;
                PrimeDataRow(ref dt, ref dr);
            //GeneralMisc
                NewDataSet.GeneralMiscRow gm = retVal.GeneralMisc.NewGeneralMiscRow();
                dt = (DataTable)retVal.GeneralMisc;
                dr = (DataRow)gm;
                PrimeDataRow(ref dt, ref dr);
            //Inventory
                NewDataSet.InventoryRow ir = retVal.Inventory.NewInventoryRow();
                dt = (DataTable)retVal.Inventory;
                dr = (DataRow)ir;
                PrimeDataRow(ref dt, ref dr);
            //Kerosene
                NewDataSet.KeroseneRow kr = retVal.Kerosene.NewKeroseneRow();
                dt = (DataTable)retVal.Kerosene;
                dr = (DataRow)kr;
                PrimeDataRow(ref dt, ref dr);
            //LPG
                NewDataSet.LPGRow lr = retVal.LPG.NewLPGRow();
                dt = (DataTable)retVal.LPG;
                dr = (DataRow)lr;
                PrimeDataRow(ref dt, ref dr);
            //MaintRout
                NewDataSet.MaintRoutRow mr = retVal.MaintRout.NewMaintRoutRow();
                dt = (DataTable)retVal.MaintRout;
                dr = (DataRow)mr;
                PrimeDataRow(ref dt, ref dr);
            //MaintTA
                NewDataSet.MaintTARow mt = retVal.MaintTA.NewMaintTARow();
                dt = (DataTable)retVal.MaintTA;
                dr = (DataRow)mt;
                PrimeDataRow(ref dt, ref dr);
            //MarineBunkers
                NewDataSet.MarineBunkersRow mbr = retVal.MarineBunkers.NewMarineBunkersRow();
                dt = (DataTable)retVal.MarineBunkers;
                dr = (DataRow)mbr;
                PrimeDataRow(ref dt, ref dr);
            //MExp
                NewDataSet.MExpRow me = retVal.MExp.NewMExpRow();
                dt = (DataTable)retVal.MExp;
                dr = (DataRow)me;
                PrimeDataRow(ref dt, ref dr);
            //MiscInput
                NewDataSet.MiscInputRow mr2 = retVal.MiscInput.NewMiscInputRow();
                dt = (DataTable)retVal.MiscInput;
                dr = (DataRow)mr2;
                PrimeDataRow(ref dt, ref dr);
            //OpexData
                NewDataSet.OpexDataRow or = retVal.OpexData.NewOpexDataRow();
                dt = (DataTable)retVal.OpexData;
                dr = (DataRow)or;
                PrimeDataRow(ref dt, ref dr);
            //Pers
                NewDataSet.PersRow pr = retVal.Pers.NewPersRow();
                dt = (DataTable)retVal.Pers;
                dr = (DataRow)pr;
                PrimeDataRow(ref dt, ref dr);
            //ProcessData
                NewDataSet.ProcessDataRow pd = retVal.ProcessData.NewProcessDataRow();
                dt = (DataTable)retVal.ProcessData;
                dr = (DataRow)pd;
                PrimeDataRow(ref dt, ref dr);
            //Resid
                NewDataSet.ResidRow rr = retVal.Resid.NewResidRow();
                dt = (DataTable)retVal.Resid;
                dr = (DataRow)rr;
                PrimeDataRow(ref dt, ref dr);
            //RPFResid
                NewDataSet.RPFResidRow rf= retVal.RPFResid.NewRPFResidRow();
                dt = (DataTable)retVal.RPFResid;
                dr = (DataRow)rf;
                PrimeDataRow(ref dt, ref dr);
            //SteamSystem
                NewDataSet.SteamSystemRow sr = retVal.SteamSystem.NewSteamSystemRow();
                dt = (DataTable)retVal.SteamSystem;
                dr = (DataRow)sr;
                PrimeDataRow(ref dt, ref dr);
            //Submissions
                NewDataSet.SubmissionRow srr = retVal.Submission.NewSubmissionRow();
                dt = (DataTable)retVal.Submission;
                dr = (DataRow)srr;
                PrimeDataRow(ref dt, ref dr);
            //Yield
                NewDataSet.YieldRow yr = retVal.Yield.NewYieldRow();
                dt = (DataTable)retVal.Yield;
                dr = (DataRow)yr;
                PrimeDataRow(ref dt, ref dr);
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }
            return retVal;
        }

        public bool PrimeBeforeWithClientDataSetData(NewDataSet before, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                foreach (DataTable table in before.Tables)
                {
                    string tableName = table.TableName;
                    if (tableName == "Submission")
                    {
                        tableName = "Submissions";
                    }
                    DataRow row = table.Rows[0];
                    string fieldName = null;
                    for (int i = 0; i < table.Columns.Count; i++)
                    { 
                        fieldName = table.Columns[i].ColumnName;
                        string str = row[i].ToString();

                        using (P12Map map = new P12Map())
                        {
                            TableAttributes_ s_ = (from a in map.TableAttributes_
                                                   where (a.TABLE == tableName) && (a.FIELD == fieldName)
                                                   select a).FirstOrDefault<TableAttributes_>();
                            if (s_ == null)
                            {
                                errorMsg = "could not locate: " + tableName + "." + fieldName;
                                break;
                            }
                            if (s_.MappedType == "decimal")
                            {
                                s_.Before = ((decimal)float.Parse(str)).ToString();
                            }
                            else
                            {
                                s_.Before = str;
                            }
                            map.SaveChanges();
                        }
                    }
                    //Row two of this table is our Electric row
                    if (tableName == "Energy")
                    {
                        DataRow row2 = table.Rows[1];
                        string electricFields = ",SubmissionID,EnergyType,TransType,Amount,PriceLocal,GenEff,";
                        tableName = "Electric";

                        for (int i = 0; i < table.Columns.Count; i++)
                        {
                            string fieldNameTest = "," + table.Columns[i].ColumnName + ",";
                            if (electricFields.IndexOf(fieldNameTest, 0) < 0)
                            {
                                continue;
                            }
                            string str = row2[i].ToString();
                            fieldName = table.Columns[i].ColumnName;
                            using (P12Map map = new P12Map())
                            {
                                TableAttributes_ s_ = (from a in map.TableAttributes_
                                                       where (a.TABLE == tableName) && (a.FIELD == fieldName)
                                                       select a).FirstOrDefault<TableAttributes_>();
                                if (s_ == null)
                                {
                                    errorMsg = "could not locate: " + tableName + "." + fieldName;
                                    break;
                                }
                                if (s_.MappedType == "decimal")
                                {
                                    s_.Before = ((decimal)float.Parse(str)).ToString();
                                }
                                else
                                {
                                    s_.Before = str;
                                }
                                map.SaveChanges();
                            }
                        }
                    }  
                }
            }
            catch (Exception exception)
            {
                errorMsg = exception.Message;
            }
            return (errorMsg.Length == 0);
        }

        public void ClearAllBeforeFields()
        {
            using (P12Map mapB = new P12Map())
            {
                object[] parmsB = new object[]{ "" };
                mapB.Database.ExecuteSqlCommand("Update TableAttributes$ set Before={0} where Before is not null;", parmsB);
            }
        }

        public void ClearAllAfterFields()
        {
            using (P12Map mapA = new P12Map())
            {
                object[] parmsA = new object[] { "" };
                mapA.Database.ExecuteSqlCommand("Update TableAttributes$ set After={0} where After is not null;", parmsA);
            }
        }

        public bool BeforeValuesNotEqualToAfterValues()
        {
            bool notEqual;
            using (P12Map mapA = new P12Map())
            {
                var howManyNotEqual = (from a in mapA.TableAttributes_
                                       where a.Before != a.After
                                       select a).ToList();
                notEqual = (howManyNotEqual.Count > 0);            

            }
            return notEqual;
        }

        public void InjectAFterBusinessObjectData(string mappedBusClass, string mappedBusField, object fieldValue)
        {
            using (P12Map map = new P12Map())
            {
                TableAttributes_ un = (from a in map.TableAttributes_
                                       where a.MappedBusClass == mappedBusClass &&
                                        a.MappedBusField == mappedBusField
                                        select a).FirstOrDefault();
                if (un == null)
                {
                    throw new Exception("could not locate: " + mappedBusClass + "." + mappedBusField);
                }
                if (fieldValue == null)
                {
                    un.After = "";
                }
                else
                {
                    un.After = fieldValue.ToString();
                }
                map.SaveChanges();
            }
        }

        public bool PrimeAfterWithBusinessObjectData(Sa.Profile.Profile3.Business.PlantFactory.FuelsRefineryCompletePlant after, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelAbsenceHoursComplete", "ManagementProfessionalAndStaffAbsence", after.PersonnelAbsenceHoursCompleteList[0].ManagementProfessionalAndStaffAbsence);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelAbsenceHoursComplete", "OperatorCraftAndClericalAbsence", after.PersonnelAbsenceHoursCompleteList[0].OperatorCraftAndClericalAbsence);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelAbsenceHoursComplete", "CategoryID", after.PersonnelAbsenceHoursCompleteList[0].CategoryID);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelAbsenceHoursComplete", "SubmissionID", after.PersonnelAbsenceHoursCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "EnergyPercent", after.ConfigurationCompleteList[0].EnergyPercent);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "UtilizationPercent", after.ConfigurationCompleteList[0].UtilizationPercent);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "Capacity", after.ConfigurationCompleteList[0].Capacity);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "ProcessType", after.ConfigurationCompleteList[0].ProcessType);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "UnitName", after.ConfigurationCompleteList[0].UnitName);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "ProcessID", after.ConfigurationCompleteList[0].ProcessID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "UnitID", after.ConfigurationCompleteList[0].UnitID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "SubmissionID", after.ConfigurationCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "AllocatedPercentOfCapacity", after.ConfigurationCompleteList[0].AllocatedPercentOfCapacity);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "BlockOperated", after.ConfigurationCompleteList[0].BlockOperated);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "PostPerShift", after.ConfigurationCompleteList[0].PostPerShift);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "ManHourWeek", after.ConfigurationCompleteList[0].ManHourWeek);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "YearsInOperation", after.ConfigurationCompleteList[0].YearsInOperation);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "InServicePercent", after.ConfigurationCompleteList[0].InServicePercent);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "SteamUtilizationPercent", after.ConfigurationCompleteList[0].SteamUtilizationPercent);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationComplete", "SteamCapacity", after.ConfigurationCompleteList[0].SteamCapacity);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationBuoyComplete", "PercentOwnership", after.ConfigurationBuoyCompleteList[0].PercentOwnership);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationBuoyComplete", "LineSize", after.ConfigurationBuoyCompleteList[0].LineSize);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationBuoyComplete", "ShipCapacity", after.ConfigurationBuoyCompleteList[0].ShipCapacity);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationBuoyComplete", "ProcessID", after.ConfigurationBuoyCompleteList[0].ProcessID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationBuoyComplete", "UnitName", after.ConfigurationBuoyCompleteList[0].UnitName);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationBuoyComplete", "UnitID", after.ConfigurationBuoyCompleteList[0].UnitID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationBuoyComplete", "SubmissionID", after.ConfigurationBuoyCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryCrudeComplete ", "Sulfur", after.CrudeCompleteList[0].Sulfur);
                InjectAFterBusinessObjectData("FuelsRefineryCrudeComplete ", "Gravity", after.CrudeCompleteList[0].Gravity);
                InjectAFterBusinessObjectData("FuelsRefineryCrudeComplete ", "Barrels", after.CrudeCompleteList[0].Barrels);
                InjectAFterBusinessObjectData("FuelsRefineryCrudeComplete ", "CrudeName", after.CrudeCompleteList[0].CrudeName);
                InjectAFterBusinessObjectData("FuelsRefineryCrudeComplete ", "CrudeNumber", after.CrudeCompleteList[0].CrudeNumber);
                InjectAFterBusinessObjectData("FuelsRefineryCrudeComplete ", "CrudeID", after.CrudeCompleteList[0].CrudeID);
                InjectAFterBusinessObjectData("FuelsRefineryCrudeComplete ", "Period", after.CrudeCompleteList[0].Period);
                InjectAFterBusinessObjectData("FuelsRefineryCrudeComplete ", "SubmissionID", after.CrudeCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationReceiptAndShipmentComplete", "Throughput", after.ConfigurationReceiptAndShipmentCompleteList[0].Throughput);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationReceiptAndShipmentComplete", "AverageSize", after.ConfigurationReceiptAndShipmentCompleteList[0].AverageSize);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationReceiptAndShipmentComplete", "ProcessType", after.ConfigurationReceiptAndShipmentCompleteList[0].ProcessType);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationReceiptAndShipmentComplete", "ProcessID", after.ConfigurationReceiptAndShipmentCompleteList[0].ProcessID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationReceiptAndShipmentComplete", "UnitID", after.ConfigurationReceiptAndShipmentCompleteList[0].UnitID);
                InjectAFterBusinessObjectData("FuelsRefineryConfigurationReceiptAndShipmentComplete", "SubmissionID", after.ConfigurationReceiptAndShipmentCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "ThousandMetricTons", after.DieselCompleteList[0].ThousandMetricTons);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "Type", after.DieselCompleteList[0].Type);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "Market", after.DieselCompleteList[0].Market);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "Grade", after.DieselCompleteList[0].Grade);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "BlendID", after.DieselCompleteList[0].BlendID);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "SubmissionID", after.DieselCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "BiodieselInBlendPercent", after.DieselCompleteList[0].BiodieselInBlendPercent);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "PercentEvaporatedAt350C", after.DieselCompleteList[0].PercentEvaporatedAt350C);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "AmericanStandardForTestingMaterials90DistillationPoint", after.DieselCompleteList[0].AmericanStandardForTestingMaterials90DistillationPoint);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "CloudPointOfBlend", after.DieselCompleteList[0].CloudPointOfBlend);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "Sulfur", after.DieselCompleteList[0].Sulfur);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "PourPointOfBlend", after.DieselCompleteList[0].PourPointOfBlend);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "Cetane", after.DieselCompleteList[0].Cetane);
                InjectAFterBusinessObjectData("FuelsRefineryDieselComplete", "Density", after.DieselCompleteList[0].Density);
                InjectAFterBusinessObjectData("FuelsRefineryEmissionComplete", "RefineryEmission", after.EmissionCompleteList[0].RefineryEmission);
                InjectAFterBusinessObjectData("FuelsRefineryEmissionComplete", "EmissionType", after.EmissionCompleteList[0].EmissionType);
                InjectAFterBusinessObjectData("FuelsRefineryEmissionComplete", "SubmissionID", after.EmissionCompleteList[0].SubmissionID);
             
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "N2", after.EnergyCompleteList[0].N2);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Butane", after.EnergyCompleteList[0].Butane);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Propylene", after.EnergyCompleteList[0].Propylene);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Propane", after.EnergyCompleteList[0].Propane);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Ethylene", after.EnergyCompleteList[0].Ethylene);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Ethane", after.EnergyCompleteList[0].Ethane);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Methane", after.EnergyCompleteList[0].Methane);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Hydrogen", after.EnergyCompleteList[0].Hydrogen);
              //moved to elect  
                //InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "GenerationEfficiency", after.EnergyCompleteList[0].GenerationEfficiency);
                InjectAFterBusinessObjectData("electric_2", "GenerationEfficiency", after.ElectricBasicList[0].GenerationEfficiency);
              
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "OverrideCalculation", after.EnergyCompleteList[0].OverrideCalculation);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "MillionBritishThermalUnitsOut", after.EnergyCompleteList[0].MillionBritishThermalUnitsOut);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "CO2", after.EnergyCompleteList[0].CO2);
              //moved to elect  
                 InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "PriceLocal", after.ElectricBasicList[0].PriceLocal);
           
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Amount", after.EnergyCompleteList[0].Amount);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "TransactionType", after.EnergyCompleteList[0].TransactionType);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "EnergyType", after.EnergyCompleteList[0].EnergyType);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "SubmissionID", after.EnergyCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "SO2", after.EnergyCompleteList[0].SO2);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "NH3", after.EnergyCompleteList[0].NH3);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "CO", after.EnergyCompleteList[0].CO);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "H2S", after.EnergyCompleteList[0].H2S);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "C5Plus", after.EnergyCompleteList[0].C5Plus);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Butylenes", after.EnergyCompleteList[0].Butylenes);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyComplete", "Isobutane", after.EnergyCompleteList[0].Isobutane);
//electric
                InjectAFterBusinessObjectData("FuelsRefineryEnergyElectricBasic", "SubmissionID", after.ElectricBasicList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyElectricBasic", "EnergyType", after.ElectricBasicList[0].EnergyType);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyElectricBasic", "TransactionType", after.ElectricBasicList[0].TransactionType);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyElectricBasic", "Amount", after.ElectricBasicList[0].Amount);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyElectricBasic", "PriceLocal", after.ElectricBasicList[0].PriceLocal);
                InjectAFterBusinessObjectData("FuelsRefineryEnergyElectricBasic", "GenerationEfficiency", after.ElectricBasicList[0].GenerationEfficiency);


                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "OtherDuty", after.FiredHeatersCompleteList[0].OtherDuty);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "StackTemperature", after.FiredHeatersCompleteList[0].StackTemperature);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "FurnaceOutTemperature", after.FiredHeatersCompleteList[0].FurnaceOutTemperature);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "FurnaceInTemperature", after.FiredHeatersCompleteList[0].FurnaceInTemperature);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "OtherCombustionDuty", after.FiredHeatersCompleteList[0].OtherCombustionDuty);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "FuelType", after.FiredHeatersCompleteList[0].FuelType);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "FiredDuty", after.FiredHeatersCompleteList[0].FiredDuty);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "ThroughputUnitOfMeasure", after.FiredHeatersCompleteList[0].ThroughputUnitOfMeasure);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "ThroughputReport", after.FiredHeatersCompleteList[0].ThroughputReport);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "ProcessFluid", after.FiredHeatersCompleteList[0].ProcessFluid);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "Service", after.FiredHeatersCompleteList[0].Service);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "ShaftDuty", after.FiredHeatersCompleteList[0].ShaftDuty);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "HeaterName", after.FiredHeatersCompleteList[0].HeaterName);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "ProcessID", after.FiredHeatersCompleteList[0].ProcessID);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "UnitID", after.FiredHeatersCompleteList[0].UnitID);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "HeaterNumber", after.FiredHeatersCompleteList[0].HeaterNumber);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "SubmissionID", after.FiredHeatersCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "SteamSuperHeated", after.FiredHeatersCompleteList[0].SteamSuperHeated);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "SteamDuty", after.FiredHeatersCompleteList[0].SteamDuty);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "ProcessDuty", after.FiredHeatersCompleteList[0].ProcessDuty);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "AbsorbedDuty", after.FiredHeatersCompleteList[0].AbsorbedDuty);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "CombustionAirTemperature", after.FiredHeatersCompleteList[0].CombustionAirTemperature);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "HeatLossPercent", after.FiredHeatersCompleteList[0].HeatLossPercent);
                InjectAFterBusinessObjectData("FuelsRefineryFiredHeatersComplete", "StackO2", after.FiredHeatersCompleteList[0].StackO2);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "ThousandMetricTons", after.GasolineCompleteList[0].ThousandMetricTons);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "MotorOctaneNumber", after.GasolineCompleteList[0].MotorOctaneNumber);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "ResearchOctaneNumber", after.GasolineCompleteList[0].ResearchOctaneNumber);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "ReidVaporPressure", after.GasolineCompleteList[0].ReidVaporPressure);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "Density", after.GasolineCompleteList[0].Density);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "Type", after.GasolineCompleteList[0].Type);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "Market", after.GasolineCompleteList[0].Market);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "Grade", after.GasolineCompleteList[0].Grade);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "BlendID", after.GasolineCompleteList[0].BlendID);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "SubmissionID", after.GasolineCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "Lead", after.GasolineCompleteList[0].Lead);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "Sulfur", after.GasolineCompleteList[0].Sulfur);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "OtherOxygen", after.GasolineCompleteList[0].OtherOxygen);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "TertAmylMethylEther", after.GasolineCompleteList[0].TertAmylMethylEther);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "EthylTertButylEther", after.GasolineCompleteList[0].EthylTertButylEther);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "MethylTertButylEther", after.GasolineCompleteList[0].MethylTertButylEther);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "Ethanol", after.GasolineCompleteList[0].Ethanol);
                InjectAFterBusinessObjectData("FuelsRefineryGasolineComplete", "Oxygen", after.GasolineCompleteList[0].Oxygen);
                InjectAFterBusinessObjectData("FuelsRefineryGeneralMiscellaneousComplete", "TotalLossMetricTons", after.GeneralMiscellaneousCompleteList[0].TotalLossMetricTons);
                InjectAFterBusinessObjectData("FuelsRefineryGeneralMiscellaneousComplete", "FlareLossMetricTons", after.GeneralMiscellaneousCompleteList[0].FlareLossMetricTons);
                InjectAFterBusinessObjectData("FuelsRefineryGeneralMiscellaneousComplete", "SubmissionID", after.GeneralMiscellaneousCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "AverageLevel", after.InventoryCompleteList[0].AverageLevel);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "LeasedPercent", after.InventoryCompleteList[0].LeasedPercent);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "TankNumber", after.InventoryCompleteList[0].TankNumber);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "RefineryStorage", after.InventoryCompleteList[0].RefineryStorage);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "ManditoryStorageLevel", after.InventoryCompleteList[0].ManditoryStorageLevel);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "MarketingStorage", after.InventoryCompleteList[0].MarketingStorage);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "TotalStorage", after.InventoryCompleteList[0].TotalStorage);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "TankType", after.InventoryCompleteList[0].TankType);
                InjectAFterBusinessObjectData("FuelsRefineryInventoryComplete", "SubmissionID", after.InventoryCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryKeroseneComplete", "ThousandMetricTons", after.KeroseneCompleteList[0].ThousandMetricTons);
                InjectAFterBusinessObjectData("FuelsRefineryKeroseneComplete", "Sulfur", after.KeroseneCompleteList[0].Sulfur);
                InjectAFterBusinessObjectData("FuelsRefineryKeroseneComplete", "Density", after.KeroseneCompleteList[0].Density);
                InjectAFterBusinessObjectData("FuelsRefineryKeroseneComplete", "Type", after.KeroseneCompleteList[0].Type);
                InjectAFterBusinessObjectData("FuelsRefineryKeroseneComplete", "Grade", after.KeroseneCompleteList[0].Grade);
                InjectAFterBusinessObjectData("FuelsRefineryKeroseneComplete", "BlendID", after.KeroseneCompleteList[0].BlendID);
                InjectAFterBusinessObjectData("FuelsRefineryKeroseneComplete", "SubmissionID", after.KeroseneCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "VolumeC5Plus", after.LiquifiedPetroleumGasCompleteList[0].VolumeC5Plus);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "SubmissionID", after.LiquifiedPetroleumGasCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "VolumeC4ene", after.LiquifiedPetroleumGasCompleteList[0].VolumeC4ene);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "VolumenC4", after.LiquifiedPetroleumGasCompleteList[0].VolumenC4);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "VolumeiC4", after.LiquifiedPetroleumGasCompleteList[0].VolumeiC4);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "VolumeC3ene", after.LiquifiedPetroleumGasCompleteList[0].VolumeC3ene);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "VolumeC3", after.LiquifiedPetroleumGasCompleteList[0].VolumeC3);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "VolumeC2LT", after.LiquifiedPetroleumGasCompleteList[0].VolumeC2LT);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "MoleOrVolume", after.LiquifiedPetroleumGasCompleteList[0].MoleOrVolume);
                InjectAFterBusinessObjectData("FuelsRefineryLiquifiedPetroleumGasComplete", "BlendID", after.LiquifiedPetroleumGasCompleteList[0].BlendID);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "ThousandMetricTons", after.MarineBunkersCompleteList[0].ThousandMetricTons);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "SubmissionID", after.MarineBunkersCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "CrackedStock", after.MarineBunkersCompleteList[0].CrackedStock);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "ViscosityOfBlendTemperatureDifferent122", after.MarineBunkersCompleteList[0].ViscosityOfBlendTemperatureDifferent122);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "ViscosityOfBlendCentiStokeAtTemperature", after.MarineBunkersCompleteList[0].ViscosityOfBlendCentiStokeAtTemperature);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "PourPointOfBlend", after.MarineBunkersCompleteList[0].PourPointOfBlend);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "Sulfur", after.MarineBunkersCompleteList[0].Sulfur);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "Density", after.MarineBunkersCompleteList[0].Density);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "Grade", after.MarineBunkersCompleteList[0].Grade);
                InjectAFterBusinessObjectData("FuelsRefineryMarineBunkersComplete", "BlendID", after.MarineBunkersCompleteList[0].BlendID);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "TotalMaintenanceExpense", after.MaintenanceExpenseAndCapitalCompleteList[0].TotalMaintenanceExpense);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "SafetyOther", after.MaintenanceExpenseAndCapitalCompleteList[0].SafetyOther);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "RegulatoryOther", after.MaintenanceExpenseAndCapitalCompleteList[0].RegulatoryOther);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "RegulatoryDiesel", after.MaintenanceExpenseAndCapitalCompleteList[0].RegulatoryDiesel);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "RegulatoryGasoline", after.MaintenanceExpenseAndCapitalCompleteList[0].RegulatoryGasoline);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "RegulatoryExpense", after.MaintenanceExpenseAndCapitalCompleteList[0].RegulatoryExpense);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "ConstraintRemoval", after.MaintenanceExpenseAndCapitalCompleteList[0].ConstraintRemoval);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "NonRegulatoryNewProcessUnitConstruction", after.MaintenanceExpenseAndCapitalCompleteList[0].NonRegulatoryNewProcessUnitConstruction);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "NonMaintenenceInvestmentExpense", after.MaintenanceExpenseAndCapitalCompleteList[0].NonMaintenenceInvestmentExpense);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "RoutineMaintenanceCapital", after.MaintenanceExpenseAndCapitalCompleteList[0].RoutineMaintenanceCapital);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "TurnaroundMaintenanceCapital", after.MaintenanceExpenseAndCapitalCompleteList[0].TurnaroundMaintenanceCapital);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "TotalMaintenanceOverhead", after.MaintenanceExpenseAndCapitalCompleteList[0].TotalMaintenanceOverhead);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "NonRefineryExclusions", after.MaintenanceExpenseAndCapitalCompleteList[0].NonRefineryExclusions);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "TotalComplexCapital", after.MaintenanceExpenseAndCapitalCompleteList[0].TotalComplexCapital);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "SubmissionID", after.MaintenanceExpenseAndCapitalCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "MaintenanceOverheadRoutine", after.MaintenanceExpenseAndCapitalCompleteList[0].MaintenanceOverheadRoutine);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "MaintenanceOverheadTurnaround", after.MaintenanceExpenseAndCapitalCompleteList[0].MaintenanceOverheadTurnaround);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "TotalMaintenance", after.MaintenanceExpenseAndCapitalCompleteList[0].TotalMaintenance);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "MaintenanceExpenseRoutine", after.MaintenanceExpenseAndCapitalCompleteList[0].MaintenanceExpenseRoutine);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "MaintenanceExpenseTurnaround", after.MaintenanceExpenseAndCapitalCompleteList[0].MaintenanceExpenseTurnaround);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "OtherCapitalInvestment", after.MaintenanceExpenseAndCapitalCompleteList[0].OtherCapitalInvestment);
                InjectAFterBusinessObjectData("FuelsRefineryMaintenanceExpenseAndCapitalComplete", "Energy", after.MaintenanceExpenseAndCapitalCompleteList[0].Energy);
                InjectAFterBusinessObjectData("FuelsRefineryMiscellaneousInputComplete", "OffsiteEnergyPercent", after.MiscellaneousInputCompleteList[0].OffsiteEnergyPercent);
                InjectAFterBusinessObjectData("FuelsRefineryMiscellaneousInputComplete", "MetricTonsInputToCrudeUnits", after.MiscellaneousInputCompleteList[0].MetricTonsInputToCrudeUnits);
                InjectAFterBusinessObjectData("FuelsRefineryMiscellaneousInputComplete", "BarrelsInputToCrudeUnits", after.MiscellaneousInputCompleteList[0].BarrelsInputToCrudeUnits);
                InjectAFterBusinessObjectData("FuelsRefineryMiscellaneousInputComplete", "SubmissionID", after.MiscellaneousInputCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "InServicePercent", after.RoutineMaintenanceCompleteList[0].InServicePercent);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "ExpenseInLocal", after.RoutineMaintenanceCompleteList[0].ExpenseInLocal);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "CostInLocal", after.RoutineMaintenanceCompleteList[0].CostInLocal);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OtherSlowdownHours", after.RoutineMaintenanceCompleteList[0].OtherSlowdownHours);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OtherHoursDown", after.RoutineMaintenanceCompleteList[0].OtherHoursDown);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OtherNumberOfDowntime", after.RoutineMaintenanceCompleteList[0].OtherNumberOfDowntime);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "MaintenanceHoursDown", after.RoutineMaintenanceCompleteList[0].MaintenanceHoursDown);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "MaintenanceNumberOfDowntime", after.RoutineMaintenanceCompleteList[0].MaintenanceNumberOfDowntime);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "RegulatoryHoursDown", after.RoutineMaintenanceCompleteList[0].RegulatoryHoursDown);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "RegulatoryNumberOfDowntime", after.RoutineMaintenanceCompleteList[0].RegulatoryNumberOfDowntime);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "ProcessID", after.RoutineMaintenanceCompleteList[0].ProcessID);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "PercentUtilization", after.RoutineMaintenanceCompleteList[0].PercentUtilization);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "UnitID", after.RoutineMaintenanceCompleteList[0].UnitID);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "SubmissionID", after.RoutineMaintenanceCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OtherDownHoursOther", after.RoutineMaintenanceCompleteList[0].OtherDownHoursOther);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OtherDownHoursOffsiteUpsets", after.RoutineMaintenanceCompleteList[0].OtherDownHoursOffsiteUpsets);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OtherHoursDownUnitUpsets", after.RoutineMaintenanceCompleteList[0].OtherHoursDownUnitUpsets);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OtherHoursDownExternal", after.RoutineMaintenanceCompleteList[0].OtherHoursDownExternal);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OtherHoursDownEconomic", after.RoutineMaintenanceCompleteList[0].OtherHoursDownEconomic);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "OverheadInLocal", after.RoutineMaintenanceCompleteList[0].OverheadInLocal);
                InjectAFterBusinessObjectData("FuelsRefineryRoutineMaintenanceComplete", "CapitalInLocal", after.RoutineMaintenanceCompleteList[0].CapitalInLocal);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "Exceptions", after.TurnaroundMaintenanceCompleteList[0].Exceptions);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "OverheadLocal", after.TurnaroundMaintenanceCompleteList[0].OverheadLocal);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "CapitalLocal", after.TurnaroundMaintenanceCompleteList[0].CapitalLocal);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "ExpenseLocal", after.TurnaroundMaintenanceCompleteList[0].ExpenseLocal);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "CostLocal", after.TurnaroundMaintenanceCompleteList[0].CostLocal);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "HoursDown", after.TurnaroundMaintenanceCompleteList[0].HoursDown);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "TurnaroundDate", after.TurnaroundMaintenanceCompleteList[0].TurnaroundDate);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "ProcessID", after.TurnaroundMaintenanceCompleteList[0].ProcessID);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "UnitID", after.TurnaroundMaintenanceCompleteList[0].UnitID);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "SubmissionID", after.TurnaroundMaintenanceCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "PreviousTurnaroundDate", after.TurnaroundMaintenanceCompleteList[0].PreviousTurnaroundDate);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "ContractorManagementAndProfessionalSalaried", after.TurnaroundMaintenanceCompleteList[0].ContractorManagementAndProfessionalSalaried);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "ContractorOperatorCraftAndClericalHours", after.TurnaroundMaintenanceCompleteList[0].ContractorOperatorCraftAndClericalHours);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "ManagementAndProfessionalOvertimePercent", after.TurnaroundMaintenanceCompleteList[0].ManagementAndProfessionalOvertimePercent);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "ManagementAndProfessionalSalariedStraightTimeHours", after.TurnaroundMaintenanceCompleteList[0].ManagementAndProfessionalSalariedStraightTimeHours);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "OperatorCraftAndClericalOvertimeHours", after.TurnaroundMaintenanceCompleteList[0].OperatorCraftAndClericalOvertimeHours);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "OperatorCraftAndClericalStraightTimeHours", after.TurnaroundMaintenanceCompleteList[0].OperatorCraftAndClericalStraightTimeHours);
                InjectAFterBusinessObjectData("FuelsRefineryTurnaroundMaintenanceComplete", "LaborCostLocal", after.TurnaroundMaintenanceCompleteList[0].LaborCostLocal);
                InjectAFterBusinessObjectData("OperatingExpenseStandard", "OtherDescription", after.OperatingExpenseStandardList[0].OtherDescription);
                InjectAFterBusinessObjectData("OperatingExpenseStandard", "ReportValue", after.OperatingExpenseStandardList[0].ReportValue);
                InjectAFterBusinessObjectData("OperatingExpenseStandard", "Property", after.OperatingExpenseStandardList[0].Property);
                InjectAFterBusinessObjectData("OperatingExpenseStandard", "SubmissionID", after.OperatingExpenseStandardList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "AbsenceHours", after.PersonnelHoursCompleteList[0].AbsenceHours);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "GeneralAndAdministrativeHours", after.PersonnelHoursCompleteList[0].GeneralAndAdministrativeHours);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "Contract", after.PersonnelHoursCompleteList[0].Contract);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "OvertimePercent", after.PersonnelHoursCompleteList[0].OvertimePercent);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "OvertimeHours", after.PersonnelHoursCompleteList[0].OvertimeHours);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "StraightTimeHours", after.PersonnelHoursCompleteList[0].StraightTimeHours);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "NumberOfPersonnel", after.PersonnelHoursCompleteList[0].NumberOfPersonnel);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "PersonnelHoursID", after.PersonnelHoursCompleteList[0].PersonnelHoursID);
                InjectAFterBusinessObjectData("FuelsRefineryPersonnelHoursComplete", "SubmissionID", after.PersonnelHoursCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("PlantOperatingConditionBasic", "UnitOfMeasure", after.PlantOperatingConditionList[0].UnitOfMeasure);
                InjectAFterBusinessObjectData("PlantOperatingConditionBasic", "ReportDateValue", after.PlantOperatingConditionList[0].ReportDateValue);
                InjectAFterBusinessObjectData("PlantOperatingConditionBasic", "ReportTextValue", after.PlantOperatingConditionList[0].ReportTextValue);
                InjectAFterBusinessObjectData("PlantOperatingConditionBasic", "ReportNumericValue", after.PlantOperatingConditionList[0].ReportNumericValue);
                InjectAFterBusinessObjectData("PlantOperatingConditionBasic", "Property", after.PlantOperatingConditionList[0].Property);
                InjectAFterBusinessObjectData("PlantOperatingConditionBasic", "UnitID", after.PlantOperatingConditionList[0].UnitID);
                InjectAFterBusinessObjectData("PlantOperatingConditionBasic", "SubmissionID", after.PlantOperatingConditionList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "ThousandMetricTons", after.FinishedResidualFuelCompleteList[0].ThousandMetricTons);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "ViscosityOfBlendTemperatureDifferent122", after.FinishedResidualFuelCompleteList[0].ViscosityOfBlendTemperatureDifferent122);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "ViscosityOfBlendCentiStokeAtTemperature", after.FinishedResidualFuelCompleteList[0].ViscosityOfBlendCentiStokeAtTemperature);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "PourPointOfBlend", after.FinishedResidualFuelCompleteList[0].PourPointOfBlend);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "Sulfur", after.FinishedResidualFuelCompleteList[0].Sulfur);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "Density", after.FinishedResidualFuelCompleteList[0].Density);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "Grade", after.FinishedResidualFuelCompleteList[0].Grade);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "BlendID", after.FinishedResidualFuelCompleteList[0].BlendID);
                InjectAFterBusinessObjectData("FuelsRefineryFinishedResidualFuelComplete", "SubmissionID", after.FinishedResidualFuelCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryProducedFuelResidualComplete", "Density", after.RefineryProducedFuelResidualCompleteList[0].Density);
                InjectAFterBusinessObjectData("FuelsRefineryProducedFuelResidualComplete", "ViscosityCentiStokesAtTemperature", after.RefineryProducedFuelResidualCompleteList[0].ViscosityCentiStokesAtTemperature);
                InjectAFterBusinessObjectData("FuelsRefineryProducedFuelResidualComplete", "ViscosityTemperature", after.RefineryProducedFuelResidualCompleteList[0].ViscosityTemperature);
                InjectAFterBusinessObjectData("FuelsRefineryProducedFuelResidualComplete", "Sulfur", after.RefineryProducedFuelResidualCompleteList[0].Sulfur);
                InjectAFterBusinessObjectData("FuelsRefineryProducedFuelResidualComplete", "EnergyType", after.RefineryProducedFuelResidualCompleteList[0].EnergyType);
                InjectAFterBusinessObjectData("FuelsRefineryProducedFuelResidualComplete", "SubmissionID", after.RefineryProducedFuelResidualCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedTotalSteam", after.SteamSystemCompleteList[0].ConsumedTotalSteam);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedToppingExtractionFromHigherPressure", after.SteamSystemCompleteList[0].ConsumedToppingExtractionFromHigherPressure);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedCondensingTurbines", after.SteamSystemCompleteList[0].ConsumedCondensingTurbines);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedOtherHeaters", after.SteamSystemCompleteList[0].ConsumedOtherHeaters);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedReboilersEvaporators", after.SteamSystemCompleteList[0].ConsumedReboilersEvaporators);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedProcessOther", after.SteamSystemCompleteList[0].ConsumedProcessOther);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedProcessFluidCatalystCracker", after.SteamSystemCompleteList[0].ConsumedProcessFluidCatalystCracker);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedProcessCoker", after.SteamSystemCompleteList[0].ConsumedProcessCoker);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedProcessCrudeAndVacuum", after.SteamSystemCompleteList[0].ConsumedProcessCrudeAndVacuum);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "TotalSupply", after.SteamSystemCompleteList[0].TotalSupply);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "SubtotalNetPurchased", after.SteamSystemCompleteList[0].SubtotalNetPurchased);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedAllOther", after.SteamSystemCompleteList[0].ConsumedAllOther);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "Purchased", after.SteamSystemCompleteList[0].Purchased);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "Sold", after.SteamSystemCompleteList[0].Sold);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "SubtotalSteamProduction", after.SteamSystemCompleteList[0].SubtotalSteamProduction);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "OtherSteamSources", after.SteamSystemCompleteList[0].OtherSteamSources);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "WasteHeatOtherBoilers", after.SteamSystemCompleteList[0].WasteHeatOtherBoilers);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "WasteHeatCokerHeavyGasOilPumparound", after.SteamSystemCompleteList[0].WasteHeatCokerHeavyGasOilPumparound);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "WasteHeatThermalCracker", after.SteamSystemCompleteList[0].WasteHeatThermalCracker);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "WasteHeatFluidCatalystCracker", after.SteamSystemCompleteList[0].WasteHeatFluidCatalystCracker);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "FiredTurbineCogeneration", after.SteamSystemCompleteList[0].FiredTurbineCogeneration);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "Calciner", after.SteamSystemCompleteList[0].Calciner);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedSteamToFlares", after.SteamSystemCompleteList[0].ConsumedSteamToFlares);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "FluidCokerCOBoiler", after.SteamSystemCompleteList[0].FluidCokerCOBoiler);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "FluidCatalystCrackerStackGas", after.SteamSystemCompleteList[0].FluidCatalystCrackerStackGas);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "FluidCatalystCrackerCoolers", after.SteamSystemCompleteList[0].FluidCatalystCrackerCoolers);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "FiredBoiler", after.SteamSystemCompleteList[0].FiredBoiler);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "FiredProcessHeaterConvectionSection", after.SteamSystemCompleteList[0].FiredProcessHeaterConvectionSection);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "H2PlantNetExportSteam", after.SteamSystemCompleteList[0].H2PlantNetExportSteam);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ActualPressure", after.SteamSystemCompleteList[0].ActualPressure);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "PressureRange", after.SteamSystemCompleteList[0].PressureRange);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "SubmissionID", after.SteamSystemCompleteList[0].SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedDeaerators", after.SteamSystemCompleteList[0].ConsumedDeaerators);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedSteamTracingTankBuildingHeat", after.SteamSystemCompleteList[0].ConsumedSteamTracingTankBuildingHeat);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedPressureControlToLowerPressure", after.SteamSystemCompleteList[0].ConsumedPressureControlToLowerPressure);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedPressureControlFromHigherPressure", after.SteamSystemCompleteList[0].ConsumedPressureControlFromHigherPressure);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedCombustionAirPreheat", after.SteamSystemCompleteList[0].ConsumedCombustionAirPreheat);
                InjectAFterBusinessObjectData("FuelsRefinerySteamSystemComplete", "ConsumedToppingExtractionToLowerPressure", after.SteamSystemCompleteList[0].ConsumedToppingExtractionToLowerPressure);
                InjectAFterBusinessObjectData("SubmissionBasic", "ReportCurrency", after.PlantSubmission.ReportCurrency);
                InjectAFterBusinessObjectData("SubmissionBasic", "UnitOfMeasure", after.PlantSubmission.UnitOfMeasure);
                InjectAFterBusinessObjectData("SubmissionBasic", "SubmissionDate", after.PlantSubmission.SubmissionDate);
                InjectAFterBusinessObjectData("SubmissionBasic", "RefineryID", after.PlantSubmission.RefineryID);
                InjectAFterBusinessObjectData("SubmissionBasic", "SubmissionID", after.PlantSubmission.SubmissionID);
                InjectAFterBusinessObjectData("FuelsRefineryMaterialBalanceComplete", "Density", after.MaterialBalanceCompleteList[0].Density);
                InjectAFterBusinessObjectData("FuelsRefineryMaterialBalanceComplete", "MetricTons", after.MaterialBalanceCompleteList[0].MetricTons);
                InjectAFterBusinessObjectData("FuelsRefineryMaterialBalanceComplete", "Barrels", after.MaterialBalanceCompleteList[0].Barrels);
                InjectAFterBusinessObjectData("FuelsRefineryMaterialBalanceComplete", "MaterialName", after.MaterialBalanceCompleteList[0].MaterialName);
                InjectAFterBusinessObjectData("FuelsRefineryMaterialBalanceComplete", "MaterialID", after.MaterialBalanceCompleteList[0].MaterialID);
                InjectAFterBusinessObjectData("FuelsRefineryMaterialBalanceComplete", "Category", after.MaterialBalanceCompleteList[0].Category);
                InjectAFterBusinessObjectData("FuelsRefineryMaterialBalanceComplete", "Period", after.MaterialBalanceCompleteList[0].Period);
                InjectAFterBusinessObjectData("FuelsRefineryMaterialBalanceComplete", "SubmissionID", after.MaterialBalanceCompleteList[0].SubmissionID);
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }
            return (errorMsg.Length == 0);
        }
    }
}
