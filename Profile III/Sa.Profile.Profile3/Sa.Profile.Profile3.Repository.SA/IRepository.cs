﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.Profile.Profile3.Repository.SA
{
    public enum DataWith
    {
        INPUTS,
        RESULTS,
        INPUTS_AND_RESULTS
    }
    public enum RepositoryMode
    {
        DEV_MODE,
        TEST_MODE,
        TEST_MODE_BUILD,
        PROD_MODE
    }
    public class RepositoryParms
    {
        public RepositoryMode RunningIn { get; set; }
        public string ConnStr { get; set; }
        public string XMLFolder { get; set; }
    }
    public interface IRepository
    {
        RepositoryParms CurrentRepositoryParms { get; set; }
        List<PlantStudyLog> GetAllStudies();
        PlantStudyLog LoadStudy(DataWith wd, int submissionID);
        void SaveNewStudy(PlantStudyLog plant);
        int GetLatestSubmissionID();
    }
}
