﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data.Linq.Mapping;

namespace Sa.Profile.Profile3.Repository.SA
{
    //note, this PlantStudyLog is not same as Ent version.  We need the StudyBusinessObject.
    [Table(Name="PlantStudyLog")]    
    public class PlantStudyLog
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, AutoSync = AutoSync.OnInsert)]
        public int ID { get; set; }

        [Column]
        public int SASubmissionID { get; set; }

        [Column]
        public int ClientSubmissionID { get; set; }

        [Column]
        public string PlantName { get; set; }

        [Column]
        public string StudyLevel { get; set; }

        [Column]
        public string StudyMode { get; set; }

        [Column]
        public string XMLFileLocation { get; set; }

        [Column]
        public DateTime TransactionDate { get; set; }

        public object StudyBusinessObject { get; set; }
        
        public static explicit operator PlantStudyLog(XElement xElem)
        {
            if (xElem == null)
            {
                return null;
            }
            PlantStudyLog psl = new PlantStudyLog();
            psl.ID = (int)xElem.Attribute("ID");
            psl.SASubmissionID = (int)xElem.Attribute("SASubmissionID");
            psl.ClientSubmissionID = (int)xElem.Attribute("ClientSubmissionID");
            psl.PlantName = (string)xElem.Attribute("PlantName");
            psl.StudyLevel = (string)xElem.Attribute("StudyLevel");
            psl.StudyMode = (string)xElem.Attribute("StudyMode");
            psl.XMLFileLocation = (string)xElem.Attribute("XMLFileLocation");
            psl.TransactionDate = (DateTime)xElem.Attribute("TransactionDate");
            return psl;
        }

    }
    [Table(Name = "CalcResultSet")]
    public class CalcResultSet
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, AutoSync = AutoSync.OnInsert)]
        public int SetID { get; set; }

        [Column]
        public string ValueName { get; set; }

        [Column]
        public decimal ResultValue { get; set; }

        public static explicit operator CalcResultSet(XElement xElem)
        {
            if(xElem == null) return null;
            return new CalcResultSet()
            {
                SetID = (int)xElem.Attribute("SetID"),
                ValueName = (string)xElem.Attribute("ValueName"),
                ResultValue = (decimal)xElem.Attribute("ResultValue")
            };
        }
    }
}
