﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Entity.Validation;

using Sa.Profile.Profile3.Business.PlantFactory;
using Sa.Profile.Profile3.Business.PlantUnits;
using Sa.Profile.Profile3.Business.PlantStudyClient;
using Sa.Profile.Profile3.Repository.SA;
using Sa.Profile.Profile3.Common;

namespace Sa.Profile.Profile3.Repository.SA
{
    public class SqlRepository : IRepository
    {
      //  private Table<Sa.Profile.Profile3.Repository.SA.PlantStudyLog> _plantStudyLog;
        public RepositoryParms CurrentRepositoryParms { get; set; }
        private string _connStr;

        public SqlRepository(RepositoryParms parms)
        {
            try
            {
                CurrentRepositoryParms = parms;
                _connStr = parms.ConnStr;
            //    _plantStudyLog = (new DataContext(parms.ConnStr)).GetTable<Sa.Profile.Profile3.Repository.SA.PlantStudyLog>();
            }
            catch (Exception ex)
            {
                string template = "SqlRepository() Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, ex.Message));
                throw infoException;
            }
        }

        public Sa.Profile.Profile3.Repository.SA.PlantStudyLog LoadStudy(DataWith wd, int ClientSubmissionID)
        {
            Sa.Profile.Profile3.Repository.SA.PlantStudyLog retStud = null;
            try
            {
                //get max sa sub id using client sub id
                using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                {
                   var foundStud = p12.PlantStudyLogs
                        .Where(x => x.ClientSubmissionID == ClientSubmissionID)
                        .OrderByDescending(p => p.SASubmissionID).FirstOrDefault();
                    if (foundStud == null)
                    {
                        throw new StudyException("PlantStudyLog entry not found for ClientSubmissionID:" + ClientSubmissionID);
                    }
                    retStud = new PlantStudyLog();
                    retStud.ClientSubmissionID = foundStud.ClientSubmissionID;
                    retStud.ID = foundStud.ID;
                    retStud.PlantName = foundStud.PlantName;
                    retStud.SASubmissionID = foundStud.SASubmissionID;
                    retStud.StudyBusinessObject = null;
                    retStud.StudyLevel = foundStud.StudyLevel;
                    retStud.StudyMode = foundStud.StudyMode;
                    retStud.TransactionDate = foundStud.TransactionDate;
                    retStud.XMLFileLocation = foundStud.XMLFileLocation;
                }

                FuelsRefineryCompletePlantStudy stud = null;
                switch (retStud.StudyLevel)
                {
                    case "FuelsRefineryCompletePlantStudy":

                        switch (wd)
                        {
                                //ttodo: pull from database
                            case DataWith.INPUTS://right now only doing from xml not db

                                stud = XmlTools.DeserializeFromXML<FuelsRefineryCompletePlantStudy>(retStud.XMLFileLocation + retStud.SASubmissionID.ToString() + ".xml");
                                retStud.StudyBusinessObject = stud;
                                break;

                            //this case needs to be split out for two modes
                            //RESULTS_MANUAL, RESULTS_AUTO: the current version
                            //RESULTS is manually done, however, another auto
                            //case is needed to just get KPIs when client requests results
                            case DataWith.RESULTS:

                                stud = new FuelsRefineryCompletePlantStudy();
                                List<Sa.Profile.Profile3.Business.PlantStudyClient.KeyPerformanceIndicator> KPIs = new List<KeyPerformanceIndicator>();


                                using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                                {
                                    p12.Database.CommandTimeout = 300;
                                    System.Data.Entity.Core.Objects.ObjectParameter status = new System.Data.Entity.Core.Objects.ObjectParameter("Stage", typeof(String));
                                    p12.CalcsForKPI(retStud.SASubmissionID, status);
                                    if (status.Value.ToString() != "spFullCalcsDone")
                                    {
                                        throw new StudyException("CalcsForKPI Error Status:" + status.Value);
                                    }
                                }
                                using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                                {
                                    //mod to call preprocess proc first
                                    var res = (from p in p12.GetKPIResults(retStud.SASubmissionID)
                                               select p).AsEnumerable();

                                    foreach (DataAccess.EF.GetKPIResults_Result r in res)
                                    {
                                        Sa.Profile.Profile3.Business.PlantStudyClient.KeyPerformanceIndicator kpi = new KeyPerformanceIndicator();

                                        kpi.TextValue = r.TextValue;
                                        if (r.NumberValue != null)
                                        {
                                            kpi.NumberValue = (decimal)r.NumberValue;
                                        }
                                        kpi.CalcTime = r.CalcTime;
                                        kpi.DataKey = r.DataKey;
                                        kpi.GroupKey = r.SubmissionID;
                                        kpi.StudyMethodology = r.Methodology;
                                        KPIs.Add(kpi);
                                    }
                                }
                                stud.KeyPerformanceIndicators = KPIs;
                                retStud.StudyBusinessObject = stud;
                                //save record of requested kpis
                                using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                                {
                                    DataAccess.EF.PlantStudyLog psl = new DataAccess.EF.PlantStudyLog();
                                    psl.TransactionDate = DateTime.Now;
                                    psl.SASubmissionID = retStud.SASubmissionID;
                                    psl.PlantName = retStud.PlantName;
                                    psl.StudyLevel = retStud.StudyLevel;
                                    psl.StudyMode = "GET_KPIS";
                                    psl.ClientSubmissionID = retStud.ClientSubmissionID;
                                    psl.XMLFileLocation = retStud.XMLFileLocation;

                                    psl.ID = 0;
                                    p12.PlantStudyLogs.Add(psl);
                                    p12.SaveChanges();
                                }
                                break;
                            case DataWith.INPUTS_AND_RESULTS: //not implemented
                                break;
                        }
                        break;
                    default:
                        break;
                }
                return retStud;
            }
            catch (DbEntityValidationException entEx)
            {
                StringBuilder res2 = new StringBuilder();
                foreach (DbEntityValidationResult res in entEx.EntityValidationErrors)
                {
                    res2.Append(res.Entry.Entity.GetType().Name);
                    res2.Append("->");
                    foreach (DbValidationError err in res.ValidationErrors)
                    {
                        res2.Append(err.PropertyName);
                        res2.Append(":");
                        res2.Append(err.ErrorMessage);
                    }
                }
                //var errMsg = entEx.EntityValidationErrors
                //    .SelectMany(x => x.ValidationErrors)
                //    .Select(x => x.ErrorMessage).ToArray();
                string template = "LoadStudy:Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, res2.ToString()));
                throw infoException;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException up)
            {
                string template = "LoadStudy:Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, up.Message),up.InnerException);
                throw infoException;
            }
            catch (System.Data.SqlClient.SqlException exs)
            {
                string template = "LoadStudy:Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, exs.Message),exs.InnerException);
                throw infoException;
            }
            catch (Exception ex)
            {
                string template = "LoadStudy:Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, ex.Message),ex.InnerException);
                throw infoException;
            }
        }


        public List<Sa.Profile.Profile3.Repository.SA.PlantStudyLog> GetAllStudies()
        {
            List<Sa.Profile.Profile3.Repository.SA.PlantStudyLog> studyLogs = new List<PlantStudyLog>();
            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
            {
                
                foreach (Sa.Profile.Profile3.DataAccess.EF.PlantStudyLog psl in p12.PlantStudyLogs)
                {
                   PlantStudyLog retStud = new PlantStudyLog();
                    retStud.ClientSubmissionID = psl.ClientSubmissionID;
                    retStud.ID = psl.ID;
                    retStud.PlantName = psl.PlantName;
                    retStud.SASubmissionID = psl.SASubmissionID;
                    retStud.StudyBusinessObject = null;
                    retStud.StudyLevel = psl.StudyLevel;
                    retStud.StudyMode = psl.StudyMode;
                    retStud.TransactionDate = psl.TransactionDate;
                    retStud.XMLFileLocation = psl.XMLFileLocation;
                    studyLogs.Add(retStud);
                }
            }
            return studyLogs;
        }

        public int GetLatestSubmissionID()
        {
            int latest=1;
            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
            {
                if (p12.PlantStudyLogs.Count() > 0)
                {
                    latest = p12.PlantStudyLogs.Max(c => c.SASubmissionID);
                }                                                
            }
            return latest;
        }

        public void SaveNewStudy(Sa.Profile.Profile3.Repository.SA.PlantStudyLog plantStudy)
        {
            string stage = "Submission";
            try
            {
                int dbSubmissionID = 0;
                short pos = 1;
                switch (plantStudy.StudyLevel)
                {
                    case "FuelsRefineryCompletePlantStudy":

                        FuelsRefineryCompletePlantStudy stud = (FuelsRefineryCompletePlantStudy)plantStudy.StudyBusinessObject;
                        FuelsRefineryCompletePlant plant = (FuelsRefineryCompletePlant)stud.Plant;

                        if (plant.PlantSubmission != null)
                        {
                            Sa.Profile.Profile3.DataAccess.EF.PlantStudyLog newPlantStudyLog = new DataAccess.EF.PlantStudyLog();
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {                             
                                Sa.Profile.Profile3.DataAccess.EF.Submission s = new Sa.Profile.Profile3.DataAccess.EF.Submission();
                                s.RefineryID = plant.PlantSubmission.RefineryID;
                                s.RptCurrency = plant.PlantSubmission.ReportCurrency;                               
                                plantStudy.ClientSubmissionID = (int)plant.PlantSubmission.SubmissionID;
                           
                                s.UOM = plant.PlantSubmission.UnitOfMeasure;
                                s.Submitted = plant.PlantSubmission.SubmissionDate;
                                //missing in client xml
                                s.DataSet = "Actual";
                                p12.Submissions.Add(s);
                                p12.SaveChanges();
                                dbSubmissionID = s.SubmissionID;//new subID 
                                plant.SubmissionID = dbSubmissionID;
                                plant.PlantSubmission.SubmissionID = dbSubmissionID;                                   
                            }                          
                            newPlantStudyLog.ClientSubmissionID = plantStudy.ClientSubmissionID;
                            newPlantStudyLog.ID = plantStudy.ID;
                            newPlantStudyLog.PlantName = plantStudy.PlantName;
                            newPlantStudyLog.SASubmissionID = dbSubmissionID;
                            newPlantStudyLog.StudyLevel = plantStudy.StudyLevel;
                            newPlantStudyLog.StudyMode = plantStudy.StudyMode;
                            newPlantStudyLog.TransactionDate = plantStudy.TransactionDate;
                            newPlantStudyLog.XMLFileLocation = plantStudy.XMLFileLocation;
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                p12.PlantStudyLogs.Add(newPlantStudyLog);
                                p12.SaveChanges();
                            }

                         }
                        else
                        {
                            throw new Exception("No Submission Data Detected!");
                        }
                        if (plant.ConfigurationCompleteList.Count > 0)
                        {
                            stage = "Config";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {                              
                                foreach (FuelsRefineryConfigurationComplete bus_item in plant.ConfigurationCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Config p12_item = new Sa.Profile.Profile3.DataAccess.EF.Config();
                                    p12_item.EnergyPcnt = (float?)bus_item.EnergyPercent; //Config
                                    p12_item.UtilPcnt = bus_item.UtilizationPercent; //Config
                                    p12_item.RptCap = (float?)bus_item.Capacity; //Config
                                    p12_item.ProcessType = bus_item.ProcessType; //Config
                                    if (bus_item.ProcessType == null)
                                    {
                                        p12_item.ProcessType = "";
                                    }
                                    p12_item.UnitName = bus_item.UnitName; //Config
                                    p12_item.ProcessID = bus_item.ProcessID; //Config
                                    p12_item.UnitID = (int)bus_item.UnitID; //Config
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.AllocPcntOfCap = bus_item.AllocatedPercentOfCapacity; //Config
                                    p12_item.BlockOp = bus_item.BlockOperated; //Config
                                    p12_item.PostPerShift = bus_item.PostPerShift; //Config
                                    p12_item.MHPerWeek = (float?)bus_item.ManHourWeek; //Config
                                    p12_item.YearsOper = bus_item.YearsInOperation; //Config
                                    p12_item.InServicePcnt = bus_item.InServicePercent; //Config
                                    p12_item.StmUtilPcnt = bus_item.SteamUtilizationPercent; //Config
                                    p12_item.RptStmCap = (float?)bus_item.SteamCapacity; //Config            
                                    p12_item.SortKey = pos;
                                    pos++;
                                    p12.Configs.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }// end if (plant.ConfigurationCompleteList.Count > 0)

                        if (plant.ConfigurationBuoyCompleteList.Count > 0)
                        {
                            stage = "ConfigBuoy";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryConfigurationBuoyComplete bus_item in plant.ConfigurationBuoyCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.ConfigBuoy p12_item = new Sa.Profile.Profile3.DataAccess.EF.ConfigBuoy();
                                  
                                    p12_item.PcntOwnership = (float?)bus_item.PercentOwnership; //ConfigBuoy
                                    p12_item.LineSize = (float?)bus_item.LineSize; //ConfigBuoy
                                    p12_item.ShipCap = (float?)bus_item.ShipCapacity; //ConfigBuoy
                                    p12_item.ProcessID = bus_item.ProcessID; //ConfigBuoy
                                    p12_item.UnitName = bus_item.UnitName; //ConfigBuoy
                                    p12_item.UnitID = (int)bus_item.UnitID; //ConfigBuoy
                                    p12_item.SubmissionID = dbSubmissionID;// (int)bus_item.SubmissionID; //ConfigBuoy  
                                    p12.ConfigBuoys.Add(p12_item);
                                }         
                                p12.SaveChanges();
                            }
                        }

                        if (plant.ConfigurationReceiptAndShipmentCompleteList.Count > 0)
                        {
                            stage = "ConfigRS";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryConfigurationReceiptAndShipmentComplete bus_item in plant.ConfigurationReceiptAndShipmentCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.ConfigR p12_item = new Sa.Profile.Profile3.DataAccess.EF.ConfigR();
                                    p12_item.Throughput = (float?)bus_item.Throughput; //ConfigRS
                                    p12_item.AvgSize = (float?)bus_item.AverageSize; //ConfigRS
                                    p12_item.ProcessType = bus_item.ProcessType; //ConfigRS
                                    p12_item.ProcessID = bus_item.ProcessID; //ConfigRS
                                    p12_item.UnitID = (int)bus_item.UnitID; //ConfigRS
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.ConfigRS.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.InventoryCompleteList.Count > 0)
                        {
                            stage = "Inventory";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryInventoryComplete bus_item in plant.InventoryCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Inventory p12_item = new Sa.Profile.Profile3.DataAccess.EF.Inventory();
                                    p12_item.AvgLevel = (float?)bus_item.AverageLevel; //Inventory
                                    p12_item.LeasedPcnt = (float?)bus_item.LeasedPercent; //Inventory
                                    p12_item.NumTank = (short?)bus_item.TankNumber; //Inventory
                                    p12_item.FuelsStorage = (double?)bus_item.RefineryStorage; //Inventory
                                    p12_item.MandStorage = (double?)bus_item.ManditoryStorageLevel; //Inventory
                                    p12_item.MktgStorage = (double?)bus_item.MarketingStorage; //Inventory
                                    p12_item.TotStorage = (double?)bus_item.TotalStorage; //Inventory
                                    p12_item.TankType = bus_item.TankType; //Inventory
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.Inventories.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }

                        if (plant.MiscellaneousInputCompleteList.Count > 0)
                        {
                            stage = "MiscInput";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryMiscellaneousInputComplete bus_item in plant.MiscellaneousInputCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.MiscInput p12_item = new Sa.Profile.Profile3.DataAccess.EF.MiscInput();
                                    p12_item.OffsiteEnergyPcnt = (float?)bus_item.OffsiteEnergyPercent; //MiscInput
                                    p12_item.CDUChargeMT = (double?)bus_item.MetricTonsInputToCrudeUnits; //MiscInput
                                    p12_item.CDUChargeBbl = (double?)bus_item.BarrelsInputToCrudeUnits; //MiscInput
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID; //MiscInput
                                    p12.MiscInputs.Add(p12_item);
                                    
                                }
                                p12.SaveChanges();
                            }
                        }

                        if (plant.PlantOperatingConditionList.Count > 0)
                        {
                            stage = "ProcessData";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (PlantOperatingConditionBasic bus_item in plant.PlantOperatingConditionList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.ProcessData p12_item = new Sa.Profile.Profile3.DataAccess.EF.ProcessData();
                   
                                    p12_item.RptUOM = bus_item.UnitOfMeasure;
                                  
                                    p12_item.RptDValue = bus_item.ReportDateValue; //ProcessData
                                    p12_item.RptTValue = bus_item.ReportTextValue; //ProcessData
                                    p12_item.RptValue = (float?)bus_item.ReportNumericValue; //ProcessData
                                    p12_item.Property = bus_item.Property; //ProcessData
                                    p12_item.UnitID = (int)bus_item.UnitID; //ProcessData
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.ProcessDatas.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.FiredHeatersCompleteList.Count > 0)
                        {
                            stage = "FiredHeaters";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryFiredHeatersComplete bus_item in plant.FiredHeatersCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.FiredHeater p12_item = new Sa.Profile.Profile3.DataAccess.EF.FiredHeater();
                                    p12_item.OtherDuty = (float?)bus_item.OtherDuty; //FiredHeaters
                                    p12_item.StackTemp = (float?)bus_item.StackTemperature; //FiredHeaters
                                    p12_item.FurnOutTemp = (float?)bus_item.FurnaceOutTemperature; //FiredHeaters
                                    p12_item.FurnInTemp = (float?)bus_item.FurnaceInTemperature; //FiredHeaters
                                    p12_item.OthCombDuty = (float?)bus_item.OtherCombustionDuty; //FiredHeaters
                                    p12_item.FuelType = bus_item.FuelType; //FiredHeaters
                                    p12_item.FiredDuty = (float?)bus_item.FiredDuty; //FiredHeaters
                                    p12_item.ThroughputUOM = bus_item.ThroughputUnitOfMeasure; //FiredHeaters
                                    p12_item.ThroughputRpt = (float?)bus_item.ThroughputReport; //FiredHeaters
                                    p12_item.ProcessFluid = bus_item.ProcessFluid; //FiredHeaters
                                    p12_item.Service = bus_item.Service; //FiredHeaters
                                    p12_item.ShaftDuty = (float?)bus_item.ShaftDuty; //FiredHeaters
                                    p12_item.HeaterName = bus_item.HeaterName; //FiredHeaters
                                    p12_item.ProcessID = bus_item.ProcessID; //FiredHeaters
                                    p12_item.UnitID = (int)bus_item.UnitID; //FiredHeaters
                                    p12_item.HeaterNo = (int)bus_item.HeaterNumber; //FiredHeaters
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    //ef did string
                                    p12_item.SteamSuperHeated = bus_item.SteamSuperHeated.ToString(); //FiredHeaters
                                    p12_item.SteamDuty = (float?)bus_item.SteamDuty; //FiredHeaters
                                    p12_item.ProcessDuty = (float?)bus_item.ProcessDuty; //FiredHeaters
                                    p12_item.AbsorbedDuty = (float?)bus_item.AbsorbedDuty; //FiredHeaters
                                    p12_item.CombAirTemp = (float?)bus_item.CombustionAirTemperature; //FiredHeaters
                                    p12_item.HeatLossPcnt = (float?)bus_item.HeatLossPercent; //FiredHeaters
                                    p12_item.StackO2 = (float?)bus_item.StackO2; //FiredHeaters
                                    p12.FiredHeaters.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        //ttodo: change loop to one single insert, needed loop for now to ident missing OpExs
                        if (plant.OperatingExpenseStandardList.Count > 0)
                        {
                            pos = 1;
                            stage = "OpexData";
                            //ugly hack for now until db fields lineup with props from xml, will remove later
                            string dbfields = ",OCCSal,MPSSal,OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock,OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,OCCBen,MPSBenAbs,MPSBenInsur,MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth,MPSBen,MaintMatl,ContMaintMatl,EquipMaint,MaintMatlST,ContMaintLabor,ContMaintInspect,ContMaintLaborST,OthContProcOp,OthContTransOp,OthContFire,OthContVacTrucks,OthContConsult,OthContSecurity,OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin,OthContLegal,OthContOth,OthCont,TAAdj,EnvirDisp,EnvirPermits,EnvirFines,EnvirSpill,EnvirLab,EnvirEng,EnvirOth,Envir,EquipNonMaint,Tax,InsurBI,InsurPC,InsurOth,OthNonVolSupply,OthNonVolSafety,OthNonVolNonPersSafety,OthNonVolComm,OthNonVolDonations,OthNonVolNonContribPers,OthNonVolDues,OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks,OthNonVolExtraExpat,OthNonVolOth,OthNonVol,STNonVol,GAPers,ChemicalsAntiknock,ChemicalsAlkyAcid,ChemicalsLube,ChemicalsH2OTreat,ChemicalsProcess,ChemicalsOthAcid,ChemicalsGasAdd,ChemicalsDieselAdd,ChemicalsOthAdd,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv,ChemicalsWasteH2O,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK,ChemicalsMEK,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv,ChemicalsAcid,ChemicalsDewaxAids,ChemicalsOth,Chemicals,CatalystsFCC,CatalystsHYC,CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT,CatalystsHYFT,CatalystsCDWax,CatalystsREF,CatalystsHYG,CatalystsS2Plant,CatalystsPetChem,CatalystsOth,Catalysts,Royalties,PurElec,PurSteam,PurFG,PurLiquid,PurSolid,PurOthN2,PurOthH2O,PurOthOth,PurOth,RefProdFG,RefProdOth,EmissionsTaxes,EmissionsPurch,EmissionsCredits,OthVolOth,OthVol,GANonPers,InvenCarry,Depreciation,Interest,STNonCash,TotRefExp,Cogen,OthRevenue,ThirdPartyTerminalRM,ThirdPartyTerminalProd,POXO2,PMAA,OthVolDemCrude,OthVolDemLightering,OthVolDemProd,STVol ,TotCashOpEx,ExclFireSafety,ExclEnvirFines,ExclOth,TotExpExcl,STSal,STBen,PersCostExclTA,PersCost,EnergyCost,NEOpex,CatalystsFCCAdditives,CatalystsFCCECat,CatalystsFCCECatSales,CatalystsFCCFresh,EnvirMonitor,";
                                
                            foreach (OperatingExpenseBasic bus_item in plant.OperatingExpenseStandardList)
                            {
                                bus_item.SubmissionID = dbSubmissionID;
                                if (pos == 1)
                                {
                                    Sa.Profile.Profile3.DataAccess.EF.OpExAll p12_item = new Sa.Profile.Profile3.DataAccess.EF.OpExAll();
                                    using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                                    {
                                        //add new
                                        p12_item.SubmissionID = dbSubmissionID;    
                                        p12_item.Currency = "USD"; //OpexData  
                                        p12_item.Scenario = "CLIENT";
                                        p12_item.DataType = "RPT";
                                        p12.OpExAlls.Add(p12_item);   
                                        p12.SaveChanges();
                                        pos++;
                                    }
                                    //first one will match for now, this is temp
                                    using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                                    {
                                        string field = bus_item.Property;
                                        string val = bus_item.ReportValue.ToString();
                                        //ttodo: add other PK fields on lookup
                                        object[] parms = new object[2];
                                        parms[0] = val;
                                        parms[1] = dbSubmissionID;
                                        string sql = "update OpExAll set " + field + "={0} where SubmissionID={1}";
                                        int retv = p12.Database.ExecuteSqlCommand(sql, parms);
                                        p12_item.SubmissionID = dbSubmissionID;// (int)bus_item.SubmissionID; //OpexData
                                        p12.SaveChanges();
                                    }
                                }//end create main record
                                else
                                {
                                    string test;
                                    if (bus_item.Property == "InsurPnC")
                                    {
                                        test = ",InsurPC,";
                                    }
                                    else
                                    {
                                        test = "," + bus_item.Property + ",";
                                    }
                                    if (dbfields.IndexOf(test) >= 0)
                                    {
                                        using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                                        {
                                            Sa.Profile.Profile3.DataAccess.EF.OpEx p12_item = (from o in p12.OpExes
                                                                                                where o.SubmissionID == bus_item.SubmissionID
                                                                                                select o).FirstOrDefault();
                                            string val = bus_item.ReportValue.ToString();
                                            string field = bus_item.Property;
                                            if (test == ",InsurPC,")
                                            {
                                                field = "InsurPC";
                                            }
                                            //ttodo: add other PK fields on lookup
                                            object[] parms = new object[2];
                                            parms[0] = val;
                                            parms[1] = dbSubmissionID;
                                            string sql = "update OpExAll set " + field + "={0} where SubmissionID={1}";
                                            int retv = p12.Database.ExecuteSqlCommand(sql, parms);
                                            p12.SaveChanges();
                                        }
                                    }//end update main record
                                    else
                                    {
                                        throw new StudyException("OpEx Field not in DB: " + bus_item.Property);
                                    }
                                }
                            } //end foreach (OperatingExpenseBasic bus_item in plant.OperatingExpenseStandardList)                       
                        }//end if has opex records

                        if (plant.PersonnelHoursCompleteList.Count > 0)
                        {
                            stage = "Pers";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryPersonnelHoursComplete bus_item in plant.PersonnelHoursCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Per p12_item = new Sa.Profile.Profile3.DataAccess.EF.Per();
                                    p12_item.AbsHrs = (float?)bus_item.AbsenceHours; //Pers
                                    p12_item.GA = (float?)bus_item.GeneralAndAdministrativeHours; //Pers
                                    p12_item.Contract = (float?)bus_item.Contract; //Pers
                                    p12_item.OVTPcnt = (float?)bus_item.OvertimePercent; //Pers
                                    p12_item.OVTHours = (float?)bus_item.OvertimeHours; //Pers
                                    p12_item.STH = (float?)bus_item.StraightTimeHours; //Pers
                                    p12_item.NumPers = (float?)bus_item.NumberOfPersonnel; //Pers
                                    p12_item.PersID = bus_item.PersonnelHoursID; //Pers
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.Pers.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.PersonnelAbsenceHoursCompleteList.Count > 0)
                        {
                            stage = "Absence";
                            pos = 1;
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryPersonnelAbsenceHoursComplete bus_item in plant.PersonnelAbsenceHoursCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Absence p12_item = new Sa.Profile.Profile3.DataAccess.EF.Absence();
                                    p12_item.MPSAbs = (float?)bus_item.ManagementProfessionalAndStaffAbsence; //Absence
                                    p12_item.OCCAbs = (float?)bus_item.OperatorCraftAndClericalAbsence; //Absence
                                    p12_item.CategoryID = bus_item.CategoryID; //Absence
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.SortKey = pos;
                                    pos++;
                                    p12.Absences.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.TurnaroundMaintenanceCompleteList.Count > 0)
                        {
                            stage = "MaintTA";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryTurnaroundMaintenanceComplete bus_item in plant.TurnaroundMaintenanceCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.MaintTA p12_item = new Sa.Profile.Profile3.DataAccess.EF.MaintTA();
                                    p12_item.TAExceptions = (short?)bus_item.Exceptions; //MaintTA
                                    p12_item.TAOvhdLocal = (float?)bus_item.OverheadLocal; //MaintTA
                                    p12_item.TACptlLocal = (float?)bus_item.CapitalLocal; //MaintTA
                                    p12_item.TAExpLocal = (float?)bus_item.ExpenseLocal; //MaintTA
                                    p12_item.TACostLocal = (float?)bus_item.CostLocal; //MaintTA
                                    p12_item.TAHrsDown = (float?)bus_item.HoursDown; //MaintTA
                                    p12_item.TADate = (DateTime?)bus_item.TurnaroundDate; //MaintTA
                                    p12_item.ProcessID = bus_item.ProcessID; //MaintTA
                                    p12_item.UnitID = (int)bus_item.UnitID; //MaintTA
                                    //not in db
                                   // p12_item.SubmissionID = submissionID;
                                    p12_item.PrevTADate = (DateTime?)bus_item.PreviousTurnaroundDate; //MaintTA
                                    p12_item.TAContMPS = (float?)bus_item.ContractorManagementAndProfessionalSalaried; //MaintTA
                                    p12_item.TAContOCC = (float?)bus_item.ContractorOperatorCraftAndClericalHours; //MaintTA
                                    p12_item.TAMPSOVTPcnt = (float?)bus_item.ManagementAndProfessionalOvertimePercent; //MaintTA
                                    p12_item.TAMPSSTH = (float?)bus_item.ManagementAndProfessionalSalariedStraightTimeHours; //MaintTA
                                    p12_item.TAOCCOVT = (float?)bus_item.OperatorCraftAndClericalOvertimeHours; //MaintTA
                                    p12_item.TAOCCSTH = (float?)bus_item.OperatorCraftAndClericalStraightTimeHours; //MaintTA
                                    p12_item.TALaborCostLocal = (float?)bus_item.LaborCostLocal; //MaintTA
                                    //xml missing these
                                    p12_item.DataSet = "TempDS";
                                    p12_item.TAID = dbSubmissionID; //for now use subID until PK can be resolved
                                    p12_item.RefineryID = "REF23";
                                    p12.MaintTAs.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.RoutineMaintenanceCompleteList.Count > 0)
                        {
                            stage = "MaintRout";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryRoutineMaintenanceComplete bus_item in plant.RoutineMaintenanceCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.MaintRout p12_item = new Sa.Profile.Profile3.DataAccess.EF.MaintRout();
                        
                                    p12_item.InservicePcnt = (float?)bus_item.InServicePercent; //MaintRout
                                    p12_item.RoutExpLocal = (float?)bus_item.ExpenseInLocal; //MaintRout
                                    p12_item.RoutCostLocal = (float?)bus_item.CostInLocal; //MaintRout
                                    p12_item.OthSlow = (float?)bus_item.OtherSlowdownHours; //MaintRout
                                    p12_item.OthDown = (float?)bus_item.OtherHoursDown; //MaintRout
                                    p12_item.OthNum = (short?)bus_item.OtherNumberOfDowntime; //MaintRout
                                    p12_item.MaintDown = (float?)bus_item.MaintenanceHoursDown; //MaintRout
                                    p12_item.MaintNum = (short?)bus_item.MaintenanceNumberOfDowntime; //MaintRout
                                    p12_item.RegDown = (float?)bus_item.RegulatoryHoursDown; //MaintRout
                                    p12_item.RegNum = (short?)bus_item.RegulatoryNumberOfDowntime; //MaintRout
                                    p12_item.ProcessID = bus_item.ProcessID; //MaintRout
                                
                                    p12_item.UtilPcnt = (float?)bus_item.PercentUtilization; //MaintRout
                                    p12_item.UnitID = (int)bus_item.UnitID; //MaintRout
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.OthDownOther = (float?)bus_item.OtherDownHoursOther; //MaintRout
                                    p12_item.OthDownOffsiteUpsets = (float?)bus_item.OtherDownHoursOffsiteUpsets; //MaintRout
                                    p12_item.OthDownUnitUpsets = (float?)bus_item.OtherHoursDownUnitUpsets; //MaintRout
                                    p12_item.OthDownExternal = (float?)bus_item.OtherHoursDownExternal; //MaintRout
                                    p12_item.OthDownEconomic = (float?)bus_item.OtherHoursDownEconomic; //MaintRout
                                    p12_item.RoutOvhdLocal = (float?)bus_item.OverheadInLocal; //MaintRout
                                    p12_item.RoutCptlLocal = (float?)bus_item.CapitalInLocal; //MaintRout
                                    p12.MaintRouts.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.MaintenanceExpenseAndCapitalCompleteList.Count > 0)
                        {
                            stage = "MExp";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryMaintenanceExpenseAndCapitalComplete bus_item in plant.MaintenanceExpenseAndCapitalCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.MExp p12_item = new Sa.Profile.Profile3.DataAccess.EF.MExp();
                                    p12_item.TotMaint = (float?)bus_item.TotalMaintenanceExpense; //MExp
                                    p12_item.SafetyOth = (float?)bus_item.SafetyOther; //MExp
                                    p12_item.RegOth = (float?)bus_item.RegulatoryOther; //MExp
                                    p12_item.RegDiesel = (float?)bus_item.RegulatoryDiesel; //MExp
                                    p12_item.RegGaso = (float?)bus_item.RegulatoryGasoline; //MExp
                                    p12_item.RegExp = (float?)bus_item.RegulatoryExpense; //MExp
                                    p12_item.ConstraintRemoval = (float?)bus_item.ConstraintRemoval; //MExp
                                    p12_item.NonRegUnit = (float?)bus_item.NonRegulatoryNewProcessUnitConstruction; //MExp
                                    p12_item.NonMaintInvestExp = (float?)bus_item.NonMaintenenceInvestmentExpense; //MExp
                                    p12_item.RoutMaintCptl = (float?)bus_item.RoutineMaintenanceCapital; //MExp
                                    p12_item.TAMaintCptl = (float?)bus_item.TurnaroundMaintenanceCapital; //MExp
                                    p12_item.MaintOvhd = (float?)bus_item.TotalMaintenanceOverhead; //MExp
                                    p12_item.NonRefExcl = (float?)bus_item.NonRefineryExclusions; //MExp
                                    p12_item.ComplexCptl = (float?)bus_item.TotalComplexCapital; //MExp
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.MaintOvhdRout = (float?)bus_item.MaintenanceOverheadRoutine; //MExp
                                    p12_item.MaintOvhdTA = (float?)bus_item.MaintenanceOverheadTurnaround; //MExp
                                    p12_item.MaintExp = (float?)bus_item.TotalMaintenance; //MExp
                                    p12_item.MaintExpRout = (float?)bus_item.MaintenanceExpenseRoutine; //MExp
                                    p12_item.MaintExpTA = (float?)bus_item.MaintenanceExpenseTurnaround; //MExp
                                    p12_item.OthInvest = (float?)bus_item.OtherCapitalInvestment; //MExp
                                    p12_item.Energy = (float?)bus_item.Energy; //MExp
                                    p12.MExps.Add(p12_item);
                                    break;
                                    //error, has too many records, only should have one
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.CrudeCompleteList.Count > 0)
                        {
                            stage = "Crude";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryCrudeComplete bus_item in plant.CrudeCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Crude p12_item = new Sa.Profile.Profile3.DataAccess.EF.Crude();
                                    p12_item.Sulfur = (float?)bus_item.Sulfur; //Crude
                                    p12_item.Gravity = (float?)bus_item.Gravity; //Crude
  
                                    p12_item.BBL = (double)bus_item.Barrels; //Crude
                                    p12_item.CrudeName = bus_item.CrudeName; //Crude
                                    p12_item.CNum = bus_item.CrudeNumber; //Crude
                                    p12_item.CrudeID = (short)bus_item.CrudeID; //Crude
                                  
                                    p12_item.Period = bus_item.Period; //Crude
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.Crudes.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.MaterialBalanceCompleteList.Count > 0)
                        {
                            stage = "Yield";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {

                                pos = 1;
                                foreach (FuelsRefineryMaterialBalanceComplete bus_item in plant.MaterialBalanceCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Yield p12_item = new Sa.Profile.Profile3.DataAccess.EF.Yield();
                                    p12_item.Density = (float?)bus_item.Density; //Yield
                                    p12_item.MT = (double?)bus_item.MetricTons; //Yield
                                
                                    p12_item.BBL = (double)bus_item.Barrels; //Yield
                                    p12_item.MaterialName = bus_item.MaterialName; //Yield
                                    p12_item.MaterialID = bus_item.MaterialID; //Yield
                                    p12_item.Category = bus_item.Category; //Yield
                                    p12_item.Period = bus_item.Period; //Yield
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;                          
                                    p12_item.SortKey = pos;
                                    p12.Yields.Add(p12_item);
                                    pos++;
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.RefineryProducedFuelResidualCompleteList.Count > 0)
                        {
                            stage = "RPFResid";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {

                                foreach (FuelsRefineryProducedFuelResidualComplete bus_item in plant.RefineryProducedFuelResidualCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.RPFResid p12_item = new Sa.Profile.Profile3.DataAccess.EF.RPFResid();
                                   
                                    p12_item.Density = (float?)bus_item.Density; //RPFResid
                                    p12_item.ViscCSAtTemp = (float?)bus_item.ViscosityCentiStokesAtTemperature; //RPFResid
                                    p12_item.ViscTemp = (float?)bus_item.ViscosityTemperature; //RPFResid
                                    p12_item.Sulfur = (float?)bus_item.Sulfur; //RPFResid
                                    p12_item.EnergyType = bus_item.EnergyType; //RPFResid
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.RPFResids.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if(plant.ElectricBasicList.Count>0)
                        {
                            stage = "Electric";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                pos = 1;

                                 foreach (FuelsRefineryEnergyElectricBasic bus_item in plant.ElectricBasicList)
                                {
                                    Sa.Profile.Profile3.DataAccess.EF.Electric p12_item = new Sa.Profile.Profile3.DataAccess.EF.Electric();

                                    bus_item.SubmissionID = dbSubmissionID;
                                    p12_item.SubmissionID = dbSubmissionID; //(int)bus_item.SubmissionID; //Electric
                                    p12_item.EnergyType = bus_item.EnergyType; //Electric
                                    p12_item.TransType = bus_item.TransactionType; //Electric
                                     //these two are null, db requires
                                    p12_item.RptMWH = (float)bus_item.Amount; //Electric
                                    if (bus_item.PriceLocal == null)
                                    {
                                        p12_item.PriceLocal = 0.0F;
                                    }
                                    else
                                    {
                                        p12_item.PriceLocal = (float)bus_item.PriceLocal; //Electric
                                    }                                   
                                    p12_item.GenEff = (float?)bus_item.GenerationEfficiency; //Electric                                        
                                    p12_item.TransCode = pos;
                                    bus_item.TransactionCode = pos; //note also done in wcf but here to for testing without wcf
                                    p12.Electrics.Add(p12_item);
                                    pos++;
                                 }
                                 p12.SaveChanges();
                            }
                        }
                        if (plant.EnergyCompleteList.Count > 0)
                        {
                            pos = 1;
                            stage = "Energy";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryEnergyComplete bus_item in plant.EnergyCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    if (bus_item.EnergyType == "PT")
                                        goto next_ener;
                                    Sa.Profile.Profile3.DataAccess.EF.Energy p12_item = new Sa.Profile.Profile3.DataAccess.EF.Energy();
                                    p12_item.N2 = (float?)bus_item.N2; //Energy
                                    p12_item.Butane = (float?)bus_item.Butane; //Energy
                                    p12_item.Propylene = (float?)bus_item.Propylene; //Energy
                                    p12_item.Propane = (float?)bus_item.Propane; //Energy
                                    p12_item.Ethylene = (float?)bus_item.Ethylene; //Energy
                                    p12_item.Ethane = (float?)bus_item.Ethane; //Energy
                                    p12_item.Methane = (float?)bus_item.Methane; //Energy
                                    p12_item.Hydrogen = (float?)bus_item.Hydrogen; //Energy                                                       
                                    p12_item.OverrideCalcs = bus_item.OverrideCalculation.ToString(); //Energy
                                  
                                    p12_item.MBTUOut = (double?)bus_item.MillionBritishThermalUnitsOut; //Energy
                                    p12_item.CO2 = (float?)bus_item.CO2; //Energy
              
                                    p12_item.TransType = bus_item.TransactionType; //Energy
                                    p12_item.EnergyType = bus_item.EnergyType; //Energy
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.SO2 = (float?)bus_item.SO2; //Energy
                                    p12_item.NH3 = (float?)bus_item.NH3; //Energy
                                    p12_item.CO = (float?)bus_item.CO; //Energy
                                    p12_item.H2S = (float?)bus_item.H2S; //Energy
                                    p12_item.C5Plus = (float?)bus_item.C5Plus; //Energy
                                    p12_item.Butylenes = (float?)bus_item.Butylenes; //Energy
                                    p12_item.Isobutane = (float?)bus_item.Isobutane; //Energy
                                    p12_item.RptSource = (double)bus_item.Amount;
                                    if (bus_item.PriceLocal == null)
                                    {
                                        p12_item.RptPriceLocal = 0.0F;
                                    }
                                    else
                                    {
                                        p12_item.RptPriceLocal = (float)bus_item.PriceLocal;
                                    }
                                    p12_item.TransCode = pos;
                                    bus_item.TransactionCode = pos; //note also done in wcf but here to for testing without wcf
                                    pos++;
                                    p12.Energies.Add(p12_item);
                                next_ener: ;
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.EmissionCompleteList.Count > 0)
                        {
                            stage = "Emissions";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryEmissionComplete bus_item in plant.EmissionCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Emission p12_item = new Sa.Profile.Profile3.DataAccess.EF.Emission();
                                    p12_item.RefEmissions = (float?)bus_item.RefineryEmission; //Emissions
                                    p12_item.EmissionType = bus_item.EmissionType; //Emissions
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.Emissions.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.SteamSystemCompleteList.Count > 0)
                        {
                            stage = "SteamSystem";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefinerySteamSystemComplete bus_item in plant.SteamSystemCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.SteamSystem p12_item = new Sa.Profile.Profile3.DataAccess.EF.SteamSystem();
                                    p12_item.TotCons = (float?)bus_item.ConsumedTotalSteam; //SteamSystem
                                    p12_item.ConsTopTurbHigh = (float?)bus_item.ConsumedToppingExtractionFromHigherPressure; //SteamSystem
                                    p12_item.ConsCondTurb = (float?)bus_item.ConsumedCondensingTurbines; //SteamSystem
                                    p12_item.ConsOthHeaters = (float?)bus_item.ConsumedOtherHeaters; //SteamSystem
                                    p12_item.ConsReboil = (float?)bus_item.ConsumedReboilersEvaporators; //SteamSystem
                                    p12_item.ConsProcessOth = (float?)bus_item.ConsumedProcessOther; //SteamSystem
                                    p12_item.ConsProcessFCC = (float?)bus_item.ConsumedProcessFluidCatalystCracker; //SteamSystem
                                    p12_item.ConsProcessCOK = (float?)bus_item.ConsumedProcessCoker; //SteamSystem
                                    p12_item.ConsProcessCDU = (float?)bus_item.ConsumedProcessCrudeAndVacuum; //SteamSystem
                                    p12_item.TotSupply = (float?)bus_item.TotalSupply; //SteamSystem
                                    p12_item.NetPur = (float?)bus_item.SubtotalNetPurchased; //SteamSystem
                                    p12_item.ConsOth = (float?)bus_item.ConsumedAllOther; //SteamSystem
                                    p12_item.Pur = (float?)bus_item.Purchased; //SteamSystem
                                    p12_item.Sold = (float?)bus_item.Sold; //SteamSystem
                                    p12_item.STProd = (float?)bus_item.SubtotalSteamProduction; //SteamSystem
                                    p12_item.OthSource = (float?)bus_item.OtherSteamSources; //SteamSystem
                                    p12_item.WasteHeatOth = (float?)bus_item.WasteHeatOtherBoilers; //SteamSystem
                                    p12_item.WasteHeatCOK = (float?)bus_item.WasteHeatCokerHeavyGasOilPumparound; //SteamSystem
                                    p12_item.WasteHeatTCR = (float?)bus_item.WasteHeatThermalCracker; //SteamSystem
                                    p12_item.WasteHeatFCC = (float?)bus_item.WasteHeatFluidCatalystCracker; //SteamSystem
                                    p12_item.FTCogen = (float?)bus_item.FiredTurbineCogeneration; //SteamSystem
                                    p12_item.Calciner = (float?)bus_item.Calciner; //SteamSystem
                                    p12_item.ConsFlares = (float?)bus_item.ConsumedSteamToFlares; //SteamSystem
                                    p12_item.FluidCokerCOBoiler = (float?)bus_item.FluidCokerCOBoiler; //SteamSystem
                                    p12_item.FCCStackGas = (float?)bus_item.FluidCatalystCrackerStackGas; //SteamSystem
                                    p12_item.FCCCatCoolers = (float?)bus_item.FluidCatalystCrackerCoolers; //SteamSystem
                                    p12_item.FiredBoiler = (float?)bus_item.FiredBoiler; //SteamSystem
                                    p12_item.FiredProcessHeater = (float?)bus_item.FiredProcessHeaterConvectionSection; //SteamSystem
                                    p12_item.H2PlantExport = (float?)bus_item.H2PlantNetExportSteam; //SteamSystem
                                    p12_item.ActualPress = (float?)bus_item.ActualPressure; //SteamSystem
                                    p12_item.PressureRange = bus_item.PressureRange; //SteamSystem
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.ConsDeaerators = (float?)bus_item.ConsumedDeaerators; //SteamSystem
                                    p12_item.ConsTracingHeat = (float?)bus_item.ConsumedSteamTracingTankBuildingHeat; //SteamSystem
                                    p12_item.ConsPressControlLow = (float?)bus_item.ConsumedPressureControlToLowerPressure; //SteamSystem
                                    p12_item.ConsPressControlHigh = (float?)bus_item.ConsumedPressureControlFromHigherPressure; //SteamSystem
                                    p12_item.ConsCombAirPreheat = (float?)bus_item.ConsumedCombustionAirPreheat; //SteamSystem
                                    p12_item.ConsTopTurbLow = (float?)bus_item.ConsumedToppingExtractionToLowerPressure; //SteamSystem
                                    p12.SteamSystems.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.DieselCompleteList.Count > 0)
                        {
                            stage = "Diesel";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryDieselComplete bus_item in plant.DieselCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Diesel p12_item = new Sa.Profile.Profile3.DataAccess.EF.Diesel();
                                    p12_item.KMT = (float?)bus_item.ThousandMetricTons; //Diesel
                                    p12_item.Type = bus_item.Type; //Diesel
                                    p12_item.Market = bus_item.Market; //Diesel
                                    p12_item.Grade = bus_item.Grade; //Diesel
                                    p12_item.BlendID = (int)bus_item.BlendID; //Diesel
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.BiodieselPcnt = (float?)bus_item.BiodieselInBlendPercent; //Diesel
                                    p12_item.E350 = (float?)bus_item.PercentEvaporatedAt350C; //Diesel
                                    p12_item.ASTM90 = (float?)bus_item.AmericanStandardForTestingMaterials90DistillationPoint; //Diesel
                                    p12_item.CloudPt = (float?)bus_item.CloudPointOfBlend; //Diesel
                                    p12_item.Sulfur = (float?)bus_item.Sulfur; //Diesel
                                    p12_item.PourPt = (float?)bus_item.PourPointOfBlend; //Diesel
                                    p12_item.Cetane = (float?)bus_item.Cetane; //Diesel
                                    p12_item.Density = (float?)bus_item.Density; //Diesel
                                    p12.Diesels.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.GasolineCompleteList.Count > 0)
                        {
                            stage = "Gasoline";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryGasolineComplete bus_item in plant.GasolineCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Gasoline p12_item = new Sa.Profile.Profile3.DataAccess.EF.Gasoline();
                                    p12_item.KMT = (float?)bus_item.ThousandMetricTons; //Gasoline
                                    p12_item.MON = (float?)bus_item.MotorOctaneNumber; //Gasoline
                                    p12_item.RON = (float?)bus_item.ResearchOctaneNumber; //Gasoline
                                    p12_item.RVP = (float?)bus_item.ReidVaporPressure; //Gasoline
                                    p12_item.Density = (float?)bus_item.Density; //Gasoline
                                    p12_item.Type = bus_item.Type; //Gasoline
                                    p12_item.Market = bus_item.Market; //Gasoline
                                    p12_item.Grade = bus_item.Grade; //Gasoline
                                    p12_item.BlendID = (int)bus_item.BlendID; //Gasoline
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.Lead = (float?)bus_item.Lead; //Gasoline
                                    p12_item.Sulfur = (float?)bus_item.Sulfur; //Gasoline
                                    p12_item.OthOxygen = (float?)bus_item.OtherOxygen; //Gasoline
                                    p12_item.TAME = (float?)bus_item.TertAmylMethylEther; //Gasoline
                                    p12_item.ETBE = (float?)bus_item.EthylTertButylEther; //Gasoline
                                    p12_item.MTBE = (float?)bus_item.MethylTertButylEther; //Gasoline
                                    p12_item.Ethanol = (float?)bus_item.Ethanol; //Gasoline
                                    p12_item.Oxygen = (float?)bus_item.Oxygen; //Gasoline
                                    p12.Gasolines.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.MaterialBalanceCompleteList.Count > 0)
                        {
                            stage = "MarineBunkers";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryMarineBunkersComplete bus_item in plant.MarineBunkersCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.MarineBunker p12_item = new Sa.Profile.Profile3.DataAccess.EF.MarineBunker();
                                    p12_item.KMT = (float?)bus_item.ThousandMetricTons; //MarineBunkers
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.CrackedStock = (float?)bus_item.CrackedStock; //MarineBunkers
                                    p12_item.ViscTemp = (float?)bus_item.ViscosityOfBlendTemperatureDifferent122; //MarineBunkers
                                    p12_item.ViscCSAtTemp = (float?)bus_item.ViscosityOfBlendCentiStokeAtTemperature; //MarineBunkers
                                    p12_item.PourPt = (float?)bus_item.PourPointOfBlend; //MarineBunkers
                                    p12_item.Sulfur = (float?)bus_item.Sulfur; //MarineBunkers
                                    p12_item.Density = (float?)bus_item.Density; //MarineBunkers
                                    p12_item.Grade = bus_item.Grade; //MarineBunkers
                                    p12_item.BlendID = (int)bus_item.BlendID; //MarineBunkers
                                    p12.MarineBunkers.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.FinishedResidualFuelCompleteList.Count > 0)
                        {
                            stage = "Resid";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryFinishedResidualFuelComplete bus_item in plant.FinishedResidualFuelCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Resid p12_item = new Sa.Profile.Profile3.DataAccess.EF.Resid();
                                    p12_item.KMT = (float?)bus_item.ThousandMetricTons; //Resid
                                    p12_item.ViscTemp = (float?)bus_item.ViscosityOfBlendTemperatureDifferent122; //Resid
                                    p12_item.ViscCSAtTemp = (float?)bus_item.ViscosityOfBlendCentiStokeAtTemperature; //Resid
                                    p12_item.PourPT = (float?)bus_item.PourPointOfBlend; //Resid
                                    p12_item.Sulfur = (float?)bus_item.Sulfur; //Resid
                                    p12_item.Density = (float?)bus_item.Density; //Resid
                                    p12_item.Grade = bus_item.Grade; //Resid
                                    p12_item.BlendID = (int)bus_item.BlendID; //Resid
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.Resids.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.KeroseneCompleteList.Count > 0)
                        {
                            stage = "Kerosene";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryKeroseneComplete bus_item in plant.KeroseneCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.Kerosene p12_item = new Sa.Profile.Profile3.DataAccess.EF.Kerosene();
                                    p12_item.KMT = (float?)bus_item.ThousandMetricTons; //Kerosene
                                    p12_item.Sulfur = (float?)bus_item.Sulfur; //Kerosene
                                    p12_item.Density = (float?)bus_item.Density; //Kerosene
                                    p12_item.Type = bus_item.Type; //Kerosene
                                    p12_item.Grade = bus_item.Grade; //Kerosene
                                    p12_item.BlendID = (int)bus_item.BlendID; //Kerosene
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.Kerosenes.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.LiquifiedPetroleumGasCompleteList.Count > 0)
                        {
                            stage = "LPG";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryLiquifiedPetroleumGasComplete bus_item in plant.LiquifiedPetroleumGasCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.LPG p12_item = new Sa.Profile.Profile3.DataAccess.EF.LPG();
                                    p12_item.VolC5Plus = (float?)bus_item.VolumeC5Plus; //LPG
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12_item.VolC4ene = (float?)bus_item.VolumeC4ene; //LPG
                                    p12_item.VolnC4 = (float?)bus_item.VolumenC4; //LPG
                                    p12_item.VoliC4 = (float?)bus_item.VolumeiC4; //LPG
                                    p12_item.VolC3ene = (float?)bus_item.VolumeC3ene; //LPG
                                    p12_item.VolC3 = (float?)bus_item.VolumeC3; //LPG
                                    p12_item.VolC2Lt = (float?)bus_item.VolumeC2LT; //LPG
                                   //ef made string
                                    p12_item.MolOrVol = bus_item.MoleOrVolume.ToString(); //LPG
                                    p12_item.BlendID = (int)bus_item.BlendID; //LPG
                                    p12.LPGs.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        if (plant.GeneralMiscellaneousCompleteList.Count > 0)
                        {
                            stage = "GeneralMisc";
                            using (Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities p12 = new Sa.Profile.Profile3.DataAccess.EF.ProfileFuels12Entities(_connStr))
                            {
                                foreach (FuelsRefineryGeneralMiscellaneousComplete bus_item in plant.GeneralMiscellaneousCompleteList)
                                {
                                    bus_item.SubmissionID = dbSubmissionID;
                                    Sa.Profile.Profile3.DataAccess.EF.GeneralMisc p12_item = new Sa.Profile.Profile3.DataAccess.EF.GeneralMisc();
                                    p12_item.TotLossMT = (float?)bus_item.TotalLossMetricTons; //GeneralMisc
                                    p12_item.FlareLossMT = (float?)bus_item.FlareLossMetricTons; //GeneralMisc
                                    p12_item.SubmissionID = dbSubmissionID;// bus_item.SubmissionID;
                                    p12.GeneralMiscs.Add(p12_item);
                                }
                                p12.SaveChanges();
                            }
                        }
                        //always save bus object for transaction history
                        XmlTools.SerializeToXML<FuelsRefineryCompletePlantStudy>(stud, plantStudy.XMLFileLocation + plantStudy.SASubmissionID + ".xml");                        
                        break;
                    default:
                        break;
                }//end switch (plantStudy.StudyLevel)
                stage = "PreCalc";
            }
            catch (DbEntityValidationException entEx)
            {
                StringBuilder res2 = new StringBuilder();
                foreach (DbEntityValidationResult res in entEx.EntityValidationErrors)
                {
                    res2.Append(res.Entry.Entity.GetType().Name);
                    res2.Append("->");
                    foreach (DbValidationError err in res.ValidationErrors)
                    {
                        res2.Append(err.PropertyName);
                        res2.Append(":");
                        res2.Append(err.ErrorMessage);
                    }
                }
                //var errMsg = entEx.EntityValidationErrors
                //    .SelectMany(x => x.ValidationErrors)
                //    .Select(x => x.ErrorMessage).ToArray();
                string template = "SaveNewStudy:Stage={0} Error:{1}";
                StudyException infoException = new StudyException(string.Format(template, stage, res2.ToString()));
                throw infoException;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException up)
            {
                string template = "SaveNewStudy:Stage={0} Error:{1}";
                StudyException infoException = new StudyException(string.Format(template, stage, up.Message));
                throw infoException;
            }
            catch (System.Data.SqlClient.SqlException exs)
            {
                string template = "SaveNewStudy:Stage={0} Error:{1}";
                StudyException infoException = new StudyException(string.Format(template, stage, exs.Message));
                throw infoException;
            }
            catch (Exception ex)
            {
                string template = "SaveNewStudy:Stage={0} Error:{1}";
                StudyException infoException = new StudyException(string.Format(template, stage, ex.Message));
                throw infoException;
            }
        }
    }
}
