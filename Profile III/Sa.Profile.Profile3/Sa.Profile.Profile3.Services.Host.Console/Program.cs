﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Reflection;
using Ninject;
using Sa.Profile.Profile3.Repository.SA;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging;

using System.Diagnostics;
using System.Runtime.Serialization;

using Sa.Profile.Profile3.Services.Library;
using Sa.Profile.Profile3.Common;

namespace Sa.Profile.Profile3.Services.Host.Console
{
    class Program
    {
        static ExceptionManager exManagerWin;
        static void Main(string[] args)
        {
            LoggingConfiguration loggingConfiguration = BuildLoggingConfig();
            LogWriter logWriter = new LogWriter(loggingConfiguration);
            exManagerWin = BuildExceptionManagerConfig(logWriter);
            ExceptionPolicy.SetExceptionManager(exManagerWin);

            Sa.Profile.Profile3.Injection.Kernel ker = new Sa.Profile.Profile3.Injection.Kernel();
            IKernel ker2 = ker.SetupDI(RepositoryMode.DEV_MODE);
            //IKernel ker2 = ker.SetupDI(RepositoryMode.TEST_MODE_BUILD);
            //IKernel ker2 = ker.SetupDI(RepositoryMode.TEST_MODE);               
            var myServ = new Sa.Profile.Profile3.Services.StudyWin.StudyWinService(ker2.Get<IRepository>());
            using (ServiceHost host = new ServiceHost(myServ))
            {
                host.Open();
                System.Console.WriteLine("Press Enter to End Service Host");
                System.Console.ReadLine();
            }
        }

        private static ExceptionManager BuildExceptionManagerConfig(LogWriter logWriter)
        {
            var policies = new List<ExceptionPolicyDefinition>();
            var mappings = new NameValueCollection();
            mappings.Add("FaultID", "{Guid}");
            mappings.Add("FaultMessage", "{Message}");

            var winStudyServicePolicy = new List<ExceptionPolicyEntry>
            {
                new ExceptionPolicyEntry(typeof(Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                    {
                        new LoggingExceptionHandler("General", 9007, TraceEventType.Error,
                         "Win Study Calculations Service", 5, typeof(TextExceptionFormatter), logWriter),                 
                        new FaultContractExceptionHandler(typeof(Sa.Profile.Profile3.Services.Library.StudyCalculationFault), "Service Error. Please contact your administrator", mappings)
                    })
            };
            var assistingAdministrators = new List<ExceptionPolicyEntry>
            {
                new ExceptionPolicyEntry(typeof (Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                     {
                       new LoggingExceptionHandler("General", 9000, TraceEventType.Error,
                         "Study Calculations Service", 5, typeof(TextExceptionFormatter), logWriter),
                       new ReplaceHandler("Application error.  Please advise your administrator and provide them with this error code: {handlingInstanceID}",
                         typeof(StudyException))
                     })
            };

            var exceptionShieldingAndLogging = new List<ExceptionPolicyEntry>
            {
                new ExceptionPolicyEntry(typeof (Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                     {
                       new LoggingExceptionHandler("General", 9001, TraceEventType.Error,
                         "Study Calculations Service", 5, typeof(TextExceptionFormatter), logWriter),
                       new WrapHandler("Application Error. Please contact your administrator.",
                         typeof(StudyException))
                     })
            };

            var loggingAndReplacing = new List<ExceptionPolicyEntry>
            {
                new ExceptionPolicyEntry(typeof (Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                     {
                       new LoggingExceptionHandler("General", 9002, TraceEventType.Error,
                         "Study Calculations Service", 5, typeof(TextExceptionFormatter), logWriter),
                       new ReplaceHandler("An application error occurred and has been logged. Please contact your administrator and provide them with this error code: {handlingInstanceID}",
                         typeof(StudyException))
                     })
            };
           // policies.Add(new ExceptionPolicyDefinition("WinStudyServicePolicy", winStudyServicePolicy));
            //policies.Add(new ExceptionPolicyDefinition("AssistingAdministrators", assistingAdministrators));
            policies.Add(new ExceptionPolicyDefinition("ExceptionShieldingAndLogging", exceptionShieldingAndLogging));
            //policies.Add(new ExceptionPolicyDefinition("LoggingAndReplacingException", loggingAndReplacing));
            //policies.Add(new ExceptionPolicyDefinition("LogAndWrap", logAndWrap));
            //policies.Add(new ExceptionPolicyDefinition("ReplacingException", replacingException));
            return new ExceptionManager(policies);
        }
        private static LoggingConfiguration BuildLoggingConfig()
        {
            // Formatters
            TextFormatter formatter = new TextFormatter("Timestamp: {timestamp}{newline}Message: {message}{newline}Category: {category}{newline}Priority: {priority}{newline}EventId: {eventid}{newline}Severity: {severity}{newline}Title:{title}{newline}Machine: {localMachine}{newline}App Domain: {localAppDomain}{newline}ProcessId: {localProcessId}{newline}Process Name: {localProcessName}{newline}Thread Name: {threadName}{newline}Win32 ThreadId:{win32ThreadId}{newline}Extended Properties: {dictionary({key} - {value}{newline})}");
            // Listeners
            var flatFileTraceListener = new FlatFileTraceListener(@"C:\KNPC\Logs\WinStudyCalculator.log", "----------------------------------------", "----------------------------------------", formatter);
            var eventLog = new EventLog("Application", ".", "Enterprise Library Logging");
            var eventLogTraceListener = new FormattedEventLogTraceListener(eventLog);
            // Build Configuration
            var config = new LoggingConfiguration();
            config.AddLogSource("General", SourceLevels.All, true).AddTraceListener(eventLogTraceListener);
            config.LogSources["General"].AddTraceListener(flatFileTraceListener);
            // Special Sources Configuration
            config.SpecialSources.LoggingErrorsAndWarnings.AddTraceListener(flatFileTraceListener);
            return config;
        }
    }
}
