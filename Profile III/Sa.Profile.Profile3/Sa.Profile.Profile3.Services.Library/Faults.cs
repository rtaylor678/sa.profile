﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Sa.Profile.Profile3.Services.Library
{
    [DataContract]
    public class StudyCalculationFault
    {
        [DataMember]
        public Guid FaultID { get; set; }
        [DataMember]
        public string FaultMessage { get; set; }
    }
}
