﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Sa.Profile.Profile3.Services.Library
{
    [DataContract]    
    public class KPR
    {
        [DataMember]
        public List<KP> KPs { get; set; }
    }
    [DataContract]
    public class KP
    {
        [DataMember]        
        public int? GroupKey
        {
            get;
            set;
        }
        [DataMember]
        public string DataKey
        {
            get;
            set;
        }
        [DataMember]
        public decimal? NumberValue
        {
            get;
            set;
        }
        [DataMember]
        public string TextValue
        {
            get;
            set;
        }
        [DataMember]
        public DateTime? CalcTime
        {
            get;
            set;
        }
        [DataMember]
        public short? StudyMethodology
        {
            get;
            set;
        }
    }
}
