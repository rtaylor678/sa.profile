﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Sa.Profile.Profile3.Services.Library
{
    [DataContract]
    public class RequestSt
    {
        [DataMember]
        public PL PLA {get;set;}
        [DataMember]
        public int SubmissionID { get; set; }
    }
    [DataContract]
    public class ResponseKs
    {
        [DataMember]
        public bool HasErrors { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public KPR ReturnKs {get;set;}
    }
}
