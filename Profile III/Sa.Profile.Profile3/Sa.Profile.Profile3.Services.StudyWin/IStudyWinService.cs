﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Sa.Profile.Profile3.Services.Library;
using Sa.Profile.Profile3.Common;

namespace Sa.Profile.Profile3.Services.StudyWin
{
    [ServiceContract]
    public interface IStudyWinService
    {
        
        [OperationContract]
        [FaultContract(typeof(StudyCalculationFault))]
        Sa.Profile.Profile3.Services.Library.ResponseKs SubmitStudy(Sa.Profile.Profile3.Services.Library.RequestSt requestStudy);
        
        [OperationContract]
        [FaultContract(typeof(StudyCalculationFault))]
        Sa.Profile.Profile3.Services.Library.ResponseKs RequestStudyResults(Sa.Profile.Profile3.Services.Library.RequestSt requestStudyResults);
    }
}
