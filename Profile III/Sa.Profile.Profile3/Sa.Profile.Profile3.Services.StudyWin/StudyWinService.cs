﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Sa.Profile.Profile3.DataAccess;
using Sa.Profile.Profile3.Repository.SA;
using Sa.Profile.Profile3.Business.Scrambler;
using Sa.Profile.Profile3.Business.PlantFactory;
using Sa.Profile.Profile3.Business.PlantStudyClient;
using Sa.Profile.Profile3.Business.PlantStudySA;
using Sa.Profile.Profile3.Common;

using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;

namespace Sa.Profile.Profile3.Services.StudyWin
{
    //[ExceptionShielding("WinStudyServicePolicy")]
    [ExceptionShielding("ExceptionShieldingAndLogging")]
    [ServiceBehavior(ConcurrencyMode=ConcurrencyMode.Single, InstanceContextMode=InstanceContextMode.Single)]
    public class StudyWinService : IStudyWinService
    {
        private IRepository _repos;
        public StudyWinService(IRepository dep)
        {
            _repos = dep;
        }

        public Sa.Profile.Profile3.Services.Library.ResponseKs SubmitStudy(Sa.Profile.Profile3.Services.Library.RequestSt requestStudy)
        {
            Sa.Profile.Profile3.Services.Library.ResponseKs theResults = new Sa.Profile.Profile3.Services.Library.ResponseKs();
            try
            {
                theResults.HasErrors = true;
                Sa.Profile.Profile3.Business.Scrambler.ScramPrep sp = new Business.Scrambler.ScramPrep();
                Sa.Profile.Profile3.Business.PlantFactory.FuelsRefineryCompletePlant plant = sp.WCFToBus(requestStudy.PLA);
                List<string> brokenRules;
                List<string> warnings;
                //validate plant again eventhough already done on client
                if (plant.ValidatePlant(out brokenRules, out warnings) == false)
                {
                    if (brokenRules.Count > 0)
                    {
                        theResults.Status = "broken rules";
                        return theResults;
                    }
                }
                FuelsRefineryCompletePlantStudy stud = new FuelsRefineryCompletePlantStudy(plant);
                Sa.Profile.Profile3.Business.PlantStudySA.FuelsRefineryCompletePlantStudySA sa = new Business.PlantStudySA.FuelsRefineryCompletePlantStudySA(_repos);
                sa.DoCPAStudy(stud);
                theResults.HasErrors = false;
                theResults.Status = "Study Data Submitted";
            }
            catch (Exception ex)
            {
                string template = "SubminStudy: Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, ex.Message),ex.InnerException);
                throw infoException;
            }
            return theResults;
        }

        //Scrambler TO DO
        
        public Sa.Profile.Profile3.Services.Library.ResponseKs RequestStudyResults(Sa.Profile.Profile3.Services.Library.RequestSt requestStudyResults)
        {
            string stage = "sa.GetStudyResults()";
            Sa.Profile.Profile3.Services.Library.ResponseKs theResults = new Sa.Profile.Profile3.Services.Library.ResponseKs();
            try
            {
                theResults.Status = "OK";
                theResults.HasErrors = false;
                Sa.Profile.Profile3.Business.PlantStudySA.FuelsRefineryCompletePlantStudySA sa = new Business.PlantStudySA.FuelsRefineryCompletePlantStudySA(_repos);
                FuelsRefineryCompletePlantStudy stud2 = sa.GetStudyResults(requestStudyResults.SubmissionID);                
                theResults.ReturnKs = new Sa.Profile.Profile3.Services.Library.KPR();
                theResults.ReturnKs.KPs = new List<Library.KP>();
                stage = "ProcessKPIs";
                //TTDO: pass this through the scrambler and let the key feilds get mapped to counter chars
                foreach (Sa.Profile.Profile3.Business.PlantStudyClient.KeyPerformanceIndicator k in stud2.KeyPerformanceIndicators)
                {
                    Sa.Profile.Profile3.Services.Library.KP newK = new Sa.Profile.Profile3.Services.Library.KP();
                    newK.CalcTime = k.CalcTime;
                    newK.DataKey = k.DataKey;
                    newK.GroupKey = k.GroupKey;
                    newK.NumberValue = k.NumberValue;
                    //newK.Source //ttodo decomiss this
                    newK.StudyMethodology = k.StudyMethodology;
                    newK.TextValue = k.TextValue;
                    theResults.ReturnKs.KPs.Add(newK);
                }
            }
            catch (Exception ex)
            {
                string template = "RequestStudyResults():Stage={0} Error:{1}";
                StudyException infoException = new StudyException(string.Format(template, stage, ex.Message),ex.InnerException);
                throw infoException;
            }
            return theResults;
        }
    }
}
