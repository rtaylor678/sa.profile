///////////////////////////////////////////////////////////
//  2ClientDatasetWcfBusinessObfuscator.cs
//  Implementation of the Class 2ClientDatasetWcfBusinessObfuscator
//  Generated by Enterprise Architect
//  Created on:      07-Nov-2014 3:13:34 PM
//  Original author: HJOHNSON
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Ninject;
using Sa.Profile.Profile3.Repository.SA;
using Sa.Profile.Profile3.Business.PlantStudyClient;
using Sa.Profile.Profile3.Business.PlantStudySA;
using Sa.Profile.Profile3.Business.PlantFactory;
using Sa.Profile.Profile3.Business.Scrambler;
using Sa.Profile.Profile3.DataAccess;
using Sa.Profile.Profile3.Common;

using NUnit.Framework;

namespace Sa.Profile.Profile3.Test
{
    [TestFixture]
    public class t2_0_ClientDatasetWcfBusinessObfuscator
    {
        IRepository _repos;
        public t2_0_ClientDatasetWcfBusinessObfuscator()
        {

        }

        ~t2_0_ClientDatasetWcfBusinessObfuscator()
        {

        }

        [SetUp]
        protected void SetUp()
        {
            _repos = TheInit.TheRepos;
        }

        [Test]
        public void t2_0_test0FullyPopulateClientDatasetWithUniqueData()
        {
            MapCheckRepository mp = new MapCheckRepository();
            mp.ClearAllBeforeFields();
            mp.ClearAllAfterFields();
            FieldIncrem.InitFieldIncrem(FieldIncremMode.LARGE);
            string err = "";
            Sa.Profile.Profile3.Business.Scrambler.NewDataSet datasetWithIncremData = mp.PrimeClientDataSetWithIncremData(out err);
            Assert.AreEqual(err.Length, 0.0D);
            //we need to compare this dataaset with prior one saved, before we do this let's convert the data to our business objects
            //which serialize better than datasets
            ScramPrep sp = new ScramPrep();
            Sa.Profile.Profile3.Business.PlantFactory.FuelsRefineryCompletePlant busWithIncremData = sp.ClientToBus(datasetWithIncremData);
            Sa.Profile.Profile3.Services.Library.PL busIncrWCF = sp.BusToWCF(busWithIncremData);
            Sa.Profile.Profile3.Services.Library.PL testSavedData = XmlTools.DeserializeFromXML<Sa.Profile.Profile3.Services.Library.PL>(ModeLocations.TEST_MODE_BUILD_DIR + "test0FullyPopulateClientDatasetWithUniqueData_input.xml");
            KellermanSoftware.CompareNetObjects.CompareLogic cc = new KellermanSoftware.CompareNetObjects.CompareLogic();
            Assert.IsTrue(cc.Compare(busIncrWCF, testSavedData).AreEqual);
        }

        [Test]
        public void t2_0_test1WriteUnfuscatedDatasetDataToBeforeColumn()
        {
            MapCheckRepository mp = new MapCheckRepository();
            mp.ClearAllBeforeFields();
            mp.ClearAllAfterFields();
            Sa.Profile.Profile3.DataAccess.FieldIncrem.InitFieldIncrem(FieldIncremMode.LARGE);
            string err = "";
            Sa.Profile.Profile3.Business.Scrambler.NewDataSet newDS = mp.PrimeClientDataSetWithIncremData(out err);
            Assert.AreEqual(err.Length, 0.0D);
            bool ret = mp.PrimeBeforeWithClientDataSetData(newDS, out err);
            Assert.IsTrue(ret);
        }

        [Test]
        public void t2_0_test2TranslateClientDatasetToBusinessObject()
        {
            // already validated in: test0FullyPopulateClientDatasetWithUniqueData
            Assert.IsNull(null);
        }

        [Test]
        public void t2_0_test3ObfuscateBusinessDataColumnDataToWcf()
        {
            // already validated in: test0FullyPopulateClientDatasetWithUniqueData
            Assert.IsNull(null);
        }

        [Test]
        public void t2_0_test4DeobfuscateWcfColumnToBusinessColumn()
        {
            MapCheckRepository mp = new MapCheckRepository();
            mp.ClearAllAfterFields();
            FieldIncrem.InitFieldIncrem(FieldIncremMode.LARGE);
            string err = "";
            Sa.Profile.Profile3.Business.Scrambler.NewDataSet datasetWithIncremData = mp.PrimeClientDataSetWithIncremData(out err);
            Assert.AreEqual(err.Length, 0.0D);
            ScramPrep sp = new ScramPrep();
            Sa.Profile.Profile3.Business.PlantFactory.FuelsRefineryCompletePlant busWithIncremData = sp.ClientToBus(datasetWithIncremData);
            Sa.Profile.Profile3.Services.Library.PL busIncrWCF = sp.BusToWCF(busWithIncremData);
            FuelsRefineryCompletePlant back = sp.WCFToBus(busIncrWCF);
            //main test done here
            Sa.Profile.Profile3.Services.Library.PL backAgain = sp.BusToWCF(back);
            KellermanSoftware.CompareNetObjects.CompareLogic cc = new KellermanSoftware.CompareNetObjects.CompareLogic();
            Assert.IsTrue(cc.Compare(busIncrWCF, backAgain).AreEqual);
            mp.PrimeAfterWithBusinessObjectData(back, out err);
            Assert.AreEqual(err.Length, 0.0D);
        }

        //sequentially this needs to be done first
        [Test]
        public void t2_0_test6WriteBusinessColumnDataAfterColumn()
        {
            //test done in: test4DeobfuscateWcfColumnToBusinessColumn
            Assert.IsNull(null);
        }

        //do all of above tests again in one swoop to show before and after using same object data,
        //note above was broken in steps to validate each part, now we bring them all together
        [Test]
        public void t2_0_test5VerifyBeforeAndAfterColumnsAreIdentical()
        {
            Sa.Profile.Profile3.DataAccess.MapCheckRepository mp = new MapCheckRepository();
            mp.ClearAllBeforeFields();
            mp.ClearAllAfterFields();
            Sa.Profile.Profile3.DataAccess.FieldIncrem.InitFieldIncrem(FieldIncremMode.LARGE);
            string err = "";
            Sa.Profile.Profile3.Business.Scrambler.NewDataSet newDS = mp.PrimeClientDataSetWithIncremData(out err);
            Assert.AreEqual(err.Length, 0.0D);
            bool ret = mp.PrimeBeforeWithClientDataSetData(newDS, out err);
            Assert.IsTrue(ret);
            ScramPrep sp = new ScramPrep();
            Sa.Profile.Profile3.Business.PlantFactory.FuelsRefineryCompletePlant cli = sp.ClientToBus(newDS);
            Sa.Profile.Profile3.Services.Library.PL plRet = sp.BusToWCF(cli);
            FuelsRefineryCompletePlant back = sp.WCFToBus(plRet);
            Assert.IsTrue(mp.PrimeAfterWithBusinessObjectData(back, out err));
            Assert.IsFalse(mp.BeforeValuesNotEqualToAfterValues());
        }


    }//end 2ClientDatasetWcfBusinessObfuscator

}