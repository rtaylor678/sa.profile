﻿  Note, there is an init class found it test: t1_0_ClientSubmitsPlantDataStudyRequest
  that contains TheInitMeth() that all repos oriented tests use, this meth is called once
  for the entire set of tests, however, there are sections of code that can be uncommented 
  to enable single debug test runs

  Note also that the BusPlant objects were once compared with Kellerman directly, however, the newly 
  added List<SABusinessObject> layer causes Kellerman to throw an exception regarding non int indexed lists, 
  because of this you will see all comparing done now via our WCF objects due to their simplicity, 
  this requires conversion of bus obj to WCF before comparing regardless of test, 
  the tests are still valid since it is the core data we are after when comparing, 
  note this only affects study input data not calc results

  IMPORTANT: 
  t1_0_ClientSubmitsPlantDataStudyRequest is an integration test using web services so the windows service
  and will need to be running before executing this test, note the web service can be started by right clicking
  on the HostDevServer Project and select View In Browser

  There is sill a pull function that is needed to complete the test:t1_0_testClientinitiatesstudysubmission,
  there is a final compare needed (see last few lines of this test)