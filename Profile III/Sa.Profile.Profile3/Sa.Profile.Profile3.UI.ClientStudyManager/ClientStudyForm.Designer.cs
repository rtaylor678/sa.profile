﻿namespace Sa.Profile.Profile3.UI.ClientStudyManager
{
    partial class ClientStudyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubmitRefineryDataFileButton = new System.Windows.Forms.Button();
            this.RequestKPIResultsButton = new System.Windows.Forms.Button();
            this.SubmissionIDTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.MainGroupBox = new System.Windows.Forms.GroupBox();
            this.ViewKPIsReceivedButton = new System.Windows.Forms.Button();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.ViewFilesSubmittedButton = new System.Windows.Forms.Button();
            this.LoadRefineryDataButton = new System.Windows.Forms.Button();
            this.LoadedFilePathLabel = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.RequestKPIsBackgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SubmitRefineryDataBackgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label2 = new System.Windows.Forms.Label();
            this.LoadedFileLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.MainGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SubmitRefineryDataFileButton
            // 
            this.SubmitRefineryDataFileButton.Location = new System.Drawing.Point(6, 63);
            this.SubmitRefineryDataFileButton.Name = "SubmitRefineryDataFileButton";
            this.SubmitRefineryDataFileButton.Size = new System.Drawing.Size(154, 23);
            this.SubmitRefineryDataFileButton.TabIndex = 0;
            this.SubmitRefineryDataFileButton.Text = "Submit Refinery Data";
            this.SubmitRefineryDataFileButton.UseVisualStyleBackColor = true;
            this.SubmitRefineryDataFileButton.Click += new System.EventHandler(this.SubmitStudyButton_Click);
            // 
            // RequestKPIResultsButton
            // 
            this.RequestKPIResultsButton.Location = new System.Drawing.Point(6, 101);
            this.RequestKPIResultsButton.Name = "RequestKPIResultsButton";
            this.RequestKPIResultsButton.Size = new System.Drawing.Size(154, 23);
            this.RequestKPIResultsButton.TabIndex = 1;
            this.RequestKPIResultsButton.Text = "Request KPI Results";
            this.RequestKPIResultsButton.UseVisualStyleBackColor = true;
            this.RequestKPIResultsButton.Click += new System.EventHandler(this.RequestStudyResultsButton_Click);
            // 
            // SubmissionIDTextBox
            // 
            this.SubmissionIDTextBox.Location = new System.Drawing.Point(282, 104);
            this.SubmissionIDTextBox.Name = "SubmissionIDTextBox";
            this.SubmissionIDTextBox.Size = new System.Drawing.Size(43, 20);
            this.SubmissionIDTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Client Submission ID:";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Cursor = System.Windows.Forms.Cursors.Default;
            this.progressBar1.Location = new System.Drawing.Point(6, 139);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.progressBar1.Size = new System.Drawing.Size(582, 23);
            this.progressBar1.TabIndex = 4;
            // 
            // MainGroupBox
            // 
            this.MainGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainGroupBox.Controls.Add(this.label3);
            this.MainGroupBox.Controls.Add(this.LoadedFileLabel);
            this.MainGroupBox.Controls.Add(this.label2);
            this.MainGroupBox.Controls.Add(this.ViewKPIsReceivedButton);
            this.MainGroupBox.Controls.Add(this.StatusLabel);
            this.MainGroupBox.Controls.Add(this.ViewFilesSubmittedButton);
            this.MainGroupBox.Controls.Add(this.LoadRefineryDataButton);
            this.MainGroupBox.Controls.Add(this.LoadedFilePathLabel);
            this.MainGroupBox.Controls.Add(this.CloseButton);
            this.MainGroupBox.Controls.Add(this.progressBar1);
            this.MainGroupBox.Controls.Add(this.SubmitRefineryDataFileButton);
            this.MainGroupBox.Controls.Add(this.RequestKPIResultsButton);
            this.MainGroupBox.Controls.Add(this.SubmissionIDTextBox);
            this.MainGroupBox.Controls.Add(this.label1);
            this.MainGroupBox.Location = new System.Drawing.Point(12, 12);
            this.MainGroupBox.Name = "MainGroupBox";
            this.MainGroupBox.Size = new System.Drawing.Size(594, 201);
            this.MainGroupBox.TabIndex = 5;
            this.MainGroupBox.TabStop = false;
            this.MainGroupBox.Text = "SA Monthly Study";
            // 
            // ViewKPIsReceivedButton
            // 
            this.ViewKPIsReceivedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ViewKPIsReceivedButton.Location = new System.Drawing.Point(434, 101);
            this.ViewKPIsReceivedButton.Name = "ViewKPIsReceivedButton";
            this.ViewKPIsReceivedButton.Size = new System.Drawing.Size(154, 23);
            this.ViewKPIsReceivedButton.TabIndex = 11;
            this.ViewKPIsReceivedButton.Text = "View KPIs Received";
            this.ViewKPIsReceivedButton.UseVisualStyleBackColor = true;
            this.ViewKPIsReceivedButton.Click += new System.EventHandler(this.ViewKPIsReceivedButton_Click);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(6, 174);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(40, 13);
            this.StatusLabel.TabIndex = 10;
            this.StatusLabel.Text = "Status:";
            // 
            // ViewFilesSubmittedButton
            // 
            this.ViewFilesSubmittedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ViewFilesSubmittedButton.Location = new System.Drawing.Point(434, 63);
            this.ViewFilesSubmittedButton.Name = "ViewFilesSubmittedButton";
            this.ViewFilesSubmittedButton.Size = new System.Drawing.Size(154, 23);
            this.ViewFilesSubmittedButton.TabIndex = 9;
            this.ViewFilesSubmittedButton.Text = "View Files Submitted";
            this.ViewFilesSubmittedButton.UseVisualStyleBackColor = true;
            this.ViewFilesSubmittedButton.Click += new System.EventHandler(this.ViewFilesSubmittedButton_Click);
            // 
            // LoadRefineryDataButton
            // 
            this.LoadRefineryDataButton.Location = new System.Drawing.Point(6, 24);
            this.LoadRefineryDataButton.Name = "LoadRefineryDataButton";
            this.LoadRefineryDataButton.Size = new System.Drawing.Size(154, 23);
            this.LoadRefineryDataButton.TabIndex = 8;
            this.LoadRefineryDataButton.Text = "Load Refinery Data File";
            this.LoadRefineryDataButton.UseVisualStyleBackColor = true;
            this.LoadRefineryDataButton.Click += new System.EventHandler(this.LoadRefineryDataButton_Click);
            // 
            // LoadedFilePathLabel
            // 
            this.LoadedFilePathLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoadedFilePathLabel.AutoEllipsis = true;
            this.LoadedFilePathLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.LoadedFilePathLabel.Location = new System.Drawing.Point(197, 30);
            this.LoadedFilePathLabel.Name = "LoadedFilePathLabel";
            this.LoadedFilePathLabel.Size = new System.Drawing.Size(391, 23);
            this.LoadedFilePathLabel.TabIndex = 6;
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.Location = new System.Drawing.Point(513, 170);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 5;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // RequestKPIsBackgroundWorker1
            // 
            this.RequestKPIsBackgroundWorker1.WorkerSupportsCancellation = true;
            this.RequestKPIsBackgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RequestKPIsBackgroundWorker1_DoWork);
            this.RequestKPIsBackgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RequestKPIsBackgroundWorker1_RunWorkerCompleted);
            // 
            // SubmitRefineryDataBackgroundWorker1
            // 
            this.SubmitRefineryDataBackgroundWorker1.WorkerSupportsCancellation = true;
            this.SubmitRefineryDataBackgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SubmitRefineryDataBackgroundWorker1_DoWork);
            this.SubmitRefineryDataBackgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.SubmitRefineryDataBackgroundWorker1_RunWorkerCompleted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(170, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "File: ";
            // 
            // LoadedFileLabel
            // 
            this.LoadedFileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoadedFileLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.LoadedFileLabel.Location = new System.Drawing.Point(197, 70);
            this.LoadedFileLabel.Name = "LoadedFileLabel";
            this.LoadedFileLabel.Size = new System.Drawing.Size(227, 23);
            this.LoadedFileLabel.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Path:";
            // 
            // ClientStudyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 225);
            this.Controls.Add(this.MainGroupBox);
            this.MinimumSize = new System.Drawing.Size(634, 263);
            this.Name = "ClientStudyForm";
            this.Text = "Study Manager";
            this.MainGroupBox.ResumeLayout(false);
            this.MainGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SubmitRefineryDataFileButton;
        private System.Windows.Forms.Button RequestKPIResultsButton;
        private System.Windows.Forms.TextBox SubmissionIDTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox MainGroupBox;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button LoadRefineryDataButton;
        private System.Windows.Forms.Label LoadedFilePathLabel;
        private System.Windows.Forms.Button ViewFilesSubmittedButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.ComponentModel.BackgroundWorker RequestKPIsBackgroundWorker1;
        private System.ComponentModel.BackgroundWorker SubmitRefineryDataBackgroundWorker1;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Button ViewKPIsReceivedButton;
        private System.Windows.Forms.Label LoadedFileLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

