﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Sa.Profile.Profile3.Injection;
using Sa.Profile.Profile3.DataAccess;
using Sa.Profile.Profile3.Repository.SA;

namespace Sa.Profile.Profile3.UI.PlantManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Kernel ker = new Kernel();
            //IKernel ker2 = ker.SetupDI(RepositoryMode.TEST_MODE);
            //IKernel ker2 = ker.SetupDI(RepositoryMode.TEST_MODE_BUILD);
            IKernel ker2 = ker.SetupDI(RepositoryMode.DEV_MODE);
          
            RepositoryCommandManager mgr = new RepositoryCommandManager(ker2.Get<IRepository>());
            Console.WriteLine("Type LIST or ADD or GET then press Enter (EXIT to end).");
            string s = Console.ReadLine().ToUpper();
            while (s != "EXIT")
            {
                mgr.ExecuteCommand(s);
                s = Console.ReadLine().ToUpper();
            }
        }
    }
}
