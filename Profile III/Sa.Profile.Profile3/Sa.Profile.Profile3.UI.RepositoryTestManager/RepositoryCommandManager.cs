﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sa.Profile.Profile3.DataAccess;
using Sa.Profile.Profile3.Repository.SA;
using Sa.Profile.Profile3.Business.Scrambler;
using Sa.Profile.Profile3.Business.PlantFactory;
using Sa.Profile.Profile3.Business.PlantStudyClient;
using Sa.Profile.Profile3.Business.PlantStudySA;

namespace Sa.Profile.Profile3.UI.PlantManager
{
    public class RepositoryCommandManager
    {
        IRepository _repos;

        public RepositoryCommandManager(IRepository repository)
        {
            this._repos = repository;
        }

        public void ExecuteCommand(string command)
        {
            try
            {
                switch (command)
                {
                    case "LIST":
                        foreach (PlantStudyLog p in _repos.GetAllStudies())
                        {
                            WritePlantDetails(p);
                        }
                        break;

                    case "GET":

                        Console.WriteLine("Please enter Study ID:");
                        int id = int.Parse(Console.ReadLine());
                        PlantStudyLog single = _repos.LoadStudy(DataWith.RESULTS, id);
                        if (single == null)
                        {
                            Console.WriteLine("Study not found for ID:" + id.ToString());
                            break;
                        }
                        WritePlantDetails(single);
                        break;

                    case "ADD": //use our xml file to load core but prompt for basics

                        //use scrambler to load xml to wcf then  to bus obj
                        Sa.Profile.Profile3.Business.Scrambler.NewDataSet ds2 = new Business.Scrambler.NewDataSet();
                        ds2.ReadXml("SubmissionFull2.xml");
                        ScramPrep sp = new ScramPrep();
                        Sa.Profile.Profile3.Services.Library.PL plRet = sp.PrimeScram("", ds2);
                        FuelsRefineryCompletePlant back = sp.WCFToBus(plRet);
                        FuelsRefineryCompletePlantStudy stud = new FuelsRefineryCompletePlantStudy();
                        stud.Plant = back;
                        PlantStudyLog newPlantStudy = CollectNewPlantInformation();
                        newPlantStudy.StudyLevel = "FuelsRefineryCompletePlantStudy";
                        ((FuelsRefineryCompletePlant)stud.Plant).SubmissionIDOverride = newPlantStudy.ClientSubmissionID;
                        newPlantStudy.StudyBusinessObject = stud;
                        _repos.SaveNewStudy(newPlantStudy);

                        Console.WriteLine("Plant added.");
                        break;

                    default:
                        Console.WriteLine("Command not understood.");
                        break;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private PlantStudyLog CollectNewPlantInformation()
        {
            PlantStudyLog p = new PlantStudyLog();
            Console.WriteLine("Enter the PlantName then press enter:");
            p.PlantName = Console.ReadLine();
            switch (_repos.CurrentRepositoryParms.RunningIn)
            {
                case RepositoryMode.DEV_MODE:
                    p.XMLFileLocation = "C:\\KNPC\\Xml\\dev\\";
                    break;
                case RepositoryMode.TEST_MODE:
                    p.XMLFileLocation = "C:\\KNPC\\Xml\\test\\";
                    break;
                case RepositoryMode.TEST_MODE_BUILD:
                    p.XMLFileLocation = "C:\\KNPC\\Xml\\test\\build\\";
                    break;
                case RepositoryMode.PROD_MODE:
                    p.XMLFileLocation = "C:\\KNPC\\Xml\\prod\\";
                    break;
            }
            Console.WriteLine("Enter StudyMode:");            
            p.StudyMode = Console.ReadLine();
            Console.WriteLine("Enter ClientSubmissionID:");
            p.ClientSubmissionID = int.Parse(Console.ReadLine());
            p.TransactionDate = DateTime.Now;
            return p;
        }

        private void WritePlantDetails(PlantStudyLog p)
        {
            if(p == null)
            {
                Console.WriteLine("No such plant exists.");
            }
                else
            {
                Console.WriteLine(
                    
                        "ID: {0}, SA_SID: {1}, ClientSID: {2}, PlantName: {3}, StudyLevel: {4}, StudyMode: {5}, XML: {6}, SubmissionDate {7}",
                        p.ID.ToString(),
                        p.SASubmissionID.ToString(),
                        p.ClientSubmissionID.ToString(),
                        p.PlantName,
                        p.StudyLevel,
                        p.StudyMode,
                        p.XMLFileLocation,
                        p.TransactionDate
                        );
            }
        }
      }
   }
