﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using Ninject;
using Sa.Profile.Profile3.Repository.SA;

namespace Sa.Profile.Profile3.UI.StudyManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Sa.Profile.Profile3.Injection.Kernel ker = new Sa.Profile.Profile3.Injection.Kernel();
            IKernel ker2 = ker.SetupDI(RepositoryMode.DEV_MODE);
            //IKernel ker2 = ker.SetupDI(RepositoryMode.TEST_MODE_BUILD);
            //IKernel ker2 = ker.SetupDI(RepositoryMode.TEST_MODE);               
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(ker2.Get<IRepository>()));
        }
    }
}
