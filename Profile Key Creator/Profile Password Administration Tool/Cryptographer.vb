﻿Imports Microsoft.DSG.Security.CryptoServices
Imports System.Security.Cryptography

Module Cryptographer

    Public EncryptionKey As String = "0S1A2C3T4W5S6G7L8C9DBRMF"
    Public EncryptionIV As String = "SACTW0S1G2L3C4D5B6R7M8F9"

    '<summary>
    '   Encrypt a string
    '</summary>
    ' <param name="aString">String to encrypt</param>
    '<returns>Encrypted string</returns>
    Public Function Encrypt(ByVal aString As String) As String
        ' Note that the key and IV must be the same for the encrypt and decrypt calls.
        Dim results As String

        Try
            Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
            tdesEngine.StringKey = EncryptionKey
            tdesEngine.StringIV = EncryptionIV
            results = tdesEngine.Encrypt(aString)
        Catch anError As Exception
            Throw anError
        End Try

        Return (results)


    End Function


    '<summary>
    '   Decrypt an encrypted string
    '</summary>
    ' <param name="aString">Encrypted string to decrypt</param>
    '<returns>Decrypted string</returns>
    Public Function Decrypt(ByVal aString As String) As String
        ' Note that the key and IV must be the same for the encrypt and decript calls.
        Dim results As String

        Try
            Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
            tdesEngine.StringKey = EncryptionKey
            tdesEngine.StringIV = EncryptionIV
            results = tdesEngine.Decrypt(aString)
        Catch anError As Exception
            Throw anError
        End Try

        Return (results)

    End Function


    Public Function EncryptWithSalt(ByVal input As String, ByVal salt As String) As String
        Dim results As String

        Try
            Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
            tdesEngine.StringKey = EncryptionKey
            tdesEngine.StringIV = EncryptionIV
            results = tdesEngine.Encrypt(input)
        Catch anError As Exception
            Throw anError
        End Try

        Return (results)

    End Function

    Public Function DecryptwithSalt(ByVal input As String, ByVal salt As String) As String
        Dim results As String

        Try
            Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
            tdesEngine.StringKey = EncryptionKey
            tdesEngine.StringIV = EncryptionIV
            results = tdesEngine.Decrypt(input)
        Catch anError As Exception
            Throw anError
        End Try

        Return (results)
    End Function
    Public Function GetRefineryID(ByVal clientKey As String) As String

        If clientKey.Length > 0 Then
            Dim tokens() As String = Decrypt(clientKey).Split("$".ToCharArray)
            Return tokens(tokens.Length - 1)
        End If

        Throw New Exception("Key is not found. If you have registered with Solomon Associates, contact Solomon Associates and someone will assit you.")
    End Function


End Module

