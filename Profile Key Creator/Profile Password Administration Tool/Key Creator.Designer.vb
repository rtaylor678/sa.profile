﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Key_Creator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRN = New System.Windows.Forms.TextBox()
        Me.txtCO = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtLOC = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPW = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnEncrypt = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtKEY = New System.Windows.Forms.TextBox()
        Me.btnCreateFile = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(60, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Refnum"
        '
        'txtRN
        '
        Me.txtRN.Location = New System.Drawing.Point(126, 29)
        Me.txtRN.MaxLength = 6
        Me.txtRN.Name = "txtRN"
        Me.txtRN.Size = New System.Drawing.Size(86, 20)
        Me.txtRN.TabIndex = 1
        '
        'txtCO
        '
        Me.txtCO.Location = New System.Drawing.Point(126, 66)
        Me.txtCO.Name = "txtCO"
        Me.txtCO.Size = New System.Drawing.Size(429, 20)
        Me.txtCO.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(47, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Company"
        '
        'txtLOC
        '
        Me.txtLOC.Location = New System.Drawing.Point(126, 107)
        Me.txtLOC.Name = "txtLOC"
        Me.txtLOC.Size = New System.Drawing.Size(429, 20)
        Me.txtLOC.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(53, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Location"
        '
        'txtPW
        '
        Me.txtPW.Location = New System.Drawing.Point(126, 144)
        Me.txtPW.Name = "txtPW"
        Me.txtPW.Size = New System.Drawing.Size(429, 20)
        Me.txtPW.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(44, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Password"
        '
        'btnEncrypt
        '
        Me.btnEncrypt.Location = New System.Drawing.Point(449, 227)
        Me.btnEncrypt.Name = "btnEncrypt"
        Me.btnEncrypt.Size = New System.Drawing.Size(106, 23)
        Me.btnEncrypt.TabIndex = 8
        Me.btnEncrypt.Text = "Encrypt"
        Me.btnEncrypt.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 188)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(108, 16)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Encrypted Key"
        '
        'txtKEY
        '
        Me.txtKEY.Location = New System.Drawing.Point(126, 186)
        Me.txtKEY.Name = "txtKEY"
        Me.txtKEY.ReadOnly = True
        Me.txtKEY.Size = New System.Drawing.Size(429, 20)
        Me.txtKEY.TabIndex = 11
        '
        'btnCreateFile
        '
        Me.btnCreateFile.Location = New System.Drawing.Point(325, 227)
        Me.btnCreateFile.Name = "btnCreateFile"
        Me.btnCreateFile.Size = New System.Drawing.Size(106, 23)
        Me.btnCreateFile.TabIndex = 12
        Me.btnCreateFile.Text = "Create Key File"
        Me.btnCreateFile.UseVisualStyleBackColor = True
        '
        'Key_Creator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(567, 262)
        Me.Controls.Add(Me.btnCreateFile)
        Me.Controls.Add(Me.txtKEY)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnEncrypt)
        Me.Controls.Add(Me.txtPW)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtLOC)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCO)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtRN)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Key_Creator"
        Me.Text = "Key Creator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtRN As System.Windows.Forms.TextBox
    Friend WithEvents txtCO As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtLOC As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPW As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnEncrypt As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtKEY As System.Windows.Forms.TextBox
    Friend WithEvents btnCreateFile As System.Windows.Forms.Button
End Class
