﻿Public Class Key_Creator

    Private Sub btnEncrypt_Click(sender As Object, e As EventArgs) Handles btnEncrypt.Click
        If txtPW.Text.Length < 8 Then
            txtPW.Text = txtPW.Text + Space(8 - txtPW.Text.Length)
        End If
        Dim id As String = Encrypt(txtCO.Text & "$" & txtLOC.Text & "$" & txtPW.Text & "$" & txtRN.Text)
        txtKEY.Text = id
        Dim ref As String = GetRefineryID(id)
        MessageBox.Show(ref, "This Key Parses to Refnum:")
    End Sub

    
    Private Sub btnCreateFile_Click(sender As Object, e As EventArgs) Handles btnCreateFile.Click
        Dim sfd As New SaveFileDialog() ' this creates an instance of the SaveFileDialog called "sfd"
        sfd.Filter = "Key Files (*.key)|*.key"
        sfd.FilterIndex = 1
        sfd.RestoreDirectory = True
        sfd.FileName = txtCO.Text + "_" + txtLOC.Text
        If sfd.ShowDialog() = DialogResult.OK Then
            Dim FileName As String = sfd.FileName ' retrieve the full path to the file selected by the user
            Dim sw As New System.IO.StreamWriter(FileName, False) ' create a StreamWriter with the FileName selected by the User
            sw.WriteLine(txtKEY.Text) ' Write the contents of TextBox to the file
            sw.Close() ' close the file
        End If
    End Sub
End Class