﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.ComponentModel;

namespace ProfileEnvironment
{
    /**
     * <summary>
     * This class provides regional currency information
     * based on the current user 
     * </summary>
     * <remarks>
     * </remarks>
     * <returns>
     * An object of values representing the regional currency properties
     * </returns>
     */
    [Serializable]
    public class RegionalCurrency
    {
        public RegionalCurrency()
        {
        }

        #region Public Properties

        [FieldNullable(IsNullable = false)]
        public string UserId { get; set; }

        [FieldNullable(IsNullable = false)]
        public string CurrencySymbol
        {
            get
            {
                return GetCurrencySymbol();
            }
        }

        [FieldNullable(IsNullable = false)]
        public string PositiveCurrencyFormat
        {
            get
            {
                return GetPositiveCurrencyFormat();
            }
        }

        [FieldNullable(IsNullable = false)]
        public string NegativeCurrencyFormat
        {
            get
            {
                return GetNegativeCurrencyFormat();
            }
        }

        [FieldNullable(IsNullable = false)]
        public string DecimalSymbol
        {
            get
            {
                return GetDecimalSymbol();
            }
        }

        [FieldNullable(IsNullable = false)]
        public string NumberOfDigitsAfterDecimal
        {
            get
            {
                return GetNumberOfDigitsAfterDecimal();
            }
        }

        [FieldNullable(IsNullable = false)]
        public string DigitGroupingSymbol
        {
            get
            {
                return GetDigitGroupingSymbol();
            }
        }

        //[FieldNullable(IsNullable = false)]
        //public string DigitGrouping
        //{
        //    get
        //    {
        //        return GetDigitGrouping();
        //    }
        //}

        #endregion Public Properties


        #region Private Methods

        private string GetCurrencySymbol()
        {
            string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.CurrencySymbol.ToString();

            return retVal;
        }
        private string GetPositiveCurrencyFormat()
        {
            int index = Convert.ToInt32(System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyPositivePattern.ToString());
            string retVal = string.Empty;

            // Create a read-write NumberFormatInfo object for the current culture.
            NumberFormatInfo numberInfo = NumberFormatInfo.CurrentInfo.Clone() as NumberFormatInfo;
            decimal value = 1.1m;

            // Assign each possible value to the CurrencyPostivePattern property.
            int rowIdx = 0;
            for (int ctr = 0; ctr <= 15; ctr++)
            {
                numberInfo.CurrencyPositivePattern = ctr;

                if (rowIdx == index)
                {

                    retVal += String.Format("{1}", ctr, value.ToString("C", numberInfo));
                    break;
                }

                rowIdx++;
            }

            return retVal;
        }  
        private string GetNegativeCurrencyFormat()
        {

            int index = Convert.ToInt32(System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyNegativePattern.ToString());
            string retVal = string.Empty;

            // Create a read-write NumberFormatInfo object for the current culture.
            NumberFormatInfo numberInfo = NumberFormatInfo.CurrentInfo.Clone() as NumberFormatInfo;
            decimal value = -1.1m;

            // Assign each possible value to the CurrencyNegativePattern property.
            int rowIdx = 0;
            for (int ctr = 0; ctr <= 15; ctr++)
            {
                numberInfo.CurrencyNegativePattern = ctr;

                if (rowIdx == index)
                {
                 
                    retVal += String.Format("{1}", ctr, value.ToString("C", numberInfo)); 
                    break;
                }

                rowIdx++;
            }

            return retVal;
        }
        private string GetDecimalSymbol()
        {
            string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator.ToString();

            return retVal;
        }
        private string GetNumberOfDigitsAfterDecimal()
        {
            string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyDecimalDigits.ToString();

            return retVal;
        }
        private string GetDigitGroupingSymbol()
        {
            string retVal = System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyGroupSeparator.ToString();

            return retVal;
        }

        //private string GetDigitGrouping()
        //{
        //  //TO DO:
        //    string retVal = System.Globalization.NumberFormatInfo.CurrentInfo..ToString();

        //    return retVal;
        //}

        #endregion Private Methods
    }
}
