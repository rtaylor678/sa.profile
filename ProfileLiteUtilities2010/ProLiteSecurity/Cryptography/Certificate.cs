﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Threading.Tasks;


namespace ProLiteSecurity.Cryptography
{
    [Serializable]
	public class Certificate
	{
		public bool Get(string name, out X509Certificate2 certificate)
		{
			try
			{
                //StoreName.My = "Personal" in MMC. This is only for client side, server would use "TrustedPeople")
                X509Store localStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                if (LookInStore(name, out certificate, localStore))
                {
                    return true;
                }

                //check next one
                X509Store currentUserStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                if (LookInStore(name, out certificate, currentUserStore))
                {
                    return true;
                }
                
                //not found in either
				return false;				
			}
			catch
			{
				certificate = new X509Certificate2();
				return false;
			}
		}

        private bool LookInStore(string name, out X509Certificate2 certificate,
            X509Store store)
        {
            bool result = false;
            try
            {
                store.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection certColl = store.Certificates.Find(X509FindType.FindBySubjectName, name, true);
                //what if certColl.Count > 1?
                certificate = certColl[0];
                result= true;
                try
                {
                    store.Close();
                    certColl.Clear();
                }
                catch (Exception ignore)//don't care if error closing out.
                { }
            }
            catch
            {
                certificate = new X509Certificate2();
                result= false;
            }
            return result;
        }
	}
}