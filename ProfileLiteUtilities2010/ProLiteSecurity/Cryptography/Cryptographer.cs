﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.DSG.Security.CryptoServices;

namespace ProLiteSecurity.Cryptography
{
    [Serializable]
    public class Cryptographer
    {
        public const string EncryptionKey = "0S1A2C3T4W5S6G7L8C9DBRMF";
        public const string EncryptionIV = "SACTW0S1G2L3C4D5B6R7M8F9";

        public Cryptographer()
        {            
            //works when in Cryptography folder  EmbeddedAssembly.Load("ProLiteSecurity.Cryptography.log4net.dll", "log4net.dll");
            //works when in Project's folder EmbeddedAssembly.Load("ProLiteSecurity.log4net.dll", "log4net.dll");
            bool loadThis =true;
            if (EmbeddedAssembly.dic != null)
            {
                if (EmbeddedAssembly.dic.Count > 0)
                {
                    foreach (Object item in EmbeddedAssembly.dic.Keys)
                    {
                        if(item.ToString().Contains("Microsoft.DSG.Security.CryptoServices"))
                        {
                            loadThis=false;
                        }
                    }
                }
            }
            if(loadThis)
            {
                EmbeddedAssembly.Load("ProLiteSecurity.Microsoft.DSG.Security.CryptoServices.dll", "Microsoft.DSG.Security.CryptoServices.dll");
                AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
            }
        }

        public string Encrypt(string aString)
        {
            // Note that the key and IV must be the same for the encrypt and decrypt calls.
            string results = string.Empty;
            try
            {
                TDES tdesEngine = new TDES(EncodingType.ASCIIEncoding);
                tdesEngine.StringKey = EncryptionKey;
                tdesEngine.StringIV = EncryptionIV;
                results = tdesEngine.Encrypt(aString);
            }
            catch (Exception anError)
            {
                throw anError;
            }

            return results;

        }

        public string Decrypt(string aString)
        {
            // Note that the key and IV must be the same for the encrypt and decript calls.
            string results = string.Empty;

            try
            {
                TDES tdesEngine = new TDES(EncodingType.ASCIIEncoding);
                tdesEngine.StringKey = EncryptionKey;
                tdesEngine.StringIV = EncryptionIV;
                results = tdesEngine.Decrypt(aString);
            }
            catch (Exception anError)
            {
                throw anError;
            }

            return results;

        }

        //for Embedded Assembly
        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return EmbeddedAssembly.Get(args.Name);
        }
        //for Embedded Assembly
        static byte[] StreamToBytes(System.IO.Stream input)
        {
            var capacity = input.CanSeek ? (int)input.Length : 0;
            using (var output = new System.IO.MemoryStream(capacity))
            {
                int readLength;
                var buffer = new byte[4096];

                do
                {
                    readLength = input.Read(buffer, 0, buffer.Length);
                    output.Write(buffer, 0, readLength);
                }
                while (readLength != 0);

                return output.ToArray();
            }
        }

    }
}
