﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Xml;
using System.Configuration;



namespace ProfileLiteUtilitiesTest
{
    [TestClass]
    public class ProfileLiteUtilitiesTester
    {
        ProfileLiteAddin2010.ProfileLiteUtilities profileLiteUtilities = null;
        //ProfileLiteWS.ProfileLiteWebServices profileLiteWebServices = null;

        [TestInitialize]
        public void CreateObject()
        {
            profileLiteUtilities = new ProfileLiteAddin2010.ProfileLiteUtilities();
            //profileLiteWebServices = new ProfileLiteWS.ProfileLiteWebServices();
            //profileLiteWebServices.Url = ConfigurationManager.AppSettings["WebServiceUrl"].ToString();
            //profileLiteWebServices.SendTracing("Domain","test");    
        }


        [TestMethod]
        public void TestFuelsDbPopulation()
        {
            profileLiteUtilities.AddSettings("CompanyName", "Location", "CoordName", "1", "1", "1", "1", "1", "1", false, 1, 2015,
                "Actual", "1", "1");
            profileLiteUtilities.AddUserDefined("Loss", "Total Fuels Refinery Physical Hydrocarbon Loss",
                6109, 0, 0, 0, 0);
            profileLiteUtilities.AddUserDefined("Flare", "Estimated Flare Losses Included in Above",
                 2993, 0, 0, 0, 0);
            profileLiteUtilities.AddMaintRoutFull(11001, "CDU", 100, 0, 0, 0, 0, 0, 200, 0, 0, 300,0,0,0,0);
            PrivateObject obj = new PrivateObject(profileLiteUtilities);
            var retVal = obj.Invoke("ReturnEntireDataset");
            System.Data.DataSet dsp = (DataSet)retVal;
            Assert.IsTrue(dsp.Tables["Settings"].Rows[0]["Company"].ToString()== "CompanyName");

            DataTable dt = dsp.Tables["UserDefined"];
            Assert.IsTrue(dt.Columns["DecPlaces"].DataType.ToString()=="System.Byte");


            //Yasref
            String CompanyLocationRefineryid = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
            
            //"Kuwait National Petroleum Company$Shuaiba Refinery$
            //String CompanyLocationRefineryid = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFjPwo9kHMAY+uC/iBaNsXEYsKBnr59iCOPxU7c5FNBkC";
            //profileLiteUtilities.SubmitData( CompanyLocationRefineryid); //this doesn't return anything so would need to check db for records after this-or put break in web service.
            
        }

        [TestMethod]
        public void TestLubesDbPopulation()
        {
            profileLiteUtilities.AddSettings("BLBOC", "SITRAH", "SUHA AHMED ALSHAFEI", "1", "1", "1", "1", "1", "1", false, 1, 2015, "TEST", "1", "1");
            profileLiteUtilities.AddConfigRS("RSCRUDE", 1, 0, "0", 0, 0);
            profileLiteUtilities.AddConfigRS("RSPROD", 2, 0, "0", 0, 0);
            profileLiteUtilities.AddInventory("FDS", 0, 0, 0, 46, 199926);
            profileLiteUtilities.AddInventory("INT", 0, 0, 0, 54, 333);
            profileLiteUtilities.AddOpexAll(444, 16, 68, 1,0, 920);
            profileLiteUtilities.AddPersAll("OCCPO", 13796, 7562, 0);
            profileLiteUtilities.AddYield_RM("MPROD", "L117", 297658, "",0,843, 39918);
            profileLiteUtilities.AddEnergy("PUR", "NA", "FG_", 74567, 0);
            profileLiteUtilities.AddElectric("SOL", "OTH", "ELE", 0, 1000, 0);
            
            profileLiteUtilities.AddConfig(10002,"LBOU-RS","CDWAX","ISO",10000,84,0,0,0,0,40);
            profileLiteUtilities.AddConfig(90001,"","STEAMGEN","SFB",19,20,0, 0, 
            0, 0, 0, 0, "", "", 0, 0, 21);
            profileLiteUtilities.AddConfig(0,"","FTCOGEN","AIR",120,0,212,50, 0, 0, 40, 
            0, "","", 0, 0, 100); //, Empty, Empty, Empty

            /*
             *this if failing - 20 args
            ProfileUtilities.AddConfig UnitID, UnitName, ProcessID, ProcessType, RptCap, UtilPcnt, RptStmCap, StmUtilPcnt, Empty, Empty, totalHoursStaffedPerWeek, _
            Empty, Empty, Empty, Empty, Empty, capacityAllocatedToLubePct, Empty, Empty, Empty
            */
            //interface:
            /*
            Sub AddConfig(ByVal UnitID As Integer, ByVal UnitName As String, ByVal ProcessID As String, ByVal ProcessType As String, _
                  ByVal RptCap As Single, ByVal UtilPcnt As Single, ByVal RptStmCap As Single, ByVal StmUtilPcnt As Single, _
                  Optional ByVal InServicePcnt As Decimal = 0, Optional ByVal YearsOper As Decimal = 0, Optional ByVal MHPerWeek As Single = 0, _
                  Optional ByVal PostPerShift As Decimal = 0, Optional ByVal BlockOp As String = "", Optional ByVal ControlType As String = "", _
                  Optional ByVal CapClass As Byte = Nothing, Optional ByVal UtilCap As Single = 0, Optional ByVal StmUtilCap As Single = 0, _
                  Optional ByVal AllocPcntOfTime As Decimal = 0, Optional ByVal AllocPcntOfCap As Decimal = 0, Optional ByVal AllocUtilPcnt As Decimal = 0)
            */
            profileLiteUtilities.AddConfig(4000, "UnitName", "ProcessID", "ProcessType",
             0, 0, 0, 0, 0, 0, 0, 0, "BlockO", "CTyp", 0, 0, 0, 0, 0, 0);

            

            profileLiteUtilities.AddProcessData(10001, "BC_OperPcnt", 100);
            profileLiteUtilities.AddProcessData(10002, "NC_LN_OperPcnt", 63);
            //this causes error during merge in ws (0 (empty) Unit ID):
            //profileLiteUtilities.AddProcessData(0, "FeedH2", 12);
            profileLiteUtilities.AddProcessData(10003, "FeedRateGas", 13);

            profileLiteUtilities.AddMaintTA(10001, 2, "RERUN", new DateTime(2015, 03, 01), 2, 3, 5, 6, new DateTime(2015, 02, 01), 0, 4);
            profileLiteUtilities.AddMaintTA(100002, 1, "TEST", new DateTime(2015, 03, 01), 0, 0, 0, 0, new DateTime(2015, 03, 01), 0, 2);
            //ProfileUtilities.AddMaintRoutFull UnitID, ProcessID, RoutCostLocal, Empty, Empty, Empty, Empty, Empty, RegDown, Empty, Empty, MaintDown, Empty, Empty, Empty, Empty
            profileLiteUtilities.AddMaintRoutFull(10001,"RERUN",609,0,0,0,0,0,11,0,0,10,0,0,0,0);
            profileLiteUtilities.AddUserDefined("Loss", "Total Lube Refinery Physical Hydrocarbon Loss",
                238.8, 0, 0, 0, 0);
            profileLiteUtilities.AddUserDefined("Flare", "Estimated Flare Losses Included in Above",
                 119, 0, 0, 0, 0);
            //---------------------------------------
            PrivateObject obj = new PrivateObject(profileLiteUtilities);
            var retVal = obj.Invoke("ReturnEntireDataset");
            System.Data.DataSet dsp = (DataSet)retVal;
            Assert.IsTrue(dsp.Tables["Settings"].Rows[0]["Company"].ToString() == "BLBOC");
            int count = 0;
            foreach (DataRow dr in dsp.Tables["ConfigRS"].Rows)
            {
                if (dr["ProcessID"].ToString() == "RSCRUDE") { count++; }
                if (dr["ProcessID"].ToString() == "RSPROD") { count++; }
            }
            Assert.AreEqual(count, 2);
            foreach (DataRow dr in dsp.Tables["Inventory"].Rows)
            {
                if (dr["Inven"].ToString() == "199926") { count++; }
                if (dr["TankType"].ToString() == "INT") { count++; }
            }
            
            //Assert.IsTrue(dsp.Tables["OpexAll"].Rows[0][""].ToString() == "");
            Assert.IsTrue(dsp.Tables["Pers"].Rows[0]["Contract"].ToString() == "7562");
            Assert.IsTrue(dsp.Tables["Yield_RM"].Rows[0]["Category"].ToString() == "MPROD");
            Assert.IsTrue(dsp.Tables["Yield_RM"].Rows[0]["MT"].ToString() == "39918");
            Assert.IsTrue(dsp.Tables["Energy"].Rows[0]["TransType"].ToString() == "PUR");
            Assert.IsTrue(dsp.Tables["Electric"].Rows[0]["TransType"].ToString() == "SOL");

            //Assert.IsTrue(dsp.Tables["Config"].Rows.Count==4);
            Assert.IsTrue(dsp.Tables["Opexall"].Rows.Count == 5);
            Assert.IsTrue(dsp.Tables["Electric"].Rows.Count == 1);
            count = 0;
            foreach (DataRow dr in dsp.Tables["MaintTA"].Rows)
            {
                if (dr["TALaborCostLocal"].ToString() == "4") { count++; }
            }
            Assert.AreEqual(count,1);
            Assert.IsTrue(dsp.Tables["MaintRout"].Rows.Count == 1);
            Assert.IsTrue(dsp.Tables["UserDefined"].Rows.Count == 2);
            Assert.IsTrue(dsp.Tables["MaintRout"].Rows.Count == 1);

            //DataTable dt = dsp.Tables["UserDefined"]; Assert.IsTrue(dt.Columns["DecPlaces"].DataType.ToString() == "System.Byte");


            //String CompanyLocationRefineryid = "RBvmBYi9YZu7o0CyY8VlFaY6Z+XfWjE2L94XKyCSW5A=";
            //profileLiteUtilities.SubmitData(CompanyLocationRefineryid); //this doesn't return anything so would need to check db for records after this-or put break in web service.

            //############### also change to 2010 and add in a MaintTA record, ensure it works with and witout the new field TALaborCostLocal
            
        }


        [TestMethod]
        public void WsTest()
        {
            //ProfileLiteWS.ProfileLiteWebServices ws = new ProfileLiteWS.ProfileLiteWebServices();
            //ws.TestWS();
            //ws.Dispose();             
        }

        [TestCleanup]
        public void DisposeObject()
        {
            profileLiteUtilities.Dispose();
        }
    }
}
