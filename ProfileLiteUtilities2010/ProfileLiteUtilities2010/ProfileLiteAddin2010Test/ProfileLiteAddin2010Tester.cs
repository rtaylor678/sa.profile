﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Xml;

namespace ProfileLiteAddin2010Test
{
    [TestClass]
    public class ProfileLiteAddin2010Tester
    {
        ProfileLiteAddin2010.ProfileLiteUtilities _profileLiteUtilities = null;

        [TestInitialize]
        public void CreateObject()
        {
            _profileLiteUtilities = new ProfileLiteAddin2010.ProfileLiteUtilities();            
        }

        [TestMethod]
        public void TestDbPopulation()
        {            
            _profileLiteUtilities.AddSettings("CompanyName", "Location", "CoordName", "1", "1", "1", "1", "1", "1", false, 1, 2015, "TEST", "1", "1");
            _profileLiteUtilities.AddUserDefined("Loss", "Total Fuels Refinery Physical Hydrocarbon Loss",
                6109, 0, 0, 0, 0);
            _profileLiteUtilities.AddUserDefined("Flare", "Estimated Flare Losses Included in Above",
                 2993, 0, 0, 0, 0);
            _profileLiteUtilities.AddMaintRoutFull(11001, "CDU", 100, 0, 0, 0, 0, 0, 200, 0, 0, 300, 0, 0, 0, 0);
            PrivateObject obj = new PrivateObject(_profileLiteUtilities);
            var retVal = obj.Invoke("ReturnEntireDataset");
            System.Data.DataSet dsp = (DataSet)retVal;
            var z = dsp.Tables["Settings"].Rows[0];
            Assert.IsTrue(dsp.Tables["Settings"].Rows[0]["Company"].ToString() == "CompanyName");

            DataTable dt = dsp.Tables["UserDefined"];
            Assert.IsTrue(dt.Columns["DecPlaces"].DataType.ToString() == "System.Byte");

            //Yasref:
            string clientKey = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
            //legacy (bapco):
            //string clientKey = "svXBytSZaapxU8S/sKKrIACUiImqZYdRsSgGXreiUoE=";
            /*
            _profileLiteUtilities.AuthorizeRefineryLogin(clientKey, "S010M0N");
            _profileLiteUtilities.SubmitData(clientKey); //this doesn't return anything so would need to check db for records after this-or put break in web service.
             */
            _profileLiteUtilities.AuthorizeRefineryLogin(clientKey, "S010M0N");
            //_profileLiteUtilities.SubmitData(clientKey); //this doesn't return anything so would need to check db for records after this-or put break in web service.
        }

        [TestMethod]
        public void TestLubesDbPopulation()
        {
            _profileLiteUtilities.AddSettings("BLBOC", "SITRAH", "SUHA AHMED ALSHAFEI", "1", "1", "1", "1", "1", "1", false, 1, 2015, "TEST", "1", "1");
            _profileLiteUtilities.AddConfigRS("RSCRUDE", 1, 0, "0", 0, 0);
            _profileLiteUtilities.AddConfigRS("RSPROD", 2, 0, "0", 0, 0);
            _profileLiteUtilities.AddInventory("FDS", 0, 0, 0, 46, 199926);
            _profileLiteUtilities.AddInventory("INT", 0, 0, 0, 54, 333);
            _profileLiteUtilities.AddOpexAll(444, 16, 68, 1, 920,211);
            _profileLiteUtilities.AddPersAll("OCCPO", 13796, 7562, 0);
            _profileLiteUtilities.AddYield_RM("MPROD", "L117", 297658, "", 0, 843, 39918);
            _profileLiteUtilities.AddEnergy("PUR", "NA", "FG_", 74567, 0);
            _profileLiteUtilities.AddElectric("SOL", "OTH", "ELE", 0, 1000, 0);

            _profileLiteUtilities.AddConfig(10002, "LBOU-RS", "CDWAX", "ISO", 10000, 84, 0, 0, 0, 0, 40);
            _profileLiteUtilities.AddConfig(90001, "", "STEAMGEN", "SFB", 19, 20, 0, 0,
            0, 0, 0, 0, "", "", 0, 0, 21);
            _profileLiteUtilities.AddConfig(0, "", "FTCOGEN", "AIR", 120, 0, 212, 50, 0, 0, 40,
            0, "", "", 0, 0, 100); //, Empty, Empty, Empty

            /*
             *this if failing - 20 args
            ProfileUtilities.AddConfig UnitID, UnitName, ProcessID, ProcessType, RptCap, UtilPcnt, RptStmCap, StmUtilPcnt, Empty, Empty, totalHoursStaffedPerWeek, _
            Empty, Empty, Empty, Empty, Empty, capacityAllocatedToLubePct, Empty, Empty, Empty
            */
            //interface:
            /*
            Sub AddConfig(ByVal UnitID As Integer, ByVal UnitName As String, ByVal ProcessID As String, ByVal ProcessType As String, _
                  ByVal RptCap As Single, ByVal UtilPcnt As Single, ByVal RptStmCap As Single, ByVal StmUtilPcnt As Single, _
                  Optional ByVal InServicePcnt As Decimal = 0, Optional ByVal YearsOper As Decimal = 0, Optional ByVal MHPerWeek As Single = 0, _
                  Optional ByVal PostPerShift As Decimal = 0, Optional ByVal BlockOp As String = "", Optional ByVal ControlType As String = "", _
                  Optional ByVal CapClass As Byte = Nothing, Optional ByVal UtilCap As Single = 0, Optional ByVal StmUtilCap As Single = 0, _
                  Optional ByVal AllocPcntOfTime As Decimal = 0, Optional ByVal AllocPcntOfCap As Decimal = 0, Optional ByVal AllocUtilPcnt As Decimal = 0)
            */
            _profileLiteUtilities.AddConfig(4000, "UnitName", "ProcessID", "ProcessType",
             0, 0, 0, 0, 0, 0, 0, 0, "BlockO", "CTyp", 0, 0, 0, 0, 0, 0);



            _profileLiteUtilities.AddProcessData(10001, "BC_OperPcnt", 100);
            _profileLiteUtilities.AddProcessData(10002, "NC_LN_OperPcnt", 63);
            //this causes error during merge in ws (0 (empty) Unit ID):
            //_profileLiteUtilities.AddProcessData(0, "FeedH2", 12);
            _profileLiteUtilities.AddProcessData(10003, "FeedRateGas", 13);

            _profileLiteUtilities.AddMaintTA(10001, 2, "RERUN", new DateTime(2015, 03, 01), 2, 3, 5, 6, new DateTime(2015, 02, 01), 0, 4);
            //ProfileUtilities.AddMaintRoutFull UnitID, ProcessID, RoutCostLocal, Empty, Empty, Empty, Empty, Empty, RegDown, Empty, Empty, MaintDown, Empty, Empty, Empty, Empty
            _profileLiteUtilities.AddMaintRoutFull(10001, "RERUN", 609, 0, 0, 0, 0, 0, 11, 0, 0, 10, 0, 0, 0, 0);
            _profileLiteUtilities.AddUserDefined("Loss", "Total Lube Refinery Physical Hydrocarbon Loss",
                238.8, 0, 0, 0, 0);
            _profileLiteUtilities.AddUserDefined("Flare", "Estimated Flare Losses Included in Above",
                 119, 0, 0, 0, 0);
            //---------------------------------------
            PrivateObject obj = new PrivateObject(_profileLiteUtilities);
            var retVal = obj.Invoke("ReturnEntireDataset");
            System.Data.DataSet dsp = (DataSet)retVal;
            Assert.IsTrue(dsp.Tables["Settings"].Rows[0]["Company"].ToString() == "BLBOC");
            int count = 0;
            foreach (DataRow dr in dsp.Tables["ConfigRS"].Rows)
            {
                if (dr["ProcessID"].ToString() == "RSCRUDE") { count++; }
                if (dr["ProcessID"].ToString() == "RSPROD") { count++; }
            }
            Assert.AreEqual(count, 2);
            foreach (DataRow dr in dsp.Tables["Inventory"].Rows)
            {
                if (dr["Inven"].ToString() == "199926") { count++; }
                if (dr["TankType"].ToString() == "INT") { count++; }
            }
            count = 0;
            //foreach(DataColumn dc in  dsp.Tables["Opexall"].Columns)
            
            foreach (DataRow dr in dsp.Tables["Opexall"].Rows)
            {
                for (int OpexCol = 0; OpexCol < dsp.Tables["Opexall"].Columns.Count; OpexCol++)
                {
                    if (dr[OpexCol].ToString() == "211") { count++; }
                }
            }
            Assert.IsTrue(count == 1);

            //Assert.IsTrue(dsp.Tables["OpexAll"].Rows[0][""].ToString() == "");
            Assert.IsTrue(dsp.Tables["Pers"].Rows[0]["Contract"].ToString() == "7562");
            Assert.IsTrue(dsp.Tables["Yield_RM"].Rows[0]["Category"].ToString() == "MPROD");
            Assert.IsTrue(dsp.Tables["Yield_RM"].Rows[0]["MT"].ToString() == "39918");
            Assert.IsTrue(dsp.Tables["Energy"].Rows[0]["TransType"].ToString() == "PUR");
            Assert.IsTrue(dsp.Tables["Electric"].Rows[0]["TransType"].ToString() == "SOL");

            //Assert.IsTrue(dsp.Tables["Config"].Rows.Count==4);
            Assert.IsTrue(dsp.Tables["Electric"].Rows.Count == 1);
            Assert.IsTrue(dsp.Tables["MaintTA"].Rows[0]["TALaborCostLocal"].ToString() == "4");
            Assert.IsTrue(dsp.Tables["MaintRout"].Rows.Count == 1);
            Assert.IsTrue(dsp.Tables["UserDefined"].Rows.Count == 2);
            Assert.IsTrue(dsp.Tables["MaintRout"].Rows.Count == 1);

            //DataTable dt = dsp.Tables["UserDefined"]; Assert.IsTrue(dt.Columns["DecPlaces"].DataType.ToString() == "System.Byte");


            //String CompanyLocationRefineryid = "RBvmBYi9YZu7o0CyY8VlFaY6Z+XfWjE2L94XKyCSW5A=";
            //_profileLiteUtilities.SubmitData(CompanyLocationRefineryid); //this doesn't return anything so would need to check db for records after this-or put break in web service.



            //############### also change to 2010 and add in a MaintTA record, ensure it works with and witout the new field TALaborCostLocal



        }


        [TestMethod]
        public void WsTest()
        {
            string addinTestresult = _profileLiteUtilities.TestAddIn();
            Assert.IsTrue(addinTestresult == "TestAddIn = Success!");
            string wsTestResult = _profileLiteUtilities.TestWebService("test",
                "XXPAC","10.10.10.10","test","testpc","ProfileLite Addin test","test",
                "","","","","");
            DateTime result = DateTime.Parse(wsTestResult);
            Assert.IsTrue(result > DateTime.Now.AddMinutes(-1));
        }

        [TestCleanup]
        public void DisposeObject()
        {
            _profileLiteUtilities.Dispose();
        }
    }
}
