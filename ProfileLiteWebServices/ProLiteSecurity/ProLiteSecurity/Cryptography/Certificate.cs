﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Threading.Tasks;


namespace ProLiteSecurityServer.Cryptography
{
    [Serializable]
	public class Certificate
	{
        
        public Certificate()
        {

        }

		public bool Get(string name, out X509Certificate2 certificate)
		{
			try
			{
                X509Store storeMy = new X509Store(StoreName.TrustedPeople, StoreLocation.LocalMachine);
				
				storeMy.Open(OpenFlags.ReadOnly);

				X509Certificate2Collection certColl = storeMy.Certificates.Find(X509FindType.FindBySubjectName, name, true);

                /*
                certColl = certColl.Find(X509FindType.FindByIssuerDistinguishedName, name, true);
				certColl = certColl.Find(X509FindType.FindBySerialNumber, name, true);
                */

				certificate = certColl[0];

				storeMy.Close();
				certColl.Clear();
				return true;
				
			}
			catch
			{
				certificate = new X509Certificate2();
				return false;
			}
		}
	}
}