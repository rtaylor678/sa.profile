Imports System.IO


Module WebServicesHelper
    

    '<summary>
    '   Return refinery id from user token string
    '</summary>
    ' <param name="context">Current context</param>
    '<returns>Refinery id</returns>
    Public Function GetRefineryID(ByVal clientKey As String) As String

       
        If clientKey.Length > 0 Then
            Dim tokens() As String = Decrypt(clientKey).Split("$".ToCharArray)
            Return tokens(tokens.Length - 1)
        End If

        Throw New Exception("Key is not found. If you have registered with Solomon Associates, contact Solomon Associates and someone will assit you.")
    End Function


    '<summary>
    '   Returns company id 
    '</summary>
    ' <param name="context">Current context</param>
    '<returns>Company id</returns>
    Public Function GetCompanyID(ByVal clientKey As String) As String

        If clientKey.Length > 0 Then
            Dim tokens() As String = Decrypt(clientKey).Split("$".ToCharArray)
            Return tokens(0)
        End If

        Throw New Exception("Key is not found. If you have registered with Solomon Associates, contact Solomon Associates and someone will assit you.")
    End Function

    Public Sub ArchiveLogs()
        Try
            Dim archiveExtension As String = ".old"
            Dim logPath As String = ConfigurationManager.AppSettings.Item("LogFilePath").ToString()
            Dim archivePath As String = logPath + archiveExtension
            Dim logsInfo As New FileInfo(logPath)
            If DateDiff("d", logsInfo.CreationTime, Now, ) > 13 Or logsInfo.Length > 10000000 Then
                'need to do it this way because id only has write access, not delete/create!
                Try
                    'truncate archive file
                    Dim fs As New FileStream(archivePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)
                    Using writer As New StreamWriter(fs)
                        fs.SetLength(0)
                    End Using

                    'read latest file into archive file
                    Dim sr As New StreamReader(logPath)
                    Dim sw As New StreamWriter(archivePath)
                    While Not sr.EndOfStream
                        Dim line As String = sr.ReadLine()
                        sw.WriteLine(line)
                    End While
                    sr.Close()
                    sw.Close()

                    'truncate latest file
                    Dim fs2 As New FileStream(logPath, FileMode.OpenOrCreate, FileAccess.ReadWrite)
                    Using writer As New StreamWriter(fs2)
                        fs2.SetLength(0)
                    End Using
                Catch exDelete As Exception
                    'do nothing
                End Try
            End If
        Catch ex As Exception
            'do nothing
        End Try
    End Sub



End Module
