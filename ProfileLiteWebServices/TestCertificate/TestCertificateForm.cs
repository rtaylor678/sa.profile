﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TestCertificate;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using TestCertificate.WsXmlProLite;



namespace TestCertificate
{
	public partial class TestCertificateForm : Form
	{

		private string clientKey = string.Empty;
		private byte[] encodedSignedCms = new byte[0];

		public TestCertificateForm()
		{
			InitializeComponent();
		}

		private void btnCheckService_Click(object sender, EventArgs e)
		{


			using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
			{
				txtCheckService.Text = client.CheckService();
			}
		}


		private void btnGetCertificate_Click(object sender, EventArgs e)
		{

			if (txtGetCertResponse.Text == string.Empty)
			{
				MessageBox.Show("Please enter text");
				txtGetCertResponse.Focus();
				return;
			}

			using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
			{
				Certificate cert = new Certificate();
				string name = txtGetCertResponse.Text + " (" + txtClientKey.Text + ")";
				clientKey = txtClientKey.Text.Trim();
				X509Certificate2 privateX509Cert = new X509Certificate2();
				bool success = cert.Get(name, out privateX509Cert);

				if (txtClientKey.Text.Length > 0 && lblGetName.Text == "Ref Num :")
				{
					if (privateX509Cert != null)
					{

						if (success == true)
						{
							lblCertResponse.Text = "Successful";
							txtGetCertResponse.Text = name;
						}
						else
						{
							lblCertResponse.Text = "Unsuccessful";
						}
					}
					else
					{
						lblCertResponse.Text = "Unsuccessful";
					}

				}
				else
				{
					MessageBox.Show("RefNum required");
				}

				NonRepudiation nonRep = new NonRepudiation();
				string errorMsg = string.Empty;
				byte[] messageBytes = System.Text.Encoding.ASCII.GetBytes(clientKey);


				nonRep.Sign(ref errorMsg, messageBytes, privateX509Cert, out encodedSignedCms, true);
                
			}

		}

        private byte[] GetEncodedSignedCmsFromNameWithRefnum(string nameWithRefNum)
        {
            using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
            {
                Certificate cert = new Certificate();
                //string name = txtGetCertResponse.Text + " (" + txtClientKey.Text + ")";
                clientKey = txtClientKey.Text.Trim();
                X509Certificate2 privateX509Cert = new X509Certificate2();
                bool success = cert.Get(nameWithRefNum, out privateX509Cert);

                if (txtClientKey.Text.Length > 0 && lblGetName.Text == "Ref Num :")
                {
                    if (privateX509Cert != null)
                    {

                        if (success == true)
                        {
                            lblCertResponse.Text = "Successful";
                            txtGetCertResponse.Text = nameWithRefNum;
                        }
                        else
                        {
                            lblCertResponse.Text = "Unsuccessful";
                        }
                    }
                    else
                    {
                        lblCertResponse.Text = "Unsuccessful";
                    }

                }
                else
                {
                    MessageBox.Show("RefNum required");
                }

                NonRepudiation nonRep = new NonRepudiation();
                string errorMsg = string.Empty;
                byte[] messageBytes = System.Text.Encoding.ASCII.GetBytes(clientKey);
                byte[] encodedAndSignedCms = new byte[0];
                nonRep.Sign(ref errorMsg, messageBytes, privateX509Cert, out encodedAndSignedCms, true);
                return encodedSignedCms;
            }
        }




		private void btnVerifyCertificate_Click(object sender, EventArgs e)
		{
			using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
			{
				X509Certificate2 x509 = new X509Certificate2();
				string errorResponse = string.Empty;
                byte[] encodedSignedCms = GetEncodedSignedCmsFromNameWithRefnum("Yanbu Aramco Sinopec Refining Company (355EUR)");

				bool success = client.AuthenticateByCertificateAndClientId(ref errorResponse, encodedSignedCms, clientKey, true);

				if (success != true)
				{
					txtGetCertResponse.Text = errorResponse;
				}
				else
				{
                    txtVerify.Clear();
					txtGetCertResponse.Text = "Verification Successful";
                    string result = System.Text.Encoding.UTF8.GetString(encodedSignedCms);
                    txtVerify.Text = result;


				}
			}

		}


		private WsXmlProLite.ClientKey GetClientKeyInfo(string clientToken)
		{
			try
			{
				using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
				{
					WsXmlProLite.ClientKey clientKey = new ClientKey();

					clientKey = client.GetClientKeyInfoFromToken(clientToken);

					return clientKey;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private void btnGetRefNum_Click(object sender, EventArgs e)
		{

			if (txtClientKey.Text == string.Empty)
			{
				MessageBox.Show("Please enter client key text");
				this.txtClientKey.Focus();
				return;
			}


			//VB.NET test
			//string x = client.GetStringContentFromToken(txtGetCertResponse.Text);

			WsXmlProLite.ClientKey clientKey = new ClientKey();
			string clientToken = txtClientKey.Text;
			clientKey = GetClientKeyInfo(clientToken);
			txtClientKey.Text = clientKey.RefNum;

			if (txtClientKey.Text.Length > 0)
			{
				lblGetName.Text = "Ref Num :";
			}
			else
			{
				lblGetName.Text = "Enter Client Key :";
			}


		}

		private void btnTestNonRepudiationSigning_Click(object sender, EventArgs e)
		{
			const string sndMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

			byte[] sndBytes = System.Text.Encoding.ASCII.GetBytes(sndMessage);

			X509Certificate2 certSndPrivate;
			TestCertificate.Certificate c = new Certificate();
			if (c.Get("NonRepudiation", out certSndPrivate))
			{
				//	verify encryption was by signer
				TestCertificate.NonRepudiation n = new NonRepudiation();
				string s = string.Empty;

				byte[] sndAuthenticated;
				if (n.Sign(ref s, sndBytes, certSndPrivate, out sndAuthenticated, false))
				{
					btnTestNonRepudiationSigning.Text = "ok";
				}
				else
				{
					btnTestNonRepudiationSigning.Text = "bad sign";
				}
			}
			else
			{
				btnTestNonRepudiationSigning.Text = "no cert";
			}
		}
	}
}