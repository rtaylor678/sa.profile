﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProLiteSecurity.Client;
using ProLiteSecurity.Cryptography;

namespace WcfProfileService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IProfile
    {
        [OperationContract]
        string CheckService();
       
        [OperationContract]
        bool AuthenticateByCertificateAndClientId(ref string errorResponse, byte[] encodedSignedCms, string clientKey, bool displaySysException);

        [OperationContract]
        string GetStringContentFromToken(ref string errorResponse, string clientTokenKey);

        [OperationContract]
        ClientKey GetClientKeyInfoFromToken(ref string errorResponse, string clientTokenKey);

        [OperationContract]
        string EncryptClientKeyValue(ref string errorResponse, string encryptText);
            
    }

   
}
