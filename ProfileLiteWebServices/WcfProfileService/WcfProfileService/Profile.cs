﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProLiteSecurity.Client;
using ProLiteSecurity.Cryptography;

namespace WcfProfileService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Profile : IProfile
    {
        public string CheckService()
        {
            return "Check Service - Successful";
        }

        public bool AuthenticateByCertificateAndClientId(ref string errorResponse, byte[] encodedSignedCms, string clientKey, bool displaySysException)
        {
            bool authenticate = false;

            try
            {

                ProLiteSecurity.Cryptography.Certificate cert = new Certificate();

                authenticate = cert.AuthenticateByCertificateAndClientId(ref errorResponse, encodedSignedCms, clientKey, displaySysException);

                return authenticate;

            }
            catch (Exception ex)
            {
                string errors = ex.Message;
                if (ex.InnerException != null)
                {
                    errors += " | " + ex.InnerException.Message;
                    errorResponse = errors;
                }

                return false;
            }

        }

        public string GetStringContentFromToken(ref string errorResponse, string clientTokenKey)
        {

            string companyInfo = null;
            try
            {
                Cryptographer crypt = new Cryptographer();

                companyInfo = crypt.Decrypt(clientTokenKey);

                return companyInfo;
            }
            catch (Exception ex)
            {
                string errors = ex.Message;
                if (ex.InnerException != null)
                {
                    errors += " | " + ex.InnerException.Message;
                    errorResponse = errors;
                }

                return string.Empty;
            }

        }

        public ClientKey GetClientKeyInfoFromToken(ref string errorResponse, string clientTokenKey)
        {

            try
            {
                ClientKey client = new ClientKey();
                ClientKey clientKey = new ClientKey();

                clientKey = client.GetClientKeyInfoFromToken(clientTokenKey);

                return clientKey;
            }
            catch (Exception ex)
            {
                string errors = ex.Message;
                if (ex.InnerException != null)
                {
                    errors += " | " + ex.InnerException.Message;
                    errorResponse = errors;
                }

                return null;
            }

        }

        public string EncryptClientKeyValue(ref string errorResponse, string encryptText)
        {

            string retVal = null;

            try
            {
                Cryptographer crypt = new Cryptographer();

                retVal = crypt.Encrypt(encryptText);

                return retVal;
            }
            catch (Exception ex)
            {
                string errors = ex.Message;
                if (ex.InnerException != null)
                {
                    errors += " | " + ex.InnerException.Message;
                    errorResponse = errors;
                }

                return string.Empty;
            }

        }
    }
}
