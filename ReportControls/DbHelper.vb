Imports System.Data.SqlClient

Class DbHelper

#Region "Constants"
    Const CONNECTSTR As String = "packet size=4096;user id=ProfileFuels;password=ProfileFuels;data source=10.10.41.7;persist security info=False;initial catalog=ProfileFuelsDEV"
    Const CONNECTSTR10 As String = "packet size=4096;user id=ProfileFuels;password=ProfileFuels;data source=10.10.41.7;persist security info=False;initial catalog=ProfileFuelsTEST"
#End Region

    Private _refineryID As String
    Public Property RefineryID() As String
        Get
            Return _refineryID
        End Get
        Set(ByVal value As String)
            _refineryID = value
        End Set
    End Property

    Private Function RefineryIs2012(RefineryID As String) As Boolean

        Dim Is2012 As String = False
        Dim cn As New SqlConnection()
        cn.ConnectionString = CONNECTSTR

        Dim ds As New DataSet

        Dim sqlstmt As String = "GetRefinery '" & RefineryID & "'"

        Dim da As SqlDataAdapter = New SqlDataAdapter(sqlstmt, cn)
        da.Fill(ds)
        If Not IsDBNull(ds) Then
            Is2012 = ds.Tables(0).Rows(0)(0).ToString
        End If

        cn.Close()
        If Is2012 = "0" Then
            Return False
        End If

        Return True
    End Function
    Function QueryDb(ByVal sqlString As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim sqlstmt As String
        Dim SqlConnection1 As New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '

        If Not RefineryIs2012(RefineryID) Then
            SqlConnection1.ConnectionString = CONNECTSTR10
        Else
            SqlConnection1.ConnectionString = CONNECTSTR
        End If
        SqlConnection1.ConnectionString = "packet size=4096;user id=ProfileFuels;data source=""10.10.4" & _
        "1.7"";persist security info=True;initial catalog=ProfileFuelsDEV;password=ProfileFu" & _
        "els"

        sqlstmt = sqlString

        'Reads only the first sql statement. 
        'It's to gaurd against attacks on the data
        Try
            If sqlstmt.ToUpper.Trim().StartsWith("SELECT") Or sqlstmt.ToUpper.Trim().StartsWith("EXEC") Then
                Dim da As SqlDataAdapter = New SqlDataAdapter(sqlstmt, SqlConnection1)
                da.Fill(ds)
            End If
        Catch ex As Exception

            Throw New Exception("A database error just occurred." + ex.Message)
        Finally
            SqlConnection1.Close()
        End Try

        Return ds

    End Function





End Class
