Partial Class ProcessBreakdown
    Inherits ReportBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        BuildReport()
        BuildUtilAndOffsites()
    End Sub

    Public Sub BuildReport()

        Dim m, t As Integer
        Dim qry, fieldFormat As String
        Dim ds As DataSet
        Dim capColName As String = IIf(Request.QueryString("UOM").StartsWith("US"), "Cap", "RptCap")
        Dim columns() As String = {"UnitName", "ProcessID", capColName, "kEDC", "kUEDC", "StdEnergy", "StdGain"}
        Dim decPlaces() As Integer = {0, 0, 0, 0, 0, 0, 0}
        Dim alignment() As String = {"left", "center", "right", "right", "right", "right", "right"}
        qry = "SELECT description, c.unitname, f.processid,c." + capColName + "," + _
             "stdenergy, stdgain, EDCNoMult*MultiFactor/1000 as kEDC, UEDCNoMult*MultiFactor/1000 as kUEDC " + _
             "FROM factorcalc f ,processid_lu g ,config c WHERE " + _
             "f.processid=g.processid AND " + _
             "f.factorset='" + Request.QueryString("yr") + "' AND " + _
             "c.submissionid=f.submissionid AND c.processid=f.processid AND f.unitid=c.unitid AND " + _
             "c.processid NOT IN ('BLENDING') AND " + _
             "f.submissionid IN " + _
             "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
             " ) ORDER BY c.Sortkey ASC, c.UnitID ASC "

        Dim db As New DbHelper
        db.RefineryID = RefineryID
        ds = db.QueryDb(qry)


        Response.Write("<br>")

        Response.Write("<table class='small' border=0>")
        'Add Date
        Response.Write("<tr><td  align=left colspan=7><strong>" + Format(CDate(Request.QueryString("sd")), "MMMM yyyy") + "</strong></td></tr>")

        'Build headers
        Response.Write("<tr><td width=25% nowrap><strong>Process Unit Name</strong></td>")
        Response.Write("<td align=center width=15%><strong>Process ID</strong></td>")
        Response.Write("<td align=center width=12%><strong>Capacity</strong></td>")
        Response.Write("<td align=center width=12%><strong>kEDC</strong></td>")
        Response.Write("<td align=center width=12%><strong>kUEDC</strong></td>")
        Response.Write("<td align=center width=12%><strong>Standard<br>Energy</strong></td>")
        Response.Write("<td align=center width=12%><strong>Estimated<br>Gain</strong></td></tr>")

        For m = 0 To ds.Tables(0).Rows.Count - 1
            Response.Write("<tr>")
            For t = 0 To columns.Length - 1
                If columns(t) <> "" Then

                    'Set number format
                    Select Case decPlaces(t)
                        Case 0
                            fieldFormat = "{0:#,##0}"
                        Case 1
                            fieldFormat = "{0:#,##0.0}"
                        Case 2
                            fieldFormat = "{0:N}"
                    End Select


                    Dim str As String = ""
                    If t = 0 Then
                        str = "&nbsp;&nbsp;&nbsp;&nbsp;"
                    End If
                    str += String.Format(fieldFormat, ds.Tables(0).Rows(m)(columns(t)))

                    If Not ((columns(t) = "StdEnergy" Or columns(t) = "StdGain") And ds.Tables(0).Rows(m)(columns(t)).ToString.Trim = "0") Then

                        If Not (columns(t) = capColName And ds.Tables(0).Rows(m)(columns(1)).ToString.Trim = "FTCOGEN") Then
                            Response.Write("<td align=" + alignment(t) + " nowrap>" + str + "</td>")
                        Else
                            Response.Write("<td>&nbsp;</td>")
                        End If

                    Else
                        Response.Write("<td>&nbsp;</td>")
                    End If

                Else
                    Response.Write("<td>&nbsp</td>")
                End If

            Next
            Response.Write("</tr>")
        Next

        'Response.Write("</table>")
    End Sub



    Sub BuildUtilAndOffsites()

        Dim m, t As Integer
        Dim qry, qry1, fieldFormat As String
        Dim ds, ds1 As DataSet
        Dim capColName As String = IIf(Request.QueryString("UOM").StartsWith("US"), "Cap", "RptCap")
        Dim columns() As String = {"UnitName", "ProcessID", capColName, "kEDC", "kUEDC", "StdEnergy", "StdGain"}
        Dim decPlaces() As Integer = {0, 0, 0, 0, 0, 0, 0}
        Dim alignment() As String = {"left", "center", "right", "right", "right", "right", "right"}



        qry1 = "SELECT EDC/1000 as TotEDC,UEDC/1000 as TotUEDC,TotRSEDC/1000 as kEDC, TotRSUEDC/1000 as kUEDC, SensHeatStdEnergy,AspStdEnergy, " + _
               " OffsitesStdEnergy,TotStdEnergy,EstGain " + _
               " FROM FactorTotCalc f WHERE " + _
               " f.factorset='" + Request.QueryString("yr").ToString + "' AND " + _
               " f.submissionid IN " + _
               " (SELECT Distinct SubmissionID FROM Submissions WHERE DataSet='" + Request.QueryString("ds") + "' AND " + _
               " Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + _
               " AND Year(PeriodStart)=" + Year(CDate(Request.QueryString("sd"))).ToString + _
               " AND RefineryID='" + RefineryID + "') "

        Dim db As New DbHelper
        db.RefineryID = RefineryID
        ds1 = db.QueryDb(qry1)


        Response.Write("<tr rowspan=2><td colspan=7> <br><strong>Utilities and Off-sites</strong></td></tr>")

        If ds1.Tables.Count > 0 Then
            Response.Write("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Receipts and Shipments</td><td>&nbsp;</td><td>&nbsp;</td>")
            Response.Write("<td align=right>" + String.Format("{0:#,##0}", Convert.ToDouble(ds1.Tables(0).Rows(0)("kEDC").ToString)) + "</td>")
            Response.Write("<td align=right>" + String.Format("{0:#,##0}", Convert.ToDouble(ds1.Tables(0).Rows(0)("kUEDC").ToString)) + "</td>")
            Response.Write("<td>&nbsp;</td>")
            Response.Write("<td>&nbsp;</td></tr>")

            Response.Write("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Sensible Heat</td><td>&nbsp;</td><td>&nbsp;</td>")
            Response.Write("<td>&nbsp;</td><td>&nbsp;</td>")
            Response.Write("<td align=right>" + String.Format("{0:#,##0}", (ds1.Tables(0).Rows(0)("SensHeatStdEnergy"))) + "</td>")
            Response.Write("<td>&nbsp;</td></tr>")

            If Not IsNothing(ds1.Tables(0).Rows(0)("AspStdEnergy").ToString.Trim) And _
               CDbl(ds1.Tables(0).Rows(0)("AspStdEnergy").ToString.Trim) > 0 Then
                Response.Write("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Asphalt</td><td>&nbsp;</td><td>&nbsp;</td>")
                Response.Write("<td>&nbsp;</td><td>&nbsp;</td>")
                Response.Write("<td align=right>" + String.Format("{0:#,##0}", ds1.Tables(0).Rows(0)("AspStdEnergy")) + "</td>")
                Response.Write("<td>&nbsp;</td></tr>")
            End If

            Response.Write("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;All Other</td><td>&nbsp;</td><td>&nbsp;</td>")
            Response.Write("<td>&nbsp;</td><td>&nbsp;</td>")
            Response.Write("<td align=right>" + String.Format("{0:#,##0}", ds1.Tables(0).Rows(0)("OffsitesStdEnergy")) + "</td>")
            Response.Write("<td>&nbsp;</td></tr>")
        End If

        Dim nextMonth As Date = DateAdd(DateInterval.Month, 1, CDate(Request.QueryString("sd")))
        Dim daysInMonth As Integer = DateDiff(DateInterval.Day, CDate(Request.QueryString("sd")), nextMonth)

        Response.Write("<tr><td><br><strong>Total Refinery</strong></td><td>&nbsp;</td><td>&nbsp;</td>")
        Response.Write("<td align=right>" + String.Format("{0:#,##0}", ds1.Tables(0).Rows(0)("TotEDC")) + "</td>")
        Response.Write("<td align=right>" + String.Format("{0:#,##0}", ds1.Tables(0).Rows(0)("TotUEDC")) + "</td>")
        Response.Write("<td align=right>" + String.Format("{0:#,##0}", ds1.Tables(0).Rows(0)("TotStdEnergy")) + "</td>")
        Response.Write("<td align=right>" + String.Format("{0:#,##0}", ds1.Tables(0).Rows(0)("EstGain") / daysInMonth) + "</td></tr>")
    End Sub


End Class


