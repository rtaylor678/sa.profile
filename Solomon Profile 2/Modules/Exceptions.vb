'********************************************************************************
'*  TODO: Comments go here...                                                   *
'********************************************************************************
Option Explicit On

Imports System

'TODO: Add Serializable attributes...
Friend Class InvalidKeyLengthException
    Inherits System.ApplicationException
    'Custom exception class for invalid key values.

#Region " Constructors "
    Friend Sub New()
        'Default constructor.
    End Sub

    Friend Sub New(ByVal aMessage As String)
        MyBase.New(aMessage)
    End Sub

    Friend Sub New(ByVal aMessage As String, ByVal innerException As System.Exception)
        MyBase.New(aMessage, innerException)
    End Sub
#End Region
End Class

'TODO: Add Serializable attributes...
Friend Class InvalidIVLengthException
    Inherits System.ApplicationException
    'Custom exception class for invalid IV values.

#Region " Constructors "
    Friend Sub New()
        'Default constructor.
    End Sub

    Friend Sub New(ByVal aMessage As String)
        MyBase.New(aMessage)
    End Sub

    Friend Sub New(ByVal aMessage As String, ByVal innerException As System.Exception)
        MyBase.New(aMessage, innerException)
    End Sub
#End Region
End Class

'TODO: Add additional custom exception classes...
