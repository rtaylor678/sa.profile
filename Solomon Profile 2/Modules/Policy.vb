Friend Module Policy

    Friend Function GetFileName() As String

        GetFileName = Nothing

        For Each f As String In IO.Directory.GetFiles(modAppDeclarations.pathCert, "*.ct")
            If IO.File.Exists(f) Then Return System.IO.Path.GetFileName(f)

        Next f

    End Function

End Module
