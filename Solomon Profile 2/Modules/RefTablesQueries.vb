Option Explicit On

Friend Class RefTablesQueries
    Inherits DataServiceExtension


#Region "Reference Queries"
    Friend Shadows Function GetReferences() As DataSet

        If AppCertificate.IsCertificateInstall() Then

            If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
                Me.Url = "https://webservices.solomononline.com/ReportServicesTestSite/WebServices/DataServices.asmx"
            End If
            Dim ds As DataSet = MyBase.GetReferences()
            Return ds
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Function

    Friend Function GetRefineryData(ByVal ReportCode As String, ByVal ds As String, ByVal scenario As String, _
                                    ByVal currency As String, ByVal startDate As Date, ByVal UOM As String, _
                                    ByVal studyYear As Integer, ByVal includeTarget As Boolean, _
                                    ByVal includeYTD As Boolean, ByVal includeAVG As Boolean) As DataSet
        If AppCertificate.IsCertificateInstall() Then
            If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
                Me.Url = "https://webservices.solomononline.com/ReportServicesTestSite/WebServices/DataServices.asmx"
            End If
            Dim dset As DataSet = MyBase.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG)
            Return dset
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Function
#End Region

#Region "Test Code"
    Friend Shared Sub Main()
        Dim service As New RefTablesQueries
        Dim refs As DataSet = service.GetReferences()
        refs.WriteXml("C:\RefsDump.xml", XmlWriteMode.WriteSchema)
        'For y = 0 To refs.Tables.Count - 1
        '    Console.WriteLine(refs.Tables(y).TableName + "," + refs.Tables(y).Rows.Count.ToString)
        'Next
        'Console.WriteLine("___________________________")
        'Dim dsRep As DataSet = service.GetRefineryData("EII", "ACTUAL", "BASE", "USD", #2/1/2005#, "US", 2004, True, True, True)
        'For y = 0 To dsRep.Tables.Count - 1
        '    Console.WriteLine(dsRep.Tables(y).TableName + "," + dsRep.Tables(y).Rows.Count.ToString)
        'Next
        'dsRep.WriteXml("C:\dump.xml")
    End Sub

#End Region
End Class
