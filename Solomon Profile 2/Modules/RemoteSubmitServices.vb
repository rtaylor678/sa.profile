Option Explicit On

Friend Class RemoteSubmitServices
    Inherits SubmitServicesExtension

#Region "Data Submission Records"

    Friend Sub SubmitData(ByVal ds As DataSet)
        If AppCertificate.IsCertificateInstall() Then
            If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
                Me.Url = "https://webservices.solomononline.com/ReportServicesTestSite/WebServices/SubmitServices.asmx"
            End If
            MyBase.SubmitRefineryData(ds)
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If

    End Sub

    Function GetData(ByVal startDate As Date, ByVal endDate As Date) As DataSet
        If AppCertificate.IsCertificateInstall() Then
            If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
                Me.Url = "https://webservices.solomononline.com/ReportServicesTestSite/WebServices/SubmitServices.asmx"
            End If
            Return MyBase.GetDataByPeriod(startDate, endDate)
        Else
            Throw New Exception("Remote service call can not envoke because Solomon Associates' certificate is not installed on your machine.")
        End If
    End Function
#End Region

#Region "Test code"
    Friend Shared Sub Main()
        Dim service As New RemoteSubmitServices
        Dim ds As New DataSet

        'Dim startPrd As String = "1" + _
        '                        "/1/" + _
        '                        "2000" + _
        '                        " 12:00:00 AM"

        'Dim endPrd As String = DateAdd(DateInterval.Month, 1, CDate(startPrd)).ToString
        'Console.WriteLine("StartDate " + startPrd + " EndDate " + endPrd)

        'Console.WriteLine("StartDate " + CDate(startPrd).ToString + " EndDate " + CDate(endPrd).ToString)
        ' ds.ReadXml("C:\MonthlyUpload.xml")
        'service.SubmitData(ds)
        ds = service.GetData(#1/1/2004#, #2/1/2004#)
        ds.WriteXml("C:\dump.xml", XmlWriteMode.WriteSchema)
        'For y = 0 To ds.Tables.Count - 1
        '    Console.WriteLine(ds.Tables(y).TableName + "," + ds.Tables(y).Rows.Count.ToString)
        'Next
    End Sub
#End Region

End Class
