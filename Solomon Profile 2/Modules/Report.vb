Option Explicit On

Imports System
Imports System.Data
Imports System.Collections

Friend Class Report
    Inherits IReport
    Private builder As System.Text.StringBuilder
    Private reportFileName As String

    Friend Sub New(ByVal filename As String)
        ' 
        ' TODO: Add constructor logic here 
        ' 
        builder = New System.Text.StringBuilder()
        reportFileName = filename
    End Sub

    Overrides Function getReport(ByVal ds As DataSet, ByVal UOM As String, ByVal currencyCode As String, ByVal replacementValues As Hashtable) As String
        Dim engine As New TemplateEngine()
        If UOM.StartsWith("US") Then
            engine.UseUSUnits = True
        Else
            engine.UseUSUnits = False
        End If

        Dim text As String = engine.RenderOutput(ds, reportFileName, replacementValues)
        Return text

    End Function

    Overrides Function getReport(ByVal ds As DataSet, ByVal company As String, ByVal location As String, ByVal UOM As String, ByVal currencyCode As String, ByVal month As Integer, _
                       ByVal year As Integer, ByVal replacementValues As Hashtable) As String
        Dim engine As New TemplateEngine()
        If UOM.StartsWith("US") Then
            engine.UseUSUnits = True
        Else
            engine.UseUSUnits = False
        End If

        Dim text As String = engine.RenderOutput(ds, reportFileName, replacementValues)
        Return text
    End Function
End Class
