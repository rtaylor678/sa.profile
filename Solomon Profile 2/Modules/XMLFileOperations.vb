'---------------------------------------------------------------------------------------
' � 2011 HSB Solomon Associates LLC
' Solomon Associates
' Two Galleria Tower, Suite 1500
' 13455 Noel Road
' Dallas, Texas 75240, United States of America
'---------------------------------------------------------------------------------------

Option Compare Binary
Option Explicit On
Option Strict On


Friend Module XMLFile

    Friend ReadOnly deleteTemporal As Boolean = True

    Friend Function ReadXMLFile(ByRef ds As DataSet, _
                                ByVal xmlPath As String) As Boolean

        ReadXMLFile = False

        If IO.File.Exists(xmlPath) Then

            Try

                ds.ReadXml(xmlPath, XmlReadMode.ReadSchema)
                ReadXMLFile = True

            Catch exXML As System.Xml.XmlException

                If IO.File.Exists(xmlPath & ".temporal") Then
                    Try
                        If IO.File.Exists(xmlPath & ".temporal") Then IO.File.Copy(xmlPath & ".temporal", xmlPath, True)
                        ds.ReadXml(xmlPath, XmlReadMode.ReadSchema)
                        ProfileMsgBox("CRIT", "NoReadXMLBackup", modFileHelper.getFile(xmlPath))
                    Catch ex As Exception
                        ProfileMsgBox("CRIT", "NoReadXMLBackupError", modFileHelper.getFile(xmlPath))
                    End Try
                Else
                    Try
                        If IO.File.Exists(xmlPath & ".standard") Then
                            If IO.File.Exists(xmlPath & ".standard") Then IO.File.Copy(xmlPath & ".standard", xmlPath, True)
                            ds.ReadXml(xmlPath, XmlReadMode.ReadSchema)
                            ProfileMsgBox("CRIT", "NoReadXMLStandard", modFileHelper.getFile(xmlPath))
                        End If
                    Catch ex As Exception
                        ProfileMsgBox("CRIT", "NoReadXMLStandardError", modFileHelper.getFile(xmlPath))
                    End Try
                End If

            Finally
            End Try

        Else

            ProfileMsgBox("CRIT", "NoFileXML", modFileHelper.getFile(xmlPath))

        End If

    End Function

    Friend Function WriteXMLFile(ByVal ds As DataSet, _
                                 ByVal xmlPath As String) As Boolean

        WriteXMLFile = False

        Try

            If IO.File.Exists(xmlPath) Then IO.File.Copy(xmlPath, xmlPath & ".temporal", True)
            ds.WriteXml(xmlPath, XmlWriteMode.WriteSchema)

            Try
                If deleteTemporal AndAlso IO.File.Exists(xmlPath & ".temporal") AndAlso IO.File.Exists(xmlPath) Then IO.File.Delete(xmlPath & ".temporal")
            Finally
            End Try

            WriteXMLFile = True

        Catch exIo As IO.IOException

            ProfileMsgBox("CRIT", "MakeXMLBackupError", modFileHelper.getFile(xmlPath))

        Catch exXML As System.Xml.XmlException

            If IO.File.Exists(xmlPath & ".temporal") Then

                Try

                    IO.File.Copy(xmlPath & ".temporal", xmlPath, True)
                    ProfileMsgBox("CRIT", "NoWriteXMLBackup", modFileHelper.getFile(xmlPath))

                Catch ex As Exception

                    ProfileMsgBox("CRIT", "NoWriteXMLBackupError", modFileHelper.getFile(xmlPath))

                End Try

            Else

                ProfileMsgBox("CRIT", "NoWriteXMLNoBackup", modFileHelper.getFile(xmlPath))

            End If

        Finally
        End Try

    End Function

End Module
