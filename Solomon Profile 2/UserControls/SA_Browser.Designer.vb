<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SA_Browser
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.wbWebBrowser = New System.Windows.Forms.WebBrowser
        Me.pageBrowser = New System.Windows.Forms.TabPage
        Me.ttBrowser = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnDump = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnDataEntry = New System.Windows.Forms.Button
        Me.tcBrowser = New System.Windows.Forms.TabControl
        Me.pnlTools = New System.Windows.Forms.Panel
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.pageBrowser.SuspendLayout()
        Me.tcBrowser.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'wbWebBrowser
        '
        Me.wbWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wbWebBrowser.Location = New System.Drawing.Point(0, 0)
        Me.wbWebBrowser.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wbWebBrowser.Name = "wbWebBrowser"
        Me.wbWebBrowser.Size = New System.Drawing.Size(563, 285)
        Me.wbWebBrowser.TabIndex = 0
        '
        'pageBrowser
        '
        Me.pageBrowser.Controls.Add(Me.wbWebBrowser)
        Me.pageBrowser.Location = New System.Drawing.Point(4, 22)
        Me.pageBrowser.Name = "pageBrowser"
        Me.pageBrowser.Size = New System.Drawing.Size(563, 285)
        Me.pageBrowser.TabIndex = 2
        Me.pageBrowser.Text = "Web Browser"
        Me.pageBrowser.UseVisualStyleBackColor = True
        '
        'btnDump
        '
        Me.btnDump.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnDump.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnDump.FlatAppearance.BorderSize = 0
        Me.btnDump.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDump.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDump.ForeColor = System.Drawing.SystemColors.Window
        Me.btnDump.Image = Global.Solomon_Profile.My.Resources.Resources.completeTest
        Me.btnDump.Location = New System.Drawing.Point(329, 0)
        Me.btnDump.Name = "btnDump"
        Me.btnDump.Size = New System.Drawing.Size(80, 34)
        Me.btnDump.TabIndex = 6
        Me.btnDump.TabStop = False
        Me.btnDump.Text = "Dump"
        Me.btnDump.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDump.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ttBrowser.SetToolTip(Me.btnDump, "Downloads historical results of report")
        Me.btnDump.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPrint.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPrint.Image = Global.Solomon_Profile.My.Resources.Resources.printer
        Me.btnPrint.Location = New System.Drawing.Point(409, 0)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(80, 34)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.TabStop = False
        Me.btnPrint.Text = "Print"
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ttBrowser.SetToolTip(Me.btnPrint, "Prints results below")
        Me.btnPrint.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = Global.Solomon_Profile.My.Resources.Resources.error_1
        Me.btnClose.Location = New System.Drawing.Point(489, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ttBrowser.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnDataEntry
        '
        Me.btnDataEntry.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnDataEntry.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnDataEntry.FlatAppearance.BorderSize = 0
        Me.btnDataEntry.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDataEntry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDataEntry.ForeColor = System.Drawing.SystemColors.Window
        Me.btnDataEntry.Image = Global.Solomon_Profile.My.Resources.Resources.log
        Me.btnDataEntry.Location = New System.Drawing.Point(249, 0)
        Me.btnDataEntry.Name = "btnDataEntry"
        Me.btnDataEntry.Size = New System.Drawing.Size(80, 34)
        Me.btnDataEntry.TabIndex = 7
        Me.btnDataEntry.TabStop = False
        Me.btnDataEntry.Text = "Edit"
        Me.btnDataEntry.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDataEntry.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ttBrowser.SetToolTip(Me.btnDataEntry, "Go back to data entry to edit data")
        Me.btnDataEntry.UseVisualStyleBackColor = False
        Me.btnDataEntry.Visible = False
        '
        'tcBrowser
        '
        Me.tcBrowser.Controls.Add(Me.pageBrowser)
        Me.tcBrowser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcBrowser.ItemSize = New System.Drawing.Size(125, 18)
        Me.tcBrowser.Location = New System.Drawing.Point(4, 46)
        Me.tcBrowser.Name = "tcBrowser"
        Me.tcBrowser.SelectedIndex = 0
        Me.tcBrowser.Size = New System.Drawing.Size(571, 311)
        Me.tcBrowser.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcBrowser.TabIndex = 7
        '
        'pnlTools
        '
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(571, 6)
        Me.pnlTools.TabIndex = 6
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnDataEntry)
        Me.pnlHeader.Controls.Add(Me.btnDump)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnPrint)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(571, 36)
        Me.pnlHeader.TabIndex = 1035
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(409, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Reports of Results"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_Browser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.tcBrowser)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.Name = "SA_Browser"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(579, 361)
        Me.pageBrowser.ResumeLayout(False)
        Me.tcBrowser.ResumeLayout(False)
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents ttBrowser As System.Windows.Forms.ToolTip
    Private WithEvents pageBrowser As System.Windows.Forms.TabPage
    Private WithEvents tcBrowser As System.Windows.Forms.TabControl
    Private WithEvents pnlTools As System.Windows.Forms.Panel
    Private WithEvents pnlHeader As System.Windows.Forms.Panel
    Private WithEvents btnPrint As System.Windows.Forms.Button
    Private WithEvents btnClose As System.Windows.Forms.Button
    Protected Friend WithEvents wbWebBrowser As System.Windows.Forms.WebBrowser
    Protected Friend WithEvents lblHeader As System.Windows.Forms.Label
    Protected Friend WithEvents btnDump As System.Windows.Forms.Button
    Protected Friend WithEvents btnDataEntry As System.Windows.Forms.Button

End Class
