Option Explicit On

Friend Class SA_Inventory
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl1 overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblHeaderRS As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents dgInventory As System.Windows.Forms.DataGridView
    Friend WithEvents lblHeaderInv As System.Windows.Forms.Label
    Friend WithEvents dgRS As System.Windows.Forms.DataGridView
    Friend WithEvents RSDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RailcarBBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TankTruckBBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TankerBerthBBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OffshoreBuoyBBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BargeBerthBBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PipelineBBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProcessID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgInventoryMap As System.Windows.Forms.DataGridView
    Friend WithEvents dgRSMap As System.Windows.Forms.DataGridView
    Friend WithEvents tcInventory As System.Windows.Forms.TabControl
    Friend WithEvents pageTransport As System.Windows.Forms.TabPage
    Friend WithEvents pageStorage As System.Windows.Forms.TabPage
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents gbTools As System.Windows.Forms.GroupBox
    Friend WithEvents lblView As System.Windows.Forms.Label
    Friend WithEvents rbLinks As System.Windows.Forms.RadioButton
    Friend WithEvents rbData As System.Windows.Forms.RadioButton
    Friend WithEvents lblWorkBook As System.Windows.Forms.Label
    Friend WithEvents cbSheets As System.Windows.Forms.ComboBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents tbFilePath As System.Windows.Forms.TextBox
    Friend WithEvents lblWorkSheet As System.Windows.Forms.Label
    Friend WithEvents InvDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumTank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FuelsStorage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AvgLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TankType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lkDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_Inventory))
        Me.dgRSMap = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgRS = New System.Windows.Forms.DataGridView
        Me.RSDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RailcarBBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TankTruckBBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TankerBerthBBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OffshoreBuoyBBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BargeBerthBBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PipelineBBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ProcessID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblHeaderRS = New System.Windows.Forms.Label
        Me.dgInventoryMap = New System.Windows.Forms.DataGridView
        Me.lkDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgInventory = New System.Windows.Forms.DataGridView
        Me.InvDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NumTank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FuelsStorage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AvgLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TankType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblHeaderInv = New System.Windows.Forms.Label
        Me.pnlTools = New System.Windows.Forms.Panel
        Me.gbTools = New System.Windows.Forms.GroupBox
        Me.btnEdit = New System.Windows.Forms.Button
        Me.chkComplete = New System.Windows.Forms.CheckBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.lblView = New System.Windows.Forms.Label
        Me.rbLinks = New System.Windows.Forms.RadioButton
        Me.lblWorkBook = New System.Windows.Forms.Label
        Me.rbData = New System.Windows.Forms.RadioButton
        Me.cbSheets = New System.Windows.Forms.ComboBox
        Me.tbFilePath = New System.Windows.Forms.TextBox
        Me.lblWorkSheet = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.tcInventory = New System.Windows.Forms.TabControl
        Me.pageStorage = New System.Windows.Forms.TabPage
        Me.pageTransport = New System.Windows.Forms.TabPage
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        CType(Me.dgRSMap, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgRS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgInventoryMap, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgInventory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTools.SuspendLayout()
        Me.gbTools.SuspendLayout()
        Me.tcInventory.SuspendLayout()
        Me.pageStorage.SuspendLayout()
        Me.pageTransport.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgRSMap
        '
        Me.dgRSMap.AllowUserToAddRows = False
        Me.dgRSMap.AllowUserToDeleteRows = False
        Me.dgRSMap.AllowUserToResizeColumns = False
        Me.dgRSMap.AllowUserToResizeRows = False
        Me.dgRSMap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgRSMap.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgRSMap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgRSMap.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10})
        Me.dgRSMap.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgRSMap.Location = New System.Drawing.Point(6, 36)
        Me.dgRSMap.MultiSelect = False
        Me.dgRSMap.Name = "dgRSMap"
        Me.dgRSMap.RowHeadersVisible = False
        Me.dgRSMap.RowHeadersWidth = 25
        Me.dgRSMap.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgRSMap.Size = New System.Drawing.Size(722, 383)
        Me.dgRSMap.TabIndex = 1
        Me.dgRSMap.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Description"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn1.FillWeight = 150.0!
        Me.DataGridViewTextBoxColumn1.HeaderText = "Monthly Throughput"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "RailcarBBL"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle3.Format = "N0"
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn2.FillWeight = 85.0!
        Me.DataGridViewTextBoxColumn2.HeaderText = "Railcar Cell Reference"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 85
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "TankTruckBBL"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle4.Format = "N0"
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn3.FillWeight = 85.0!
        Me.DataGridViewTextBoxColumn3.HeaderText = "Tank Truck Cell Reference"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 85
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "TankerBerthBBL"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle5.Format = "N0"
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn6.FillWeight = 85.0!
        Me.DataGridViewTextBoxColumn6.HeaderText = "Tanker Berth Cell Reference"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 85
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "OffshoreBuoyBBL"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle6.Format = "N0"
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn7.FillWeight = 85.0!
        Me.DataGridViewTextBoxColumn7.HeaderText = "Offshore Buoy Cell Reference"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 85
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "BargeBerthBBL"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle7.Format = "N0"
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn8.FillWeight = 85.0!
        Me.DataGridViewTextBoxColumn8.HeaderText = "Barge Berth Cell Reference"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 85
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "PipelineBBL"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle8.Format = "N0"
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn9.FillWeight = 85.0!
        Me.DataGridViewTextBoxColumn9.HeaderText = "Pipeline Cell Reference"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 85
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "ProcessID"
        Me.DataGridViewTextBoxColumn10.HeaderText = "ProcessID"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'dgRS
        '
        Me.dgRS.AllowUserToAddRows = False
        Me.dgRS.AllowUserToDeleteRows = False
        Me.dgRS.AllowUserToResizeColumns = False
        Me.dgRS.AllowUserToResizeRows = False
        Me.dgRS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgRS.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgRS.ColumnHeadersHeight = 21
        Me.dgRS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgRS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RSDescription, Me.RailcarBBL, Me.TankTruckBBL, Me.TankerBerthBBL, Me.OffshoreBuoyBBL, Me.BargeBerthBBL, Me.PipelineBBL, Me.ProcessID})
        Me.dgRS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgRS.Location = New System.Drawing.Point(6, 36)
        Me.dgRS.MultiSelect = False
        Me.dgRS.Name = "dgRS"
        Me.dgRS.RowHeadersVisible = False
        Me.dgRS.RowHeadersWidth = 25
        Me.dgRS.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgRS.Size = New System.Drawing.Size(722, 383)
        Me.dgRS.TabIndex = 1027
        '
        'RSDescription
        '
        Me.RSDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.RSDescription.DataPropertyName = "Description"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.RSDescription.DefaultCellStyle = DataGridViewCellStyle10
        Me.RSDescription.FillWeight = 150.0!
        Me.RSDescription.HeaderText = "Monthly Throughput, bbl"
        Me.RSDescription.Name = "RSDescription"
        Me.RSDescription.ReadOnly = True
        Me.RSDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'RailcarBBL
        '
        Me.RailcarBBL.DataPropertyName = "RailcarBBL"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle11.Format = "N0"
        Me.RailcarBBL.DefaultCellStyle = DataGridViewCellStyle11
        Me.RailcarBBL.FillWeight = 85.0!
        Me.RailcarBBL.HeaderText = "Railcar"
        Me.RailcarBBL.Name = "RailcarBBL"
        Me.RailcarBBL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RailcarBBL.Width = 85
        '
        'TankTruckBBL
        '
        Me.TankTruckBBL.DataPropertyName = "TankTruckBBL"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle12.Format = "N0"
        Me.TankTruckBBL.DefaultCellStyle = DataGridViewCellStyle12
        Me.TankTruckBBL.FillWeight = 85.0!
        Me.TankTruckBBL.HeaderText = "Tank Truck"
        Me.TankTruckBBL.Name = "TankTruckBBL"
        Me.TankTruckBBL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TankTruckBBL.Width = 85
        '
        'TankerBerthBBL
        '
        Me.TankerBerthBBL.DataPropertyName = "TankerBerthBBL"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle13.Format = "N0"
        Me.TankerBerthBBL.DefaultCellStyle = DataGridViewCellStyle13
        Me.TankerBerthBBL.FillWeight = 85.0!
        Me.TankerBerthBBL.HeaderText = "Tanker Berth"
        Me.TankerBerthBBL.Name = "TankerBerthBBL"
        Me.TankerBerthBBL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TankerBerthBBL.Width = 85
        '
        'OffshoreBuoyBBL
        '
        Me.OffshoreBuoyBBL.DataPropertyName = "OffshoreBuoyBBL"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle14.Format = "N0"
        Me.OffshoreBuoyBBL.DefaultCellStyle = DataGridViewCellStyle14
        Me.OffshoreBuoyBBL.FillWeight = 85.0!
        Me.OffshoreBuoyBBL.HeaderText = "Offshore Buoy"
        Me.OffshoreBuoyBBL.Name = "OffshoreBuoyBBL"
        Me.OffshoreBuoyBBL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.OffshoreBuoyBBL.Width = 85
        '
        'BargeBerthBBL
        '
        Me.BargeBerthBBL.DataPropertyName = "BargeBerthBBL"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle15.Format = "N0"
        Me.BargeBerthBBL.DefaultCellStyle = DataGridViewCellStyle15
        Me.BargeBerthBBL.FillWeight = 85.0!
        Me.BargeBerthBBL.HeaderText = "Barge Berth"
        Me.BargeBerthBBL.Name = "BargeBerthBBL"
        Me.BargeBerthBBL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.BargeBerthBBL.Width = 85
        '
        'PipelineBBL
        '
        Me.PipelineBBL.DataPropertyName = "PipelineBBL"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle16.Format = "N0"
        Me.PipelineBBL.DefaultCellStyle = DataGridViewCellStyle16
        Me.PipelineBBL.FillWeight = 85.0!
        Me.PipelineBBL.HeaderText = "Pipeline"
        Me.PipelineBBL.Name = "PipelineBBL"
        Me.PipelineBBL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PipelineBBL.Width = 85
        '
        'ProcessID
        '
        Me.ProcessID.DataPropertyName = "ProcessID"
        Me.ProcessID.HeaderText = "ProcessID"
        Me.ProcessID.Name = "ProcessID"
        Me.ProcessID.ReadOnly = True
        Me.ProcessID.Visible = False
        '
        'lblHeaderRS
        '
        Me.lblHeaderRS.BackColor = System.Drawing.Color.Transparent
        Me.lblHeaderRS.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeaderRS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderRS.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblHeaderRS.Location = New System.Drawing.Point(6, 0)
        Me.lblHeaderRS.Name = "lblHeaderRS"
        Me.lblHeaderRS.Size = New System.Drawing.Size(722, 36)
        Me.lblHeaderRS.TabIndex = 867
        Me.lblHeaderRS.Text = "Inventory - Raw Material Receipts and Product Shipments:   DATA"
        Me.lblHeaderRS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgInventoryMap
        '
        Me.dgInventoryMap.AllowUserToAddRows = False
        Me.dgInventoryMap.AllowUserToDeleteRows = False
        Me.dgInventoryMap.AllowUserToResizeColumns = False
        Me.dgInventoryMap.AllowUserToResizeRows = False
        Me.dgInventoryMap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgInventoryMap.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dgInventoryMap.ColumnHeadersHeight = 46
        Me.dgInventoryMap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgInventoryMap.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.lkDescription, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn4})
        Me.dgInventoryMap.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgInventoryMap.Location = New System.Drawing.Point(6, 36)
        Me.dgInventoryMap.MultiSelect = False
        Me.dgInventoryMap.Name = "dgInventoryMap"
        Me.dgInventoryMap.RowHeadersVisible = False
        Me.dgInventoryMap.RowHeadersWidth = 25
        Me.dgInventoryMap.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgInventoryMap.Size = New System.Drawing.Size(722, 383)
        Me.dgInventoryMap.TabIndex = 0
        Me.dgInventoryMap.Visible = False
        '
        'lkDescription
        '
        Me.lkDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkDescription.DataPropertyName = "Description"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lkDescription.DefaultCellStyle = DataGridViewCellStyle18
        Me.lkDescription.FillWeight = 300.0!
        Me.lkDescription.HeaderText = "Hydrocarbon Storage"
        Me.lkDescription.Name = "lkDescription"
        Me.lkDescription.ReadOnly = True
        Me.lkDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "TankType"
        Me.DataGridViewTextBoxColumn5.HeaderText = "TankType"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "AvgLevel"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle19.Format = "N1"
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle19
        Me.DataGridViewTextBoxColumn4.HeaderText = "Average Inventory Level Cell Reference"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgInventory
        '
        Me.dgInventory.AllowUserToAddRows = False
        Me.dgInventory.AllowUserToDeleteRows = False
        Me.dgInventory.AllowUserToResizeColumns = False
        Me.dgInventory.AllowUserToResizeRows = False
        Me.dgInventory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgInventory.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.dgInventory.ColumnHeadersHeight = 47
        Me.dgInventory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgInventory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.InvDescription, Me.NumTank, Me.FuelsStorage, Me.AvgLevel, Me.TankType})
        Me.dgInventory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgInventory.Location = New System.Drawing.Point(6, 36)
        Me.dgInventory.MultiSelect = False
        Me.dgInventory.Name = "dgInventory"
        Me.dgInventory.RowHeadersVisible = False
        Me.dgInventory.RowHeadersWidth = 25
        Me.dgInventory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgInventory.Size = New System.Drawing.Size(722, 383)
        Me.dgInventory.TabIndex = 1022
        '
        'InvDescription
        '
        Me.InvDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.InvDescription.DataPropertyName = "Description"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.InvDescription.DefaultCellStyle = DataGridViewCellStyle21
        Me.InvDescription.FillWeight = 300.0!
        Me.InvDescription.HeaderText = "Hydrocarbon Storage"
        Me.InvDescription.Name = "InvDescription"
        Me.InvDescription.ReadOnly = True
        Me.InvDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'NumTank
        '
        Me.NumTank.DataPropertyName = "NumTank"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle22.Format = "N0"
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.NumTank.DefaultCellStyle = DataGridViewCellStyle22
        Me.NumTank.FillWeight = 75.0!
        Me.NumTank.HeaderText = "Number of Tanks"
        Me.NumTank.Name = "NumTank"
        Me.NumTank.ReadOnly = True
        Me.NumTank.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NumTank.Width = 75
        '
        'FuelsStorage
        '
        Me.FuelsStorage.DataPropertyName = "FuelsStorage"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle23.Format = "N0"
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.FuelsStorage.DefaultCellStyle = DataGridViewCellStyle23
        Me.FuelsStorage.HeaderText = "Total Storage Capacity, bbl"
        Me.FuelsStorage.Name = "FuelsStorage"
        Me.FuelsStorage.ReadOnly = True
        Me.FuelsStorage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'AvgLevel
        '
        Me.AvgLevel.DataPropertyName = "AvgLevel"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle24.Format = "N1"
        Me.AvgLevel.DefaultCellStyle = DataGridViewCellStyle24
        Me.AvgLevel.FillWeight = 75.0!
        Me.AvgLevel.HeaderText = "Average Inventory Level, %"
        Me.AvgLevel.Name = "AvgLevel"
        Me.AvgLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AvgLevel.Width = 75
        '
        'TankType
        '
        Me.TankType.DataPropertyName = "TankType"
        Me.TankType.HeaderText = "TankType"
        Me.TankType.Name = "TankType"
        Me.TankType.ReadOnly = True
        Me.TankType.Visible = False
        '
        'lblHeaderInv
        '
        Me.lblHeaderInv.BackColor = System.Drawing.Color.Transparent
        Me.lblHeaderInv.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeaderInv.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderInv.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblHeaderInv.Location = New System.Drawing.Point(6, 0)
        Me.lblHeaderInv.Name = "lblHeaderInv"
        Me.lblHeaderInv.Size = New System.Drawing.Size(722, 36)
        Me.lblHeaderInv.TabIndex = 1024
        Me.lblHeaderInv.Text = "Inventory - Storage Tankage:   DATA"
        Me.lblHeaderInv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTools
        '
        Me.pnlTools.BackColor = System.Drawing.SystemColors.Control
        Me.pnlTools.Controls.Add(Me.gbTools)
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 105)
        Me.pnlTools.TabIndex = 1
        '
        'gbTools
        '
        Me.gbTools.BackColor = System.Drawing.SystemColors.Control
        Me.gbTools.Controls.Add(Me.btnEdit)
        Me.gbTools.Controls.Add(Me.chkComplete)
        Me.gbTools.Controls.Add(Me.btnBrowse)
        Me.gbTools.Controls.Add(Me.lblView)
        Me.gbTools.Controls.Add(Me.rbLinks)
        Me.gbTools.Controls.Add(Me.lblWorkBook)
        Me.gbTools.Controls.Add(Me.rbData)
        Me.gbTools.Controls.Add(Me.cbSheets)
        Me.gbTools.Controls.Add(Me.tbFilePath)
        Me.gbTools.Controls.Add(Me.lblWorkSheet)
        Me.gbTools.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTools.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbTools.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbTools.Location = New System.Drawing.Point(0, 0)
        Me.gbTools.Name = "gbTools"
        Me.gbTools.Padding = New System.Windows.Forms.Padding(0, 4, 0, 8)
        Me.gbTools.Size = New System.Drawing.Size(742, 99)
        Me.gbTools.TabIndex = 1
        Me.gbTools.TabStop = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnEdit.Image = Global.Solomon_Profile.My.Resources.Resources.log
        Me.btnEdit.Location = New System.Drawing.Point(6, 15)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(134, 28)
        Me.btnEdit.TabIndex = 950
        Me.btnEdit.TabStop = False
        Me.btnEdit.Text = "Edit Configuration"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnEdit, "Edit ""read-only"" configuration data")
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'chkComplete
        '
        Me.chkComplete.AutoSize = True
        Me.chkComplete.BackColor = System.Drawing.Color.Transparent
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkComplete.Location = New System.Drawing.Point(9, 49)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(136, 17)
        Me.chkComplete.TabIndex = 1
        Me.chkComplete.Text = "Check when completed"
        Me.chkComplete.UseVisualStyleBackColor = False
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(711, 15)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(21, 21)
        Me.btnBrowse.TabIndex = 7
        Me.btnBrowse.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Browse for bridge workbook")
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'lblView
        '
        Me.lblView.AutoSize = True
        Me.lblView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblView.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblView.Location = New System.Drawing.Point(6, 72)
        Me.lblView.Name = "lblView"
        Me.lblView.Size = New System.Drawing.Size(33, 13)
        Me.lblView.TabIndex = 949
        Me.lblView.Text = "View"
        Me.lblView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbLinks
        '
        Me.rbLinks.AutoSize = True
        Me.rbLinks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbLinks.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbLinks.Location = New System.Drawing.Point(92, 70)
        Me.rbLinks.Name = "rbLinks"
        Me.rbLinks.Size = New System.Drawing.Size(53, 17)
        Me.rbLinks.TabIndex = 948
        Me.rbLinks.Text = "LINKS"
        Me.rbLinks.UseVisualStyleBackColor = True
        '
        'lblWorkBook
        '
        Me.lblWorkBook.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWorkBook.AutoSize = True
        Me.lblWorkBook.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkBook.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblWorkBook.Location = New System.Drawing.Point(389, 18)
        Me.lblWorkBook.Name = "lblWorkBook"
        Me.lblWorkBook.Size = New System.Drawing.Size(65, 13)
        Me.lblWorkBook.TabIndex = 945
        Me.lblWorkBook.Text = "Workbook"
        Me.lblWorkBook.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbData
        '
        Me.rbData.AutoSize = True
        Me.rbData.Checked = True
        Me.rbData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbData.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbData.Location = New System.Drawing.Point(41, 70)
        Me.rbData.Name = "rbData"
        Me.rbData.Size = New System.Drawing.Size(52, 17)
        Me.rbData.TabIndex = 947
        Me.rbData.TabStop = True
        Me.rbData.Text = "DATA"
        Me.rbData.UseVisualStyleBackColor = True
        '
        'cbSheets
        '
        Me.cbSheets.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSheets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSheets.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cbSheets.FormattingEnabled = True
        Me.cbSheets.Location = New System.Drawing.Point(464, 42)
        Me.cbSheets.Name = "cbSheets"
        Me.cbSheets.Size = New System.Drawing.Size(241, 21)
        Me.cbSheets.TabIndex = 8
        '
        'tbFilePath
        '
        Me.tbFilePath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFilePath.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tbFilePath.Location = New System.Drawing.Point(464, 15)
        Me.tbFilePath.Name = "tbFilePath"
        Me.tbFilePath.ReadOnly = True
        Me.tbFilePath.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbFilePath.Size = New System.Drawing.Size(241, 21)
        Me.tbFilePath.TabIndex = 6
        '
        'lblWorkSheet
        '
        Me.lblWorkSheet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWorkSheet.AutoSize = True
        Me.lblWorkSheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkSheet.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblWorkSheet.Location = New System.Drawing.Point(389, 45)
        Me.lblWorkSheet.Name = "lblWorkSheet"
        Me.lblWorkSheet.Size = New System.Drawing.Size(69, 13)
        Me.lblWorkSheet.TabIndex = 946
        Me.lblWorkSheet.Text = "Worksheet"
        Me.lblWorkSheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.SystemColors.Window
        Me.btnImport.Image = Global.Solomon_Profile.My.Resources.Resources.excel16
        Me.btnImport.Location = New System.Drawing.Point(500, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 34)
        Me.btnImport.TabIndex = 2
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Update input form with linked values")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(340, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon_Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(420, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.TabStop = False
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'tcInventory
        '
        Me.tcInventory.Controls.Add(Me.pageStorage)
        Me.tcInventory.Controls.Add(Me.pageTransport)
        Me.tcInventory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcInventory.ItemSize = New System.Drawing.Size(140, 18)
        Me.tcInventory.Location = New System.Drawing.Point(4, 145)
        Me.tcInventory.Name = "tcInventory"
        Me.tcInventory.SelectedIndex = 0
        Me.tcInventory.Size = New System.Drawing.Size(742, 451)
        Me.tcInventory.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcInventory.TabIndex = 1031
        '
        'pageStorage
        '
        Me.pageStorage.Controls.Add(Me.dgInventory)
        Me.pageStorage.Controls.Add(Me.dgInventoryMap)
        Me.pageStorage.Controls.Add(Me.lblHeaderInv)
        Me.pageStorage.Location = New System.Drawing.Point(4, 22)
        Me.pageStorage.Name = "pageStorage"
        Me.pageStorage.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageStorage.Size = New System.Drawing.Size(734, 425)
        Me.pageStorage.TabIndex = 1
        Me.pageStorage.Text = "Table 1 - Storage"
        Me.pageStorage.UseVisualStyleBackColor = True
        '
        'pageTransport
        '
        Me.pageTransport.Controls.Add(Me.dgRS)
        Me.pageTransport.Controls.Add(Me.dgRSMap)
        Me.pageTransport.Controls.Add(Me.lblHeaderRS)
        Me.pageTransport.Location = New System.Drawing.Point(4, 22)
        Me.pageTransport.Name = "pageTransport"
        Me.pageTransport.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageTransport.Size = New System.Drawing.Size(734, 425)
        Me.pageTransport.TabIndex = 0
        Me.pageTransport.Text = "Table 1 - Transport"
        Me.pageTransport.UseVisualStyleBackColor = True
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1032
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(500, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Inventory"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_Inventory
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.tcInventory)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.MediumBlue
        Me.Name = "SA_Inventory"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        CType(Me.dgRSMap, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgRS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgInventoryMap, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgInventory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTools.ResumeLayout(False)
        Me.gbTools.ResumeLayout(False)
        Me.gbTools.PerformLayout()
        Me.tcInventory.ResumeLayout(False)
        Me.pageStorage.ResumeLayout(False)
        Me.pageTransport.ResumeLayout(False)
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private CStartDate As Date
    Private CEndDate As Date
    Private IsJustScrolling As Boolean

    Private HasRights, IsLoaded, IsLeaving As Boolean
    Private MyParent As Main
    Friend InventoryNotSaved, Conf As Boolean

    Private dvWB As New DataView

    Friend Sub SetDataSets()
        MyParent = CType(Me.ParentForm, Main)
        HasRights = MyParent.MyParent.InvenRights

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Inventory'")
        chkComplete.Checked = CType(row(0)!Checked, Boolean)

        dgInventory.DataSource = MyParent.dsRefineryInformation.Tables("Inventory")
        dgRS.DataSource = MyParent.dsRefineryInformation.Tables("ConfigRS")

        dgInventoryMap.DataSource = MyParent.dsMappings.Tables("Inventory")
        dgRSMap.DataSource = MyParent.dsMappings.Tables("ConfigRS")

        'tbFilePath.Text = My.Settings.workbookInventory
        'cbSheets.Text = My.Settings.worksheetInventory

        Dim dt As DataTable = MyParent.dsMappings.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = 'Inventory'")

        If (dr.GetUpperBound(0) = -1) Then
        Else
            tbFilePath.Text = dr(0).Item(tbFilePath.Name).ToString
            cbSheets.Text = dr(0).Item(cbSheets.Name).ToString
        End If

        IsLoaded = True
        Me.Show()
    End Sub

    Private Sub chkComplete_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComplete.CheckedChanged
        If IsLoaded = False Then Exit Sub
        Dim chk As CheckBox = CType(sender, CheckBox)
        MyParent.DataEntryComplete(chk.Checked, "Inventory")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Inventory'")
        row(0)!Checked = chk.Checked
        InventoryNotSaved = True
        btnSave.BackColor = Color.Red
        btnSave.ForeColor = Color.White
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SaveInventory()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim result As DialogResult
        Dim tmpBool As Boolean = pageStorage.Visible
        Dim row As DataRow
        Dim col As DataColumn

        If rbData.Checked Then
            result = ProfileMsgBox("YN", "CLEAR", "inventory tables")
            If result = DialogResult.Yes Then
                If tmpBool Then
                    For Each row In MyParent.dsRefineryInformation.Tables("Inventory").Rows
                        row.Item("AvgLevel") = DBNull.Value
                    Next
                Else
                    For Each row In MyParent.dsRefineryInformation.Tables("Inventory").Rows
                        row.Item("NumTank") = DBNull.Value
                        row.Item("FuelsStorage") = DBNull.Value
                        row.Item("AvgLevel") = DBNull.Value
                    Next
                End If
                For Each row In MyParent.dsRefineryInformation.Tables("ConfigRS").Rows
                    For Each col In MyParent.dsRefineryInformation.Tables("ConfigRS").Columns
                        If col.ColumnName <> "Description" And col.ColumnName <> "ProcessID" Then row.Item(col) = DBNull.Value
                    Next
                Next
                ChangesMade(btnSave, chkComplete, InventoryNotSaved)
            End If
        Else
            result = ProfileMsgBox("YN", "CLEAR", "inventory mappings")
            If result = DialogResult.Yes Then
                For Each row In MyParent.dsMappings.Tables("Inventory").Rows
                    row!lkAvgLevel = DBNull.Value
                Next
                For Each row In MyParent.dsMappings.Tables("ConfigRS").Rows
                    For Each col In MyParent.dsMappings.Tables("ConfigRS").Columns
                        If col.ColumnName <> "lkDescription" And col.ColumnName <> "lkProcessID" Then row.Item(col) = DBNull.Value
                    Next
                Next
                ChangesMade(btnSave, chkComplete, InventoryNotSaved)
            End If
        End If
    End Sub

    Private Sub dgInventory_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgInventory.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, InventoryNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgInventory_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgInventory.LostFocus
        Try
            dgInventory.SelectedCells(0).Selected = False
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dgRS_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgRS.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, InventoryNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgRS_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgRS.LostFocus
        Try
            dgRS.SelectedCells(0).Selected = False
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If InventoryNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Inventory")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveInventory()
                Case DialogResult.No
                    MyParent.dsRefineryInformation.RejectChanges()
                    MyParent.dsMappings.Tables("Inventory").RejectChanges()
                    MyParent.dsMappings.Tables("ConfigRS").RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, InventoryNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        selectWorkBook(Me.Cursor, tbFilePath, cbSheets, MyParent.MyParent.pnlProgress, dvWB)
    End Sub

    Friend Function ImportInventory() As Boolean
        Dim MyExcel As Object = OpenExcel()
        Dim MyWB As Object = OpenWorkbook(MyExcel, tbFilePath.Text)
        Dim MyWS As Object = OpenSheet(MyExcel, MyWB, cbSheets.Text)

        If MyExcel Is Nothing Then Exit Function
        If MyWB Is Nothing Then Exit Function
        If MyWS Is Nothing Then Exit Function

        Try
            With MyParent.dsMappings.Tables("Inventory")
                For i As Integer = 0 To .Rows.Count - 1
                    If Not IsDBNull(.Rows(i)!AvgLevel) Then
                        If IsNumeric(MyWS.Range(.Rows(i)!AvgLevel).value) Then
                            MyParent.dsRefineryInformation.Tables("Inventory").Rows(i)!AvgLevel = MyWS.Range(.Rows(i)!AvgLevel).value
                        Else
                            MyParent.dsRefineryInformation.Tables("Inventory").Rows(i)!AvgLevel = DBNull.Value
                        End If
                    End If
                Next
            End With
        Catch ex As Exception
            ProfileMsgBox("CRIT", "MAPPING", "inventory - storage")
        End Try

        Try
            With MyParent.dsMappings.Tables("ConfigRS")
                For i As Integer = 0 To .Rows.Count - 1
                    If Not IsDBNull(.Rows(i)!RailcarBBL) Then
                        If IsNumeric(MyWS.Range(.Rows(i)!RailcarBBL).value) Then
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!RailcarBBL = MyWS.Range(.Rows(i)!RailcarBBL).value
                        Else
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!RailcarBBL = DBNull.Value
                        End If
                    End If

                    If Not IsDBNull(.Rows(i)!TankTruckBBL) Then
                        If IsNumeric(MyWS.Range(.Rows(i)!TankTruckBBL).value) Then
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!TankTruckBBL = MyWS.Range(.Rows(i)!TankTruckBBL).value
                        Else
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!TankTruckBBL = DBNull.Value
                        End If
                    End If

                    If Not IsDBNull(.Rows(i)!TankerBerthBBL) Then
                        If IsNumeric(MyWS.Range(.Rows(i)!TankerBerthBBL).value) Then
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!TankerBerthBBL = MyWS.Range(.Rows(i)!TankerBerthBBL).value
                        Else
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!TankerBerthBBL = DBNull.Value
                        End If
                    End If

                    If Not IsDBNull(.Rows(i)!OffshoreBuoyBBL) Then
                        If IsNumeric(MyWS.Range(.Rows(i)!OffshoreBuoyBBL).value) Then
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!OffshoreBuoyBBL = MyWS.Range(.Rows(i)!OffshoreBuoyBBL).value
                        Else
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!OffshoreBuoyBBL = DBNull.Value
                        End If
                    End If

                    If Not IsDBNull(.Rows(i)!BargeBerthBBL) Then
                        If IsNumeric(MyWS.Range(.Rows(i)!BargeBerthBBL).value) Then
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!BargeBerthBBL = MyWS.Range(.Rows(i)!BargeBerthBBL).value
                        Else
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!BargeBerthBBL = DBNull.Value
                        End If
                    End If

                    If Not IsDBNull(.Rows(i)!PipelineBBL) Then
                        If IsNumeric(MyWS.Range(.Rows(i)!PipelineBBL).value) Then
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!PipelineBBL = MyWS.Range(.Rows(i)!PipelineBBL).value
                        Else
                            MyParent.dsRefineryInformation.Tables("ConfigRS").Rows(i)!PipelineBBL = DBNull.Value
                        End If
                    End If
                Next
            End With

        Catch ex As Exception
            ProfileMsgBox("CRIT", "MAPPING", "inventory - receipts and shipments")
            GoTo CleanupExcel
        End Try

        ChangesMade(btnSave, chkComplete, InventoryNotSaved)
        ImportInventory = True

CleanupExcel:
        CloseExcel(MyExcel, MyWB, MyWS)
        Return ImportInventory
    End Function

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim result As DialogResult = ProfileMsgBox("YN", "IMPORT", "inventory")
        If result = DialogResult.Yes Then
            ImportInventory()
        End If
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        MyParent.EditConfiguration("Refinery Information")
        MyParent.SA_RefineryInfo1.tcRefInfo.SelectedTab = MyParent.SA_RefineryInfo1.pageInventory
    End Sub

    Private Sub SA_Inventory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        viewLinksData()

        Me.btnSave.TabIndex = 0
        Me.btnImport.TabIndex = 1
        Me.btnEdit.TabIndex = 1
        Me.btnClear.TabIndex = 3

        Me.tbFilePath.TabIndex = 4
        Me.btnBrowse.TabIndex = 5
        Me.cbSheets.TabIndex = 6

        Me.dgRSMap.TabIndex = 8
        Me.dgInventoryMap.TabIndex = 9

        Me.btnPreview.TabIndex = 10
        Me.chkComplete.TabIndex = 11

        Me.btnClose.TabIndex = 12

    End Sub

    Private Sub rbData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbData.CheckedChanged
        viewLinksData()
    End Sub

    Private Sub viewLinksData()
        dgInventory.Visible = rbData.Checked
        dgRS.Visible = rbData.Checked
        dgInventoryMap.Visible = Not rbData.Checked
        dgRSMap.Visible = Not rbData.Checked

        If rbData.Checked Then
            lblHeaderInv.Text = "Inventory - Storage Tankage:   DATA"
            lblHeaderRS.Text = "Inventory - Raw Material Receipts and Product Shipments:   DATA"
        Else
            lblHeaderInv.Text = "Inventory - Storage Tankage:   LINKS"
            lblHeaderRS.Text = "Inventory - Raw Material Receipts and Product Shipments:   LINKS"
        End If

        If IsLoaded And HasRights Then ViewingLinks(pnlHeader, gbTools, rbData.Checked)

    End Sub

    Private Sub tbFilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFilePath.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Inventory", tbFilePath.Name, tbFilePath.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, InventoryNotSaved)

        My.Settings.workbookInventory = tbFilePath.Text
        My.Settings.Save()
    End Sub

    Private Sub cbSheets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Inventory", cbSheets.Name, cbSheets.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, InventoryNotSaved)

        My.Settings.worksheetInventory = cbSheets.Text
        My.Settings.Save()
    End Sub

    Private Sub dgInventoryMap_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgInventoryMap.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, InventoryNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgRSMap_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgRSMap.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, InventoryNotSaved)
        Else
            e.Cancel = True
        End If

    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        MyParent.PrintPreviews("Table 1", "Inventory")
    End Sub

    Private Sub dgInventory_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgInventory.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgRS_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgRS.DataError
        dgvDataError(sender, e)
    End Sub
End Class
