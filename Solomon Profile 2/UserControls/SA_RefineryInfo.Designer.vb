<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SA_RefineryInfo
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_RefineryInfo))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.btnAdd = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSubmit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.pnlTools = New System.Windows.Forms.Panel
        Me.tcRefInfo = New System.Windows.Forms.TabControl
        Me.pageTargets = New System.Windows.Forms.TabPage
        Me.cmbDesc = New System.Windows.Forms.ComboBox
        Me.dgRefTargets = New System.Windows.Forms.DataGridView
        Me.ChartTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AxisLabelUS = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AxisLabelMetric = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Target = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cmbCurrencyCode = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.PropertyRef = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SortKeyRef = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SectionHeader = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label16 = New System.Windows.Forms.Label
        Me.pageInventory = New System.Windows.Forms.TabPage
        Me.dgInventory = New System.Windows.Forms.DataGridView
        Me.InvDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NumTank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FuelsStorage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AvgLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TankType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label23 = New System.Windows.Forms.Label
        Me.pageStablizers = New System.Windows.Forms.TabPage
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.AnnRSPROD_BB = New System.Windows.Forms.TextBox
        Me.lbPeriods = New System.Windows.Forms.ListBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.AnnRSCRUDE_BB = New System.Windows.Forms.TextBox
        Me.AnnElecConsMWH = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.AnnCokeBbl = New System.Windows.Forms.TextBox
        Me.AnnRSPROD_OMB = New System.Windows.Forms.TextBox
        Me.AnnInputBbl = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.EffDate = New System.Windows.Forms.ComboBox
        Me.AnnRSCRUDE_OMB = New System.Windows.Forms.TextBox
        Me.AnnRSCRUDE_RAIL = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.AnnRSPROD_TB = New System.Windows.Forms.TextBox
        Me.AnnRSPROD_RAIL = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.AnnRSCRUDE_TB = New System.Windows.Forms.TextBox
        Me.AnnRSCRUDE_TT = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.AnnRSPROD_TT = New System.Windows.Forms.TextBox
        Me.pageHistory = New System.Windows.Forms.TabPage
        Me.dgMaintHist = New System.Windows.Forms.DataGridView
        Me.PeriodStart = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RoutCostLocal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RoutMatlLocal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlAnnual = New System.Windows.Forms.Panel
        Me.gbAnnual = New System.Windows.Forms.GroupBox
        Me.chkIncrement = New System.Windows.Forms.CheckBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtYearTot1 = New System.Windows.Forms.TextBox
        Me.txtYearMat2 = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.lb1KCurr = New System.Windows.Forms.Label
        Me.txtYearTot2 = New System.Windows.Forms.TextBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtYearMat1 = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.tcRefInfo.SuspendLayout()
        Me.pageTargets.SuspendLayout()
        CType(Me.dgRefTargets, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pageInventory.SuspendLayout()
        CType(Me.dgInventory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pageStablizers.SuspendLayout()
        Me.pageHistory.SuspendLayout()
        CType(Me.dgMaintHist, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAnnual.SuspendLayout()
        Me.gbAnnual.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.Transparent
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(12, 333)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(118, 24)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.TabStop = False
        Me.btnAdd.Text = "Create New Period"
        Me.ToolTip1.SetToolTip(Me.btnAdd, "Create a new process unit")
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'btnSubmit
        '
        Me.btnSubmit.BackColor = System.Drawing.Color.Transparent
        Me.btnSubmit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubmit.ForeColor = System.Drawing.Color.Black
        Me.btnSubmit.Image = Global.Solomon_Profile.My.Resources.Resources.completeTest
        Me.btnSubmit.Location = New System.Drawing.Point(191, 128)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(70, 28)
        Me.btnSubmit.TabIndex = 10
        Me.btnSubmit.TabStop = False
        Me.btnSubmit.Text = "Refresh"
        Me.btnSubmit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSubmit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSubmit, "Refresh annual data or populate months")
        Me.btnSubmit.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(420, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon_Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(500, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.TabStop = False
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'pnlTools
        '
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 4, 0, 8)
        Me.pnlTools.Size = New System.Drawing.Size(742, 6)
        Me.pnlTools.TabIndex = 1
        '
        'tcRefInfo
        '
        Me.tcRefInfo.Controls.Add(Me.pageTargets)
        Me.tcRefInfo.Controls.Add(Me.pageInventory)
        Me.tcRefInfo.Controls.Add(Me.pageStablizers)
        Me.tcRefInfo.Controls.Add(Me.pageHistory)
        Me.tcRefInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcRefInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcRefInfo.ItemSize = New System.Drawing.Size(150, 18)
        Me.tcRefInfo.Location = New System.Drawing.Point(4, 46)
        Me.tcRefInfo.Name = "tcRefInfo"
        Me.tcRefInfo.SelectedIndex = 0
        Me.tcRefInfo.Size = New System.Drawing.Size(742, 550)
        Me.tcRefInfo.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcRefInfo.TabIndex = 891
        '
        'pageTargets
        '
        Me.pageTargets.Controls.Add(Me.cmbDesc)
        Me.pageTargets.Controls.Add(Me.dgRefTargets)
        Me.pageTargets.Controls.Add(Me.Label16)
        Me.pageTargets.Location = New System.Drawing.Point(4, 22)
        Me.pageTargets.Name = "pageTargets"
        Me.pageTargets.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageTargets.Size = New System.Drawing.Size(734, 524)
        Me.pageTargets.TabIndex = 1
        Me.pageTargets.Text = "Performance Targets"
        Me.pageTargets.UseVisualStyleBackColor = True
        '
        'cmbDesc
        '
        Me.cmbDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbDesc.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cmbDesc.FormattingEnabled = True
        Me.cmbDesc.Items.AddRange(New Object() {"Average Data", "Performance Indicators", "Energy", "Maintenance", "Operating Expenses", "Personnel", "Yields and Margins"})
        Me.cmbDesc.Location = New System.Drawing.Point(225, 9)
        Me.cmbDesc.Name = "cmbDesc"
        Me.cmbDesc.Size = New System.Drawing.Size(173, 21)
        Me.cmbDesc.TabIndex = 968
        Me.cmbDesc.Text = "Average Data"
        '
        'dgRefTargets
        '
        Me.dgRefTargets.AllowUserToAddRows = False
        Me.dgRefTargets.AllowUserToDeleteRows = False
        Me.dgRefTargets.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgRefTargets.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgRefTargets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgRefTargets.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ChartTitle, Me.AxisLabelUS, Me.AxisLabelMetric, Me.Target, Me.cmbCurrencyCode, Me.PropertyRef, Me.SortKeyRef, Me.SectionHeader})
        Me.dgRefTargets.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgRefTargets.Location = New System.Drawing.Point(6, 36)
        Me.dgRefTargets.MultiSelect = False
        Me.dgRefTargets.Name = "dgRefTargets"
        Me.dgRefTargets.RowHeadersWidth = 25
        Me.dgRefTargets.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgRefTargets.Size = New System.Drawing.Size(722, 482)
        Me.dgRefTargets.TabIndex = 966
        '
        'ChartTitle
        '
        Me.ChartTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ChartTitle.DataPropertyName = "ChartTitle"
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ChartTitle.DefaultCellStyle = DataGridViewCellStyle2
        Me.ChartTitle.FillWeight = 300.0!
        Me.ChartTitle.HeaderText = "Target Description"
        Me.ChartTitle.Name = "ChartTitle"
        Me.ChartTitle.ReadOnly = True
        '
        'AxisLabelUS
        '
        Me.AxisLabelUS.DataPropertyName = "AxisLabelUS"
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.AxisLabelUS.DefaultCellStyle = DataGridViewCellStyle3
        Me.AxisLabelUS.FillWeight = 150.0!
        Me.AxisLabelUS.HeaderText = "Unit of Measure"
        Me.AxisLabelUS.Name = "AxisLabelUS"
        Me.AxisLabelUS.ReadOnly = True
        Me.AxisLabelUS.Width = 150
        '
        'AxisLabelMetric
        '
        Me.AxisLabelMetric.DataPropertyName = "AxisLabelMetric"
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.AxisLabelMetric.DefaultCellStyle = DataGridViewCellStyle4
        Me.AxisLabelMetric.FillWeight = 150.0!
        Me.AxisLabelMetric.HeaderText = "Unit of Measure"
        Me.AxisLabelMetric.Name = "AxisLabelMetric"
        Me.AxisLabelMetric.ReadOnly = True
        Me.AxisLabelMetric.Width = 150
        '
        'Target
        '
        Me.Target.DataPropertyName = "Target"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.MediumBlue
        Me.Target.DefaultCellStyle = DataGridViewCellStyle5
        Me.Target.FillWeight = 80.0!
        Me.Target.HeaderText = "Target Value"
        Me.Target.Name = "Target"
        Me.Target.Width = 80
        '
        'cmbCurrencyCode
        '
        Me.cmbCurrencyCode.DataPropertyName = "CurrencyCode"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbCurrencyCode.DefaultCellStyle = DataGridViewCellStyle6
        Me.cmbCurrencyCode.HeaderText = "Currency"
        Me.cmbCurrencyCode.MaxDropDownItems = 2
        Me.cmbCurrencyCode.Name = "cmbCurrencyCode"
        '
        'PropertyRef
        '
        Me.PropertyRef.DataPropertyName = "Property"
        Me.PropertyRef.HeaderText = "Property"
        Me.PropertyRef.Name = "PropertyRef"
        Me.PropertyRef.Visible = False
        '
        'SortKeyRef
        '
        Me.SortKeyRef.DataPropertyName = "SortKey"
        Me.SortKeyRef.HeaderText = "Sort Key"
        Me.SortKeyRef.Name = "SortKeyRef"
        Me.SortKeyRef.ReadOnly = True
        Me.SortKeyRef.Visible = False
        '
        'SectionHeader
        '
        Me.SectionHeader.DataPropertyName = "SectionHeader"
        Me.SectionHeader.HeaderText = "SectionHeader"
        Me.SectionHeader.Name = "SectionHeader"
        Me.SectionHeader.ReadOnly = True
        Me.SectionHeader.Visible = False
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(6, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(722, 36)
        Me.Label16.TabIndex = 965
        Me.Label16.Text = "Refinery Level Performance Targets"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pageInventory
        '
        Me.pageInventory.Controls.Add(Me.dgInventory)
        Me.pageInventory.Controls.Add(Me.Label23)
        Me.pageInventory.Location = New System.Drawing.Point(4, 22)
        Me.pageInventory.Name = "pageInventory"
        Me.pageInventory.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageInventory.Size = New System.Drawing.Size(734, 524)
        Me.pageInventory.TabIndex = 3
        Me.pageInventory.Text = "Inventory - Storage"
        Me.pageInventory.UseVisualStyleBackColor = True
        '
        'dgInventory
        '
        Me.dgInventory.AllowUserToAddRows = False
        Me.dgInventory.AllowUserToDeleteRows = False
        Me.dgInventory.AllowUserToResizeColumns = False
        Me.dgInventory.AllowUserToResizeRows = False
        Me.dgInventory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgInventory.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgInventory.ColumnHeadersHeight = 47
        Me.dgInventory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgInventory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.InvDescription, Me.NumTank, Me.FuelsStorage, Me.AvgLevel, Me.TankType})
        Me.dgInventory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgInventory.Location = New System.Drawing.Point(6, 36)
        Me.dgInventory.MultiSelect = False
        Me.dgInventory.Name = "dgInventory"
        Me.dgInventory.RowHeadersVisible = False
        Me.dgInventory.RowHeadersWidth = 25
        Me.dgInventory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgInventory.Size = New System.Drawing.Size(722, 482)
        Me.dgInventory.TabIndex = 1023
        '
        'InvDescription
        '
        Me.InvDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.InvDescription.DataPropertyName = "Description"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.InvDescription.DefaultCellStyle = DataGridViewCellStyle8
        Me.InvDescription.FillWeight = 300.0!
        Me.InvDescription.HeaderText = "Hydrocarbon Storage"
        Me.InvDescription.Name = "InvDescription"
        Me.InvDescription.ReadOnly = True
        Me.InvDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'NumTank
        '
        Me.NumTank.DataPropertyName = "NumTank"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle9.Format = "N0"
        Me.NumTank.DefaultCellStyle = DataGridViewCellStyle9
        Me.NumTank.FillWeight = 75.0!
        Me.NumTank.HeaderText = "Number of Tanks"
        Me.NumTank.Name = "NumTank"
        Me.NumTank.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NumTank.Width = 75
        '
        'FuelsStorage
        '
        Me.FuelsStorage.DataPropertyName = "FuelsStorage"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle10.Format = "N0"
        Me.FuelsStorage.DefaultCellStyle = DataGridViewCellStyle10
        Me.FuelsStorage.HeaderText = "Total Storage Capacity, bbl"
        Me.FuelsStorage.Name = "FuelsStorage"
        Me.FuelsStorage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'AvgLevel
        '
        Me.AvgLevel.DataPropertyName = "AvgLevel"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle11.Format = "N1"
        Me.AvgLevel.DefaultCellStyle = DataGridViewCellStyle11
        Me.AvgLevel.FillWeight = 75.0!
        Me.AvgLevel.HeaderText = "Average Inventory Level, %"
        Me.AvgLevel.Name = "AvgLevel"
        Me.AvgLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AvgLevel.Visible = False
        Me.AvgLevel.Width = 75
        '
        'TankType
        '
        Me.TankType.DataPropertyName = "TankType"
        Me.TankType.HeaderText = "TankType"
        Me.TankType.Name = "TankType"
        Me.TankType.ReadOnly = True
        Me.TankType.Visible = False
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label23.Location = New System.Drawing.Point(6, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(722, 36)
        Me.Label23.TabIndex = 966
        Me.Label23.Text = "Storage Tankage"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pageStablizers
        '
        Me.pageStablizers.Controls.Add(Me.btnAdd)
        Me.pageStablizers.Controls.Add(Me.Label15)
        Me.pageStablizers.Controls.Add(Me.Label20)
        Me.pageStablizers.Controls.Add(Me.Label14)
        Me.pageStablizers.Controls.Add(Me.Label10)
        Me.pageStablizers.Controls.Add(Me.Label5)
        Me.pageStablizers.Controls.Add(Me.Label3)
        Me.pageStablizers.Controls.Add(Me.AnnRSPROD_BB)
        Me.pageStablizers.Controls.Add(Me.lbPeriods)
        Me.pageStablizers.Controls.Add(Me.Label6)
        Me.pageStablizers.Controls.Add(Me.Label2)
        Me.pageStablizers.Controls.Add(Me.AnnRSCRUDE_BB)
        Me.pageStablizers.Controls.Add(Me.AnnElecConsMWH)
        Me.pageStablizers.Controls.Add(Me.Label7)
        Me.pageStablizers.Controls.Add(Me.AnnCokeBbl)
        Me.pageStablizers.Controls.Add(Me.AnnRSPROD_OMB)
        Me.pageStablizers.Controls.Add(Me.AnnInputBbl)
        Me.pageStablizers.Controls.Add(Me.Label8)
        Me.pageStablizers.Controls.Add(Me.EffDate)
        Me.pageStablizers.Controls.Add(Me.AnnRSCRUDE_OMB)
        Me.pageStablizers.Controls.Add(Me.AnnRSCRUDE_RAIL)
        Me.pageStablizers.Controls.Add(Me.Label9)
        Me.pageStablizers.Controls.Add(Me.Label13)
        Me.pageStablizers.Controls.Add(Me.AnnRSPROD_TB)
        Me.pageStablizers.Controls.Add(Me.AnnRSPROD_RAIL)
        Me.pageStablizers.Controls.Add(Me.Label11)
        Me.pageStablizers.Controls.Add(Me.Label4)
        Me.pageStablizers.Controls.Add(Me.AnnRSCRUDE_TB)
        Me.pageStablizers.Controls.Add(Me.AnnRSCRUDE_TT)
        Me.pageStablizers.Controls.Add(Me.Label12)
        Me.pageStablizers.Controls.Add(Me.Label1)
        Me.pageStablizers.Controls.Add(Me.AnnRSPROD_TT)
        Me.pageStablizers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pageStablizers.Location = New System.Drawing.Point(4, 22)
        Me.pageStablizers.Name = "pageStablizers"
        Me.pageStablizers.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageStablizers.Size = New System.Drawing.Size(734, 524)
        Me.pageStablizers.TabIndex = 0
        Me.pageStablizers.Text = "EDC Stabilizers"
        Me.pageStablizers.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(6, 36)
        Me.Label15.Margin = New System.Windows.Forms.Padding(3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(722, 76)
        Me.Label15.TabIndex = 965
        Me.Label15.Text = resources.GetString("Label15.Text")
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(6, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(722, 36)
        Me.Label20.TabIndex = 964
        Me.Label20.Text = "EDC Stabilizers"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(141, 401)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(474, 20)
        Me.Label14.TabIndex = 354
        Me.Label14.Text = "^ All annual figures are based on a 365 day year."
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(13, 115)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(117, 16)
        Me.Label10.TabIndex = 129
        Me.Label10.Text = "Periods*"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(268, 131)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 26)
        Me.Label5.TabIndex = 340
        Me.Label5.Text = "Annual Net Input Barrels^"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(141, 382)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(474, 20)
        Me.Label3.TabIndex = 353
        Me.Label3.Text = "* Click save to refresh, or simply click on a different period within the list bo" & _
            "x to update."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'AnnRSPROD_BB
        '
        Me.AnnRSPROD_BB.BackColor = System.Drawing.Color.White
        Me.AnnRSPROD_BB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSPROD_BB.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSPROD_BB.Location = New System.Drawing.Point(382, 335)
        Me.AnnRSPROD_BB.Name = "AnnRSPROD_BB"
        Me.AnnRSPROD_BB.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSPROD_BB.TabIndex = 16
        Me.AnnRSPROD_BB.Tag = "0"
        Me.AnnRSPROD_BB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbPeriods
        '
        Me.lbPeriods.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbPeriods.ForeColor = System.Drawing.Color.MediumBlue
        Me.lbPeriods.FormatString = "y"
        Me.lbPeriods.FormattingEnabled = True
        Me.lbPeriods.Location = New System.Drawing.Point(13, 131)
        Me.lbPeriods.Name = "lbPeriods"
        Me.lbPeriods.Size = New System.Drawing.Size(117, 199)
        Me.lbPeriods.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(382, 199)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(105, 26)
        Me.Label6.TabIndex = 341
        Me.Label6.Text = "Annual Product Shipments, bbl"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(499, 131)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 26)
        Me.Label2.TabIndex = 352
        Me.Label2.Text = "Annual Coke Sales, bbl"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AnnRSCRUDE_BB
        '
        Me.AnnRSCRUDE_BB.BackColor = System.Drawing.Color.White
        Me.AnnRSCRUDE_BB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSCRUDE_BB.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSCRUDE_BB.Location = New System.Drawing.Point(271, 335)
        Me.AnnRSCRUDE_BB.Name = "AnnRSCRUDE_BB"
        Me.AnnRSCRUDE_BB.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSCRUDE_BB.TabIndex = 15
        Me.AnnRSCRUDE_BB.Tag = "0"
        Me.AnnRSCRUDE_BB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AnnElecConsMWH
        '
        Me.AnnElecConsMWH.BackColor = System.Drawing.Color.White
        Me.AnnElecConsMWH.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnElecConsMWH.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnElecConsMWH.Location = New System.Drawing.Point(382, 160)
        Me.AnnElecConsMWH.Name = "AnnElecConsMWH"
        Me.AnnElecConsMWH.Size = New System.Drawing.Size(105, 21)
        Me.AnnElecConsMWH.TabIndex = 5
        Me.AnnElecConsMWH.Tag = "0"
        Me.AnnElecConsMWH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(141, 227)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(99, 20)
        Me.Label7.TabIndex = 342
        Me.Label7.Text = "Railcar"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'AnnCokeBbl
        '
        Me.AnnCokeBbl.BackColor = System.Drawing.Color.White
        Me.AnnCokeBbl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnCokeBbl.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnCokeBbl.Location = New System.Drawing.Point(493, 160)
        Me.AnnCokeBbl.Name = "AnnCokeBbl"
        Me.AnnCokeBbl.Size = New System.Drawing.Size(105, 21)
        Me.AnnCokeBbl.TabIndex = 6
        Me.AnnCokeBbl.Tag = "0"
        Me.AnnCokeBbl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AnnRSPROD_OMB
        '
        Me.AnnRSPROD_OMB.BackColor = System.Drawing.Color.White
        Me.AnnRSPROD_OMB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSPROD_OMB.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSPROD_OMB.Location = New System.Drawing.Point(382, 309)
        Me.AnnRSPROD_OMB.Name = "AnnRSPROD_OMB"
        Me.AnnRSPROD_OMB.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSPROD_OMB.TabIndex = 14
        Me.AnnRSPROD_OMB.Tag = "0"
        Me.AnnRSPROD_OMB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AnnInputBbl
        '
        Me.AnnInputBbl.BackColor = System.Drawing.Color.White
        Me.AnnInputBbl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnInputBbl.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnInputBbl.Location = New System.Drawing.Point(271, 160)
        Me.AnnInputBbl.Name = "AnnInputBbl"
        Me.AnnInputBbl.Size = New System.Drawing.Size(105, 21)
        Me.AnnInputBbl.TabIndex = 4
        Me.AnnInputBbl.Tag = "0"
        Me.AnnInputBbl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(141, 256)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(99, 20)
        Me.Label8.TabIndex = 343
        Me.Label8.Text = "Tank Truck"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EffDate
        '
        Me.EffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EffDate.ForeColor = System.Drawing.Color.MediumBlue
        Me.EffDate.FormatString = "y"
        Me.EffDate.FormattingEnabled = True
        Me.EffDate.Location = New System.Drawing.Point(144, 160)
        Me.EffDate.Name = "EffDate"
        Me.EffDate.Size = New System.Drawing.Size(121, 21)
        Me.EffDate.TabIndex = 3
        '
        'AnnRSCRUDE_OMB
        '
        Me.AnnRSCRUDE_OMB.BackColor = System.Drawing.Color.White
        Me.AnnRSCRUDE_OMB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSCRUDE_OMB.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSCRUDE_OMB.Location = New System.Drawing.Point(271, 309)
        Me.AnnRSCRUDE_OMB.Name = "AnnRSCRUDE_OMB"
        Me.AnnRSCRUDE_OMB.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSCRUDE_OMB.TabIndex = 13
        Me.AnnRSCRUDE_OMB.Tag = "0"
        Me.AnnRSCRUDE_OMB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AnnRSCRUDE_RAIL
        '
        Me.AnnRSCRUDE_RAIL.BackColor = System.Drawing.Color.White
        Me.AnnRSCRUDE_RAIL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSCRUDE_RAIL.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSCRUDE_RAIL.Location = New System.Drawing.Point(271, 228)
        Me.AnnRSCRUDE_RAIL.Name = "AnnRSCRUDE_RAIL"
        Me.AnnRSCRUDE_RAIL.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSCRUDE_RAIL.TabIndex = 7
        Me.AnnRSCRUDE_RAIL.Tag = "0"
        Me.AnnRSCRUDE_RAIL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(141, 283)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(99, 20)
        Me.Label9.TabIndex = 344
        Me.Label9.Text = "Tanker Berth"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(264, 199)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(123, 26)
        Me.Label13.TabIndex = 349
        Me.Label13.Text = "Annual Raw Material Receipts, bbl"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AnnRSPROD_TB
        '
        Me.AnnRSPROD_TB.BackColor = System.Drawing.Color.White
        Me.AnnRSPROD_TB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSPROD_TB.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSPROD_TB.Location = New System.Drawing.Point(382, 282)
        Me.AnnRSPROD_TB.Name = "AnnRSPROD_TB"
        Me.AnnRSPROD_TB.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSPROD_TB.TabIndex = 12
        Me.AnnRSPROD_TB.Tag = "0"
        Me.AnnRSPROD_TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AnnRSPROD_RAIL
        '
        Me.AnnRSPROD_RAIL.BackColor = System.Drawing.Color.White
        Me.AnnRSPROD_RAIL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSPROD_RAIL.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSPROD_RAIL.Location = New System.Drawing.Point(382, 228)
        Me.AnnRSPROD_RAIL.Name = "AnnRSPROD_RAIL"
        Me.AnnRSPROD_RAIL.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSPROD_RAIL.TabIndex = 8
        Me.AnnRSPROD_RAIL.Tag = "0"
        Me.AnnRSPROD_RAIL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(141, 310)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(99, 20)
        Me.Label11.TabIndex = 345
        Me.Label11.Text = "Offshore Buoy"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(379, 131)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 26)
        Me.Label4.TabIndex = 348
        Me.Label4.Text = "Annual Electricity Consumption, MWh"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AnnRSCRUDE_TB
        '
        Me.AnnRSCRUDE_TB.BackColor = System.Drawing.Color.White
        Me.AnnRSCRUDE_TB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSCRUDE_TB.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSCRUDE_TB.Location = New System.Drawing.Point(271, 282)
        Me.AnnRSCRUDE_TB.Name = "AnnRSCRUDE_TB"
        Me.AnnRSCRUDE_TB.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSCRUDE_TB.TabIndex = 11
        Me.AnnRSCRUDE_TB.Tag = "0"
        Me.AnnRSCRUDE_TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AnnRSCRUDE_TT
        '
        Me.AnnRSCRUDE_TT.BackColor = System.Drawing.Color.White
        Me.AnnRSCRUDE_TT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSCRUDE_TT.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSCRUDE_TT.Location = New System.Drawing.Point(271, 255)
        Me.AnnRSCRUDE_TT.Name = "AnnRSCRUDE_TT"
        Me.AnnRSCRUDE_TT.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSCRUDE_TT.TabIndex = 9
        Me.AnnRSCRUDE_TT.Tag = "0"
        Me.AnnRSCRUDE_TT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(141, 336)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(99, 20)
        Me.Label12.TabIndex = 346
        Me.Label12.Text = "Barge Berth"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(141, 143)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(124, 14)
        Me.Label1.TabIndex = 347
        Me.Label1.Text = "Starting Month"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AnnRSPROD_TT
        '
        Me.AnnRSPROD_TT.BackColor = System.Drawing.Color.White
        Me.AnnRSPROD_TT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnRSPROD_TT.ForeColor = System.Drawing.Color.MediumBlue
        Me.AnnRSPROD_TT.Location = New System.Drawing.Point(382, 255)
        Me.AnnRSPROD_TT.Name = "AnnRSPROD_TT"
        Me.AnnRSPROD_TT.Size = New System.Drawing.Size(105, 21)
        Me.AnnRSPROD_TT.TabIndex = 10
        Me.AnnRSPROD_TT.Tag = "0"
        Me.AnnRSPROD_TT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pageHistory
        '
        Me.pageHistory.Controls.Add(Me.dgMaintHist)
        Me.pageHistory.Controls.Add(Me.pnlAnnual)
        Me.pageHistory.Controls.Add(Me.Label17)
        Me.pageHistory.Location = New System.Drawing.Point(4, 22)
        Me.pageHistory.Name = "pageHistory"
        Me.pageHistory.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageHistory.Size = New System.Drawing.Size(734, 524)
        Me.pageHistory.TabIndex = 2
        Me.pageHistory.Text = "Historical Costs"
        Me.pageHistory.UseVisualStyleBackColor = True
        '
        'dgMaintHist
        '
        Me.dgMaintHist.AllowUserToAddRows = False
        Me.dgMaintHist.AllowUserToDeleteRows = False
        Me.dgMaintHist.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgMaintHist.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgMaintHist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgMaintHist.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PeriodStart, Me.RoutCostLocal, Me.RoutMatlLocal})
        Me.dgMaintHist.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgMaintHist.Location = New System.Drawing.Point(6, 36)
        Me.dgMaintHist.MultiSelect = False
        Me.dgMaintHist.Name = "dgMaintHist"
        Me.dgMaintHist.RowHeadersVisible = False
        Me.dgMaintHist.RowHeadersWidth = 25
        Me.dgMaintHist.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgMaintHist.Size = New System.Drawing.Size(442, 482)
        Me.dgMaintHist.TabIndex = 967
        '
        'PeriodStart
        '
        Me.PeriodStart.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.PeriodStart.DataPropertyName = "PeriodStart"
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle13.Format = "y"
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PeriodStart.DefaultCellStyle = DataGridViewCellStyle13
        Me.PeriodStart.FillWeight = 140.0!
        Me.PeriodStart.HeaderText = "Month"
        Me.PeriodStart.MinimumWidth = 100
        Me.PeriodStart.Name = "PeriodStart"
        Me.PeriodStart.ReadOnly = True
        Me.PeriodStart.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'RoutCostLocal
        '
        Me.RoutCostLocal.DataPropertyName = "RoutCostLocal"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle14.Format = "N1"
        Me.RoutCostLocal.DefaultCellStyle = DataGridViewCellStyle14
        Me.RoutCostLocal.FillWeight = 110.0!
        Me.RoutCostLocal.HeaderText = "Total Cost"
        Me.RoutCostLocal.Name = "RoutCostLocal"
        Me.RoutCostLocal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RoutCostLocal.Width = 110
        '
        'RoutMatlLocal
        '
        Me.RoutMatlLocal.DataPropertyName = "RoutMatlLocal"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle15.Format = "N1"
        Me.RoutMatlLocal.DefaultCellStyle = DataGridViewCellStyle15
        Me.RoutMatlLocal.FillWeight = 110.0!
        Me.RoutMatlLocal.HeaderText = "Material Cost"
        Me.RoutMatlLocal.Name = "RoutMatlLocal"
        Me.RoutMatlLocal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RoutMatlLocal.Width = 110
        '
        'pnlAnnual
        '
        Me.pnlAnnual.Controls.Add(Me.gbAnnual)
        Me.pnlAnnual.Dock = System.Windows.Forms.DockStyle.Right
        Me.pnlAnnual.Location = New System.Drawing.Point(448, 36)
        Me.pnlAnnual.Name = "pnlAnnual"
        Me.pnlAnnual.Padding = New System.Windows.Forms.Padding(6, 4, 4, 4)
        Me.pnlAnnual.Size = New System.Drawing.Size(280, 482)
        Me.pnlAnnual.TabIndex = 966
        '
        'gbAnnual
        '
        Me.gbAnnual.BackColor = System.Drawing.Color.Transparent
        Me.gbAnnual.Controls.Add(Me.btnSubmit)
        Me.gbAnnual.Controls.Add(Me.chkIncrement)
        Me.gbAnnual.Controls.Add(Me.Label18)
        Me.gbAnnual.Controls.Add(Me.Label19)
        Me.gbAnnual.Controls.Add(Me.txtYearTot1)
        Me.gbAnnual.Controls.Add(Me.txtYearMat2)
        Me.gbAnnual.Controls.Add(Me.Label21)
        Me.gbAnnual.Controls.Add(Me.lb1KCurr)
        Me.gbAnnual.Controls.Add(Me.txtYearTot2)
        Me.gbAnnual.Controls.Add(Me.Label22)
        Me.gbAnnual.Controls.Add(Me.txtYearMat1)
        Me.gbAnnual.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbAnnual.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAnnual.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbAnnual.Location = New System.Drawing.Point(6, 4)
        Me.gbAnnual.Name = "gbAnnual"
        Me.gbAnnual.Size = New System.Drawing.Size(270, 169)
        Me.gbAnnual.TabIndex = 0
        Me.gbAnnual.TabStop = False
        Me.gbAnnual.Text = "Tools"
        '
        'chkIncrement
        '
        Me.chkIncrement.AutoSize = True
        Me.chkIncrement.BackColor = System.Drawing.Color.Transparent
        Me.chkIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncrement.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkIncrement.Location = New System.Drawing.Point(84, 105)
        Me.chkIncrement.Name = "chkIncrement"
        Me.chkIncrement.Padding = New System.Windows.Forms.Padding(4, 0, 0, 0)
        Me.chkIncrement.Size = New System.Drawing.Size(167, 17)
        Me.chkIncrement.TabIndex = 0
        Me.chkIncrement.Text = "Check to enter data in years"
        Me.chkIncrement.UseVisualStyleBackColor = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(6, 80)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(67, 13)
        Me.Label18.TabIndex = 777
        Me.Label18.Text = "Second Year"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label19.Location = New System.Drawing.Point(6, 53)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(53, 13)
        Me.Label19.TabIndex = 776
        Me.Label19.Text = "First Year"
        '
        'txtYearTot1
        '
        Me.txtYearTot1.BackColor = System.Drawing.SystemColors.Control
        Me.txtYearTot1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYearTot1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtYearTot1.Location = New System.Drawing.Point(76, 50)
        Me.txtYearTot1.Name = "txtYearTot1"
        Me.txtYearTot1.ReadOnly = True
        Me.txtYearTot1.Size = New System.Drawing.Size(88, 21)
        Me.txtYearTot1.TabIndex = 6
        Me.txtYearTot1.Tag = "1"
        Me.txtYearTot1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtYearMat2
        '
        Me.txtYearMat2.BackColor = System.Drawing.SystemColors.Control
        Me.txtYearMat2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYearMat2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtYearMat2.Location = New System.Drawing.Point(173, 77)
        Me.txtYearMat2.Name = "txtYearMat2"
        Me.txtYearMat2.ReadOnly = True
        Me.txtYearMat2.Size = New System.Drawing.Size(88, 21)
        Me.txtYearMat2.TabIndex = 9
        Me.txtYearMat2.Tag = "1"
        Me.txtYearMat2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label21.Location = New System.Drawing.Point(170, 33)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(93, 14)
        Me.Label21.TabIndex = 774
        Me.Label21.Text = "Materials"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lb1KCurr
        '
        Me.lb1KCurr.BackColor = System.Drawing.Color.Transparent
        Me.lb1KCurr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lb1KCurr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lb1KCurr.Location = New System.Drawing.Point(81, 17)
        Me.lb1KCurr.Name = "lb1KCurr"
        Me.lb1KCurr.Size = New System.Drawing.Size(168, 16)
        Me.lb1KCurr.TabIndex = 772
        Me.lb1KCurr.Text = "------- k local currency -------"
        Me.lb1KCurr.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtYearTot2
        '
        Me.txtYearTot2.BackColor = System.Drawing.SystemColors.Control
        Me.txtYearTot2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYearTot2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtYearTot2.Location = New System.Drawing.Point(76, 77)
        Me.txtYearTot2.Name = "txtYearTot2"
        Me.txtYearTot2.ReadOnly = True
        Me.txtYearTot2.Size = New System.Drawing.Size(88, 21)
        Me.txtYearTot2.TabIndex = 8
        Me.txtYearTot2.Tag = "1"
        Me.txtYearTot2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label22.Location = New System.Drawing.Point(73, 33)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(91, 14)
        Me.Label22.TabIndex = 773
        Me.Label22.Text = "Total"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtYearMat1
        '
        Me.txtYearMat1.BackColor = System.Drawing.SystemColors.Control
        Me.txtYearMat1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYearMat1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtYearMat1.Location = New System.Drawing.Point(173, 50)
        Me.txtYearMat1.Name = "txtYearMat1"
        Me.txtYearMat1.ReadOnly = True
        Me.txtYearMat1.Size = New System.Drawing.Size(88, 21)
        Me.txtYearMat1.TabIndex = 7
        Me.txtYearMat1.Tag = "1"
        Me.txtYearMat1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(6, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(722, 36)
        Me.Label17.TabIndex = 965
        Me.Label17.Text = "Historical Non-Turnaround Maintenance Costs"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1034
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(580, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Refinery Information"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_RefineryInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.tcRefInfo)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "SA_RefineryInfo"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.tcRefInfo.ResumeLayout(False)
        Me.pageTargets.ResumeLayout(False)
        CType(Me.dgRefTargets, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pageInventory.ResumeLayout(False)
        CType(Me.dgInventory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pageStablizers.ResumeLayout(False)
        Me.pageStablizers.PerformLayout()
        Me.pageHistory.ResumeLayout(False)
        CType(Me.dgMaintHist, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAnnual.ResumeLayout(False)
        Me.gbAnnual.ResumeLayout(False)
        Me.gbAnnual.PerformLayout()
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents tcRefInfo As System.Windows.Forms.TabControl
    Friend WithEvents pageStablizers As System.Windows.Forms.TabPage
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents AnnRSPROD_BB As System.Windows.Forms.TextBox
    Friend WithEvents lbPeriods As System.Windows.Forms.ListBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents AnnRSCRUDE_BB As System.Windows.Forms.TextBox
    Friend WithEvents AnnElecConsMWH As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents AnnCokeBbl As System.Windows.Forms.TextBox
    Friend WithEvents AnnRSPROD_OMB As System.Windows.Forms.TextBox
    Friend WithEvents AnnInputBbl As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents EffDate As System.Windows.Forms.ComboBox
    Friend WithEvents AnnRSCRUDE_OMB As System.Windows.Forms.TextBox
    Friend WithEvents AnnRSCRUDE_RAIL As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents AnnRSPROD_TB As System.Windows.Forms.TextBox
    Friend WithEvents AnnRSPROD_RAIL As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents AnnRSCRUDE_TB As System.Windows.Forms.TextBox
    Friend WithEvents AnnRSCRUDE_TT As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents AnnRSPROD_TT As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents pageTargets As System.Windows.Forms.TabPage
    Friend WithEvents pageHistory As System.Windows.Forms.TabPage
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents dgRefTargets As System.Windows.Forms.DataGridView
    Friend WithEvents ChartTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AxisLabelUS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AxisLabelMetric As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmbCurrencyCode As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents PropertyRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SortKeyRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SectionHeader As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmbDesc As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents pnlAnnual As System.Windows.Forms.Panel
    Friend WithEvents gbAnnual As System.Windows.Forms.GroupBox
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents chkIncrement As System.Windows.Forms.CheckBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtYearTot1 As System.Windows.Forms.TextBox
    Friend WithEvents txtYearMat2 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents lb1KCurr As System.Windows.Forms.Label
    Friend WithEvents txtYearTot2 As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtYearMat1 As System.Windows.Forms.TextBox
    Friend WithEvents dgMaintHist As System.Windows.Forms.DataGridView
    Friend WithEvents PeriodStart As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RoutCostLocal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RoutMatlLocal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pageInventory As System.Windows.Forms.TabPage
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents dgInventory As System.Windows.Forms.DataGridView
    Friend WithEvents InvDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumTank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FuelsStorage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AvgLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TankType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button

End Class
