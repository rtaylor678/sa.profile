<!-- config-start -->
FILE=_CONFIG/MaintRoutHist.xml
<!-- config-end -->
<!-- template-start -->
<table class='small' width="75%" border="0">
  <tr>
    <td width=2%></td>
    <td width=2%></td>
    <td width=46%></td>
    <td width=20%></td>
    <td width=5%></td>
    <td width=20%></td>
    <td width=5%></td>
  </tr>
  <tr>
    <td colspan=3></td>
    <td colspan=4 align=center><strong>Refinery Non-Turnaround Maintenance Costs</strong></td>
  </tr>
  <tr>
    <td colspan=3></td>
    <td colspan=4 align=center><strong>----- (CurrencyCode/1000) -----</strong></td>
  </tr>
  <tr>
    <td colspan=3 align=center></td>
    <td colspan=2 align=center><strong>Total</strong></td>
    <td colspan=2 align=center><strong>Materials</strong></td>
  </tr>
  <tr>
    <td colspan=7 align=left><strong>Months</strong></td>
  </tr>
  SECTION(MaintRoutHist,,PeriodStart DESC)
  BEGIN
  <tr>
    <td></td>
    <td colspan=2>Format(PeriodStart,'MMMM yyyy') </td>
    <td align=right> Format(RoutCostLocal,'#,##0.0')</td>
    <td></td>
    <td align=right> Format(RoutMatlLocal,'#,##0.0')</td>
    <td></td>
  </tr>
  END
  <tr>
    <td></td>
    <td></td>
    <td><strong>Total</strong></td>
    <td align=right><strong>Formula(MaintRoutHist,SUM(RoutCostLocal),,'#,##0.0')</strong></td>
    <td></td>
    <td align=right><strong>Formula(MaintRoutHist,SUM(RoutMatlLocal),,'#,##0.0')</strong></td>
    <td></td>
  </tr>
</table>
<!-- template-end -->
