<!-- config-start -->
FILE=_CONFIG/Crude.xml;
<!-- config-end -->
<!-- template-start-->

<table class='small' width=100% border=0>
  <tr>
    <td width=10%>&nbsp</td>
    <td width=40%>&nbsp</td>
    <td width=13%>&nbsp</td>
    <td width=2%>&nbsp</td>
    <td width=8%>&nbsp</td>
    <td width=2%>&nbsp</td>
    <td width=8%>&nbsp</td>
    <td width=2%>&nbsp</td>
    <td width=11%>&nbsp</td>
    <td width=4%>&nbsp</td>
  </tr>
  <tr>
    <td height=30 align=center valign=bottom><strong>Solomon</br>Crude</br>Number</strong></td>
    <td align=center valign=bottom><strong>Crude Name</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Total Barrels Processed in Month</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Gravity</br>(API)</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Sulfur</br>(wt %)</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Delivered</br>Crude Cost</br>(USD/bbl)</strong></td>
  </tr>
  <tr>
    <td colspan=10>&nbsp</td>
  </tr>

  SECTION(Crude,,)
  BEGIN
  <tr>
    <td>@CNum</td>
    <td>@CrudeName</td>
    <td align=right>Format(BBL,'#,##0')</td>
    <td></td>
    <td align=right>Format(Gravity,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(Sulfur,'N')</td>
    <td></td>
    <td align=right>Format(CostPerBBL,'N')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=10>&nbsp</td>
  </tr>

  <tr>
    <td></td>
    <td><strong>Total Crude Charge</strong></td>
    <td align=right><strong>Formula(Crude,SUM(BBL),,'#,##0')</strong></td>
    <td colspan=7></td>
  </tr>
 
</table>

<!-- template-end -->

